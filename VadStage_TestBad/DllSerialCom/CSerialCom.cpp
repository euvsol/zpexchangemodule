#include "stdafx.h"
#include "CSerialCom.h"

CSerialCom::CSerialCom()
{
	m_nRet = 0;
	m_osRead.hEvent = NULL;
	m_osWrite.hEvent = NULL;
	m_hComm = NULL;
	m_hCommThread = NULL;
	m_bSerialConnected = FALSE;
	m_hSerialEvent = NULL;

	m_strSendMsg = _T("");
	m_strReceivedMsg = _T("");
	m_strSendEndOfStreamSymbol.Empty();
	m_strReceiveEndOfStreamSymbol.Empty();
}

CSerialCom::~CSerialCom()
{
	CloseSerialPort();
}

int CSerialCom::OpenSerialPort(CString sPortName, DWORD dwBaud, BYTE wByte, BYTE wStop, BYTE wParity)
{
	int nRet = 0;

	BOOL bRet = FALSE;

	COMMTIMEOUTS	timeouts;
	DCB				dcb;
	DWORD			dwThreadID;

	m_bSerialConnected = FALSE;

	int pos = sPortName.Find(_T("COM"));
	CString port = sPortName.Mid(pos + 3, 2);

	m_wPortID = atoi((LPSTR)(LPCTSTR)port); // COM1-> 1, COM2->2,,,,,

	m_hSerialEvent = CreateEvent(NULL, TRUE, FALSE, NULL);

	m_osRead.Offset = 0;
	m_osRead.OffsetHigh = 0;
	if (NULL == (m_osRead.hEvent = CreateEvent(NULL, TRUE, FALSE, NULL)))
	{
		m_osRead.hEvent = NULL;
		nRet = IDS_ERROR_SERIAL_OPENPORT;
		return nRet;
	}
	m_osWrite.Offset = 0;
	m_osWrite.OffsetHigh = 0;
	if (NULL == (m_osWrite.hEvent = CreateEvent(NULL, TRUE, FALSE, NULL)))
	{
		m_osWrite.hEvent = NULL;
		nRet = IDS_ERROR_SERIAL_OPENPORT;
		return nRet;
	}

	//20190718 jkseo, 임시로 주석처리
	//if (m_wPortID > 9)
		m_sPortName = _T("\\\\.\\");

	m_sPortName += sPortName;
	m_hComm = CreateFile(m_sPortName, GENERIC_READ | GENERIC_WRITE, 0, NULL, OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL | FILE_FLAG_OVERLAPPED, NULL);
	if (m_hComm == (HANDLE)-1)
	{
		nRet = IDS_ERROR_SERIAL_OPENPORT;
		return nRet;
	}

	SetCommMask(m_hComm, EV_RXCHAR | EV_CTS | EV_DSR | EV_RING | EV_RLSD | EV_BREAK);
	SetupComm(m_hComm, 8192, 8192);
	PurgeComm(m_hComm, PURGE_TXABORT | PURGE_TXCLEAR | PURGE_RXABORT | PURGE_RXCLEAR);

	timeouts.ReadIntervalTimeout = 50;
	timeouts.ReadTotalTimeoutMultiplier = 0;
	timeouts.ReadTotalTimeoutConstant = 0;
	timeouts.WriteTotalTimeoutMultiplier = 2 * CBR_9600 / dwBaud;
	timeouts.WriteTotalTimeoutConstant = 0;
	SetCommTimeouts(m_hComm, &timeouts);

	dcb.DCBlength = sizeof(DCB);
	GetCommState(m_hComm, &dcb);
	dcb.BaudRate = dwBaud;
	dcb.ByteSize = wByte;
	dcb.Parity = wParity;
	dcb.StopBits = wStop;
	dcb.fInX = FALSE;
	dcb.fOutX = FALSE;
	dcb.XonChar = ASCII_XON;
	dcb.XoffChar = ASCII_XOFF;
	dcb.XonLim = 100;
	dcb.XoffLim = 100;

	bRet = SetCommState(m_hComm, &dcb);
	if (bRet == FALSE)
	{
		nRet = IDS_ERROR_SERIAL_OPENPORT;
		return nRet;
	}

	m_bSerialConnected = TRUE;
	m_hCommThread = CreateThread(NULL, 0, (LPTHREAD_START_ROUTINE)ReceiveThreadFunc, this, 0, &dwThreadID);
	if (!m_hCommThread)
	{
		CloseSerialPort();
		nRet = IDS_ERROR_SERIAL_OPENPORT;
		return nRet;
	}

	return nRet;
}

void CSerialCom::CloseSerialPort()
{
	m_bSerialConnected = FALSE;

	if (m_osRead.hEvent != NULL)
		CloseHandle(m_osRead.hEvent);
	if (m_osWrite.hEvent != NULL)
		CloseHandle(m_osWrite.hEvent);
	if (m_hCommThread != NULL)
	{
		SuspendThread(m_hCommThread);
		TerminateThread(m_hCommThread, FALSE);
		m_hCommThread = NULL;
	}
	SetCommMask(m_hComm, 0);
	PurgeComm(m_hComm, PURGE_TXABORT | PURGE_TXCLEAR | PURGE_RXABORT | PURGE_RXCLEAR);
	CloseHandle(m_hComm);

	if (m_hSerialEvent != NULL)
		CloseHandle(m_hSerialEvent);
}

void CSerialCom::ClearSerialPort()
{
	PurgeComm(m_hComm, PURGE_TXABORT | PURGE_TXCLEAR | PURGE_RXABORT | PURGE_RXCLEAR);
}

int	CSerialCom::Send(char *lParam, int nTimeOut, int nRetryCnt)
{
	MutexLock(m_hMutex);

	int nRet = 0;
	int nCntRetry = 0;
	DWORD dwRet = 0;

	while (nCntRetry < nRetryCnt)
	{
		if (m_bSerialConnected)
		{
			ResetEvent(m_hSerialEvent);

			dwRet = WriteComm((BYTE *)(LPSTR)lParam, strlen(lParam));
			if (dwRet == 0)
			{
				nRet = IDS_ERROR_SERIAL_SEND;
				ResetEvent(m_hSerialEvent);
				MutexUnlock(m_hMutex);
				return nRet;					
			}

			if (nTimeOut != 0)
			{
				nRet = WaitEvent(m_hSerialEvent, nTimeOut);
				if (nRet != 0)
				{
					ResetEvent(m_hSerialEvent);
					MutexUnlock(m_hMutex);
					return nRet;
				}
			}
			else
			{
				if (nRet == strlen(lParam))
					nRet = 0;

				SetEvent(m_hSerialEvent);
			}
		}
		else
		{
			nRet = IDC_ERROR_SERIAL_NOT_CONNECTED;
			MutexUnlock(m_hMutex);
			return nRet;
		}

		nCntRetry++;

		if (nRet == 0)
		{
			break;
		}
	}

	MutexUnlock(m_hMutex);

	return nRet;
}

int	CSerialCom::Send(int nSize, char *lParam, int nTimeOut, int nRetryCnt)
{
	MutexLock(m_hMutex);

	int nRet = 0;
	int nCntRetry = 0;
	DWORD dwRet = 0;

	while (nCntRetry < nRetryCnt)
	{
		if (m_bSerialConnected)
		{
			ResetEvent(m_hSerialEvent);

			dwRet = WriteComm((BYTE *)(LPSTR)lParam, nSize);
			if (dwRet == 0)
			{
				nRet = IDS_ERROR_SERIAL_SEND;
				ResetEvent(m_hSerialEvent);
				MutexUnlock(m_hMutex);
				return nRet;
			}

			if (nTimeOut != 0)
			{
				nRet = WaitEvent(m_hSerialEvent, nTimeOut);
				if (nRet != 0)
				{
					ResetEvent(m_hSerialEvent);
					MutexUnlock(m_hMutex);
					return nRet;
				}
			}
			else
			{
				if (dwRet == nSize)
					nRet = 0;

				SetEvent(m_hSerialEvent);
			}
		}
		else
		{
			nRet = IDC_ERROR_SERIAL_NOT_CONNECTED;
			MutexUnlock(m_hMutex);
			return nRet;
		}

		nCntRetry++;

		if (nRet == 0)
		{
			break;
		}
	}

	MutexUnlock(m_hMutex);

	return nRet;
}

int	CSerialCom::SendData(char *lParam, int nTimeOut, int nRetryCnt)
{
	int nRet = 0;

	return nRet;
}

DWORD CSerialCom::WriteComm(BYTE *pBuff, DWORD_PTR nToWrite)
{
	DWORD	dwWritten, dwError, dwErrorFlags;
	COMSTAT	comstat;

	if (!m_bSerialConnected)
	{
		dwWritten = 0;
		return -1;
	}

	if (!WriteFile(m_hComm, pBuff, nToWrite, &dwWritten, &m_osWrite))
	{
		if (GetLastError() == ERROR_IO_PENDING)
		{
			while (!GetOverlappedResult(m_hComm, &m_osWrite, &dwWritten, TRUE))
			{
				dwError = GetLastError();
				if (dwError != ERROR_IO_INCOMPLETE)
				{
					ClearCommError(m_hComm, &dwErrorFlags, &comstat);
					break;
				}
			}
		}
		else
		{
			dwWritten = 0;
			ClearCommError(m_hComm, &dwErrorFlags, &comstat);
		}
	}

	return dwWritten;
}

DWORD CSerialCom::ReadComm(BYTE *pBuff, DWORD nToRead)
{
	CString str;
	DWORD	dwRead, dwError, dwErrorFlags;
	COMSTAT	comstat;

	ClearCommError(m_hComm, &dwErrorFlags, &comstat);
	
	dwRead = comstat.cbInQue;

	if (dwRead > 0)
	{
		if (!ReadFile(m_hComm, pBuff, nToRead, &dwRead, &m_osRead))
		{
			if (GetLastError() == ERROR_IO_PENDING)
			{
				while (!GetOverlappedResult(m_hComm, &m_osRead, &dwRead, TRUE))
				{
					dwError = GetLastError();
					str.Format(_T(" comm read error ... %d "), dwError);
					if (dwError != ERROR_IO_INCOMPLETE)
					{
						dwRead = 0;
						ClearCommError(m_hComm, &dwErrorFlags, &comstat);
						break;
					}
				}
			}
			else
			{
				dwRead = 0;
				ClearCommError(m_hComm, &dwErrorFlags, &comstat);
			}
		}
	}
	
	return dwRead;
}

int CSerialCom::WaitReceiveEventThread()
{
	int			nRet = 0;
	int			total = 0;
	int			nCheck = 0;

	CString		str = _T("");
	DWORD		dwEvent;
	BOOL		bOk = TRUE;
	BYTE		bufbyte[512];
	DWORD		dwRead;	

	memset(m_inbuf, '\0', 512);
	memset(bufbyte, '\0', 512);

	while (m_bSerialConnected)
	{
		dwEvent = 0;
		WaitCommEvent(m_hComm, &dwEvent, NULL);
		if ((dwEvent & EV_RXCHAR) == EV_RXCHAR)
		{
			do
			{
				dwRead = ReadComm(m_inbuf, 512);
				SetEvent(m_hSerialEvent);
				if (dwRead > 0)
				{
					memcpy(bufbyte, m_inbuf, sizeof(BYTE) * 512);
					nRet = ReceiveData((LPSTR)bufbyte, dwRead);
					m_nRet = nRet;
					memset(bufbyte, '\0', 512);
					memset(m_inbuf, '\0', 512);
					total = 0;
				}
				else
				{

				}							
			} while (dwRead);
		}
		else
		{
			nRet = IDC_ERROR_SERIAL_NOT_CONNECTED;
			m_nRet = nRet;
			SetEvent(m_hSerialEvent);
		}
	}

	TRACE(_T("CSerial::Receive out, m_bSerialConnected = %d\n"), m_bSerialConnected);
	m_hCommThread = NULL;

	return TRUE;
}

int	CSerialCom::ReceiveData(char *lParam, DWORD dwRead)
{
	int nRet = 0;

	return nRet;
}
