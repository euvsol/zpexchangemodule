#include "pch.h"
#include "CSmarActCtrl.h"

#define Z_AXIS 0
#define R_AXIS 1
#define G_AXIS 2


CSmarActCtrl::CSmarActCtrl()
{
	m_dHandle = NULL;

	for (int nIdx = 0; nIdx < 5; nIdx++)
	{
		m_bAmplifierEnabled[nIdx]	   = FALSE;
		m_bClosedLoopActive[nIdx]	   = FALSE;
		m_bActivelyMoving[nIdx]		   = FALSE;
		m_bSensorPresent[nIdx]		   = FALSE;
		m_bIsCalibrated[nIdx]		   = FALSE;
		m_bIsReferenced[nIdx]		   = FALSE;
		m_bEndstopReached[nIdx]		   = FALSE;
		m_bRangeLimitReached[nIdx]	   = FALSE;
		m_bFollowingErrorReached[nIdx] = FALSE;
	}
	

	m_bConnected = FALSE;

	m_dCurrentPos = 0.0;
	m_dPmToMicro = 1000000;
	m_nNoOfChannels	= 0;
}

CSmarActCtrl::~CSmarActCtrl()
{
	if (m_dHandle != NULL)
		SA_CTL_Close(m_dHandle);
}

int CSmarActCtrl::OpenDevice()
{
	int nRet;

	const char *version = SA_CTL_GetFullVersionString();
	TRACE("SmarActCTL library version : %s\n", version);

	// Find available MCS2 devices
	char deviceList[1024];
	size_t ioDeviceListLen = sizeof(deviceList);
	nRet = SA_CTL_FindDevices("", deviceList, &ioDeviceListLen);
	if (nRet != SA_CTL_ERROR_NONE) return nRet;
	
	if (strlen(deviceList) == 0) 
		return -1;
	
	//char *ptr;
	//strtok_r(deviceList, "\n", &ptr);
	char *locator = deviceList;
	nRet = SA_CTL_Open(&m_dHandle, locator, "");
	if (nRet != SA_CTL_ERROR_NONE) return nRet;
	m_bConnected = TRUE;
	return nRet;
}

void CSmarActCtrl::CloseDevice()
{
	if (m_dHandle != NULL)
	{
		SA_CTL_Close(m_dHandle);
		m_dHandle = NULL;
		m_bConnected = FALSE;
	}
}

int CSmarActCtrl::GetNoOfChannel()
{
	int nRet;
	nRet = SA_CTL_GetProperty_i32(m_dHandle, 0, SA_CTL_PKEY_NUMBER_OF_CHANNELS, &m_nNoOfChannels, 0);
	return nRet;
}

int CSmarActCtrl::Calibrate(int nChannel)
{
	int nRet;
	nRet = SA_CTL_SetProperty_i32(m_dHandle, nChannel, SA_CTL_PKEY_CALIBRATION_OPTIONS, 0);
	if (nRet != SA_CTL_ERROR_NONE) return nRet;
	nRet = SA_CTL_Calibrate(m_dHandle, nChannel, 0);
	return nRet;
}

int CSmarActCtrl::Reference(int nChannel)
{
	int nRet;
	nRet = SA_CTL_SetProperty_i32(m_dHandle, nChannel, SA_CTL_PKEY_REFERENCING_OPTIONS, SA_CTL_REF_OPT_BIT_START_DIR); //BACKWARDS
	if (nRet != SA_CTL_ERROR_NONE) return nRet;
	nRet = SA_CTL_SetProperty_i64(m_dHandle, nChannel, SA_CTL_PKEY_MOVE_VELOCITY, 1000000000);
	if (nRet != SA_CTL_ERROR_NONE) return nRet;
	nRet = SA_CTL_SetProperty_i64(m_dHandle, nChannel, SA_CTL_PKEY_MOVE_ACCELERATION, 10000000000);
	if (nRet != SA_CTL_ERROR_NONE) return nRet;
	nRet = SA_CTL_Reference(m_dHandle, nChannel, 0);
	return nRet;
}

int CSmarActCtrl::Move(int nChannel, double dMoveDist)
{
	int nRet;
	if(nChannel == R_AXIS)
		nRet = SA_CTL_Move(m_dHandle, nChannel, dMoveDist * m_dPmToMicro * 1000, 0);
	else
		nRet = SA_CTL_Move(m_dHandle, nChannel, dMoveDist * m_dPmToMicro, 0);

	return nRet;
}

int CSmarActCtrl::TMove(int nChannel, double dMoveDist)
{
	int nRet;
	if (nChannel == R_AXIS)
		nRet = SA_CTL_Move(m_dHandle, nChannel, dMoveDist * m_dPmToMicro * 1000, tHandle);
	else
		nRet = SA_CTL_Move(m_dHandle, nChannel, dMoveDist * m_dPmToMicro, tHandle);

	return nRet;
}

int CSmarActCtrl::Stop(int nChannel)
{
	int nRet;
	nRet = SA_CTL_Stop(m_dHandle, nChannel, 0);
	return nRet;
}

int CSmarActCtrl::State(int nChannel)
{
	int nRet;
	int32_t state;
	
	nRet = SA_CTL_GetProperty_i32(m_dHandle, nChannel, SA_CTL_PKEY_CHANNEL_STATE, &state, 0);
	if (nRet == SA_CTL_ERROR_NONE)
	{
		if ((state & SA_CTL_CH_STATE_BIT_AMPLIFIER_ENABLED) != 0)
			m_bAmplifierEnabled[nChannel] = TRUE;
		else
			m_bAmplifierEnabled[nChannel] = FALSE;

		if ((state & SA_CTL_CH_STATE_BIT_CLOSED_LOOP_ACTIVE) != 0)
			m_bClosedLoopActive[nChannel] = TRUE;
		else
			m_bClosedLoopActive[nChannel] = FALSE;

		if ((state & SA_CTL_CH_STATE_BIT_ACTIVELY_MOVING) != 0)
			m_bActivelyMoving[nChannel] = TRUE;
		else
			m_bActivelyMoving[nChannel] = FALSE;

		if ((state & SA_CTL_CH_STATE_BIT_SENSOR_PRESENT) != 0)
			m_bSensorPresent[nChannel] = TRUE;
		else
			m_bSensorPresent[nChannel] = FALSE;

		if ((state & SA_CTL_CH_STATE_BIT_IS_CALIBRATED) != 0)
			m_bIsCalibrated[nChannel] = TRUE;
		else
			m_bIsCalibrated[nChannel] = FALSE;

		if ((state & SA_CTL_CH_STATE_BIT_IS_REFERENCED) != 0)
			m_bIsReferenced[nChannel] = TRUE;
		else
			m_bIsReferenced[nChannel] = FALSE;

		if ((state & SA_CTL_CH_STATE_BIT_END_STOP_REACHED) != 0)
			m_bEndstopReached[nChannel] = TRUE;
		else
			m_bEndstopReached[nChannel] = FALSE;

		if ((state & SA_CTL_CH_STATE_BIT_RANGE_LIMIT_REACHED) != 0)
			m_bRangeLimitReached[nChannel] = TRUE;
		else
			m_bRangeLimitReached[nChannel] = FALSE;

		if ((state & SA_CTL_CH_STATE_BIT_FOLLOWING_LIMIT_REACHED) != 0)
			m_bFollowingErrorReached[nChannel] = TRUE;
		else
			m_bFollowingErrorReached[nChannel] = FALSE;
	}

	return nRet;
}

int CSmarActCtrl::GetPos(int nChannel)
{
	int nRet;
	int64_t pos;
	nRet = SA_CTL_GetProperty_i64(m_dHandle, nChannel, SA_CTL_PKEY_POSITION, &pos, 0);
	if (nRet == SA_CTL_ERROR_NONE)
	{
		if (nChannel == R_AXIS) //Rotate Axis
			m_dCurrentPos = pos / m_dPmToMicro / 1000;
		else
			m_dCurrentPos = pos / m_dPmToMicro;
	}

	return nRet;
}

int CSmarActCtrl::GetPosType(int nChannel)
{
	int nRet;
	int32_t type;
	char buf[SA_CTL_STRING_MAX_LENGTH];
	size_t ioStringSize = sizeof(buf);
	nRet = SA_CTL_GetProperty_s(m_dHandle, nChannel, SA_CTL_PKEY_POSITIONER_TYPE_NAME, buf, &ioStringSize);
	if (nRet != 0) return nRet;
	nRet = SA_CTL_GetProperty_i32(m_dHandle, nChannel, SA_CTL_PKEY_POSITIONER_TYPE, &type, 0);
	if (nRet != 0) return nRet;
	m_strPosType.Format(_T("%s (%d)"), buf, type);
	return nRet;
}

int CSmarActCtrl::SetMoveMode(int nChannel, int mode)
{
	int nRet;
	nRet = SA_CTL_SetProperty_i32(m_dHandle, nChannel, SA_CTL_PKEY_MOVE_MODE, mode);
	return nRet;
}

int CSmarActCtrl::SetVelocity(int nChannel, double dVelocity)
{
	int nRet;
	nRet = SA_CTL_SetProperty_i64(m_dHandle, nChannel, SA_CTL_PKEY_MOVE_VELOCITY, dVelocity * m_dPmToMicro);
	return nRet;
}

int CSmarActCtrl::SetAcceleration(int nChannel, double dAcceleration)
{
	int nRet;
	nRet = SA_CTL_SetProperty_i64(m_dHandle, nChannel, SA_CTL_PKEY_MOVE_ACCELERATION, dAcceleration * m_dPmToMicro);
	return nRet;
}

int CSmarActCtrl::SetLogicalScaleOffset(int nChannel, double dPos)
{
	int nRet;
	if(nChannel == R_AXIS)
		nRet = SA_CTL_SetProperty_i64(m_dHandle, nChannel, SA_CTL_PKEY_LOGICAL_SCALE_OFFSET, dPos * m_dPmToMicro * 1000);
	else
		nRet = SA_CTL_SetProperty_i64(m_dHandle, nChannel, SA_CTL_PKEY_LOGICAL_SCALE_OFFSET, dPos * m_dPmToMicro);
	return nRet;
}

int CSmarActCtrl::SetMaxClFrequency(int nChannel, double dValue)
{
	int nRet;
	nRet = SA_CTL_SetProperty_i32(m_dHandle, nChannel, SA_CTL_PKEY_MAX_CL_FREQUENCY, dValue);
	return nRet;
}

int CSmarActCtrl::SetHoldTime(int nChannel, double dValue)
{
	int nRet;
	nRet = SA_CTL_SetProperty_i32(m_dHandle, nChannel, SA_CTL_PKEY_HOLD_TIME, dValue);
	return nRet;
}

int CSmarActCtrl::CreateOutputBuffer()
{
	int nRet;
	nRet = SA_CTL_CreateOutputBuffer(m_dHandle, &tHandle);
	return nRet;
}


int CSmarActCtrl::FlushOutputBuffer()
{
	int nRet;
	nRet = SA_CTL_FlushOutputBuffer(m_dHandle, tHandle);
	return nRet;
}


int CSmarActCtrl::CancelOutputBuffer()
{
	int nRet;
	nRet = SA_CTL_CancelOutputBuffer(m_dHandle, tHandle);
	return nRet;
}