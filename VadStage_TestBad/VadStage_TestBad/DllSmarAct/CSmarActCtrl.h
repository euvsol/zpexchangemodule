#pragma once
#include "Includes/SmarActControl.h"

class AFX_EXT_CLASS CSmarActCtrl
{
public:
	CSmarActCtrl();
	~CSmarActCtrl();

private:
	SA_CTL_DeviceHandle_t m_dHandle;

	

public:

	CString m_strPosType;
	CString m_strCurrPos;

	double m_dCurrentPos;
	double m_dPmToMicro;

	int32_t m_nNoOfChannels;

	BOOL m_bConnected;

	BOOL m_bAmplifierEnabled[5];
	BOOL m_bClosedLoopActive[5];
	BOOL m_bActivelyMoving[5];
	BOOL m_bSensorPresent[5];
	BOOL m_bIsCalibrated[5];
	BOOL m_bIsReferenced[5];
	BOOL m_bEndstopReached[5];
	BOOL m_bRangeLimitReached[5];
	BOOL m_bFollowingErrorReached[5];
	
	int  OpenDevice();
	void CloseDevice();

	int  Calibrate(int nChannel);
	int  Reference(int nChannel);
		
	int  Move(int nChannel, double dMoveDist);
	int  TMove(int nChannel, double dMoveDist);

	int  Stop(int nChannel);
	int  State(int nChannel);
	
	int  GetNoOfChannel();
	int  GetPos(int nChannel);
	int  GetPosType(int nChannel);

	int  SetMoveMode(int nChannel, int nMode);
	int  SetVelocity(int nChannel, double dVelocity);
	int  SetAcceleration(int nChannel, double dAcceleration);
	int  SetLogicalScaleOffset(int nChannel, double dPos);
	int  SetMaxClFrequency(int nChannel, double dValue);
	int  SetHoldTime(int nChannel, double dValue);


	SA_CTL_TransmitHandle_t tHandle;

	int CreateOutputBuffer();
	int FlushOutputBuffer();
	int CancelOutputBuffer();
	
};