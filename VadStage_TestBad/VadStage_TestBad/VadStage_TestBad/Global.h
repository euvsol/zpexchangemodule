#pragma once

CNavigationStageDlg*			g_pNavigationStage = NULL;
CStateTestDlg*					g_pStageRun = NULL;
CAutoMessageDlg*				g_pAM = NULL;
//CTempDialog*					g_pTemp = NULL;
CStageReportDlg*				g_pStageReport = NULL;
CTabDlg*						g_pTab = NULL;
CZPExchangeDlg*					g_pZPExchange = NULL;
CCamZPExchangeDlg*				g_pCamZPExchange = NULL;
CDetecterApertureChangeDlg*		g_pDetecterApertureChange = NULL;
MIL_ID	                        g_milApplication = M_NULL;
MIL_ID	                        g_milSystemHost = M_NULL;
MIL_ID	                        g_milSystemGigEVision = M_NULL;
CAFModuleDlg*					g_pAF = NULL;
CLinearRevolverDlg*				g_pLRevolver = NULL;