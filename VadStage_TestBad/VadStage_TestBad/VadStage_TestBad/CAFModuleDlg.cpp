﻿// CAFModuleDlg.cpp: 구현 파일
//

#include "pch.h"
#include "Include.h"
#include "Extern.h"


// CAFModuleDlg 대화 상자

IMPLEMENT_DYNAMIC(CAFModuleDlg, CDialogEx)

CAFModuleDlg::CAFModuleDlg(CWnd* pParent /*=nullptr*/)
	: CDialogEx(IDD_AUTOFOCUS_DIALOG, pParent)
{
	m_bDeviceConnect = FALSE;
	m_bHomePosition = FALSE;
	m_bInRangeStatus = FALSE;
	m_bInFocusStatus = FALSE;
	m_bAfOnStatus = FALSE;
	m_bBusyStatus = FALSE;
	m_bSensorLimitCW = FALSE;
	m_bSensorLimitCCW = FALSE;

	m_nCurrentObjNum = -1;
	m_nAbsolutePosZ = 0.0;
	m_nCmd = -1;;

	m_pCommandThread = NULL;
}

CAFModuleDlg::~CAFModuleDlg()
{
}

void CAFModuleDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_AFM_LED_CURRENT_EDIT, m_LedCurrEdit);
	DDX_Control(pDX, IDC_AFM_LED_CURRENT_SLIDER, m_LedCurrSlider);
	DDX_Control(pDX, IDC_AFM_LED_PWM_EDIT, m_LedPwmEdit);
	DDX_Control(pDX, IDC_AFM_LED_PWM_SLIDER, m_LedPwmSlider);
	DDX_Control(pDX, IDC_AFM_ZAXIS_POS_EDIT, m_ZPosCtrl);
	DDX_Control(pDX, IDC_AFM_ZAXIS_POS_EDIT2, m_ZumPosCtrl);
}


BEGIN_MESSAGE_MAP(CAFModuleDlg, CDialogEx)
	ON_WM_DESTROY()
	ON_WM_TIMER()
	ON_BN_CLICKED(IDC_AFM_LASERON_BUTTON, &CAFModuleDlg::OnBnClickedAfmLaseronButton)
	ON_BN_CLICKED(IDC_AFM_LASEROFF_BUTTON, &CAFModuleDlg::OnBnClickedAfmLaseroffButton)
	ON_BN_CLICKED(IDC_AFM_AFON_BUTTON, &CAFModuleDlg::OnBnClickedAfmAfonButton)
	ON_BN_CLICKED(IDC_AFM_AFOFF_BUTTON, &CAFModuleDlg::OnBnClickedAfmAfoffButton)
	ON_BN_CLICKED(IDC_AFM_HOME_BUTTON, &CAFModuleDlg::OnBnClickedAfmHomeButton)
	ON_BN_CLICKED(IDC_AFM_MAKE0_BUTTON, &CAFModuleDlg::OnBnClickedAfmMake0Button)
	ON_BN_CLICKED(IDC_AFM_SAVE_BUTTON, &CAFModuleDlg::OnBnClickedAfmSaveButton)
	ON_BN_CLICKED(IDC_AFM_ZUP_BUTTON, &CAFModuleDlg::OnBnClickedAfmZupButton)
	ON_BN_CLICKED(IDC_AFM_ZDOWN_BUTTON, &CAFModuleDlg::OnBnClickedAfmZdownButton)
	ON_BN_CLICKED(IDC_AFM_MOTION_STOP_BUTTON, &CAFModuleDlg::OnBnClickedAfmMotionStopButton)
	ON_NOTIFY(NM_CUSTOMDRAW, IDC_AFM_LED_CURRENT_SLIDER, &CAFModuleDlg::OnNMCustomdrawAfmLedCurrentSlider)
	ON_NOTIFY(NM_CUSTOMDRAW, IDC_AFM_LED_PWM_SLIDER, &CAFModuleDlg::OnNMCustomdrawAfmLedPwmSlider)
	ON_BN_CLICKED(IDC_AFM_CONNECT_BUTTON, &CAFModuleDlg::OnBnClickedAfmConnectButton)
	ON_BN_CLICKED(IDC_AFM_DISCONNECT_BUTTON2, &CAFModuleDlg::OnBnClickedAfmDisconnectButton2)
END_MESSAGE_MAP()


// CAFModuleDlg 메시지 처리기




BOOL CAFModuleDlg::OnInitDialog()
{
	CDialogEx::OnInitDialog();

	InitializeControls();

	return TRUE;  // return TRUE unless you set the focus to a control
				  // 예외: OCX 속성 페이지는 FALSE를 반환해야 합니다.
}


void CAFModuleDlg::InitializeControls()
{
	m_LedIcon[0] = (HICON)::LoadImage(::AfxGetInstanceHandle(), MAKEINTRESOURCE(IDI_LEDOFF), IMAGE_ICON, 32, 32, LR_DEFAULTCOLOR);
	m_LedIcon[1] = (HICON)::LoadImage(::AfxGetInstanceHandle(), MAKEINTRESOURCE(IDI_LEDONGREEN), IMAGE_ICON, 32, 32, LR_DEFAULTCOLOR);
	m_LedIcon[2] = (HICON)::LoadImage(::AfxGetInstanceHandle(), MAKEINTRESOURCE(IDI_LEDONRED), IMAGE_ICON, 32, 32, LR_DEFAULTCOLOR);

	((CStatic*)GetDlgItem(IDC_ICON_AFM_CONNECT))->SetIcon(m_LedIcon[2]);
	((CStatic*)GetDlgItem(IDC_ICON_AFM_OBJECT0))->SetIcon(m_LedIcon[0]);
	((CStatic*)GetDlgItem(IDC_ICON_AFM_OBJECT1))->SetIcon(m_LedIcon[0]);
	((CStatic*)GetDlgItem(IDC_ICON_AFM_INFOCUS))->SetIcon(m_LedIcon[0]);
	((CStatic*)GetDlgItem(IDC_ICON_AFM_INRANGE))->SetIcon(m_LedIcon[0]);
	((CStatic*)GetDlgItem(IDC_ICON_AFM_HOME))->SetIcon(m_LedIcon[0]);
	((CStatic*)GetDlgItem(IDC_ICON_AFM_BUSY))->SetIcon(m_LedIcon[0]);
	((CStatic*)GetDlgItem(IDC_ICON_AFM_LIMIT_CW))->SetIcon(m_LedIcon[0]);
	((CStatic*)GetDlgItem(IDC_ICON_AFM_LIMIT_CCW))->SetIcon(m_LedIcon[0]);

	m_LedCurrSlider.SetRange(50, 1200);
	m_LedCurrSlider.SetRangeMin(50);
	m_LedCurrSlider.SetRangeMax(1200);

	m_LedCurrSlider.SetTicFreq(100);
	m_LedCurrSlider.SetLineSize(10);
	m_LedCurrSlider.SetPageSize(100);

	m_LedPwmSlider.SetRange(0, 100);
	m_LedPwmSlider.SetRangeMin(0);
	m_LedPwmSlider.SetRangeMax(100);

	m_LedPwmSlider.SetTicFreq(10);
	m_LedPwmSlider.SetLineSize(1);
	m_LedPwmSlider.SetPageSize(10);

	m_LedCurrSlider.SetPos(380);
	m_LedPwmSlider.SetPos(10);

	SetDlgItemText(IDC_AFM_ZMOVE_EDIT, _T("10"));
}


void CAFModuleDlg::OnDestroy()
{
	CDialogEx::OnDestroy();

	CloseDevice();
	KillTimer(AFM_UPDATE_TIMER);
}


void CAFModuleDlg::OnTimer(UINT_PTR nIDEvent)
{
	KillTimer(nIDEvent);
	switch (nIDEvent)
	{
	case AFM_UPDATE_TIMER:
		GetDeviceStatus();
		SetTimer(nIDEvent, 300, NULL);
		break;
	default:
		break;
	}

	CDialogEx::OnTimer(nIDEvent);
}


int CAFModuleDlg::OpenDevice()
{
	int nRet;

	CString StrPORT = _T("COM5");
	int nBaudrate = 9600;
	//nRet = OpenSerialPort(g_pConfig->m_chPORT[SERIAL_AFMODULE], g_pConfig->m_nBAUD_RATE[SERIAL_AFMODULE]);
	nRet = OpenSerialPort(StrPORT, nBaudrate);

	if (nRet == ErrOK)
	{
		u_short nCurrent, nPwm;
		GetLedCurrentValue(0, &nCurrent);
		GetLedPwmValue(0, &nPwm);
		m_LedCurrSlider.SetPos(nCurrent);
		m_LedPwmSlider.SetPos(nPwm);

		m_bDeviceConnect = TRUE;

		GetDlgItem(IDC_AFM_LASERON_BUTTON)->EnableWindow(TRUE);
		GetDlgItem(IDC_AFM_HOME_BUTTON)->EnableWindow(TRUE);
		GetDlgItem(IDC_AFM_MAKE0_BUTTON)->EnableWindow(TRUE);
		GetDlgItem(IDC_AFM_SAVE_BUTTON)->EnableWindow(TRUE);
		GetDlgItem(IDC_AFM_ZUP_BUTTON)->EnableWindow(TRUE);
		GetDlgItem(IDC_AFM_ZDOWN_BUTTON)->EnableWindow(TRUE);
		GetDlgItem(IDC_AFM_MOTION_STOP_BUTTON)->EnableWindow(TRUE);
		GetDlgItem(IDC_AFM_ZMOVE_EDIT)->EnableWindow(TRUE);
		GetDlgItem(IDC_AFM_LED_CURRENT_SLIDER)->EnableWindow(TRUE);
		GetDlgItem(IDC_AFM_LED_PWM_SLIDER)->EnableWindow(TRUE);

		SetTimer(AFM_UPDATE_TIMER, 300, NULL);
	}

	return nRet;
}

void CAFModuleDlg::GetDeviceStatus()
{
	m_bDeviceConnect = IsDeviceConnected();
	if (m_bDeviceConnect == TRUE)
		((CStatic*)GetDlgItem(IDC_ICON_AFM_CONNECT))->SetIcon(m_LedIcon[1]);
	else
		((CStatic*)GetDlgItem(IDC_ICON_AFM_CONNECT))->SetIcon(m_LedIcon[0]);

	m_bInRangeStatus = GetInRangeStatus();
	if (m_bInRangeStatus == TRUE)
		((CStatic*)GetDlgItem(IDC_ICON_AFM_INRANGE))->SetIcon(m_LedIcon[1]);
	else
		((CStatic*)GetDlgItem(IDC_ICON_AFM_INRANGE))->SetIcon(m_LedIcon[0]);

	m_bInFocusStatus = GetInFocusStatus();
	if (m_bInFocusStatus == TRUE)
		((CStatic*)GetDlgItem(IDC_ICON_AFM_INFOCUS))->SetIcon(m_LedIcon[1]);
	else
		((CStatic*)GetDlgItem(IDC_ICON_AFM_INFOCUS))->SetIcon(m_LedIcon[0]);

	m_bHomePosition = IsHomePosition();
	if (m_bHomePosition == TRUE)
		((CStatic*)GetDlgItem(IDC_ICON_AFM_HOME))->SetIcon(m_LedIcon[1]);
	else
		((CStatic*)GetDlgItem(IDC_ICON_AFM_HOME))->SetIcon(m_LedIcon[0]);

	m_bBusyStatus = IsMotionMoving();
	if (m_bBusyStatus == TRUE)
		((CStatic*)GetDlgItem(IDC_ICON_AFM_BUSY))->SetIcon(m_LedIcon[2]);
	else
		((CStatic*)GetDlgItem(IDC_ICON_AFM_BUSY))->SetIcon(m_LedIcon[0]);

	m_bSensorLimitCW = IsLimitCW();
	if (m_bSensorLimitCW == TRUE)
		((CStatic*)GetDlgItem(IDC_ICON_AFM_LIMIT_CW))->SetIcon(m_LedIcon[1]);
	else
		((CStatic*)GetDlgItem(IDC_ICON_AFM_LIMIT_CW))->SetIcon(m_LedIcon[0]);

	m_bSensorLimitCCW = IsLimitCCW();
	if (m_bSensorLimitCCW == TRUE)
		((CStatic*)GetDlgItem(IDC_ICON_AFM_LIMIT_CCW))->SetIcon(m_LedIcon[1]);
	else
		((CStatic*)GetDlgItem(IDC_ICON_AFM_LIMIT_CCW))->SetIcon(m_LedIcon[0]);

	m_nCurrentObjNum = GetObjectNumber();
	if (m_nCurrentObjNum == 0)
	{
		((CStatic*)GetDlgItem(IDC_ICON_AFM_OBJECT0))->SetIcon(m_LedIcon[2]);
		((CStatic*)GetDlgItem(IDC_ICON_AFM_OBJECT1))->SetIcon(m_LedIcon[0]);
	}
	else
	{
		((CStatic*)GetDlgItem(IDC_ICON_AFM_OBJECT0))->SetIcon(m_LedIcon[0]);
		((CStatic*)GetDlgItem(IDC_ICON_AFM_OBJECT1))->SetIcon(m_LedIcon[2]);
	}

	m_nAbsolutePosZ = GetAbsoluteZPos();

	CString tmp;
	tmp.Format("%d", m_nAbsolutePosZ);
	if (m_ZPosCtrl.m_hWnd != NULL)
		m_ZPosCtrl.SetWindowText(tmp);

	u_short puStepMm, puUstep;

	int nRet = ATF_ReadStepPerMmConversion(&puStepMm);
	if (nRet != 0)
	{
		if (m_ZumPosCtrl.m_hWnd != NULL)
			m_ZumPosCtrl.SetWindowText(_T("ReadMicrostep Error"));
		return;
	}

	nRet = ATF_ReadMicrostep(&puUstep);
	if (nRet != 0)
	{
		if (m_ZumPosCtrl.m_hWnd != NULL)
			m_ZumPosCtrl.SetWindowText(_T("ReadMicrostep Error"));
		return;
	}

	double Zpos = (double)((double)(m_nAbsolutePosZ)/((double)(puStepMm*puUstep) / 1000));

	tmp.Format("%.3f", Zpos);
	if (m_ZumPosCtrl.m_hWnd != NULL)
		m_ZumPosCtrl.SetWindowText(tmp);
}

int CAFModuleDlg::MoveTopPosition()
{
	int nRet = 0;

	nRet = MoveZaxisUp(22500);
	WaitSec(1);

	return nRet;
}

BOOL CAFModuleDlg::Command(int nCmd)
{
	if (m_bDeviceConnect != TRUE)
	{
		AfxMessageBox(_T("디바이스와 연결이 되지 않았습니다."));
		return FALSE;
	}

	if (m_pCommandThread != NULL)
	{
		//g_pLog->Display(0, _T("이미 다른 작업이 수행 중이므로 동작 할 수 없습니다!"));
		return -1;	//Error Code정의 후 변경
	}

	m_nCmd = nCmd;

	m_pCommandThread = AfxBeginThread(CAFModuleDlg::CommandThread, this);

	if (m_pCommandThread == NULL)
	{
		return FALSE;
	}

	return TRUE;
}

UINT CAFModuleDlg::CommandThread(LPVOID pClass)
{
	CAFModuleDlg* pDlg = (CAFModuleDlg *)pClass;

	int nRet = 0;

	nRet = pDlg->MoveTopPosition();

	pDlg->WaitSec(2);

	if (nRet == ErrOK)
	{
		switch (pDlg->m_nCmd)
		{
		case LR_HOME:
			g_pLRevolver->MoveHomeRevolver();
			pDlg->m_nChangeobjnumber = 0;
			break;
		case LR_LEFT:	//100x
			nRet = pDlg->MoveHomeAfm();
			pDlg->WaitSec(5);
			g_pLRevolver->MoveLeft();
			nRet = pDlg->ChangeObjNumber(1);
			if (nRet != ErrOK) break;
			pDlg->WaitSec(2);
			nRet = pDlg->MoveHomeAfm();
			if (nRet != ErrOK) break;
			pDlg->WaitSec(8);
			nRet = pDlg->MoveZaxisDown(156.5);	//확인필요
			if (nRet != ErrOK) break;
			pDlg->m_LedPwmSlider.SetPos(30);
			pDlg->m_nChangeobjnumber = 1;
			break;
		case LR_RIGHT:	//4x
			nRet = pDlg->MoveHomeAfm();
			pDlg->WaitSec(5);
			g_pLRevolver->MoveRight();
			nRet = pDlg->ChangeObjNumber(0);
			if (nRet != ErrOK) break;
			pDlg->WaitSec(2);
			nRet = pDlg->MoveHomeAfm();
			if (nRet != ErrOK) break;
			pDlg->WaitSec(8);
			pDlg->m_LedPwmSlider.SetPos(10);
			pDlg->m_nChangeobjnumber = 2;
			break;
		}
	}

	pDlg->m_pCommandThread = NULL;

	if (nRet != ErrOK)
	{
		//g_pAlarm->SetAlarm(pDlg->GetErrorCode(nRet));
		//CString strMsg;
		//strMsg.Format(_T("에러가 발생하였습니다. (Error Code : %d)"), pDlg->GetErrorCode(nRet));
		//AfxMessageBox(strMsg);
	}

	return 0;
}


int CAFModuleDlg::GetErrorCode(int nCode)
{
	if (nCode == ErrNoAccess)				 return ATF_NO_ACCESS_ERROR;
	else if (nCode == ErrWrongType)			 return ATF_WRONG_TYPE_ERROR;
	else if (nCode == ErrOutOfBound)		 return ATF_OUT_OF_BOUND_ERROR;
	else if (nCode == ErrInvalid)			 return ATF_INVALID_ERROR;
	else if (nCode == ErrUnavailable)		 return ATF_UNAVAILABLE_ERROR;
	else if (nCode == ErrNotSupported)		 return ATF_NOT_SUPPORTED_ERROR;
	else if (nCode == ErrSntaxError)		 return ATF_SNTAX_ERROR;
	else if (nCode == ErrNoresources)		 return ATF_NO_RESOURCES_ERROR;
	else if (nCode == ErrInternal)			 return ATF_INTERNAL_ERROR;
	else if (nCode == ErrOperFailed)		 return ATF_OPER_FAILED_ERROR;
	else if (nCode == ErrTimeout)			 return ATF_TIMEOUT_ERROR;
	else if (nCode == ErrChksum)			 return ATF_CHKSUM_ERROR;
	else if (nCode == ErrOperFailedWithNack) return ATF_OPER_FAILED_WITH_NACK;
	else									 return ATF_UNKNOWN_ERROR;
}


void CAFModuleDlg::OnBnClickedAfmLaseronButton()
{

	if (m_bDeviceConnect != TRUE)
	{
		AfxMessageBox(_T("디바이스와 연결이 되지 않았습니다."));
		return;
	}

	if (IDYES != AfxMessageBox("LASER를 켤까요?", MB_YESNO)) return;

	int nRet;
	nRet = LaserOn();
	if (nRet != ErrOK)
	{
		CString strMsg;
		strMsg.Format(_T("Failed to turn on the laser. (Error Code : %d)"), GetErrorCode(nRet));
		AfxMessageBox(strMsg);
	}
	else
	{
		GetDlgItem(IDC_AFM_LASERON_BUTTON)->EnableWindow(FALSE);
		GetDlgItem(IDC_AFM_LASEROFF_BUTTON)->EnableWindow(TRUE);
		GetDlgItem(IDC_AFM_AFON_BUTTON)->EnableWindow(TRUE);
		GetDlgItem(IDC_AFM_AFOFF_BUTTON)->EnableWindow(FALSE);
	}
}


void CAFModuleDlg::OnBnClickedAfmLaseroffButton()
{

	if (m_bDeviceConnect != TRUE)
	{
		AfxMessageBox(_T("디바이스와 연결이 되지 않았습니다."));
		return;
	}

	int nRet;
	nRet = LaserOff();
	if (nRet != ErrOK)
	{
		CString strMsg;
		strMsg.Format(_T("Failed to turn off the laser. (Error Code : %d)"), GetErrorCode(nRet));
		AfxMessageBox(strMsg);
	}
	else
	{
		GetDlgItem(IDC_AFM_LASERON_BUTTON)->EnableWindow(TRUE);
		GetDlgItem(IDC_AFM_LASEROFF_BUTTON)->EnableWindow(FALSE);
		GetDlgItem(IDC_AFM_AFON_BUTTON)->EnableWindow(FALSE);
		GetDlgItem(IDC_AFM_AFOFF_BUTTON)->EnableWindow(FALSE);
	}
}


void CAFModuleDlg::OnBnClickedAfmAfonButton()
{

	if (m_bDeviceConnect != TRUE)
	{
		AfxMessageBox(_T("디바이스와 연결이 되지 않았습니다."));
		return;
	}

	int nRet = 0;

	if (IDYES != AfxMessageBox("AUTO FOCUS를 동작할까요?", MB_YESNO)) return;

	if (m_nCurrentObjNum == 1)
	{
		nRet = SetAFMotionLimit(200, 200);

		if (nRet != ErrOK)
		{
			CString strMsg;
			strMsg.Format(_T("Failed to set af motion limit. (Error Code : %d)"), GetErrorCode(nRet));
			AfxMessageBox(strMsg);
			return;
		}
	}

	nRet = AFOn();

	if (nRet != ErrOK)
	{
		CString strMsg;
		strMsg.Format(_T("Failed to turn on the AF. (Error Code : %d)"), GetErrorCode(nRet));
		AfxMessageBox(strMsg);
	}
	else
	{
		GetDlgItem(IDC_AFM_AFON_BUTTON)->EnableWindow(FALSE);
		GetDlgItem(IDC_AFM_AFOFF_BUTTON)->EnableWindow(TRUE);
		GetDlgItem(IDC_AFM_ZUP_BUTTON)->EnableWindow(FALSE);
		GetDlgItem(IDC_AFM_ZDOWN_BUTTON)->EnableWindow(FALSE);

		m_bAfOnStatus = TRUE;
	}
}


void CAFModuleDlg::OnBnClickedAfmAfoffButton()
{

	if (m_bDeviceConnect != TRUE)
	{
		AfxMessageBox(_T("디바이스와 연결이 되지 않았습니다."));
		return;
	}

	int nRet;

	nRet = AFOff();

	if (nRet != ErrOK)
	{
		CString strMsg;
		strMsg.Format(_T("Failed to turn off the AF. (Error Code : %d)"), GetErrorCode(nRet));
		AfxMessageBox(strMsg);
	}
	else
	{
		GetDlgItem(IDC_AFM_AFON_BUTTON)->EnableWindow(TRUE);
		GetDlgItem(IDC_AFM_AFOFF_BUTTON)->EnableWindow(FALSE);
		GetDlgItem(IDC_AFM_ZUP_BUTTON)->EnableWindow(TRUE);
		GetDlgItem(IDC_AFM_ZDOWN_BUTTON)->EnableWindow(TRUE);

		m_bAfOnStatus = FALSE;
	}
}


void CAFModuleDlg::OnBnClickedAfmHomeButton()
{

	if (m_bDeviceConnect != TRUE)
	{
		AfxMessageBox(_T("디바이스와 연결이 되지 않았습니다."));
		return;
	}

	if (IDYES != AfxMessageBox("HOME 위치로 이동할까요?", MB_YESNO)) return;

	int nRet;

	nRet = MoveHomeAfm();

	if (nRet != ErrOK)
	{
		CString strMsg;
		strMsg.Format(_T("Failed to move home position. (Error Code : %d)"), GetErrorCode(nRet));
		AfxMessageBox(strMsg);
	}
}


void CAFModuleDlg::OnBnClickedAfmMake0Button()
{

	if (m_bDeviceConnect != TRUE)
	{
		AfxMessageBox(_T("디바이스와 연결이 되지 않았습니다."));
		return;
	}

	int nRet;

	nRet = Make0();

	if (nRet != ErrOK)
	{
		CString strMsg;
		strMsg.Format(_T("Failed to set make0. (Error Code : %d)"), GetErrorCode(nRet));
		AfxMessageBox(strMsg);
	}
}


void CAFModuleDlg::OnBnClickedAfmSaveButton()
{

	if (m_bDeviceConnect != TRUE)
	{
		AfxMessageBox(_T("디바이스와 연결이 되지 않았습니다."));
		return;
	}

	int nRet;

	nRet = SaveAll();

	if (nRet != ErrOK)
	{
		CString strMsg;
		strMsg.Format(_T("Failed to save the setting value. (Error Code : %d)"), GetErrorCode(nRet));
		AfxMessageBox(strMsg);
	}
}


void CAFModuleDlg::OnBnClickedAfmZupButton()
{

	if (m_bDeviceConnect != TRUE)
	{
		AfxMessageBox(_T("디바이스와 연결이 되지 않았습니다."));
		return;
	}

	int nRet;
	CString sMoveValue;

	GetDlgItemText(IDC_AFM_ZMOVE_EDIT, sMoveValue);
	float nMoveValue = atof(sMoveValue);
	if (nMoveValue < 0 || nMoveValue > 10000) return;

	nRet = MoveZaxisUp(nMoveValue);

	if (nRet != ErrOK)
	{
		CString strMsg;
		strMsg.Format(_T("Failed to move z-axis. (Error Code : %d)"), GetErrorCode(nRet));
		AfxMessageBox(strMsg);
	}
}


void CAFModuleDlg::OnBnClickedAfmZdownButton()
{

	if (m_bDeviceConnect != TRUE)
	{
		AfxMessageBox(_T("디바이스와 연결이 되지 않았습니다."));
		return;
	}

	int nRet;
	CString sMoveValue;

	GetDlgItemText(IDC_AFM_ZMOVE_EDIT, sMoveValue);
	float nMoveValue = atof(sMoveValue);
	float test = (nMoveValue * -10) + m_nAbsolutePosZ;
	if (test < -5000)
	{
		AfxMessageBox(_T("Error : Software llimit."));
		return;
	}

	if (nMoveValue < 0 || nMoveValue > 10000) return;

	nRet = MoveZaxisDown(nMoveValue);

	if (nRet != ErrOK)
	{
		CString strMsg;
		strMsg.Format(_T("Failed to move z-axis. (Error Code : %d)"), GetErrorCode(nRet));
		AfxMessageBox(strMsg);
	}
}

void CAFModuleDlg::OnBnClickedAfmMotionStopButton()
{

	if (m_bDeviceConnect != TRUE)
	{
		AfxMessageBox(_T("디바이스와 연결이 되지 않았습니다."));
		return;
	}

	MoveZaxisStop();
}

///////////////////////////////////////////////////////////////
// Light Control Function
///////////////////////////////////////////////////////////////

void CAFModuleDlg::OnNMCustomdrawAfmLedCurrentSlider(NMHDR *pNMHDR, LRESULT *pResult)
{
	LPNMCUSTOMDRAW pNMCD = reinterpret_cast<LPNMCUSTOMDRAW>(pNMHDR);
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.

	int nPos;
	CString strPos;

	nPos = m_LedCurrSlider.GetPos();
	strPos.Format(_T("%d"), nPos);
	m_LedCurrEdit.SetWindowText(strPos);

	SetLedCurrentValue(0, nPos);

	*pResult = 0;
}

void CAFModuleDlg::OnNMCustomdrawAfmLedPwmSlider(NMHDR *pNMHDR, LRESULT *pResult)
{
	LPNMCUSTOMDRAW pNMCD = reinterpret_cast<LPNMCUSTOMDRAW>(pNMHDR);
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.

	int nPos;
	CString strPos;

	nPos = m_LedPwmSlider.GetPos();
	strPos.Format(_T("%d"), nPos);
	m_LedPwmEdit.SetWindowText(strPos);

	SetLedPwmValue(0, nPos);

	*pResult = 0;
}


void CAFModuleDlg::OnBnClickedAfmConnectButton()
{
	if (!m_bDeviceConnect)
	{
		int nRet = OpenDevice();
		if (nRet != ErrOK)
			AfxMessageBox(_T("AF Module OpenDevice Error !"));
	}
}


void CAFModuleDlg::OnBnClickedAfmDisconnectButton2()
{
	if (m_bDeviceConnect)
	{
		CloseDevice();
		KillTimer(AFM_UPDATE_TIMER);
		m_bDeviceConnect = FALSE;

		((CStatic*)GetDlgItem(IDC_ICON_AFM_CONNECT))->SetIcon(m_LedIcon[2]);
		((CStatic*)GetDlgItem(IDC_ICON_AFM_OBJECT0))->SetIcon(m_LedIcon[0]);
		((CStatic*)GetDlgItem(IDC_ICON_AFM_OBJECT1))->SetIcon(m_LedIcon[0]);
		((CStatic*)GetDlgItem(IDC_ICON_AFM_INFOCUS))->SetIcon(m_LedIcon[0]);
		((CStatic*)GetDlgItem(IDC_ICON_AFM_INRANGE))->SetIcon(m_LedIcon[0]);
		((CStatic*)GetDlgItem(IDC_ICON_AFM_HOME))->SetIcon(m_LedIcon[0]);
		((CStatic*)GetDlgItem(IDC_ICON_AFM_BUSY))->SetIcon(m_LedIcon[0]);
		((CStatic*)GetDlgItem(IDC_ICON_AFM_LIMIT_CW))->SetIcon(m_LedIcon[0]);
		((CStatic*)GetDlgItem(IDC_ICON_AFM_LIMIT_CCW))->SetIcon(m_LedIcon[0]);

		GetDlgItem(IDC_AFM_HOME_BUTTON)->EnableWindow(FALSE);
		GetDlgItem(IDC_AFM_LASERON_BUTTON)->EnableWindow(FALSE);
		GetDlgItem(IDC_AFM_LASEROFF_BUTTON)->EnableWindow(FALSE);
		GetDlgItem(IDC_AFM_MAKE0_BUTTON)->EnableWindow(FALSE);
		GetDlgItem(IDC_AFM_AFON_BUTTON)->EnableWindow(FALSE);
		GetDlgItem(IDC_AFM_AFOFF_BUTTON)->EnableWindow(FALSE);
		GetDlgItem(IDC_AFM_SAVE_BUTTON)->EnableWindow(FALSE);
		GetDlgItem(IDC_AFM_ZUP_BUTTON)->EnableWindow(FALSE);
		GetDlgItem(IDC_AFM_ZDOWN_BUTTON)->EnableWindow(FALSE);
		GetDlgItem(IDC_AFM_MOTION_STOP_BUTTON)->EnableWindow(FALSE);
		GetDlgItem(IDC_AFM_ZMOVE_EDIT)->EnableWindow(FALSE);

		CString tmp;
		tmp = _T(" ");
		if (m_ZPosCtrl.m_hWnd != NULL)
			m_ZPosCtrl.SetWindowText(tmp);
	}
}
