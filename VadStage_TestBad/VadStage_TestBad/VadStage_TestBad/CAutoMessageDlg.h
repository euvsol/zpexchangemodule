﻿#pragma once


// CAutoMessageDlg 대화 상자

class CAutoMessageDlg : public CDialogEx
{
	DECLARE_DYNAMIC(CAutoMessageDlg)

public:
	CAutoMessageDlg(CWnd* pParent = nullptr);   // 표준 생성자입니다.
	virtual ~CAutoMessageDlg();

// 대화 상자 데이터입니다.
//#ifdef AFX_DESIGN_TIME
	enum { IDD = IDD_AUTOMESSAGE_DIALOG };
//#endif

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 지원입니다.

	DECLARE_MESSAGE_MAP()

public :
	CString m_strMessage;
	double m_WaitTime;
	afx_msg void OnTimer(UINT_PTR nIDEvent);
	virtual INT_PTR DoModal(CString str, double WaitTime);
	virtual void OnOK();
	virtual BOOL OnInitDialog();
	CString m_strMessageString;
};
