﻿#pragma once


// CDetecterApertureChangeDlg 대화 상자

class CDetecterApertureChangeDlg : public CDialogEx, public CSmarActCtrl, public CECommon
{
	DECLARE_DYNAMIC(CDetecterApertureChangeDlg)

public:
	CDetecterApertureChangeDlg(CWnd* pParent = nullptr);   // 표준 생성자입니다.
	virtual ~CDetecterApertureChangeDlg();

// 대화 상자 데이터입니다.
//#ifdef AFX_DESIGN_TIME
	enum { IDD = IDD_DETECTER_APERTURE_CHANGE_DIALOG };
//#endif

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 지원입니다.

	DECLARE_MESSAGE_MAP()
public:

	HICON	m_LedIcon[3];

	BOOL m_bStopDetecterAxis;
	BOOL m_bStopApertureAxis;

	afx_msg void OnDestroy();
	afx_msg void OnTimer(UINT_PTR nIDEvent);
	virtual BOOL OnInitDialog();
	afx_msg void OnBnClickedButtonDetecterMove();
	double m_dVelocityDetecter;
	double m_dAccelerationDetecter;
	double m_dMoveDistDetecter;
	int m_nMoveTypeDetecter;
	int m_nMoveTypeAperture;
	afx_msg void OnBnClickedButtonDetecterStop();
	afx_msg void OnBnClickedButtonDetecterReference();
	afx_msg void OnBnClickedButtonDetecterCalibrate();
	CStatic m_dCurrentPosDetecter;
	CStatic m_dCurrentPosAperture;
	afx_msg void OnBnClickedButtonDetecterApertureConnect();
	CString m_strPosTypeDetecter;
	CString m_strPosTypeAperture;
	afx_msg void OnBnClickedButtonDetecterApertureDisconnect();
	afx_msg void OnBnClickedButtonApertureMove();

	double m_dVelocityAperture;
	double m_dAccelerationAperture;
	double m_dMoveDistAperture;
	afx_msg void OnBnClickedButtonApertureStop();
	afx_msg void OnBnClickedButtonApertureReference();
	afx_msg void OnBnClickedButtonApertureCalibrate();
	CStatic m_detector_amp_enable;
	CStatic m_detector_closeloop;
	CStatic m_detector_activemove;
	CStatic m_detector_sensor;
	CStatic m_detector_calibrated;
	CStatic m_detector_referenced;
	CStatic m_detector_endstop;
	CStatic m_detector_rangelimit;
	CStatic m_detector_followingerror;
	CStatic m_aperture_ampenable;
	CStatic m_aperture_closeloop;
	CStatic m_aperture_active;
	CStatic m_aperture_sensor;
	CStatic m_aperture_calibrated;
	CStatic m_aperture_referenced;
	CStatic m_aperture_endstop;
	CStatic m_aperture_rangelimit;
	CStatic m_aperture_followingerror;
	CStatic m_detetor_aperture_icon_con;
	double m_detecter_start_pos;
	double m_detecter_end_pos;
	double m_detecter_dwell_pos;
	double m_aperture_start_pos;
	double m_aperture_end_pos;
	double m_aperture_dwell;

	void DetectorBackForthStart();
	void ApertureBackForthStart();

	afx_msg void OnBnClickedButtonDetecterStartPos();
	afx_msg void OnBnClickedButtonApertureStart();
	double m_detector_step;
	afx_msg void OnBnClickedButtonDetecterTestStart();
	CStatic m_detect_target_text;
	CStatic m_detect_current_text;
	double m_detector_test_vel;
	double m_aperture_test_vel;

	BOOL m_bDecteterTestFlag;
	afx_msg void OnBnClickedButtonDetecterTestStop();

	double d_pos_test_target;
	afx_msg void OnBnClickedButtonDetecterTestStart2();
	afx_msg void OnBnClickedButtonDetecterTestStart3();

	afx_msg void OnBnClickedButtonDetecterTestStart4();
	CEdit m_detector_test_count;
	CEdit m_aperture_test_count;
};
