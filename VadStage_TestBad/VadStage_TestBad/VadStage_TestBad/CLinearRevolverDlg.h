﻿#pragma once


// CLinearRevolverDlg 대화 상자

class CLinearRevolverDlg : public CDialogEx , public CLinearRevolverCtrl
{
	DECLARE_DYNAMIC(CLinearRevolverDlg)

public:
	CLinearRevolverDlg(CWnd* pParent = nullptr);   // 표준 생성자입니다.
	virtual ~CLinearRevolverDlg();

// 대화 상자 데이터입니다.
//#ifdef AFX_DESIGN_TIME
	enum { IDD = IDD_LINEAR_REVOLVER_DIALOG };
//#endif

protected:

	HICON			m_LedIcon[3];
	BOOL	        m_bMovePossible;
	BOOL			m_bDeviceConnect;

	void GetDeviceStatus();
	void SetControlEnable(BOOL bState);
	void InitializeControls();

	

	DECLARE_MESSAGE_MAP()
public :

	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 지원입니다.

	int  OpenDevice();
	void Display4xImage();
	void Display100xImage();
	BOOL Is_LLC2_Connected() { return m_bDeviceConnect; }
	afx_msg void OnTimer(UINT_PTR nIDEvent);
	afx_msg void OnDestroy();
	virtual BOOL OnInitDialog();
	afx_msg void OnBnClickedLlc2HomeButton();
	afx_msg void OnBnClickedLlc2RightButton();
	afx_msg void OnBnClickedLlc2LeftButton();
	afx_msg void OnBnClickedLlc2ConButton();
	afx_msg void OnBnClickedLlc2DisconButton();
};
