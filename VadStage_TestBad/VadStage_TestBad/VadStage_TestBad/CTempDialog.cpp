﻿// CTempDialog.cpp: 구현 파일
//


#include "pch.h"
#include "Include.h"
#include "Extern.h"

// CTempDialog 대화 상자

IMPLEMENT_DYNAMIC(CTempDialog, CDialogEx)

CTempDialog::CTempDialog(CWnd* pParent /*=nullptr*/)
	: CDialogEx(IDD_TEMP_DIALOG, pParent)
{

}

CTempDialog::~CTempDialog()
{
}

void CTempDialog::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
}


BEGIN_MESSAGE_MAP(CTempDialog, CDialogEx)
	ON_WM_DESTROY()
	ON_WM_TIMER()
END_MESSAGE_MAP()


// CTempDialog 메시지 처리기


BOOL CTempDialog::OnInitDialog()
{
	__super::OnInitDialog();

	// TODO:  여기에 추가 초기화 작업을 추가합니다.

	return TRUE;  // return TRUE unless you set the focus to a control
				  // 예외: OCX 속성 페이지는 FALSE를 반환해야 합니다.
}


void CTempDialog::OnDestroy()
{
	__super::OnDestroy();

	// TODO: 여기에 메시지 처리기 코드를 추가합니다.
}


void CTempDialog::OnTimer(UINT_PTR nIDEvent)
{
	// TODO: 여기에 메시지 처리기 코드를 추가 및/또는 기본값을 호출합니다.

	__super::OnTimer(nIDEvent);
}

//int CTempDialog::OpenPort(CString sPortName, DWORD dwBaud, BYTE wByte, BYTE wStop, BYTE wParity)
//{
//	int nRet = 0;
//
//	nRet = OpenSerialPort(sPortName, dwBaud, wByte, wStop, wParity);
//
//	if (nRet != 0)
//	{
//		m_strSendMsg = _T("SERIAL PORT OPEN FAIL");
//		nRet = -81001;
//	}
//	else m_strSendMsg = _T("SERIAL PORT OPEN");
//
//	SaveLogFile("LLC TMP_Com", _T((LPSTR)(LPCTSTR)("PC -> LLC TMP : " + m_strSendMsg)));	//통신 상태 기록.
//
//	return nRet;
//}
//
//
//int CTempDialog::ReceiveData(char *lParam, DWORD dwRead)
//{
//	int nRet = 0;
//
//	//SetEvent(m_hStateOkEvent);	//Sets the specified event object to the signaled state.
//
//	memset(m_BYTE, '\0', 15);
//	memcpy(m_BYTE, lParam, sizeof(BYTE) * 15);
//
//	CString temp_str;
//
//	CString Recevidata;
//	CString Recevidata_Mid;
//	CString Revevidata_value;
//}
//
//
//int	CTempDialog::SendData(char *lParam, int nTimeOut, int nRetryCnt)
//{
//	int nRet = 0;
//
//	//ResetEvent(m_hStateOkEvent);	//Sets the specified event object to the nonsignaled state.
//
//	m_strSendMsg = lParam;
//	nRet = Send(lParam, nTimeOut);
//
//	//nRet = WaitEvent(m_hStateOkEvent, nTimeOut);
//	if (nRet != 0)
//	{
//		m_strSendMsg = _T("Send Fail");
//		m_TMP_SendState = FALSE;
//		nRet = -81002;
//	}
//	else
//	{
//		m_strSendMsg = _T("Send Ok");
//		m_TMP_SendState = TRUE;
//	}
//
//	SaveLogFile("LLC TMP_Com", _T((LPSTR)(LPCTSTR)("PC -> LLC TMP : " + m_strSendMsg)));	//통신 상태 기록.
//
//	return nRet;
//}
//
//
//int	CTempDialog::SendData(int nSize, BYTE *lParam, int nTimeOut, int nRetryCnt)
//{
//	int nRet = 0;
//	m_strSendMsg = lParam;
//	nRet = Send(nSize, lParam, nTimeOut);
//	if (nRet != 0)
//	{
//		m_strSendMsg = _T("Send Fail");
//		m_TMP_SendState = FALSE;
//		nRet = -81002;
//	}
//	else
//	{
//		m_strSendMsg = _T("Send Ok");
//		m_TMP_SendState = TRUE;
//	}
//
//	SaveLogFile("LLC TMP_Com", _T((LPSTR)(LPCTSTR)("PC -> LLC TMP : " + m_strSendMsg)));	//통신 상태 기록.
//
//	return nRet;
//}
//