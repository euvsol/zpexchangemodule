﻿// CLinearRevolverDlg.cpp: 구현 파일
//

#include "pch.h"
#include "Include.h"
#include "Extern.h"



// CLinearRevolverDlg 대화 상자

IMPLEMENT_DYNAMIC(CLinearRevolverDlg, CDialogEx)

CLinearRevolverDlg::CLinearRevolverDlg(CWnd* pParent /*=nullptr*/)
	: CDialogEx(IDD_LINEAR_REVOLVER_DIALOG, pParent)
{
	m_bMovePossible = FALSE;
	m_bDeviceConnect = FALSE;
}

CLinearRevolverDlg::~CLinearRevolverDlg()
{
}

void CLinearRevolverDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
}


BEGIN_MESSAGE_MAP(CLinearRevolverDlg, CDialogEx)
	ON_WM_TIMER()
	ON_WM_DESTROY()
	ON_BN_CLICKED(IDC_LLC2_HOME_BUTTON, &CLinearRevolverDlg::OnBnClickedLlc2HomeButton)
	ON_BN_CLICKED(IDC_LLC2_RIGHT_BUTTON, &CLinearRevolverDlg::OnBnClickedLlc2RightButton)
	ON_BN_CLICKED(IDC_LLC2_LEFT_BUTTON, &CLinearRevolverDlg::OnBnClickedLlc2LeftButton)
	ON_BN_CLICKED(IDC_LLC2_CON_BUTTON, &CLinearRevolverDlg::OnBnClickedLlc2ConButton)
	ON_BN_CLICKED(IDC_LLC2_DISCON_BUTTON, &CLinearRevolverDlg::OnBnClickedLlc2DisconButton)
END_MESSAGE_MAP()


// CLinearRevolverDlg 메시지 처리기



void CLinearRevolverDlg::OnTimer(UINT_PTR nIDEvent)
{
	KillTimer(nIDEvent);
	switch (nIDEvent)
	{
	case REVOLVER_UPDATE_TIMER:
		GetDeviceStatus();
		SetTimer(nIDEvent, 200, NULL);
		break;
	default:
		break;
	}

	__super::OnTimer(nIDEvent);
}


void CLinearRevolverDlg::OnDestroy()
{
	__super::OnDestroy();

	CloseDevice();
	KillTimer(REVOLVER_UPDATE_TIMER);
}


BOOL CLinearRevolverDlg::OnInitDialog()
{
	__super::OnInitDialog();

	InitializeControls();

	return TRUE;  // return TRUE unless you set the focus to a control
				  // 예외: OCX 속성 페이지는 FALSE를 반환해야 합니다.
}


void CLinearRevolverDlg::InitializeControls()
{
	m_LedIcon[0] = (HICON)::LoadImage(::AfxGetInstanceHandle(), MAKEINTRESOURCE(IDI_LEDOFF), IMAGE_ICON, 32, 32, LR_DEFAULTCOLOR);
	m_LedIcon[1] = (HICON)::LoadImage(::AfxGetInstanceHandle(), MAKEINTRESOURCE(IDI_LEDONGREEN), IMAGE_ICON, 32, 32, LR_DEFAULTCOLOR);
	m_LedIcon[2] = (HICON)::LoadImage(::AfxGetInstanceHandle(), MAKEINTRESOURCE(IDI_LEDONRED), IMAGE_ICON, 32, 32, LR_DEFAULTCOLOR);

	((CStatic*)GetDlgItem(IDC_ICON_REVOLVER_CONNECT))->SetIcon(m_LedIcon[2]);
	((CStatic*)GetDlgItem(IDC_ICON_REVOLVER_LEFT))->SetIcon(m_LedIcon[0]);
	((CStatic*)GetDlgItem(IDC_ICON_REVOLVER_RIGHT))->SetIcon(m_LedIcon[0]);
}

int CLinearRevolverDlg::OpenDevice()
{
	int nRet = 0;
	CString StrPORT = _T("COM6");

	nRet = OpenSerialPort(StrPORT);
	//nRet = OpenSerialPort(g_pConfig->m_chPORT[SERIAL_REVOLVER]);

	if (nRet == 1)
	{
		m_bDeviceConnect = TRUE;
		((CStatic*)GetDlgItem(IDC_ICON_REVOLVER_CONNECT))->SetIcon(m_LedIcon[1]);
		SetControlEnable(m_bDeviceConnect);
		SetTimer(REVOLVER_UPDATE_TIMER, 200, NULL);
	}

	return nRet;
}

void CLinearRevolverDlg::Display4xImage()
{
	if (m_bMovePossible == FALSE)
	{
		AfxMessageBox(_T("Revolver를 동작 할 수 없는 상태입니다."));
		return;
	}

	if (g_pAF->Is_AFM_AfOnStatus() == TRUE)
	{
		AfxMessageBox(_T("AF 동작을 멈춘 후 동작하세요."));
		return;
	}

	if (g_pAF->Is_AFM_BusyStatus() == TRUE)
	{
		AfxMessageBox(_T("Z축이 동작중입니다."));
		return;
	}

	g_pAF->Command(LR_RIGHT);
}

void CLinearRevolverDlg::Display100xImage()
{
	if (m_bMovePossible == FALSE)
	{
		AfxMessageBox(_T("Revolver를 동작 할 수 없는 상태입니다."));
		return;
	}

	if (g_pAF->Is_AFM_AfOnStatus() == TRUE)
	{
		AfxMessageBox(_T("AF 동작을 멈춘 후 동작하세요."));
		return;
	}

	if (g_pAF->Is_AFM_BusyStatus() == TRUE)
	{
		AfxMessageBox(_T("Z축이 동작중입니다."));
		return;
	}

	g_pAF->Command(LR_LEFT);
}

void CLinearRevolverDlg::GetDeviceStatus()
{
	long nRet = CheckStatus();
	if (nRet == 1)
		m_bMovePossible = FALSE;
	else
		m_bMovePossible = TRUE;

	nRet = CheckDirection();
	if (nRet == 1)
	{
		((CStatic*)GetDlgItem(IDC_ICON_REVOLVER_LEFT))->SetIcon(m_LedIcon[2]);
		((CStatic*)GetDlgItem(IDC_ICON_REVOLVER_RIGHT))->SetIcon(m_LedIcon[0]);
	}
	else if (nRet == 2)
	{
		((CStatic*)GetDlgItem(IDC_ICON_REVOLVER_LEFT))->SetIcon(m_LedIcon[0]);
		((CStatic*)GetDlgItem(IDC_ICON_REVOLVER_RIGHT))->SetIcon(m_LedIcon[2]);
	}
	else
	{
		((CStatic*)GetDlgItem(IDC_ICON_REVOLVER_LEFT))->SetIcon(m_LedIcon[0]);
		((CStatic*)GetDlgItem(IDC_ICON_REVOLVER_RIGHT))->SetIcon(m_LedIcon[0]);
	}
}

void CLinearRevolverDlg::SetControlEnable(BOOL bState)
{
	GetDlgItem(IDC_LLC2_HOME_BUTTON)->EnableWindow(bState);
	//GetDlgItem(IDC_LLC2_LEFT_BUTTON)->EnableWindow(bState);
	GetDlgItem(IDC_LLC2_RIGHT_BUTTON)->EnableWindow(bState);
}


void CLinearRevolverDlg::OnBnClickedLlc2HomeButton()
{
	if (m_bDeviceConnect != TRUE)
	{
		AfxMessageBox(_T("디바이스와 연결이 되지 않았습니다."));
		return;
	}

	if (IDYES != AfxMessageBox("HOME 위치로 이동할까요?", MB_YESNO)) return;

	if (m_bMovePossible == FALSE)
	{
		AfxMessageBox(_T("동작 할 수 없는 상태입니다."));
		return;
	}

	if (g_pAF->Is_AFM_AfOnStatus() == TRUE)
	{
		AfxMessageBox(_T("AF 동작을 멈춘 후 동작하세요."));
		return;
	}

	if (g_pAF->Is_AFM_BusyStatus() == TRUE)
	{
		AfxMessageBox(_T("Z축이 동작중입니다."));
		return;
	}

	g_pAF->Command(LR_HOME);
}


void CLinearRevolverDlg::OnBnClickedLlc2RightButton()
{
	if (m_bDeviceConnect != TRUE)
	{
		AfxMessageBox(_T("디바이스와 연결이 되지 않았습니다."));
		return;
	}

	Display4xImage();
}


void CLinearRevolverDlg::OnBnClickedLlc2LeftButton()
{
	if (m_bDeviceConnect != TRUE)
	{
		AfxMessageBox(_T("디바이스와 연결이 되지 않았습니다."));
		return;
	}

	Display100xImage();
}


void CLinearRevolverDlg::OnBnClickedLlc2ConButton()
{
	if (!m_bDeviceConnect)
	{
		int nRet = OpenDevice();
		if (nRet != TRUE)
			AfxMessageBox(_T("Linear Revolver OpenDevice Error !"));
	}

}


void CLinearRevolverDlg::OnBnClickedLlc2DisconButton()
{
	if (m_bDeviceConnect)
	{
		CloseDevice();
		KillTimer(AFM_UPDATE_TIMER);
		m_bDeviceConnect = FALSE;
		((CStatic*)GetDlgItem(IDC_ICON_REVOLVER_CONNECT))->SetIcon(m_LedIcon[2]);

		GetDlgItem(IDC_LLC2_HOME_BUTTON)->EnableWindow(FALSE);
		GetDlgItem(IDC_LLC2_RIGHT_BUTTON)->EnableWindow(FALSE);
		GetDlgItem(IDC_LLC2_LEFT_BUTTON)->EnableWindow(FALSE);
	}
}
