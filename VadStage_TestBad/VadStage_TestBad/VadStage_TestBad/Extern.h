#pragma once

extern	CNavigationStageDlg*			g_pNavigationStage;
extern  CStateTestDlg*					g_pStageRun;
extern CAutoMessageDlg*					g_pAM;
//extern CTempDialog*						g_pTemp;
extern CStageReportDlg*					g_pStageReport;
extern CTabDlg*							g_pTab;
extern CZPExchangeDlg*					g_pZPExchange;
extern CCamZPExchangeDlg*				g_pCamZPExchange;
extern CDetecterApertureChangeDlg*		g_pDetecterApertureChange;
extern CAFModuleDlg*					g_pAF;
extern CLinearRevolverDlg*				g_pLRevolver;
extern MIL_ID							g_milApplication;
extern MIL_ID							g_milSystemHost;
extern MIL_ID							g_milSystemGigEVision;