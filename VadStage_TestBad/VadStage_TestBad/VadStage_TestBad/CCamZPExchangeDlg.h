﻿#pragma once


// CCamZPExchangeDlg 대화 상자

class CCamZPExchangeDlg : public CDialogEx, public CECommon
{
	DECLARE_DYNAMIC(CCamZPExchangeDlg)

public:
	CCamZPExchangeDlg(CWnd* pParent = nullptr);   // 표준 생성자입니다.
	virtual ~CCamZPExchangeDlg();

// 대화 상자 데이터입니다.
//#ifdef AFX_DESIGN_TIME
	enum { IDD = IDD_CAM_ZPEXCHANGE_DIALOG };
//#endif

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 지원입니다.

	BOOL			m_bThreadExitFlag;
	CWinThread*		m_pStatusThread;
	BOOL			m_bGrab;
	BOOL			m_bConnected;

	static UINT	ZpCamGrabThread(LPVOID pParam);

	DECLARE_MESSAGE_MAP()
public:
	int OpenDevice();
	CString GetRootPath();

	MIL_ID	MilDigitizer;
	MIL_ID  m_MilGrayModel;
	MIL_ID  m_MilImageModel;
	MIL_ID  m_MilPMModel;
	MIL_ID  m_MilResultModel;

	CCameraWnd	m_CameraWnd;

	virtual BOOL OnInitDialog();
	afx_msg void OnDestroy();
	afx_msg void OnBnClickedButtonZpexGrab();
	afx_msg void OnBnClickedButtonZpexStop();
	afx_msg void OnBnClickedButtonZpexFindModel();
	afx_msg void OnBnClickedButtonZpexRegPm();
	afx_msg void OnBnClickedCheckZpexShowRoi();
};
