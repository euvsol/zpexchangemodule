﻿// CTabDlg.cpp: 구현 파일
//


#include "pch.h"
#include "Include.h"
#include "Extern.h"


// CTabDlg 대화 상자

IMPLEMENT_DYNAMIC(CTabDlg, CDialogEx)

CTabDlg::CTabDlg(CWnd* pParent /*=nullptr*/)
	: CDialogEx(IDD_TAB_DIALOG, pParent)
{

}

CTabDlg::~CTabDlg()
{
}

void CTabDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
}


BEGIN_MESSAGE_MAP(CTabDlg, CDialogEx)
	ON_BN_CLICKED(IDC_BTN_TAB_STAGE, &CTabDlg::OnBnClickedBtnTabStage)
	ON_BN_CLICKED(IDC_BTN_TAB_ZONPLATE, &CTabDlg::OnBnClickedBtnTabZonplate)
	ON_BN_CLICKED(IDC_BTN_TAB_OPTICSTAGE_CHANGE, &CTabDlg::OnBnClickedBtnTabOpticstageChange)
	ON_BN_CLICKED(IDC_BTN_TAB_CAMERA, &CTabDlg::OnBnClickedBtnTabCamera)
END_MESSAGE_MAP()


// CTabDlg 메시지 처리기


void CTabDlg::OnBnClickedBtnTabStage()
{
	SetDisplay(STAGE);
}

void CTabDlg::OnBnClickedBtnTabZonplate()
{
	SetDisplay(ZONPLATE_EXCHANGE);
}
void CTabDlg::OnBnClickedBtnTabOpticstageChange()
{
// IDC_BTN_TAB_OPTICSTAGE_CHANGE
	SetDisplay(DETECTER_APERTURE_CHANGE);
}

void CTabDlg::OnBnClickedBtnTabCamera()
{
	SetDisplay(CAMERA_CONFIG);
}


int CTabDlg::SetDisplay(int Mode)
{
	if (g_pStageReport != NULL) g_pStageReport->ShowWindow(SW_HIDE);

	g_pStageReport->ShowWindow(SW_HIDE);
	g_pStageRun->ShowWindow(SW_HIDE);
	g_pZPExchange->ShowWindow(SW_HIDE);
	g_pDetecterApertureChange->ShowWindow(SW_HIDE);
	g_pAF->ShowWindow(SW_HIDE);
	g_pLRevolver->ShowWindow(SW_HIDE);
	switch (Mode)
	{
	case STAGE:
		g_pStageReport->ShowWindow(SW_SHOW);
		g_pStageRun->ShowWindow(SW_SHOW);
		break;
	case ZONPLATE_EXCHANGE:
		g_pZPExchange->ShowWindow(SW_SHOW);
		break;
	case DETECTER_APERTURE_CHANGE :
		g_pDetecterApertureChange->ShowWindow(SW_SHOW);
		break;
	case CAMERA_CONFIG:
		g_pAF->ShowWindow(SW_SHOW);
		g_pLRevolver->ShowWindow(SW_SHOW);
		break;
	default:
		break;
	}

	return 0;
}








