﻿#pragma once


// CStageReportDlg 대화 상자

class CStageReportDlg : public CDialogEx, public CECommon
{
	DECLARE_DYNAMIC(CStageReportDlg)

public:
	CStageReportDlg(CWnd* pParent = nullptr);   // 표준 생성자입니다.
	virtual ~CStageReportDlg();

// 대화 상자 데이터입니다.
//#ifdef AFX_DESIGN_TIME
	enum { IDD = IDD_STAGE_REPORT_DIALOG };
//#endif

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 지원입니다.

	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnDestroy();
	afx_msg void OnTimer(UINT_PTR nIDEvent);
	virtual BOOL OnInitDialog();
	void StageReport();
	void StageReportSTD();
	void StageReportReset();

	double get_xLoad_data_pre;
	double get_yLoad_data_pre;
	double get_xInPos_data_pre;
	double get_yInPos_data_pre;

	//double get_xLoad_data_[STAGE_CHECK_DATA_SIZE] = { 0.0, };
	//double get_yLoad_data_[STAGE_CHECK_DATA_SIZE] = { 0.0, };
	//double get_xInPos_data_[STAGE_CHECK_DATA_SIZE] = { 0.0, };
	//double get_yInPos_data_[STAGE_CHECK_DATA_SIZE] = { 0.0, };
	
	double *get_xLoad_data;
	double *get_yLoad_data;
	double *get_xInPos_data;
	double *get_yInPos_data;

	int GetDataCnt = 0;
	

	
	CEdit m_Axis0_load;
	CEdit m_Axis1_load;
	CEdit m_Axis0_Inpos;
	CEdit m_Axis1_Inpos;
	CEdit m_Axis0_Inpos_ave;
	CEdit m_Axis1_Inpos_ave;
	CEdit m_Axis0_Inpos_std;
	CEdit m_Axis1_Inpos_std;
	afx_msg void OnBnClickedBtn();

	double XLoadSum = 0.0;
	double YLoadSum = 0.0;
	double XLoadAvg = 0.0;
	double YLoadAvg = 0.0;

	double XInposSum = 0.0;
	double YInposSum = 0.0;
	double XInposAvg = 0.0;
	double YInposAvg = 0.0;

	double XInposVariance = 0.0;
	double YInposVariance = 0.0;
	double XInposSTD = 0.0;
	double YInposSTD = 0.0;

	double XLoadVariance = 0.0;
	double YLoadVariance = 0.0;
	double XLoadSTD = 0.0;
	double YLoadSTD = 0.0;

	double X_Inpos_value = 0.0;
	double Y_Inpos_value = 0.0;
	double X_Load_value = 0.0;
	double Y_Load_value = 0.0;
	afx_msg void OnBnClickedBtnAuto();
	afx_msg void OnBnClickedBtn2();

	bool DataMeasurement_flag; 
	bool DataReset_flag; 
	CEdit m_Axis0_load_ave;
	CEdit m_Axis1_load_ave;
	CEdit m_Axis0_load_std;
	CEdit m_Axis1_load_std;
	CEdit m_getdatacnt;
	afx_msg void OnBnClickedBtn3();
	CEdit m_axix_0_load_max;
	CEdit m_axix_1_load_max;
	CEdit m_axix_0_inpos_max;
	CEdit m_axix_1_inpos_max;
};
