#include "pch.h"
#include "Include.h"
#include "Extern.h"

BEGIN_MESSAGE_MAP(CCameraWnd, CWnd)
	ON_WM_LBUTTONDOWN()
	ON_WM_LBUTTONUP()
	ON_WM_MOUSEMOVE()
	ON_WM_PAINT()
	ON_WM_TIMER()
	ON_WM_DESTROY()
	ON_WM_SETCURSOR()
END_MESSAGE_MAP()

CCameraWnd::CCameraWnd()
{
	m_nDigSizeX		 = 1544;
	m_nDigSizeY		 = 1544;
	m_nDigBand		 = 3;
	
	m_MilImage		 = M_NULL;
	m_MilOMDisplay	 = M_NULL;

	m_bCrossLine	 = FALSE;
	m_bPMRectDisplay = FALSE;
	m_bPMSuccess	 = FALSE;

	m_dResultPosX	 = 0;
	m_dResultPosY	 = 0;
	m_dResultAngle	 = 0;
	m_dResultScore	 = 0;
}

CCameraWnd::~CCameraWnd()
{
}

void CCameraWnd::OnDestroy()
{
	CWnd::OnDestroy();

	if (m_MilOMDisplay != M_NULL)
	{
		MbufFree(m_MilOMDisplay);
		m_MilOMDisplay = M_NULL;
	}

	if (m_MilImage != M_NULL)
	{
		MbufFree(m_MilImage);
		m_MilImage = M_NULL;
	}
}

BOOL CCameraWnd::PreTranslateMessage(MSG* pMsg)
{
	if (pMsg->message == WM_KEYDOWN)
	{
		switch (pMsg->wParam)
		{
		case VK_ESCAPE:
			return TRUE;
		case VK_RETURN:
			return TRUE;
		default:
			break;
		}
	}

	return CWnd::PreTranslateMessage(pMsg);
}

BOOL CCameraWnd::Create(DWORD dwStyle, RECT& rect, CWnd* pParentWnd)
{
	BOOL ret;
	static CString className = AfxRegisterWndClass(CS_HREDRAW | CS_VREDRAW, AfxGetApp()->LoadStandardCursor(IDC_ARROW));
	ret = CWnd::Create(className, NULL, dwStyle, rect, pParentWnd, 0);

	rcClient.left = 0;
	rcClient.top = 0;
	rcClient.right = rect.right - rect.left;
	rcClient.bottom = rect.bottom - rect.top;

	int nPMRectSize = 200;
	m_RectTrackerOM.m_rect.left = rcClient.Width() / 2 - nPMRectSize;
	m_RectTrackerOM.m_rect.top = rcClient.Height() / 2 - nPMRectSize;
	m_RectTrackerOM.m_rect.right = rcClient.Width() / 2 + nPMRectSize;
	m_RectTrackerOM.m_rect.bottom = rcClient.Height() / 2 + nPMRectSize;
	m_RectTrackerOM.m_nStyle = CRectTracker::dottedLine;

	return ret;
}

void CCameraWnd::OnLButtonDown(UINT nFlags, CPoint point)
{
	if (m_bPMRectDisplay)
		m_RectTrackerOM.Track(this, point, true);
	
	CWnd::OnLButtonDown(nFlags, point);
}

void CCameraWnd::OnLButtonUp(UINT nFlags, CPoint point)
{
	CWnd::OnLButtonUp(nFlags, point);
}

void CCameraWnd::OnMouseMove(UINT nFlags, CPoint point)
{
	m_ptMousePoint = point;
	
	CWnd::OnMouseMove(nFlags, point);
}


void CCameraWnd::OnPaint()
{
	CPaintDC dc(this); // device context for painting
					   // TODO: 여기에 메시지 처리기 코드를 추가합니다.
					   // 그리기 메시지에 대해서는 CWnd::OnPaint()을(를) 호출하지 마십시오.

	SetBkMode(dc.m_hDC, TRANSPARENT);

	if (m_MilOMDisplay == M_NULL) return;

	HDC hDC;
	MbufControl(m_MilOMDisplay, M_DC_ALLOC, M_DEFAULT);
	hDC = (HDC)MbufInquire(m_MilOMDisplay, M_DC_HANDLE, M_NULL);

	BitBlt(dc.GetSafeHdc(), 0, 0, rcClient.Width(), rcClient.Height(), hDC, 0, 0, SRCCOPY);

	if (m_bCrossLine)
	{
		CPen hpen, *hpenOld;
		hpen.CreatePen(PS_DOT, 1, BLUE);
		hpenOld = dc.SelectObject(&hpen);
		dc.MoveTo(0, m_nDigSizeY / 2);
		dc.LineTo(m_nDigSizeX, m_nDigSizeY / 2);
		dc.MoveTo(m_nDigSizeX / 2, 0);
		dc.LineTo(m_nDigSizeX / 2, m_nDigSizeY);
		dc.SelectObject(hpenOld);
		hpen.DeleteObject();
	}

	if (m_bPMRectDisplay)
	{
		CBrush hBrush, *pOldbrush;
		hBrush.CreateSolidBrush(WHITE);
		pOldbrush = dc.SelectObject(&hBrush);
		dc.FrameRect(m_RectTrackerOM.m_rect, &hBrush);
		dc.SelectObject(pOldbrush);
		hBrush.DeleteObject();
		CPen hpen, *hpenOld;
		hpen.CreatePen(PS_DOT, 1, YELLOW);
		hpenOld = dc.SelectObject(&hpen);
		dc.MoveTo(m_RectTrackerOM.m_rect.left, m_RectTrackerOM.m_rect.top + m_RectTrackerOM.m_rect.Height() / 2);
		dc.LineTo(m_RectTrackerOM.m_rect.right, m_RectTrackerOM.m_rect.top + m_RectTrackerOM.m_rect.Height() / 2);
		dc.MoveTo(m_RectTrackerOM.m_rect.left + m_RectTrackerOM.m_rect.Width() / 2, m_RectTrackerOM.m_rect.top);
		dc.LineTo(m_RectTrackerOM.m_rect.left + m_RectTrackerOM.m_rect.Width() / 2, m_RectTrackerOM.m_rect.bottom);
		dc.SelectObject(hpenOld);
		hpen.DeleteObject();
	}

	if (m_bPMSuccess)
	{
		int nCrossSize = 10;
		int nCrossRectSize = m_RectTrackerOM.m_rect.Width() / 2;
		CPen hpen, *hpenOld;
		hpen.CreatePen(PS_SOLID, 2, RED);
		hpenOld = dc.SelectObject(&hpen);
		dc.MoveTo(m_dResultPosX - nCrossSize, m_dResultPosY);
		dc.LineTo(m_dResultPosX + nCrossSize, m_dResultPosY);
		dc.MoveTo(m_dResultPosX, m_dResultPosY - nCrossSize);
		dc.LineTo(m_dResultPosX, m_dResultPosY + nCrossSize);
		dc.MoveTo(m_dResultPosX - nCrossRectSize, m_dResultPosY - nCrossRectSize);
		dc.LineTo(m_dResultPosX - nCrossRectSize, m_dResultPosY + nCrossRectSize);
		dc.LineTo(m_dResultPosX + nCrossRectSize, m_dResultPosY + nCrossRectSize);
		dc.LineTo(m_dResultPosX + nCrossRectSize, m_dResultPosY - nCrossRectSize);
		dc.LineTo(m_dResultPosX - nCrossRectSize, m_dResultPosY - nCrossRectSize);
		dc.SelectObject(hpenOld);
		hpen.DeleteObject();
		
		CString str; CRect rect;
		rect.left = m_dResultPosX - nCrossRectSize;
		rect.top = m_dResultPosY - nCrossRectSize - 30;
		rect.right = rect.left + 500;
		rect.bottom = rect.top + 30;
		str.Format("Matching Score : %.2f %%", m_dResultScore);
		dc.SetTextColor(RED);
		dc.DrawText(str, rect, DT_LEFT | DT_WORDBREAK | DT_EDITCONTROL);
	}

	double pixelvalue_x = 0.0, pixelvalue_y = 0.0;
	pixelvalue_x = (double)m_nDigSizeX / rcClient.Width();
	pixelvalue_y = (double)m_nDigSizeY / rcClient.Height();
	int pixel_x = (int)((abs((double)m_ptMousePoint.x - m_nDigSizeX / 2)));
	int pixel_y = (int)((abs((double)m_ptMousePoint.y - m_nDigSizeY / 2)));
	CString str;
	str.Format("center_point_x=%d, center_point_y=%d, center_distance_x=%.3f um, center_distance_y=%.3f um", pixel_x, pixel_y, 0.86 * pixel_x, 0.86 * pixel_y);
	dc.SetTextColor(RED);
	dc.DrawText(str, rcClient, DT_LEFT | DT_WORDBREAK | DT_EDITCONTROL);

	MbufControl(m_MilOMDisplay, M_DC_FREE, M_DEFAULT);
}

void CCameraWnd::OnTimer(UINT_PTR nIDEvent)
{
	switch (nIDEvent)
	{
	case ZPEXCHANGE_DISPLAY_TIMER:
		KillTimer(nIDEvent);
		if (m_MilImage != M_NULL)
			MbufCopy(m_MilImage, m_MilOMDisplay);
		Invalidate(FALSE);
		SetTimer(nIDEvent, 100, 0);
		break;
	}

	CWnd::OnTimer(nIDEvent);
}

BOOL CCameraWnd::OnSetCursor(CWnd* pWnd, UINT nHitTest, UINT message)
{
	if (m_bPMRectDisplay)
	{
		if (m_RectTrackerOM.SetCursor(this, nHitTest))
			return true;
	}

	return CWnd::OnSetCursor(pWnd, nHitTest, message);
}
