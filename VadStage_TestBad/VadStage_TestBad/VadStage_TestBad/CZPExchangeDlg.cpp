﻿// CZPExchangeDlg.cpp: 구현 파일
//

#include "pch.h"
#include "Include.h"
#include "Extern.h"



// CZPExchangeDlg 대화 상자

IMPLEMENT_DYNAMIC(CZPExchangeDlg, CDialogEx)

CZPExchangeDlg::CZPExchangeDlg(CWnd* pParent /*=nullptr*/)
	: CDialogEx(IDD_ZPEXCHANGE_DIALOG, pParent)
	, m_dMoveDistZ(0)
	, m_dVelocityZ(1000)
	, m_dAccelerationZ(1000)
	, m_dVelocityR(5)
	, m_dAccelerationR(1000)
	, m_dMoveDistR(0)
	, m_dVelocityG(5)
	, m_dAccelerationG(1000)
	, m_dMoveDistG(0)
	, m_strPosTypeZ(_T(""))
	, m_strPosTypeR(_T(""))
	, m_strPosTypeG(_T(""))
	, m_nMoveTypeZ(0)
	, m_nMoveTypeR(0)
	, m_nMoveTypeG(0)
	, m_dReleasePos(175)
	, m_dGripPos(20)
	, m_dZaxisTargetPos(-5500)
	, m_dZaxisReleasePos(-15000)
	, m_nStagePosNum(0)
	, m_dZaxisReleaseTargetPos(0)
	, m_ZGZROffSet(3000)
	, m_GripperPos_NoEndStop(155)
	, m_Gripper_Holding_Limit(-200)
{
	m_bStopZaxis = FALSE;
	m_bStopRaxis = FALSE;
	m_bStopGrip  = FALSE;
	m_bRunLongTestStopFlag = FALSE; 
	m_bStageConnected = FALSE;

	for (int nIdx = 0; nIdx < 7; nIdx++)
	{
		m_stCassetPos[nIdx].x_pos = 0.0;
		m_stCassetPos[nIdx].y_pos = 0.0;
		m_stCassetPos[nIdx].zg_pos = 0.0;
		m_stCassetPos[nIdx].zr_pos = 0.0;
	}
}

CZPExchangeDlg::~CZPExchangeDlg()
{
}

void CZPExchangeDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
	DDX_Text(pDX, IDC_EDIT_MOVE_DISTANCE_Z, m_dMoveDistZ);
	DDX_Text(pDX, IDC_EDIT_VELOCITY_Z, m_dVelocityZ);
	DDX_Text(pDX, IDC_EDIT_ACCELERATION_Z, m_dAccelerationZ);
	DDX_Text(pDX, IDC_EDIT_VELOCITY_R, m_dVelocityR);
	DDX_Text(pDX, IDC_EDIT_ACCELERATION_R, m_dAccelerationR);
	DDX_Text(pDX, IDC_EDIT_MOVE_DISTANCE_R, m_dMoveDistR);
	DDX_Text(pDX, IDC_EDIT_VELOCITY_G, m_dVelocityG);
	DDX_Text(pDX, IDC_EDIT_ACCELERATION_G, m_dAccelerationG);
	DDX_Text(pDX, IDC_EDIT_MOVE_DISTANCE_G, m_dMoveDistG);
	DDX_Text(pDX, IDC_STATIC_POSITIONER_TYPE_Z, m_strPosTypeZ);
	DDX_Text(pDX, IDC_STATIC_POSITIONER_TYPE_R, m_strPosTypeR);
	DDX_Text(pDX, IDC_STATIC_POSITIONER_TYPE_G, m_strPosTypeG);
	DDX_Radio(pDX, IDC_RADIO_RELATIVE_MODE_Z, m_nMoveTypeZ);
	DDX_Radio(pDX, IDC_RADIO_RELATIVE_MODE_R, m_nMoveTypeR);
	DDX_Radio(pDX, IDC_RADIO_RELATIVE_MODE_G, m_nMoveTypeG);
	DDX_Control(pDX, IDC_STATIC_CURRENT_POS_Z, m_CurrPositionZ);
	DDX_Control(pDX, IDC_STATIC_CURRENT_POS_R, m_CurrPositionR);
	DDX_Control(pDX, IDC_STATIC_CURRENT_POS_G, m_CurrPositionG);
	DDX_Text(pDX, IDC_EDIT_ZPEX_RELEASE_POS, m_dReleasePos);
	DDX_Text(pDX, IDC_EDIT_ZPEX_GRIP_POS, m_dGripPos);
	DDX_Control(pDX, IDC_STATIC_XPOS_CAS_1, m_xpos_cas_1);
	DDX_Control(pDX, IDC_STATIC_XPOS_CAS_2, m_xpos_cas_2);
	DDX_Control(pDX, IDC_STATIC_XPOS_CAS_3, m_xpos_cas_3);
	DDX_Control(pDX, IDC_STATIC_XPOS_CAS_4, m_xpos_cas_4);
	DDX_Control(pDX, IDC_STATIC_XPOS_CAS_5, m_xpos_cas_5);
	DDX_Control(pDX, IDC_STATIC_XPOS_CAS_6, m_xpos_cas_6);
	DDX_Control(pDX, IDC_STATIC_XPOS_CAS_7, m_xpos_cas_7);
	DDX_Control(pDX, IDC_STATIC_YPOS_CAS_1, m_ypos_cas_1);
	DDX_Control(pDX, IDC_STATIC_YPOS_CAS_2, m_ypos_cas_2);
	DDX_Control(pDX, IDC_STATIC_YPOS_CAS_3, m_ypos_cas_3);
	DDX_Control(pDX, IDC_STATIC_YPOS_CAS_4, m_ypos_cas_4);
	DDX_Control(pDX, IDC_STATIC_YPOS_CAS_5, m_ypos_cas_5);
	DDX_Control(pDX, IDC_STATIC_YPOS_CAS_6, m_ypos_cas_6);
	DDX_Control(pDX, IDC_STATIC_YPOS_CAS_7, m_ypos_cas_7);
	DDX_Text(pDX, IDC_EDIT_TARGET_POS, m_dZaxisTargetPos);
	DDX_Text(pDX, IDC_EDIT_REL_POS, m_dZaxisReleasePos);
	DDX_Radio(pDX, IDC_RADIO_ZPEX_POS1, m_nStagePosNum);
	DDX_Control(pDX, IDC_STATIC_ZPOS_CAS_1, m_zpos_cas_1);
	DDX_Control(pDX, IDC_STATIC_ZPOS_CAS_2, m_zpos_cas_2);
	DDX_Control(pDX, IDC_STATIC_ZPOS_CAS_3, m_zpos_cas_3);
	DDX_Control(pDX, IDC_STATIC_ZPOS_CAS_4, m_zpos_cas_4);
	DDX_Control(pDX, IDC_STATIC_ZPOS_CAS_5, m_zpos_cas_5);
	DDX_Control(pDX, IDC_STATIC_ZPOS_CAS_6, m_zpos_cas_6);
	DDX_Control(pDX, IDC_STATIC_ZPOS_CAS_7, m_zpos_cas_7);
	DDX_Control(pDX, IDC_STATIC_ZPOS_CAS_R_1, m_zrpos_cas_1);
	DDX_Control(pDX, IDC_STATIC_ZPOS_CAS_R_2, m_zrpos_cas_2);
	DDX_Control(pDX, IDC_STATIC_ZPOS_CAS_R_3, m_zrpos_cas_3);
	DDX_Control(pDX, IDC_STATIC_ZPOS_CAS_R_4, m_zrpos_cas_4);
	DDX_Control(pDX, IDC_STATIC_ZPOS_CAS_R_5, m_zrpos_cas_5);
	DDX_Control(pDX, IDC_STATIC_ZPOS_CAS_R_6, m_zrpos_cas_6);
	DDX_Control(pDX, IDC_STATIC_ZPOS_CAS_R_7, m_zrpos_cas_7);
	DDX_Control(pDX, IDC_STATIC_CNT, m_testcnt);
	DDX_Control(pDX, IDC_STATIC_CAS, m_currentcassette);
	DDX_Control(pDX, IDC_STATIC_STATE, m_teststate);
	DDX_Text(pDX, IDC_EDIT_TARGET_POS_R, m_dZaxisReleaseTargetPos);
	DDX_Text(pDX, IDC_EDIT_Z_OFFSET, m_ZGZROffSet);
	DDX_Text(pDX, IDC_EDIT_G_POS_NOENDSTOP, m_GripperPos_NoEndStop);
	DDX_Text(pDX, IDC_EDIT_G_POS_NOENDSTOP2, m_Gripper_Holding_Limit);
}


BEGIN_MESSAGE_MAP(CZPExchangeDlg, CDialogEx)
	ON_WM_DESTROY()
	ON_WM_TIMER()
	ON_BN_CLICKED(IDC_BUTTON_CONNECT, &CZPExchangeDlg::OnBnClickedButtonConnect)
	ON_BN_CLICKED(IDC_BUTTON_DISCONNECT, &CZPExchangeDlg::OnBnClickedButtonDisconnect)
	ON_BN_CLICKED(IDC_BUTTON_CALIBRATE_Z, &CZPExchangeDlg::OnBnClickedButtonCalibrateZ)
	ON_BN_CLICKED(IDC_BUTTON_REFERENCE_Z, &CZPExchangeDlg::OnBnClickedButtonReferenceZ)
	ON_BN_CLICKED(IDC_BUTTON_MOVE_Z, &CZPExchangeDlg::OnBnClickedButtonMoveZ)
	ON_BN_CLICKED(IDC_BUTTON_STOP_Z, &CZPExchangeDlg::OnBnClickedButtonStopZ)
	ON_BN_CLICKED(IDC_BUTTON_CALIBRATE_R, &CZPExchangeDlg::OnBnClickedButtonCalibrateR)
	ON_BN_CLICKED(IDC_BUTTON_REFERENCE_R, &CZPExchangeDlg::OnBnClickedButtonReferenceR)
	ON_BN_CLICKED(IDC_BUTTON_MOVE_R, &CZPExchangeDlg::OnBnClickedButtonMoveR)
	ON_BN_CLICKED(IDC_BUTTON_STOP_R, &CZPExchangeDlg::OnBnClickedButtonStopR)
	ON_BN_CLICKED(IDC_BUTTON_CALIBRATE_G, &CZPExchangeDlg::OnBnClickedButtonCalibrateG)
	ON_BN_CLICKED(IDC_BUTTON_REFERENCE_G, &CZPExchangeDlg::OnBnClickedButtonReferenceG)
	ON_BN_CLICKED(IDC_BUTTON_MOVE_G, &CZPExchangeDlg::OnBnClickedButtonMoveG)
	ON_BN_CLICKED(IDC_BUTTON_STOP_G, &CZPExchangeDlg::OnBnClickedButtonStopG)
	ON_CONTROL_RANGE(BN_CLICKED, IDC_RADIO_RELATIVE_MODE_Z, IDC_RADIO_ABSOLUTE_MODE_Z, CZPExchangeDlg::OnChangeMoveType)
	ON_CONTROL_RANGE(BN_CLICKED, IDC_RADIO_RELATIVE_MODE_R, IDC_RADIO_ABSOLUTE_MODE_R, CZPExchangeDlg::OnChangeMoveType)
	ON_CONTROL_RANGE(BN_CLICKED, IDC_RADIO_RELATIVE_MODE_G, IDC_RADIO_ABSOLUTE_MODE_G, CZPExchangeDlg::OnChangeMoveType)
	ON_BN_CLICKED(IDC_BUTTON_ZPEX_SET_SCALE, &CZPExchangeDlg::OnBnClickedButtonZpexSetScale)
	ON_BN_CLICKED(IDC_BUTTON_ZPEX_RELEASE, &CZPExchangeDlg::OnBnClickedButtonZpexRelease)
	ON_BN_CLICKED(IDC_BUTTON_ZPEX_GRIP, &CZPExchangeDlg::OnBnClickedButtonZpexGrip)
	ON_BN_CLICKED(IDC_CAS_MOVE, &CZPExchangeDlg::OnBnClickedCasMove)
	ON_BN_CLICKED(IDC_CAS_POS_SET, &CZPExchangeDlg::OnBnClickedCasPosSet)
	ON_BN_CLICKED(IDC_BUTTON_TARGET_POS, &CZPExchangeDlg::OnBnClickedButtonTargetPos)
	ON_BN_CLICKED(IDC_BUTTON_TARGET_POS_SET, &CZPExchangeDlg::OnBnClickedButtonTargetPosSet)
	ON_BN_CLICKED(IDC_BUTTON_REL_POS_SET, &CZPExchangeDlg::OnBnClickedButtonRelPosSet)
	ON_BN_CLICKED(IDC_BUTTON_ZPEX_GRIP_SET, &CZPExchangeDlg::OnBnClickedButtonZpexGripSet)
	ON_BN_CLICKED(IDC_BUTTON_ZPEX_RELEASE_SET, &CZPExchangeDlg::OnBnClickedButtonZpexReleaseSet)
	ON_BN_CLICKED(IDC_BUTTON_REL_POS, &CZPExchangeDlg::OnBnClickedButtonRelPos)
	ON_BN_CLICKED(IDC_CAS_GRIP, &CZPExchangeDlg::OnBnClickedCasGrip)
	ON_BN_CLICKED(IDC_CAS_REL, &CZPExchangeDlg::OnBnClickedCasRel)
	ON_BN_CLICKED(IDC_CAS_STOP, &CZPExchangeDlg::OnBnClickedCasStop)
	ON_CONTROL_RANGE(BN_CLICKED, IDC_RADIO_ZPEX_POS1, IDC_RADIO_ZPEX_POS7, CZPExchangeDlg::OnChangeStagePos)
	ON_BN_CLICKED(IDC_CAS_ZPOS_SET, &CZPExchangeDlg::OnBnClickedCasZposSet)
	ON_BN_CLICKED(IDC_CAS_LONGRUN, &CZPExchangeDlg::OnBnClickedCasLongrun)
	ON_BN_CLICKED(IDC_CAS_LONGRUN_TEST_STOP, &CZPExchangeDlg::OnBnClickedCasLongrunTestStop)
	ON_BN_CLICKED(IDC_BUTTON_MOVE2, &CZPExchangeDlg::OnBnClickedButtonMove2)
	ON_BN_CLICKED(IDC_BUTTON_ZPM_CANCEL_BUFFER, &CZPExchangeDlg::OnBnClickedButtonZpmCancelBuffer)
	ON_BN_CLICKED(IDC_CAS_SOLO_LONGRUN, &CZPExchangeDlg::OnBnClickedCasSoloLongrun)
	ON_BN_CLICKED(IDC_BUTTON_TARGET_POS_R, &CZPExchangeDlg::OnBnClickedButtonTargetPosR)
	ON_BN_CLICKED(IDC_BUTTON_TARGET_POS_SET_R, &CZPExchangeDlg::OnBnClickedButtonTargetPosSetR)
	ON_BN_CLICKED(IDC_CAS_HOME, &CZPExchangeDlg::OnBnClickedCasHome)
END_MESSAGE_MAP()


// CZPExchangeDlg 메시지 처리기




BOOL CZPExchangeDlg::OnInitDialog()
{
	__super::OnInitDialog();

	SetLogPath(LOG_PATH);

	m_LedIcon[0] = (HICON)::LoadImage(::AfxGetInstanceHandle(), MAKEINTRESOURCE(IDI_LEDOFF), IMAGE_ICON, 32, 32, LR_DEFAULTCOLOR);
	m_LedIcon[1] = (HICON)::LoadImage(::AfxGetInstanceHandle(), MAKEINTRESOURCE(IDI_LEDONGREEN), IMAGE_ICON, 32, 32, LR_DEFAULTCOLOR);
	m_LedIcon[2] = (HICON)::LoadImage(::AfxGetInstanceHandle(), MAKEINTRESOURCE(IDI_LEDONRED), IMAGE_ICON, 32, 32, LR_DEFAULTCOLOR);

	((CStatic*)GetDlgItem(IDC_ICON_CAS_1))->SetIcon(m_LedIcon[0]);
	((CStatic*)GetDlgItem(IDC_ICON_CAS_2))->SetIcon(m_LedIcon[0]);
	((CStatic*)GetDlgItem(IDC_ICON_CAS_3))->SetIcon(m_LedIcon[0]);
	((CStatic*)GetDlgItem(IDC_ICON_CAS_4))->SetIcon(m_LedIcon[0]);
	((CStatic*)GetDlgItem(IDC_ICON_CAS_5))->SetIcon(m_LedIcon[0]);
	((CStatic*)GetDlgItem(IDC_ICON_CAS_6))->SetIcon(m_LedIcon[0]);
	((CStatic*)GetDlgItem(IDC_ICON_CAS_7))->SetIcon(m_LedIcon[0]);

	((CStatic*)GetDlgItem(IDC_ICON_ZPEX_LONGRUN))->SetIcon(m_LedIcon[0]);
	((CStatic*)GetDlgItem(IDC_ICON_ZPEX_CONNECT))->SetIcon(m_LedIcon[0]);
	((CStatic*)GetDlgItem(IDC_ICON_AMPLIFIER_ENABLED_Z))->SetIcon(m_LedIcon[0]);
	((CStatic*)GetDlgItem(IDC_ICON_CLOSED_LOOP_ACTIVE_Z))->SetIcon(m_LedIcon[0]);
	((CStatic*)GetDlgItem(IDC_ICON_ACTIVERY_MOVING_Z))->SetIcon(m_LedIcon[0]);
	((CStatic*)GetDlgItem(IDC_ICON_SENSOR_PRESENT_Z))->SetIcon(m_LedIcon[0]);
	((CStatic*)GetDlgItem(IDC_ICON_CALIBRATE_Z))->SetIcon(m_LedIcon[0]);
	((CStatic*)GetDlgItem(IDC_ICON_REFERENCED_Z))->SetIcon(m_LedIcon[0]);
	((CStatic*)GetDlgItem(IDC_ICON_ENDSTOP_REACHED_Z))->SetIcon(m_LedIcon[0]);
	((CStatic*)GetDlgItem(IDC_ICON_RANGE_LIMIT_REACHED_Z))->SetIcon(m_LedIcon[0]);
	((CStatic*)GetDlgItem(IDC_ICON_FOLLOWING_ERROR_LIMIT_REACHED_Z))->SetIcon(m_LedIcon[0]);
	((CStatic*)GetDlgItem(IDC_ICON_AMPLIFIER_ENABLED_R))->SetIcon(m_LedIcon[0]);
	((CStatic*)GetDlgItem(IDC_ICON_CLOSED_LOOP_ACTIVE_R))->SetIcon(m_LedIcon[0]);
	((CStatic*)GetDlgItem(IDC_ICON_ACTIVERY_MOVING_R))->SetIcon(m_LedIcon[0]);
	((CStatic*)GetDlgItem(IDC_ICON_SENSOR_PRESENT_R))->SetIcon(m_LedIcon[0]);
	((CStatic*)GetDlgItem(IDC_ICON_CALIBRATE_R))->SetIcon(m_LedIcon[0]);
	((CStatic*)GetDlgItem(IDC_ICON_REFERENCED_R))->SetIcon(m_LedIcon[0]);
	((CStatic*)GetDlgItem(IDC_ICON_ENDSTOP_REACHED_R))->SetIcon(m_LedIcon[0]);
	((CStatic*)GetDlgItem(IDC_ICON_RANGE_LIMIT_REACHED_R))->SetIcon(m_LedIcon[0]);
	((CStatic*)GetDlgItem(IDC_ICON_FOLLOWING_ERROR_LIMIT_REACHED_R))->SetIcon(m_LedIcon[0]);
	((CStatic*)GetDlgItem(IDC_ICON_AMPLIFIER_ENABLED_G))->SetIcon(m_LedIcon[0]);
	((CStatic*)GetDlgItem(IDC_ICON_CLOSED_LOOP_ACTIVE_G))->SetIcon(m_LedIcon[0]);
	((CStatic*)GetDlgItem(IDC_ICON_ACTIVERY_MOVING_G))->SetIcon(m_LedIcon[0]);
	((CStatic*)GetDlgItem(IDC_ICON_SENSOR_PRESENT_G))->SetIcon(m_LedIcon[0]);
	((CStatic*)GetDlgItem(IDC_ICON_CALIBRATE_G))->SetIcon(m_LedIcon[0]);
	((CStatic*)GetDlgItem(IDC_ICON_REFERENCED_G))->SetIcon(m_LedIcon[0]);
	((CStatic*)GetDlgItem(IDC_ICON_ENDSTOP_REACHED_G))->SetIcon(m_LedIcon[0]);
	((CStatic*)GetDlgItem(IDC_ICON_RANGE_LIMIT_REACHED_G))->SetIcon(m_LedIcon[0]);
	((CStatic*)GetDlgItem(IDC_ICON_FOLLOWING_ERROR_LIMIT_REACHED_G))->SetIcon(m_LedIcon[0]);
	((CStatic*)GetDlgItem(IDC_ICON_ZPEX_GRIPPER_BREAK))->SetIcon(m_LedIcon[0]);
	


	LoadTeachingPos();

	OnReadCasettePos();


	GRIPPER_GRIP_NOENDSTOP_POS = 155;
	GRIPHOLDLIMINTVALUE = -300;

	m_GripperPos_NoEndStop = GRIPPER_GRIP_NOENDSTOP_POS;
	m_Gripper_Holding_Limit = GRIPHOLDLIMINTVALUE;
	return TRUE;  // return TRUE unless you set the focus to a control
				  // 예외: OCX 속성 페이지는 FALSE를 반환해야 합니다.
}


void CZPExchangeDlg::OnDestroy()
{
	__super::OnDestroy();

	// TODO: 여기에 메시지 처리기 코드를 추가합니다.
}



void CZPExchangeDlg::OnTimer(UINT_PTR nIDEvent)
{
	KillTimer(nIDEvent);
	switch (nIDEvent)
	{
	case ZPEXCHANGE_UPDATE_TIMER:
		GetDeviceStatus();
		SetTimer(nIDEvent, 100, NULL);
		break;
	default:
		break;
	}

	__super::OnTimer(nIDEvent);
}

void CZPExchangeDlg::OnChangeMoveType(UINT value)
{
	UpdateData(TRUE);
}

void CZPExchangeDlg::OnChangeStagePos(UINT value)
{
	UpdateData(TRUE);
}

void CZPExchangeDlg::GetDeviceStatus()
{
	double x_pos;
	double y_pos;
	
	double x_pos_margin;
	double y_pos_margin;

	int nRet;
	
	if (m_bConnected == TRUE)
	{
		((CStatic*)GetDlgItem(IDC_ICON_ZPEX_CONNECT))->SetIcon(m_LedIcon[1]);
		g_pDetecterApertureChange->m_detetor_aperture_icon_con.SetIcon(m_LedIcon[1]);
	}
	else
	{
		((CStatic*)GetDlgItem(IDC_ICON_ZPEX_CONNECT))->SetIcon(m_LedIcon[0]);
		g_pDetecterApertureChange->m_detetor_aperture_icon_con.SetIcon(m_LedIcon[0]);
	}

	if (m_bConnected == FALSE) return;

	if (!GetPos(Z_AXIS))
	{
		CString strTemp;
		m_dZpos = m_dCurrentPos;
		strTemp.Format("%f", m_dCurrentPos);
		m_CurrPositionZ.SetWindowText(strTemp);

		State(Z_AXIS);
		if (m_bAmplifierEnabled[Z_AXIS] == TRUE)
			((CStatic*)GetDlgItem(IDC_ICON_AMPLIFIER_ENABLED_Z))->SetIcon(m_LedIcon[1]);
		else
			((CStatic*)GetDlgItem(IDC_ICON_AMPLIFIER_ENABLED_Z))->SetIcon(m_LedIcon[0]);

		if (m_bClosedLoopActive[Z_AXIS] == TRUE)
			((CStatic*)GetDlgItem(IDC_ICON_CLOSED_LOOP_ACTIVE_Z))->SetIcon(m_LedIcon[1]);
		else
			((CStatic*)GetDlgItem(IDC_ICON_CLOSED_LOOP_ACTIVE_Z))->SetIcon(m_LedIcon[0]);

		if (m_bActivelyMoving[Z_AXIS] == TRUE)
			((CStatic*)GetDlgItem(IDC_ICON_ACTIVERY_MOVING_Z))->SetIcon(m_LedIcon[1]);
		else
			((CStatic*)GetDlgItem(IDC_ICON_ACTIVERY_MOVING_Z))->SetIcon(m_LedIcon[0]);

		if (m_bIsCalibrated[Z_AXIS] == TRUE)
			((CStatic*)GetDlgItem(IDC_ICON_CALIBRATE_Z))->SetIcon(m_LedIcon[1]);
		else
			((CStatic*)GetDlgItem(IDC_ICON_CALIBRATE_Z))->SetIcon(m_LedIcon[0]);

		if (m_bIsReferenced[Z_AXIS] == TRUE)
			((CStatic*)GetDlgItem(IDC_ICON_REFERENCED_Z))->SetIcon(m_LedIcon[1]);
		else
			((CStatic*)GetDlgItem(IDC_ICON_REFERENCED_Z))->SetIcon(m_LedIcon[0]);

		if (m_bEndstopReached[Z_AXIS] == TRUE)
			((CStatic*)GetDlgItem(IDC_ICON_ENDSTOP_REACHED_Z))->SetIcon(m_LedIcon[2]);
		else
			((CStatic*)GetDlgItem(IDC_ICON_ENDSTOP_REACHED_Z))->SetIcon(m_LedIcon[0]);

		if (m_bSensorPresent[Z_AXIS] == TRUE)
			((CStatic*)GetDlgItem(IDC_ICON_SENSOR_PRESENT_Z))->SetIcon(m_LedIcon[1]);
		else
			((CStatic*)GetDlgItem(IDC_ICON_SENSOR_PRESENT_Z))->SetIcon(m_LedIcon[0]);

		if(m_bRangeLimitReached[Z_AXIS] == TRUE)
			((CStatic*)GetDlgItem(IDC_ICON_RANGE_LIMIT_REACHED_Z))->SetIcon(m_LedIcon[2]);
		else
			((CStatic*)GetDlgItem(IDC_ICON_RANGE_LIMIT_REACHED_Z))->SetIcon(m_LedIcon[0]);

		if(m_bFollowingErrorReached[Z_AXIS] == TRUE)
			((CStatic*)GetDlgItem(IDC_ICON_FOLLOWING_ERROR_LIMIT_REACHED_Z))->SetIcon(m_LedIcon[2]);
		else
			((CStatic*)GetDlgItem(IDC_ICON_FOLLOWING_ERROR_LIMIT_REACHED_Z))->SetIcon(m_LedIcon[0]);
	}

	if (!GetPos(R_AXIS))
	{
		CString strTemp;
		strTemp.Format("%f", m_dCurrentPos);
		m_CurrPositionR.SetWindowText(strTemp);

		State(R_AXIS);
		if (m_bAmplifierEnabled[R_AXIS] == TRUE)
			((CStatic*)GetDlgItem(IDC_ICON_AMPLIFIER_ENABLED_R))->SetIcon(m_LedIcon[1]);
		else
			((CStatic*)GetDlgItem(IDC_ICON_AMPLIFIER_ENABLED_R))->SetIcon(m_LedIcon[0]);

		if (m_bClosedLoopActive[R_AXIS] == TRUE)
			((CStatic*)GetDlgItem(IDC_ICON_CLOSED_LOOP_ACTIVE_R))->SetIcon(m_LedIcon[1]);
		else
			((CStatic*)GetDlgItem(IDC_ICON_CLOSED_LOOP_ACTIVE_R))->SetIcon(m_LedIcon[0]);

		if (m_bActivelyMoving[R_AXIS] == TRUE)
			((CStatic*)GetDlgItem(IDC_ICON_ACTIVERY_MOVING_R))->SetIcon(m_LedIcon[1]);
		else
			((CStatic*)GetDlgItem(IDC_ICON_ACTIVERY_MOVING_R))->SetIcon(m_LedIcon[0]);

		if (m_bIsCalibrated[R_AXIS] == TRUE)
			((CStatic*)GetDlgItem(IDC_ICON_CALIBRATE_R))->SetIcon(m_LedIcon[1]);
		else
			((CStatic*)GetDlgItem(IDC_ICON_CALIBRATE_R))->SetIcon(m_LedIcon[0]);

		if (m_bIsReferenced[R_AXIS] == TRUE)
			((CStatic*)GetDlgItem(IDC_ICON_REFERENCED_R))->SetIcon(m_LedIcon[1]);
		else
			((CStatic*)GetDlgItem(IDC_ICON_REFERENCED_R))->SetIcon(m_LedIcon[0]);

		if (m_bEndstopReached[R_AXIS] == TRUE)
			((CStatic*)GetDlgItem(IDC_ICON_ENDSTOP_REACHED_R))->SetIcon(m_LedIcon[2]);
		else
			((CStatic*)GetDlgItem(IDC_ICON_ENDSTOP_REACHED_R))->SetIcon(m_LedIcon[0]);

		if (m_bSensorPresent[R_AXIS] == TRUE)
			((CStatic*)GetDlgItem(IDC_ICON_SENSOR_PRESENT_R))->SetIcon(m_LedIcon[1]);
		else
			((CStatic*)GetDlgItem(IDC_ICON_SENSOR_PRESENT_R))->SetIcon(m_LedIcon[0]);

		if (m_bRangeLimitReached[R_AXIS] == TRUE)
			((CStatic*)GetDlgItem(IDC_ICON_RANGE_LIMIT_REACHED_R))->SetIcon(m_LedIcon[2]);
		else
			((CStatic*)GetDlgItem(IDC_ICON_RANGE_LIMIT_REACHED_R))->SetIcon(m_LedIcon[0]);

		if (m_bFollowingErrorReached[R_AXIS] == TRUE)
			((CStatic*)GetDlgItem(IDC_ICON_FOLLOWING_ERROR_LIMIT_REACHED_R))->SetIcon(m_LedIcon[2]);
		else
			((CStatic*)GetDlgItem(IDC_ICON_FOLLOWING_ERROR_LIMIT_REACHED_R))->SetIcon(m_LedIcon[0]);
	}

	if (!GetPos(G_AXIS))
	{
		CString strTemp;
		m_dGpos = m_dCurrentPos;
		strTemp.Format("%f", m_dCurrentPos);
		m_CurrPositionG.SetWindowText(strTemp);

		State(G_AXIS);
		if (m_bAmplifierEnabled[G_AXIS] == TRUE)
			((CStatic*)GetDlgItem(IDC_ICON_AMPLIFIER_ENABLED_G))->SetIcon(m_LedIcon[1]);
		else
			((CStatic*)GetDlgItem(IDC_ICON_AMPLIFIER_ENABLED_G))->SetIcon(m_LedIcon[0]);

		if (m_bClosedLoopActive[G_AXIS] == TRUE)
			((CStatic*)GetDlgItem(IDC_ICON_CLOSED_LOOP_ACTIVE_G))->SetIcon(m_LedIcon[1]);
		else
			((CStatic*)GetDlgItem(IDC_ICON_CLOSED_LOOP_ACTIVE_G))->SetIcon(m_LedIcon[0]);

		if (m_bActivelyMoving[G_AXIS] == TRUE)
			((CStatic*)GetDlgItem(IDC_ICON_ACTIVERY_MOVING_G))->SetIcon(m_LedIcon[1]);
		else
			((CStatic*)GetDlgItem(IDC_ICON_ACTIVERY_MOVING_G))->SetIcon(m_LedIcon[0]);

		if (m_bIsCalibrated[G_AXIS] == TRUE)
			((CStatic*)GetDlgItem(IDC_ICON_CALIBRATE_G))->SetIcon(m_LedIcon[1]);
		else
			((CStatic*)GetDlgItem(IDC_ICON_CALIBRATE_G))->SetIcon(m_LedIcon[0]);

		if (m_bIsReferenced[G_AXIS] == TRUE)
			((CStatic*)GetDlgItem(IDC_ICON_REFERENCED_G))->SetIcon(m_LedIcon[1]);
		else
			((CStatic*)GetDlgItem(IDC_ICON_REFERENCED_G))->SetIcon(m_LedIcon[0]);

		if (m_bEndstopReached[G_AXIS] == TRUE)
			((CStatic*)GetDlgItem(IDC_ICON_ENDSTOP_REACHED_G))->SetIcon(m_LedIcon[2]);
		else
			((CStatic*)GetDlgItem(IDC_ICON_ENDSTOP_REACHED_G))->SetIcon(m_LedIcon[0]);

		if (m_bSensorPresent[G_AXIS] == TRUE)
			((CStatic*)GetDlgItem(IDC_ICON_SENSOR_PRESENT_G))->SetIcon(m_LedIcon[1]);
		else
			((CStatic*)GetDlgItem(IDC_ICON_SENSOR_PRESENT_G))->SetIcon(m_LedIcon[0]);

		if (m_bRangeLimitReached[G_AXIS] == TRUE)
			((CStatic*)GetDlgItem(IDC_ICON_RANGE_LIMIT_REACHED_G))->SetIcon(m_LedIcon[2]);
		else
			((CStatic*)GetDlgItem(IDC_ICON_RANGE_LIMIT_REACHED_G))->SetIcon(m_LedIcon[0]);

		if (m_bFollowingErrorReached[G_AXIS] == TRUE)
			((CStatic*)GetDlgItem(IDC_ICON_FOLLOWING_ERROR_LIMIT_REACHED_G))->SetIcon(m_LedIcon[2]);
		else
			((CStatic*)GetDlgItem(IDC_ICON_FOLLOWING_ERROR_LIMIT_REACHED_G))->SetIcon(m_LedIcon[0]);
	}

	if (!GetPos(DETECTER_AXIS))
	{
		CString strTemp;
		CString strTemp1;
		m_dDpos = m_dCurrentPos;
		strTemp.Format("%f", m_dCurrentPos);
		g_pDetecterApertureChange->m_dCurrentPosDetecter.SetWindowText(strTemp);
		
		if (g_pDetecterApertureChange->m_bDecteterTestFlag)
		{
			g_pDetecterApertureChange->m_detect_current_text.SetWindowText(strTemp);
			strTemp1.Format("%f", g_pDetecterApertureChange->d_pos_test_target);
			SaveLogFile("Aperture&Detector_pos_report", _T((LPSTR)(LPCTSTR)("D_Targetpos : " + strTemp + " D_Currentpos : " + strTemp1)));

		}

		State(DETECTER_AXIS);
		if (m_bAmplifierEnabled[DETECTER_AXIS] == TRUE)
			g_pDetecterApertureChange->m_detector_amp_enable.SetIcon(m_LedIcon[1]);
			//((CStatic*)GetDlgItem(IDC_ICON_DETECTER_AMPLIFIER_ENABLED))->SetIcon(m_LedIcon[1]);
		else
			g_pDetecterApertureChange->m_detector_amp_enable.SetIcon(m_LedIcon[0]);
			//((CStatic*)GetDlgItem(IDC_ICON_DETECTER_AMPLIFIER_ENABLED))->SetIcon(m_LedIcon[0]);
		
		if (m_bClosedLoopActive[DETECTER_AXIS] == TRUE)
			g_pDetecterApertureChange->m_detector_closeloop.SetIcon(m_LedIcon[1]);
			//((CStatic*)GetDlgItem(IDC_ICON_DETECTER_CLOSED_LOOP_ACTIVE))->SetIcon(m_LedIcon[1]);
		else
			g_pDetecterApertureChange->m_detector_closeloop.SetIcon(m_LedIcon[0]);
			//((CStatic*)GetDlgItem(IDC_ICON_DETECTER_CLOSED_LOOP_ACTIVE))->SetIcon(m_LedIcon[0]);

		if (m_bActivelyMoving[DETECTER_AXIS] == TRUE)
			g_pDetecterApertureChange->m_detector_activemove.SetIcon(m_LedIcon[1]);
			//((CStatic*)GetDlgItem(IDC_ICON_DETECTER_ACTIVERY_MOVING))->SetIcon(m_LedIcon[1]);
		else
			g_pDetecterApertureChange->m_detector_activemove.SetIcon(m_LedIcon[0]);
			//((CStatic*)GetDlgItem(IDC_ICON_DETECTER_ACTIVERY_MOVING))->SetIcon(m_LedIcon[0]);
		
		if (m_bIsCalibrated[DETECTER_AXIS] == TRUE)
			g_pDetecterApertureChange->m_detector_calibrated.SetIcon(m_LedIcon[1]);
			//((CStatic*)GetDlgItem(IDC_ICON_DETECTER_CALIBRATE))->SetIcon(m_LedIcon[1]);
		else
			g_pDetecterApertureChange->m_detector_calibrated.SetIcon(m_LedIcon[0]);
			//((CStatic*)GetDlgItem(IDC_ICON_DETECTER_CALIBRATE))->SetIcon(m_LedIcon[0]);
		
		if (m_bIsReferenced[DETECTER_AXIS] == TRUE)
			g_pDetecterApertureChange->m_detector_referenced.SetIcon(m_LedIcon[1]);
			//((CStatic*)GetDlgItem(IDC_ICON_DETECTER_REFERENCED))->SetIcon(m_LedIcon[1]);
		else
			g_pDetecterApertureChange->m_detector_referenced.SetIcon(m_LedIcon[0]);
			//((CStatic*)GetDlgItem(IDC_ICON_DETECTER_REFERENCED))->SetIcon(m_LedIcon[0]);
		
		if (m_bEndstopReached[DETECTER_AXIS] == TRUE)
			g_pDetecterApertureChange->m_detector_endstop.SetIcon(m_LedIcon[2]);
			//((CStatic*)GetDlgItem(IDC_ICON_DETECTER_ENDSTOP_REACHED))->SetIcon(m_LedIcon[2]);
		else
			g_pDetecterApertureChange->m_detector_endstop.SetIcon(m_LedIcon[0]);
			//((CStatic*)GetDlgItem(IDC_ICON_DETECTER_ENDSTOP_REACHED))->SetIcon(m_LedIcon[0]);

		if (m_bSensorPresent[DETECTER_AXIS] == TRUE)
			g_pDetecterApertureChange->m_detector_sensor.SetIcon(m_LedIcon[1]);
			//((CStatic*)GetDlgItem(IDC_ICON_DETECTER_SENSOR_PRESENT))->SetIcon(m_LedIcon[1]);
		else
			g_pDetecterApertureChange->m_detector_sensor.SetIcon(m_LedIcon[0]);
			//((CStatic*)GetDlgItem(IDC_ICON_DETECTER_SENSOR_PRESENT))->SetIcon(m_LedIcon[0]);

		if (m_bRangeLimitReached[DETECTER_AXIS] == TRUE)
			g_pDetecterApertureChange->m_detector_rangelimit.SetIcon(m_LedIcon[2]);
			//((CStatic*)GetDlgItem(IDC_ICON_DETECTER_RANGE_LIMIT_REACHED))->SetIcon(m_LedIcon[2]);
		else
			g_pDetecterApertureChange->m_detector_rangelimit.SetIcon(m_LedIcon[0]);
			//((CStatic*)GetDlgItem(IDC_ICON_DETECTER_RANGE_LIMIT_REACHED))->SetIcon(m_LedIcon[0]);

		if (m_bFollowingErrorReached[DETECTER_AXIS] == TRUE)
			g_pDetecterApertureChange->m_detector_followingerror.SetIcon(m_LedIcon[2]);
			//((CStatic*)GetDlgItem(IDC_ICON_DETECTER_FOLLOWING_ERROR_LIMIT_REACHED))->SetIcon(m_LedIcon[2]);
		else
			g_pDetecterApertureChange->m_detector_followingerror.SetIcon(m_LedIcon[0]);
			//((CStatic*)GetDlgItem(IDC_ICON_DETECTER_FOLLOWING_ERROR_LIMIT_REACHED))->SetIcon(m_LedIcon[0]);
	}

	if (!GetPos(APERTURE_AXIS))
	{
		CString strTemp;
		m_dApos = m_dCurrentPos;
		strTemp.Format("%f", m_dCurrentPos);
		g_pDetecterApertureChange->m_dCurrentPosAperture.SetWindowText(strTemp);

		State(APERTURE_AXIS);
		if (m_bAmplifierEnabled[APERTURE_AXIS] == TRUE)
			g_pDetecterApertureChange->m_aperture_ampenable.SetIcon(m_LedIcon[1]);
			//((CStatic*)GetDlgItem(IDC_ICON_APERTURE_AMPLIFIER_ENABLED))->SetIcon(m_LedIcon[1]);
		else
			g_pDetecterApertureChange->m_aperture_ampenable.SetIcon(m_LedIcon[0]);
			//((CStatic*)GetDlgItem(IDC_ICON_APERTURE_AMPLIFIER_ENABLED))->SetIcon(m_LedIcon[0]);

		if (m_bClosedLoopActive[APERTURE_AXIS] == TRUE)
			g_pDetecterApertureChange->m_aperture_closeloop.SetIcon(m_LedIcon[1]);
			//((CStatic*)GetDlgItem(IDC_ICON_APERTURE_CLOSED_LOOP_ACTIVE))->SetIcon(m_LedIcon[1]);
		else
			g_pDetecterApertureChange->m_aperture_closeloop.SetIcon(m_LedIcon[0]);
			//((CStatic*)GetDlgItem(IDC_ICON_APERTURE_CLOSED_LOOP_ACTIVE))->SetIcon(m_LedIcon[0]);

		if (m_bActivelyMoving[APERTURE_AXIS] == TRUE)
			g_pDetecterApertureChange->m_aperture_active.SetIcon(m_LedIcon[1]);
			//((CStatic*)GetDlgItem(IDC_ICON_APERTURE_ACTIVERY_MOVING))->SetIcon(m_LedIcon[1]);
		else
			g_pDetecterApertureChange->m_aperture_active.SetIcon(m_LedIcon[0]);
			//((CStatic*)GetDlgItem(IDC_ICON_APERTURE_ACTIVERY_MOVING))->SetIcon(m_LedIcon[0]);

		if (m_bIsCalibrated[APERTURE_AXIS] == TRUE)
			g_pDetecterApertureChange->m_aperture_calibrated.SetIcon(m_LedIcon[1]);
			//((CStatic*)GetDlgItem(IDC_ICON_APERTURE_CALIBRATE))->SetIcon(m_LedIcon[1]);
		else
			g_pDetecterApertureChange->m_aperture_calibrated.SetIcon(m_LedIcon[0]);
			//((CStatic*)GetDlgItem(IDC_ICON_APERTURE_CALIBRATE))->SetIcon(m_LedIcon[0]);

		if (m_bIsReferenced[APERTURE_AXIS] == TRUE)
			g_pDetecterApertureChange->m_aperture_referenced.SetIcon(m_LedIcon[1]);
			//((CStatic*)GetDlgItem(IDC_ICON_APERTURE_REFERENCED))->SetIcon(m_LedIcon[1]);
		else
			g_pDetecterApertureChange->m_aperture_referenced.SetIcon(m_LedIcon[0]);
			//((CStatic*)GetDlgItem(IDC_ICON_APERTURE_REFERENCED))->SetIcon(m_LedIcon[0]);

		if (m_bEndstopReached[APERTURE_AXIS] == TRUE)
			g_pDetecterApertureChange->m_aperture_endstop.SetIcon(m_LedIcon[2]);
			//((CStatic*)GetDlgItem(IDC_ICON_APERTURE_ENDSTOP_REACHED))->SetIcon(m_LedIcon[2]);
		else
			g_pDetecterApertureChange->m_aperture_endstop.SetIcon(m_LedIcon[0]);
			//((CStatic*)GetDlgItem(IDC_ICON_APERTURE_ENDSTOP_REACHED))->SetIcon(m_LedIcon[0]);

		if (m_bSensorPresent[APERTURE_AXIS] == TRUE)
			g_pDetecterApertureChange->m_aperture_sensor.SetIcon(m_LedIcon[1]);
			//((CStatic*)GetDlgItem(IDC_ICON_APERTURE_SENSOR_PRESENT))->SetIcon(m_LedIcon[1]);
		else
			g_pDetecterApertureChange->m_aperture_sensor.SetIcon(m_LedIcon[0]);
			//((CStatic*)GetDlgItem(IDC_ICON_APERTURE_SENSOR_PRESENT))->SetIcon(m_LedIcon[0]);

		if (m_bRangeLimitReached[APERTURE_AXIS] == TRUE)
			g_pDetecterApertureChange->m_aperture_rangelimit.SetIcon(m_LedIcon[2]);
			//((CStatic*)GetDlgItem(IDC_ICON_APERTURE_RANGE_LIMIT_REACHED))->SetIcon(m_LedIcon[2]);
		else
			g_pDetecterApertureChange->m_aperture_rangelimit.SetIcon(m_LedIcon[0]);
			//((CStatic*)GetDlgItem(IDC_ICON_APERTURE_RANGE_LIMIT_REACHED))->SetIcon(m_LedIcon[0]);

		if (m_bFollowingErrorReached[APERTURE_AXIS] == TRUE)
			g_pDetecterApertureChange->m_aperture_followingerror.SetIcon(m_LedIcon[2]);
			//((CStatic*)GetDlgItem(IDC_ICON_APERTURE_FOLLOWING_ERROR_LIMIT_REACHED))->SetIcon(m_LedIcon[2]);
		else
			g_pDetecterApertureChange->m_aperture_followingerror.SetIcon(m_LedIcon[0]);
			//((CStatic*)GetDlgItem(IDC_ICON_APERTURE_FOLLOWING_ERROR_LIMIT_REACHED))->SetIcon(m_LedIcon[0]);
	}

	x_pos_margin = 0.1;
	y_pos_margin = 0.1;
	
	x_pos = g_pNavigationStage->GetPosmm(STAGE_ACS_X_AXIS);
	y_pos = g_pNavigationStage->GetPosmm(STAGE_ACS_Y_AXIS);

	if ((((m_stCassetPos[0].x_pos + x_pos_margin) > x_pos) && ((m_stCassetPos[0].x_pos - x_pos_margin) < x_pos)) &&
		(((m_stCassetPos[0].y_pos + y_pos_margin) > y_pos) && ((m_stCassetPos[0].y_pos - y_pos_margin) < y_pos)))
	{
		((CStatic*)GetDlgItem(IDC_ICON_CAS_1))->SetIcon(m_LedIcon[1]);
	}
	else
	{

		((CStatic*)GetDlgItem(IDC_ICON_CAS_1))->SetIcon(m_LedIcon[0]);
	}

	if ((((m_stCassetPos[1].x_pos + x_pos_margin) > x_pos) && ((m_stCassetPos[1].x_pos - x_pos_margin) < x_pos)) &&
		(((m_stCassetPos[1].y_pos + y_pos_margin) > y_pos) && ((m_stCassetPos[1].y_pos - y_pos_margin) < y_pos)))
	{
		((CStatic*)GetDlgItem(IDC_ICON_CAS_2))->SetIcon(m_LedIcon[1]);
	}
	else
	{
		((CStatic*)GetDlgItem(IDC_ICON_CAS_2))->SetIcon(m_LedIcon[0]);
	}

	if ((((m_stCassetPos[2].x_pos + x_pos_margin) > x_pos) && ((m_stCassetPos[2].x_pos - x_pos_margin) < x_pos)) &&
		(((m_stCassetPos[2].y_pos + y_pos_margin) > y_pos) && ((m_stCassetPos[2].y_pos - y_pos_margin) < y_pos)))
	{
		((CStatic*)GetDlgItem(IDC_ICON_CAS_3))->SetIcon(m_LedIcon[1]);
	}
	else
	{
		((CStatic*)GetDlgItem(IDC_ICON_CAS_3))->SetIcon(m_LedIcon[0]);
	}


	if ((((m_stCassetPos[3].x_pos + x_pos_margin) > x_pos) && ((m_stCassetPos[3].x_pos - x_pos_margin) < x_pos)) &&
		(((m_stCassetPos[3].y_pos + y_pos_margin) > y_pos) && ((m_stCassetPos[3].y_pos - y_pos_margin) < y_pos)))
	{
		((CStatic*)GetDlgItem(IDC_ICON_CAS_4))->SetIcon(m_LedIcon[1]);
	}
	else
	{
		((CStatic*)GetDlgItem(IDC_ICON_CAS_4))->SetIcon(m_LedIcon[0]);
	}

	if ((((m_stCassetPos[4].x_pos + x_pos_margin) > x_pos) && ((m_stCassetPos[4].x_pos - x_pos_margin) < x_pos)) &&
		(((m_stCassetPos[4].y_pos + y_pos_margin) > y_pos) && ((m_stCassetPos[4].y_pos - y_pos_margin) < y_pos)))
	{
		((CStatic*)GetDlgItem(IDC_ICON_CAS_5))->SetIcon(m_LedIcon[1]);
	}
	else
	{
		((CStatic*)GetDlgItem(IDC_ICON_CAS_5))->SetIcon(m_LedIcon[0]);
	}


	if ((((m_stCassetPos[5].x_pos + x_pos_margin) > x_pos) && ((m_stCassetPos[5].x_pos - x_pos_margin) < x_pos)) &&
		(((m_stCassetPos[5].y_pos + y_pos_margin) > y_pos) && ((m_stCassetPos[5].y_pos - y_pos_margin) < y_pos)))
	{
		((CStatic*)GetDlgItem(IDC_ICON_CAS_6))->SetIcon(m_LedIcon[1]);
	}
	else
	{
		((CStatic*)GetDlgItem(IDC_ICON_CAS_6))->SetIcon(m_LedIcon[0]);
	}


	if ((((m_stCassetPos[6].x_pos + x_pos_margin) > x_pos) && ((m_stCassetPos[6].x_pos - x_pos_margin) < x_pos)) &&
		(((m_stCassetPos[6].y_pos + y_pos_margin) > y_pos) && ((m_stCassetPos[6].y_pos - y_pos_margin) < y_pos)))
	{
		((CStatic*)GetDlgItem(IDC_ICON_CAS_7))->SetIcon(m_LedIcon[1]);
	}
	else
	{
		((CStatic*)GetDlgItem(IDC_ICON_CAS_7))->SetIcon(m_LedIcon[0]);
	}

}

void CZPExchangeDlg::OnBnClickedButtonConnect()
{
	int nRet;
	CString strTemp;
	
	strTemp = _T(" OnBnClickedButtonConnect() Click!");
	::AfxMessageBox((strTemp));
	SaveLogFile("ZonePlateExChangeModuleReport", _T((LPSTR)(LPCTSTR)(strTemp)));

	if (!m_bConnected)
	{
		nRet = OpenDevice();
		if (nRet == 0)
		{
			nRet = GetPosType(Z_AXIS);
			if (nRet == 0) m_strPosTypeZ = m_strPosType;

			nRet = GetPosType(R_AXIS);
			if (nRet == 0) m_strPosTypeR = m_strPosType;

			nRet = GetPosType(G_AXIS);
			if (nRet == 0) m_strPosTypeG = m_strPosType;


			nRet = GetPosType(DETECTER_AXIS);
			if (nRet == 0) g_pDetecterApertureChange->m_strPosTypeDetecter = m_strPosType;

			nRet = GetPosType(APERTURE_AXIS);
			if (nRet == 0) g_pDetecterApertureChange->m_strPosTypeAperture = m_strPosType;

			UpdateData(FALSE);

			//SET MAX CL FREQUENCY & HOLD TIME?????
			//nRet = SetMaxClFrequency(Z_AXIS, 5000);
			//nRet = SetMaxClFrequency(R_AXIS, 5000);
			//nRet = SetMaxClFrequency(G_AXIS, 5000);
			//nRet = SetHoldTime(Z_AXIS, SA_CTL_HOLD_TIME_INFINITE);
			//nRet = SetHoldTime(R_AXIS, SA_CTL_HOLD_TIME_INFINITE);
			//nRet = SetHoldTime(G_AXIS, SA_CTL_HOLD_TIME_INFINITE);
			//Rotate Axis shift & set to zero
			//nRet = SetLogicalScaleOffset(R_AXIS, 90);
			//if()


			m_bStageConnected = TRUE;
			SetTimer(ZPEXCHANGE_UPDATE_TIMER, 100, NULL);

			strTemp = _T(" Connection complete !");
			SaveLogFile("ZonePlateExChangeModuleReport", _T((LPSTR)(LPCTSTR)(strTemp)));

		}
		else
		{
			m_bStageConnected = FALSE;
			strTemp.Format("Fail to connect. ( %d )", nRet);
			AfxMessageBox(strTemp);
			SaveLogFile("ZonePlateExChangeModuleReport", _T((LPSTR)(LPCTSTR)(strTemp)));
		}
	}
}


void CZPExchangeDlg::OnBnClickedButtonDisconnect()
{
	CString strTemp;

	strTemp = _T(" OnBnClickedButtonDisconnect() Click!");
	::AfxMessageBox((strTemp));
	SaveLogFile("ZonePlateExChangeModuleReport", _T((LPSTR)(LPCTSTR)(strTemp)));

	if (m_bConnected)
	{
		CloseDevice();
	}
}

void CZPExchangeDlg::OnBnClickedButtonCalibrateZ()
{
	if (m_bConnected != TRUE)
	{
		AfxMessageBox(_T("Device not connected."));
		return;
	}

	if (m_bActivelyMoving[Z_AXIS] == TRUE || m_bActivelyMoving[R_AXIS] == TRUE || m_bActivelyMoving[G_AXIS] == TRUE)
	{
		AfxMessageBox(_T("스테이지가 동작중이므로 동작할 수 없습니다."), MB_ICONWARNING);
		return;
	}

	int nRet;
	CString strTemp;

	nRet = Calibrate(Z_AXIS);
	if (nRet != 0)
	{
		strTemp.Format(_T("[Z-axis] Fail to calibrate. ( %d )"), nRet);
		AfxMessageBox(strTemp);
		return;
	}

	m_bStopZaxis = FALSE;

	while (TRUE)
	{
		WaitSec(1);

		if (m_bStopZaxis == TRUE)
			break;

		if (m_bActivelyMoving[Z_AXIS] == FALSE)
		{
			strTemp.Format(_T("[Z-axis] Success to calibrate."));
			AfxMessageBox(strTemp);
			break;
		}
	}
}


void CZPExchangeDlg::OnBnClickedButtonReferenceZ()
{
	if (m_bConnected != TRUE)
	{
		AfxMessageBox(_T("Device not connected."));
		return;
	}

	if (m_bActivelyMoving[Z_AXIS] == TRUE || m_bActivelyMoving[R_AXIS] == TRUE || m_bActivelyMoving[G_AXIS] == TRUE)
	{
		AfxMessageBox(_T("스테이지가 동작중이므로 동작할 수 없습니다."), MB_ICONWARNING);
		return;
	}

	int nRet;
	CString strTemp;

	nRet = Reference(Z_AXIS);
	if (nRet != 0)
	{
		strTemp.Format(_T("[Z-axis] Fail to reference. ( %d )"), nRet);
		AfxMessageBox(strTemp);
		return;
	}

	m_bStopZaxis = FALSE;

	while (TRUE)
	{
		WaitSec(1);

		if (m_bStopZaxis == TRUE)
			break;

		if (m_bActivelyMoving[Z_AXIS] == FALSE)
		{
			strTemp.Format(_T("[Z-axis] Success to reference."));
			AfxMessageBox(strTemp);
			break;
		}
	}
}


void CZPExchangeDlg::OnBnClickedButtonMoveZ()
{
	if (m_bConnected != TRUE)
	{
		AfxMessageBox(_T("Device not connected."));
		return;
	}

	if (m_bActivelyMoving[Z_AXIS] == TRUE || m_bActivelyMoving[R_AXIS] == TRUE || m_bActivelyMoving[G_AXIS] == TRUE)
	{
		AfxMessageBox(_T("스테이지가 동작중이므로 동작할 수 없습니다."), MB_ICONWARNING);
		return;
	}

	UpdateData(TRUE);

	int nRet;
	CString strTemp;

	if (m_nMoveTypeZ == 0)
	{
		nRet = SetMoveMode(Z_AXIS, SA_CTL_MOVE_MODE_CL_RELATIVE);
		if (nRet != SA_CTL_ERROR_NONE)
		{
			strTemp.Format(_T("[Z-axis] Fail to set relative move mode. ( %d )"), nRet);
			AfxMessageBox(strTemp);
			return;
		}
	}
	else if (m_nMoveTypeZ == 1)
	{
		nRet = SetMoveMode(Z_AXIS, SA_CTL_MOVE_MODE_CL_ABSOLUTE);
		if (nRet != SA_CTL_ERROR_NONE)
		{
			strTemp.Format(_T("[Z-axis] Fail to set absolute move mode. ( %d )"), nRet);
			AfxMessageBox(strTemp);
			return;
		}
	}

	nRet = SetVelocity(Z_AXIS, m_dVelocityZ);
	if (nRet != SA_CTL_ERROR_NONE)
	{
		strTemp.Format(_T("[Z-axis] Fail to set velocity. ( %d )"), nRet);
		AfxMessageBox(strTemp);
		return;
	}

	nRet = SetAcceleration(Z_AXIS, m_dAccelerationZ);
	if (nRet != SA_CTL_ERROR_NONE)
	{
		strTemp.Format(_T("[Z-axis] Fail to set acceleration. ( %d )"), nRet);
		AfxMessageBox(strTemp);
		return;
	}
	
	nRet = Move(Z_AXIS, m_dMoveDistZ);
	if (nRet != SA_CTL_ERROR_NONE)
	{
		strTemp.Format(_T("[Z-axis] Fail to move. ( %d )"), nRet);
		AfxMessageBox(strTemp);
		return;
	}

	while (TRUE)
	{
		WaitSec(1);

		if (m_bEndstopReached[Z_AXIS] == TRUE)
		{
			strTemp.Format(_T("[Z-axis] Reached endstop position."));
			AfxMessageBox(strTemp);
			break;
		}

		if (m_bActivelyMoving[Z_AXIS] == FALSE || m_bStopZaxis == TRUE)
			break;
	}

}


void CZPExchangeDlg::OnBnClickedButtonStopZ()
{
	if (m_bConnected != TRUE)
	{
		AfxMessageBox(_T("Device not connected."));
		return;
	}

	m_bStopZaxis = TRUE;

	int nRet;
	CString strTemp;

	nRet = Stop(Z_AXIS);
	if (nRet != SA_CTL_ERROR_NONE)
	{
		strTemp.Format(_T("[Z-axis] Fail to stop motion. ( %d )"), nRet);
		AfxMessageBox(strTemp);
	}
}


void CZPExchangeDlg::OnBnClickedButtonCalibrateR()
{
	if (m_bConnected != TRUE)
	{
		AfxMessageBox(_T("Device not connected."));
		return;
	}

	if (m_bActivelyMoving[Z_AXIS] == TRUE || m_bActivelyMoving[R_AXIS] == TRUE || m_bActivelyMoving[G_AXIS] == TRUE)
	{
		AfxMessageBox(_T("스테이지가 동작중이므로 동작할 수 없습니다."), MB_ICONWARNING);
		return;
	}

	int nRet;
	CString strTemp;

	nRet = Calibrate(R_AXIS);
	if (nRet != 0)
	{
		strTemp.Format(_T("[R-axis] Fail to calibrate. ( %d )"), nRet);
		AfxMessageBox(strTemp);
		return;
	}

	m_bStopRaxis = FALSE;

	while (TRUE)
	{
		WaitSec(1);

		if (m_bStopRaxis == TRUE)
			break;

		if (m_bActivelyMoving[R_AXIS] == FALSE)
		{
			strTemp.Format(_T("[R-axis] Success to calibrate."), nRet);
			AfxMessageBox(strTemp);
			break;
		}
	}
}


void CZPExchangeDlg::OnBnClickedButtonReferenceR()
{
	if (m_bConnected != TRUE)
	{
		AfxMessageBox(_T("Device not connected."));
		return;
	}

	if (m_bActivelyMoving[Z_AXIS] == TRUE || m_bActivelyMoving[R_AXIS] == TRUE || m_bActivelyMoving[G_AXIS] == TRUE)
	{
		AfxMessageBox(_T("스테이지가 동작중이므로 동작할 수 없습니다."), MB_ICONWARNING);
		return;
	}

	int nRet;
	CString strTemp;

	nRet = Reference(R_AXIS);
	if (nRet != 0)
	{
		strTemp.Format(_T("[R-axis] Fail to reference. ( %d )"), nRet);
		AfxMessageBox(strTemp);
		return;
	}

	m_bStopRaxis = FALSE;

	while (TRUE)
	{
		WaitSec(1);

		if (m_bStopRaxis == TRUE)
			break;

		if (m_bActivelyMoving[R_AXIS] == FALSE)
		{
			strTemp.Format(_T("[R-axis] Success to reference."), nRet);
			AfxMessageBox(strTemp);
			break;
		}
	}
}


void CZPExchangeDlg::OnBnClickedButtonMoveR()
{
	if (m_bConnected != TRUE)
	{
		AfxMessageBox(_T("Device not connected."));
		return;
	}

	if (m_bActivelyMoving[Z_AXIS] == TRUE || m_bActivelyMoving[R_AXIS] == TRUE || m_bActivelyMoving[G_AXIS] == TRUE)
	{
		AfxMessageBox(_T("스테이지가 동작중이므로 동작할 수 없습니다."), MB_ICONWARNING);
		return;
	}

	UpdateData(TRUE);

	int nRet;
	CString strTemp;

	if (m_nMoveTypeR == 0)
	{
		nRet = SetMoveMode(R_AXIS, SA_CTL_MOVE_MODE_CL_RELATIVE);
		if (nRet != SA_CTL_ERROR_NONE)
		{
			strTemp.Format(_T("[R-axis] Fail to set relative move mode. ( %d )"), nRet);
			AfxMessageBox(strTemp);
			return;
		}
	}
	else if (m_nMoveTypeR == 1)
	{
		nRet = SetMoveMode(R_AXIS, SA_CTL_MOVE_MODE_CL_ABSOLUTE);
		if (nRet != SA_CTL_ERROR_NONE)
		{
			strTemp.Format(_T("[R-axis] Fail to set absolute move mode. ( %d )"), nRet);
			AfxMessageBox(strTemp);
			return;
		}
	}

	nRet = SetVelocity(R_AXIS, m_dVelocityR);
	if (nRet != SA_CTL_ERROR_NONE)
	{
		strTemp.Format(_T("[R-axis] Fail to set velocity. ( %d )"), nRet);
		AfxMessageBox(strTemp);
		return;
	}

	nRet = SetAcceleration(R_AXIS, m_dAccelerationR);
	if (nRet != SA_CTL_ERROR_NONE)
	{
		strTemp.Format(_T("[R-axis] Fail to set acceleration. ( %d )"), nRet);
		AfxMessageBox(strTemp);
		return;
	}

	nRet = Move(R_AXIS, m_dMoveDistR);
	if (nRet != SA_CTL_ERROR_NONE)
	{
		strTemp.Format(_T("[R-axis] Fail to move. ( %d )"), nRet);
		AfxMessageBox(strTemp);
		return;
	}

	m_bStopRaxis = FALSE;

	while (TRUE)
	{
		WaitSec(1);

		if (m_bEndstopReached[R_AXIS] == TRUE)
		{
			strTemp.Format(_T("[R-axis] Reached endstop position."));
			AfxMessageBox(strTemp);
			break;
		}

		if (m_bActivelyMoving[R_AXIS] == FALSE || m_bStopRaxis == TRUE)
			break;
	}
}


void CZPExchangeDlg::OnBnClickedButtonStopR()
{
	if (m_bConnected != TRUE)
	{
		AfxMessageBox(_T("Device not connected."));
		return;
	}

	m_bStopRaxis = TRUE;
	
	int nRet;
	CString strTemp;

	nRet = Stop(R_AXIS);
	if (nRet != SA_CTL_ERROR_NONE)
	{
		strTemp.Format(_T("[R-axis] Fail to stop motion. ( %d )"), nRet);
		AfxMessageBox(strTemp);
	}
}


void CZPExchangeDlg::OnBnClickedButtonCalibrateG()
{
	if (m_bConnected != TRUE)
	{
		AfxMessageBox(_T("Device not connected."));
		return;
	}

	if (m_bActivelyMoving[Z_AXIS] == TRUE || m_bActivelyMoving[R_AXIS] == TRUE || m_bActivelyMoving[G_AXIS] == TRUE)
	{
		AfxMessageBox(_T("스테이지가 동작중이므로 동작할 수 없습니다."), MB_ICONWARNING);
		return;
	}

	int nRet;
	CString strTemp;

	nRet = Calibrate(G_AXIS);
	if (nRet != 0)
	{		
		strTemp.Format(_T("[Gripper] Fail to calibrate. ( %d )"), nRet);
		AfxMessageBox(strTemp);
		return;
	}

	m_bStopGrip = FALSE;

	while (TRUE)
	{
		WaitSec(1);

		if (m_bStopGrip == TRUE)
			break;

		if (m_bActivelyMoving[G_AXIS] == FALSE)
		{
			strTemp.Format(_T("[Gripper] Success to calibrate."));
			AfxMessageBox(strTemp);
			break;
		}
	}
}


void CZPExchangeDlg::OnBnClickedButtonReferenceG()
{
	if (m_bConnected != TRUE)
	{
		AfxMessageBox(_T("Device not connected."));
		return;
	}

	if (m_bActivelyMoving[Z_AXIS] == TRUE || m_bActivelyMoving[R_AXIS] == TRUE || m_bActivelyMoving[G_AXIS] == TRUE)
	{
		AfxMessageBox(_T("스테이지가 동작중이므로 동작할 수 없습니다."), MB_ICONWARNING);
		return;
	}

	int nRet;
	CString strTemp;

	nRet = Reference(G_AXIS);
	if (nRet != 0)
	{
		strTemp.Format(_T("[Gripper] Fail to reference. ( %d )"), nRet);
		AfxMessageBox(strTemp);
		return;
	}

	m_bStopGrip = FALSE;

	while (TRUE)
	{
		WaitSec(1);

		if (m_bStopGrip == TRUE)
			break;

		if (m_bActivelyMoving[G_AXIS] == FALSE)
		{
			strTemp.Format(_T("[Gripper] Success to reference."));
			AfxMessageBox(strTemp);
			break;
		}
	}
}


void CZPExchangeDlg::OnBnClickedButtonMoveG()
{
	if (m_bConnected != TRUE)
	{
		AfxMessageBox(_T("Device not connected."));
		return;
	}

	if (m_bActivelyMoving[Z_AXIS] == TRUE || m_bActivelyMoving[R_AXIS] == TRUE || m_bActivelyMoving[G_AXIS] == TRUE)
	{
		AfxMessageBox(_T("스테이지가 동작중이므로 동작할 수 없습니다."), MB_ICONWARNING);
		return;
	}

	UpdateData(TRUE);

	int nRet;
	CString strTemp;

	if (m_nMoveTypeG == 0)
	{
		nRet = SetMoveMode(G_AXIS, SA_CTL_MOVE_MODE_CL_RELATIVE);
		if (nRet != SA_CTL_ERROR_NONE)
		{
			strTemp.Format(_T("[Gripper] Fail to set relative move mode. ( %d )"), nRet);
			AfxMessageBox(strTemp);
			return;
		}
	}
	else if (m_nMoveTypeG == 1)
	{
		nRet = SetMoveMode(G_AXIS, SA_CTL_MOVE_MODE_CL_ABSOLUTE);
		if (nRet != SA_CTL_ERROR_NONE)
		{
			strTemp.Format(_T("[Gripper] Fail to set absolute move mode. ( %d )"), nRet);
			AfxMessageBox(strTemp);
			return;
		}
	}

	nRet = SetVelocity(G_AXIS, m_dVelocityG);
	if (nRet != SA_CTL_ERROR_NONE)
	{
		strTemp.Format(_T("[Gripper] Fail to set velocity. ( %d )"), nRet);
		AfxMessageBox(strTemp);
		return;
	}

	nRet = SetAcceleration(G_AXIS, m_dAccelerationG);
	if (nRet != SA_CTL_ERROR_NONE)
	{
		strTemp.Format(_T("[Gripper] Fail to set acceleration. ( %d )"), nRet);
		AfxMessageBox(strTemp);
		return;
	}

	nRet = Move(G_AXIS, m_dMoveDistG);
	if (nRet != SA_CTL_ERROR_NONE)
	{
		strTemp.Format(_T("[Gripper] Fail to move. ( %d )"), nRet);
		AfxMessageBox(strTemp);
		return;
	}

	m_bStopGrip = FALSE;

	while (TRUE)
	{
		WaitSec(1);

		if (m_bEndstopReached[G_AXIS] == TRUE)
		{
			strTemp.Format(_T("[Gripper] Reached endstop position."));
			AfxMessageBox(strTemp);
			break;
		}

		if (m_bActivelyMoving[G_AXIS] == FALSE || m_bStopGrip == TRUE)
			break;
	}
}


void CZPExchangeDlg::OnBnClickedButtonStopG()
{
	if (m_bConnected != TRUE)
	{
		AfxMessageBox(_T("Device not connected."));
		return;
	}
	
	m_bStopGrip = TRUE;

	int nRet;
	CString strTemp;
	
	nRet = Stop(G_AXIS);
	if (nRet != SA_CTL_ERROR_NONE)
	{
		strTemp.Format(_T("[Gripper] Fail to stop. ( %d )"), nRet);
		AfxMessageBox(strTemp);
	}
}

void CZPExchangeDlg::OnBnClickedButtonZpexSetScale()
{
	if (m_bConnected != TRUE)
	{
		AfxMessageBox(_T("Device not connected."));
		return;
	}

	CString strTemp;
	m_CurrPositionR.GetWindowText(strTemp);
	if (atoi(strTemp) != 0)
	{
		AfxMessageBox(_T("Not zero position."));
		return;
	}

	int nRet;
	nRet = SetLogicalScaleOffset(R_AXIS, -90);
	if (nRet != 0)
		AfxMessageBox(_T("[R-axis] Fail to set logical scale offset."));
}


void CZPExchangeDlg::OnBnClickedButtonZpexRelease()
{
	GAxisReleaseMove();
}


void CZPExchangeDlg::OnBnClickedButtonZpexGrip()
{
	GAxisGripMove();
}

void CZPExchangeDlg::OnReadCasettePos()
{

	CString x_pos;
	CString y_pos;
	CString z_pos;
	CString zr_pos;

	x_pos.Format("%3.3f", m_stCassetPos[0].x_pos);
	y_pos.Format("%3.3f", m_stCassetPos[0].y_pos);
	z_pos.Format("%3.3f", m_stCassetPos[0].zg_pos);
	zr_pos.Format("%3.3f", m_stCassetPos[0].zr_pos);
	m_xpos_cas_1.SetWindowTextA(x_pos);
	m_ypos_cas_1.SetWindowTextA(y_pos);
	m_zpos_cas_1.SetWindowTextA(z_pos);
	m_zrpos_cas_1.SetWindowTextA(zr_pos);

	x_pos.Format("%3.3f", m_stCassetPos[1].x_pos);
	y_pos.Format("%3.3f", m_stCassetPos[1].y_pos);
	z_pos.Format("%3.3f", m_stCassetPos[1].zg_pos);
	zr_pos.Format("%3.3f", m_stCassetPos[1].zr_pos);
	m_xpos_cas_2.SetWindowTextA(x_pos);
	m_ypos_cas_2.SetWindowTextA(y_pos);
	m_zpos_cas_2.SetWindowTextA(z_pos);
	m_zrpos_cas_2.SetWindowTextA(zr_pos);

	x_pos.Format("%3.3f", m_stCassetPos[2].x_pos);
	y_pos.Format("%3.3f", m_stCassetPos[2].y_pos);
	z_pos.Format("%3.3f", m_stCassetPos[2].zg_pos);
	zr_pos.Format("%3.3f", m_stCassetPos[2].zr_pos);
	m_xpos_cas_3.SetWindowTextA(x_pos);
	m_ypos_cas_3.SetWindowTextA(y_pos);
	m_zpos_cas_3.SetWindowTextA(z_pos);
	m_zrpos_cas_3.SetWindowTextA(zr_pos);

	x_pos.Format("%3.3f", m_stCassetPos[3].x_pos);
	y_pos.Format("%3.3f", m_stCassetPos[3].y_pos);
	z_pos.Format("%3.3f", m_stCassetPos[3].zg_pos);
	zr_pos.Format("%3.3f", m_stCassetPos[3].zr_pos);
	m_xpos_cas_4.SetWindowTextA(x_pos);
	m_ypos_cas_4.SetWindowTextA(y_pos);
	m_zpos_cas_4.SetWindowTextA(z_pos);
	m_zrpos_cas_4.SetWindowTextA(zr_pos);

	x_pos.Format("%3.3f", m_stCassetPos[4].x_pos);
	y_pos.Format("%3.3f", m_stCassetPos[4].y_pos);
	z_pos.Format("%3.3f", m_stCassetPos[4].zg_pos);
	zr_pos.Format("%3.3f", m_stCassetPos[4].zr_pos);
	m_xpos_cas_5.SetWindowTextA(x_pos);
	m_ypos_cas_5.SetWindowTextA(y_pos);
	m_zpos_cas_5.SetWindowTextA(z_pos);
	m_zrpos_cas_5.SetWindowTextA(zr_pos);

	x_pos.Format("%3.3f", m_stCassetPos[5].x_pos);
	y_pos.Format("%3.3f", m_stCassetPos[5].y_pos);
	z_pos.Format("%3.3f", m_stCassetPos[5].zg_pos);
	zr_pos.Format("%3.3f", m_stCassetPos[5].zr_pos);
	m_xpos_cas_6.SetWindowTextA(x_pos);
	m_ypos_cas_6.SetWindowTextA(y_pos);
	m_zpos_cas_6.SetWindowTextA(z_pos);
	m_zrpos_cas_6.SetWindowTextA(zr_pos);

	x_pos.Format("%3.3f", m_stCassetPos[6].x_pos);
	y_pos.Format("%3.3f", m_stCassetPos[6].y_pos);
	z_pos.Format("%3.3f", m_stCassetPos[6].zg_pos);
	zr_pos.Format("%3.3f", m_stCassetPos[6].zr_pos);
	m_xpos_cas_7.SetWindowTextA(x_pos);
	m_ypos_cas_7.SetWindowTextA(y_pos);
	m_zpos_cas_7.SetWindowTextA(z_pos);
	m_zrpos_cas_7.SetWindowTextA(zr_pos);

}

void CZPExchangeDlg::LoadTeachingPos()
{
	CString strAppName, strKeyName;
	char chGetString[64];

	strAppName.Format(_T("ZP_TEACH_POS"));

	for (int nIdx = 0; nIdx < 7; nIdx++)
	{
		strKeyName.Format(_T("XPos%d"), nIdx + 1);
		GetPrivateProfileString(strAppName, strKeyName, "", chGetString, 64, CONFIG_FILE_FULLPATH);
		sscanf(chGetString, "%lf", &m_stCassetPos[nIdx].x_pos);

		strKeyName.Format(_T("YPos%d"), nIdx + 1);
		GetPrivateProfileString(strAppName, strKeyName, "", chGetString, 64, CONFIG_FILE_FULLPATH);
		sscanf(chGetString, "%lf", &m_stCassetPos[nIdx].y_pos);

		strKeyName.Format(_T("ZPos%d"), nIdx + 1);
		GetPrivateProfileString(strAppName, strKeyName, "", chGetString, 64, CONFIG_FILE_FULLPATH);
		sscanf(chGetString, "%lf", &m_stCassetPos[nIdx].zg_pos);

		strKeyName.Format(_T("ZRPos%d"), nIdx + 1);
		GetPrivateProfileString(strAppName, strKeyName, "", chGetString, 64, CONFIG_FILE_FULLPATH);
		sscanf(chGetString, "%lf", &m_stCassetPos[nIdx].zr_pos);
	}
}

void CZPExchangeDlg::SaveTeachingPos()
{
	CString strAppName, strKeyName, strValue;

	strAppName.Format(_T("ZP_TEACH_POS"));

	strKeyName.Format(_T("XPos%d"), m_nStagePosNum + 1);
	strValue.Format("%lf", m_stCassetPos[m_nStagePosNum].x_pos);
	WritePrivateProfileString(strAppName, strKeyName, strValue, CONFIG_FILE_FULLPATH);

	strKeyName.Format(_T("YPos%d"), m_nStagePosNum + 1);
	strValue.Format("%lf", m_stCassetPos[m_nStagePosNum].y_pos);
	WritePrivateProfileString(strAppName, strKeyName, strValue, CONFIG_FILE_FULLPATH);

	strKeyName.Format(_T("ZPos%d"), m_nStagePosNum + 1);
	strValue.Format("%lf", m_stCassetPos[m_nStagePosNum].zg_pos);
	WritePrivateProfileString(strAppName, strKeyName, strValue, CONFIG_FILE_FULLPATH);

	strKeyName.Format(_T("ZRPos%d"), m_nStagePosNum + 1);
	strValue.Format("%lf", m_stCassetPos[m_nStagePosNum].zr_pos);
	WritePrivateProfileString(strAppName, strKeyName, strValue, CONFIG_FILE_FULLPATH);
}

BOOL CZPExchangeDlg::StageMovableZAxisCheck()
{
	BOOL nRet = TRUE;

	if (m_dZpos > StageMovingZLimit)
	{
		nRet = FALSE;
	}

	return nRet;
}

void CZPExchangeDlg::OnBnClickedCasMove()
{
	CString str;
	CString ZPosValue;
	CString ZLimitValue;
	double x_move_pos = 0.0;
	double y_move_pos = 0.0;
	
	SaveLogFile("ZonePlateExChangeModuleReport", _T((LPSTR)(LPCTSTR)(" OnBnClickedCasMove")));

	if (m_bActivelyMoving[Z_AXIS] == TRUE || m_bActivelyMoving[R_AXIS] == TRUE || m_bActivelyMoving[G_AXIS] == TRUE)
	{
		str = _T("스테이지가 동작중이므로 동작할 수 없습니다.");
		AfxMessageBox((str), MB_ICONWARNING);
		SaveLogFile("ZonePlateExChangeModuleReport", _T((LPSTR)(LPCTSTR)(str)));
		return;
	}

	if(!StageMovableZAxisCheck())
	//if (Zpos > StageMovingZLimit)
	{
		str = _T(" Z축 높이 확인 필요. Stage 동작 불가!");
		ZPosValue.Format("%5.5f", m_dZpos);
		ZLimitValue.Format("%5.5f", StageMovingZLimit);
		
		::AfxMessageBox((str), MB_ICONERROR);
		SaveLogFile("ZonePlateExChangeModuleReport", _T((LPSTR)(LPCTSTR)(str + " Z축 높이 : " + ZPosValue + " , Z축 Limit 높이 : " + ZLimitValue)));
		return;

	}

	if ((m_nStagePosNum >= 0) && (m_nStagePosNum < 8))
	{
		CString x_move_pos_str;
		CString y_move_pos_str;
		CString stage_cas_str;
		
		x_move_pos = m_stCassetPos[m_nStagePosNum].x_pos;
		y_move_pos = m_stCassetPos[m_nStagePosNum].y_pos;

		x_move_pos_str.Format("%3.7f", x_move_pos);
		y_move_pos_str.Format("%3.7f", y_move_pos);
		stage_cas_str.Format("%d", m_nStagePosNum);


		if (g_pNavigationStage->MoveAbsolutePosition(x_move_pos, y_move_pos) != XY_NAVISTAGE_OK)
		{
			str = _T(" Navigation Stage 동작 불가!");
			::AfxMessageBox((str), MB_ICONERROR);
			SaveLogFile("ZonePlateExChangeModuleReport", _T((LPSTR)(LPCTSTR)(str)));
		}
		else
		{
			str = _T(" Navigation Stage Moving");
			SaveLogFile("ZonePlateExChangeModuleReport", _T((LPSTR)(LPCTSTR)(str + " 카세트 번호 : " + stage_cas_str +" , X Position : " + x_move_pos_str +" , Y Position :"+ y_move_pos_str)));
		}
	}
	else
	{
		str = _T(" 카세트 위치 확인 필요. Stage 동작 불가!");
		::AfxMessageBox((str), MB_ICONERROR);
		SaveLogFile("ZonePlateExChangeModuleReport", _T((LPSTR)(LPCTSTR)(str)));
	}
	
}


void CZPExchangeDlg::OnBnClickedCasPosSet()
{
	double current_x_pos;
	double current_y_pos;
	CString str;
	CString current_x_pos_str;
	CString current_y_pos_str;
	CString current_cas_pos_str;
	str.Empty();
	current_x_pos_str.Empty();
	current_y_pos_str.Empty();

	current_x_pos = g_pNavigationStage->GetPosmm(STAGE_ACS_X_AXIS);
	current_y_pos = g_pNavigationStage->GetPosmm(STAGE_ACS_Y_AXIS);

	current_x_pos_str.Format("%3.5f", current_x_pos);
	current_y_pos_str.Format("%3.5f", current_y_pos);
	current_cas_pos_str.Format("%d", m_nStagePosNum);

	str = _T(" OnBnClickedCasPosSet() Click!");
	::AfxMessageBox((str));
	SaveLogFile("ZonePlateExChangeModuleReport", _T((LPSTR)(LPCTSTR)(str + ",  Sel Casset Num : " + current_cas_pos_str)));

	if ((m_nStagePosNum >= 0) && (m_nStagePosNum < 8))
	{
		m_stCassetPos[m_nStagePosNum].x_pos = current_x_pos;
		m_stCassetPos[m_nStagePosNum].y_pos = current_y_pos;
		str = _T(" 선택된 카세트 위치에 좌표 저장!");
		::AfxMessageBox((str));
		SaveLogFile("ZonePlateExChangeModuleReport", _T((LPSTR)(LPCTSTR)(str + ",  Sel Casset Num : " + current_cas_pos_str + " , XPos : " + current_x_pos_str + " , YPos : " + current_y_pos_str)));

	}
	else
	{
		str = _T(" 카세트 선택이 올바르지 않습니다. 확인필요!");
		::AfxMessageBox((str), MB_ICONERROR);
		SaveLogFile("ZonePlateExChangeModuleReport", _T((LPSTR)(LPCTSTR)(str + ",  Sel Casset Num : " + current_cas_pos_str )));
	}
	
	SaveTeachingPos();
	OnReadCasettePos();

}


void CZPExchangeDlg::OnBnClickedButtonTargetPos()
{
	UpdateData(TRUE);
	double ZSetPos = m_dZaxisTargetPos;
	ZAxisTargetMove(ZSetPos);
}


void CZPExchangeDlg::OnBnClickedButtonTargetPosSet()
{
	UpdateData(TRUE);
	ZPositiveLimit = m_dZaxisTargetPos + 10;
}


void CZPExchangeDlg::OnBnClickedButtonRelPosSet()
{
	UpdateData(TRUE);
	StageMovingZLimit = m_dZaxisReleasePos + 10;
}


void CZPExchangeDlg::OnBnClickedButtonZpexGripSet()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	UpdateData(TRUE);
	GGripPos = m_dGripPos;
}


void CZPExchangeDlg::OnBnClickedButtonZpexReleaseSet()
{
	UpdateData(TRUE);
	GReleasePos = m_dReleasePos;
}


void CZPExchangeDlg::OnBnClickedButtonRelPos()
{
	ZAxisReleaseMove();
}

BOOL CZPExchangeDlg::ZAxisReGripPosZoneMove(double ztargetpos)
{
	double ZaxisGripZone = 0.0;
	int ZaxisGripZoneVel = 0.0;
	int nRet;
	CString strTemp;

	if (m_bConnected != TRUE)
	{
		AfxMessageBox(_T("Device not connected."));
		SaveLogFile("ZonePlateExChangeModuleReport", _T((LPSTR)(LPCTSTR)(strTemp)));
		return FALSE;
	}

	if (m_bActivelyMoving[Z_AXIS] == TRUE || m_bActivelyMoving[R_AXIS] == TRUE || m_bActivelyMoving[G_AXIS] == TRUE)
	{
		AfxMessageBox(_T("스테이지가 동작중이므로 동작할 수 없습니다."), MB_ICONWARNING);
		return FALSE;
	}

	UpdateData(TRUE);



	nRet = SetMoveMode(Z_AXIS, SA_CTL_MOVE_MODE_CL_ABSOLUTE);
	if (nRet != SA_CTL_ERROR_NONE)
	{
		strTemp.Format(_T("[Z Axis] Fail to set absolute move mode. ( %d )"), nRet);
		SaveLogFile("ZonePlateExChangeModuleReport", _T((LPSTR)(LPCTSTR)(strTemp)));
		AfxMessageBox(strTemp);
		return FALSE;
	}

	ZaxisGripZone = ztargetpos - 3000;
	ZaxisGripZoneVel = 50;

	nRet = SetVelocity(Z_AXIS, ZaxisGripZoneVel);
	if (nRet != SA_CTL_ERROR_NONE)
	{
		strTemp.Format(_T("[Z Axis] Fail to set velocity. ( %d )"), nRet);
		SaveLogFile("ZonePlateExChangeModuleReport", _T((LPSTR)(LPCTSTR)(strTemp)));
		AfxMessageBox(strTemp);
		return FALSE;
	}

	nRet = SetAcceleration(Z_AXIS, 100);
	if (nRet != SA_CTL_ERROR_NONE)
	{
		strTemp.Format(_T("[Z Axis] Fail to set acceleration. ( %d )"), nRet);
		SaveLogFile("ZonePlateExChangeModuleReport", _T((LPSTR)(LPCTSTR)(strTemp)));
		AfxMessageBox(strTemp);
		return FALSE;
	}

	//if ((ztargetpos > StageMovingZLimit))
	//{
	//	strTemp.Format(_T("[Z Axis] Setting value is out of position. ( %f ))"), ztargetpos);
	//	SaveLogFile("ZonePlateExChangeModuleReport", _T((LPSTR)(LPCTSTR)(strTemp)));
	//	AfxMessageBox(strTemp);
	//	return FALSE;
	//}

	strTemp.Format(_T("[Z Axis] Z Axis Grip Zone 까지 이동. ( %f )"), ZaxisGripZone);
	SaveLogFile("ZonePlateExChangeModuleReport", _T((LPSTR)(LPCTSTR)(strTemp)));
	//nRet = Move(Z_AXIS, m_ZaxisReleasePos);
	nRet = Move(Z_AXIS, ZaxisGripZone);
	if (nRet != SA_CTL_ERROR_NONE)
	{
		strTemp.Format(_T("[Z Axis] Fail to move. ( %d )"), nRet);
		SaveLogFile("ZonePlateExChangeModuleReport", _T((LPSTR)(LPCTSTR)(strTemp)));
		AfxMessageBox(strTemp);
		return FALSE;
	}

	m_bStopZaxis = FALSE;

	while (TRUE)
	{
		WaitSec(1);

		if ((m_bStopZaxis == TRUE) || (m_bRunLongTestStopFlag == TRUE))
			return TRUE;

		if (m_bEndstopReached[Z_AXIS] == TRUE)
		{
			strTemp.Format(_T("[Z_Axis] Reached endstop position."));
			SaveLogFile("ZonePlateExChangeModuleReport", _T((LPSTR)(LPCTSTR)(strTemp)));
			AfxMessageBox(strTemp);
			break;
		}

		if (m_bActivelyMoving[Z_AXIS] == FALSE)
			break;
	}

	return TRUE;
}

BOOL CZPExchangeDlg::ZAxisReleaseMove()
{
	double ZaxisLowSpeedZone = 0.0;
	int ZaxisLowSpeedVel = 0.0;
	int nRet;
	CString strTemp;

	if (m_bConnected != TRUE)
	{
		AfxMessageBox(_T("Device not connected."));
		return FALSE;
	}

	if (m_bActivelyMoving[Z_AXIS] == TRUE || m_bActivelyMoving[R_AXIS] == TRUE || m_bActivelyMoving[G_AXIS] == TRUE)
	{
		AfxMessageBox(_T("스테이지가 동작중이므로 동작할 수 없습니다."), MB_ICONWARNING);
		return FALSE;
	}

	UpdateData(TRUE);



	nRet = SetMoveMode(Z_AXIS, SA_CTL_MOVE_MODE_CL_ABSOLUTE);
	if (nRet != SA_CTL_ERROR_NONE)
	{
		strTemp.Format(_T("[Z Axis] Fail to set absolute move mode. ( %d )"), nRet);
		AfxMessageBox(strTemp);
		return FALSE;
	}

	//nRet = SetVelocity(Z_AXIS, m_dVelocityZ);
	if (m_dZpos > m_dZaxisTargetPos + 3000)
	{
		ZaxisLowSpeedZone = m_dZaxisTargetPos - 3000;
		ZaxisLowSpeedVel = 50;

	}
	else
	{
		ZaxisLowSpeedZone = m_dZaxisReleasePos;
		ZaxisLowSpeedVel = m_dVelocityZ;
	}

	nRet = SetVelocity(Z_AXIS, ZaxisLowSpeedVel);
	if (nRet != SA_CTL_ERROR_NONE)
	{
		strTemp.Format(_T("[Z Axis] Fail to set velocity. ( %d )"), nRet);
		AfxMessageBox(strTemp);
		return FALSE;
	}

	nRet = SetAcceleration(Z_AXIS, 100);
	if (nRet != SA_CTL_ERROR_NONE)
	{
		strTemp.Format(_T("[Z Axis] Fail to set acceleration. ( %d )"), nRet);
		AfxMessageBox(strTemp);
		return FALSE;
	}

	if ((m_dZaxisReleasePos > StageMovingZLimit))
	{
		strTemp.Format(_T("[Z Axis] Setting value is out of position. ( %f ))"), m_dZaxisReleasePos);
		AfxMessageBox(strTemp);
		return FALSE;
	}

	
	//nRet = Move(Z_AXIS, m_ZaxisReleasePos);
	nRet = Move(Z_AXIS, ZaxisLowSpeedZone);
	if (nRet != SA_CTL_ERROR_NONE)
	{
		strTemp.Format(_T("[Z Axis] Fail to move. ( %d )"), nRet);
		AfxMessageBox(strTemp);
		return FALSE;
	}

	m_bStopZaxis = FALSE;

	while (TRUE)
	{
		WaitSec(1);


		if ((m_bStopZaxis == TRUE) || (m_bRunLongTestStopFlag == TRUE))
			return TRUE;

		if (m_bEndstopReached[Z_AXIS] == TRUE)
		{
			strTemp.Format(_T("[Z_Axis] Reached endstop position."));
			AfxMessageBox(strTemp);
			break;
		}


		if (m_bActivelyMoving[Z_AXIS] == FALSE) 
			break;


	}

	
	nRet = SetVelocity(Z_AXIS, m_dVelocityZ);
	if (nRet != SA_CTL_ERROR_NONE)
	{
		strTemp.Format(_T("[Z Axis] Fail to set velocity. ( %d )"), nRet);
		AfxMessageBox(strTemp);
		return FALSE;
	}

	nRet = Move(Z_AXIS, m_dZaxisReleasePos);
	if (nRet != SA_CTL_ERROR_NONE)
	{
		strTemp.Format(_T("[Z Axis] Fail to move. ( %d )"), nRet);
		AfxMessageBox(strTemp);
		return FALSE;
	}

	m_bStopZaxis = FALSE;

	while (TRUE)
	{
		WaitSec(1);

		if ((m_bStopZaxis == TRUE) || (m_bRunLongTestStopFlag == TRUE))
			return TRUE;

		if (m_bEndstopReached[Z_AXIS] == TRUE)
		{
			strTemp.Format(_T("[Z_Axis] Reached endstop position."));
			AfxMessageBox(strTemp);
			break;
		}

		if (m_bActivelyMoving[Z_AXIS] == FALSE)
			break;
	}

	return TRUE;
}

BOOL CZPExchangeDlg::ZAxisTargetMove(double ZAxisPos)
{

	int nRet;
	CString strTemp;
	double ZaxisHighSpeedZone = 0.0;
	double SetZaxisTargetPos = ZAxisPos;

	if (m_bConnected != TRUE)
	{
		strTemp = _T("Device not connected.");
		::AfxMessageBox(strTemp, MB_ICONERROR);
		SaveLogFile("ZonePlateExChangeModuleReport", _T((LPSTR)(LPCTSTR)(strTemp)));

		return FALSE;
	}

	if (m_bActivelyMoving[Z_AXIS] == TRUE || m_bActivelyMoving[R_AXIS] == TRUE || m_bActivelyMoving[G_AXIS] == TRUE)
	{
		strTemp = _T("스테이지가 동작중이므로 동작할 수 없습니다.");
		::AfxMessageBox(strTemp, MB_ICONERROR);
		SaveLogFile("ZonePlateExChangeModuleReport", _T((LPSTR)(LPCTSTR)(strTemp)));
		return FALSE;
	}

	UpdateData(TRUE);


	nRet = SetMoveMode(Z_AXIS, SA_CTL_MOVE_MODE_CL_ABSOLUTE);
	if (nRet != SA_CTL_ERROR_NONE)
	{
		strTemp.Format(_T("[Z Axis] Fail to set absolute move mode. ( %d )"), nRet);
		AfxMessageBox(strTemp, MB_ICONERROR);
		SaveLogFile("ZonePlateExChangeModuleReport", _T((LPSTR)(LPCTSTR)(strTemp)));
		return FALSE;
	}

	nRet = SetVelocity(Z_AXIS, m_dVelocityZ);
	strTemp.Format(_T("[Z Axis] Set Velocity. ( %lf um/s)"), m_dVelocityZ);
	SaveLogFile("ZonePlateExChangeModuleReport", _T((LPSTR)(LPCTSTR)(strTemp)));

	if (nRet != SA_CTL_ERROR_NONE)
	{
		strTemp.Format(_T("[Z Axis] Fail to set velocity. ( %d )"), nRet);
		AfxMessageBox(strTemp, MB_ICONERROR);
		SaveLogFile("ZonePlateExChangeModuleReport", _T((LPSTR)(LPCTSTR)(strTemp)));
		return FALSE;
	}

	nRet = SetAcceleration(Z_AXIS, 1000);
	if (nRet != SA_CTL_ERROR_NONE)
	{
		strTemp.Format(_T("[Z Axis] Fail to set acceleration. ( %d )"), nRet);
		AfxMessageBox(strTemp, MB_ICONERROR);
		SaveLogFile("ZonePlateExChangeModuleReport", _T((LPSTR)(LPCTSTR)(strTemp)));
		return FALSE;
	}

	//if ((SetZaxisTargetPos > ZPositiveLimit))
	if ((SetZaxisTargetPos > ZPositiveLimit))
	{
		strTemp.Format(_T("[Z Axis] Setting value is out of position. ( %lf ))"), SetZaxisTargetPos);
		AfxMessageBox(strTemp, MB_ICONERROR);
		SaveLogFile("ZonePlateExChangeModuleReport", _T((LPSTR)(LPCTSTR)(strTemp)));
		return FALSE;
	}

	ZaxisHighSpeedZone = SetZaxisTargetPos - 1000;
	strTemp.Format(_T("Moving Until High Speed Zone. ( high Seed Zone range : %lf )"), ZaxisHighSpeedZone);
	SaveLogFile("ZonePlateExChangeModuleReport", _T((LPSTR)(LPCTSTR)(strTemp)));
	nRet = Move(Z_AXIS, ZaxisHighSpeedZone);
	if (nRet != SA_CTL_ERROR_NONE)
	{
		strTemp.Format(_T("[Z Axis] Fail to move. ( %d )"), nRet);
		AfxMessageBox(strTemp, MB_ICONERROR);
		SaveLogFile("ZonePlateExChangeModuleReport", _T((LPSTR)(LPCTSTR)(strTemp)));
		return FALSE;
	}

	m_bStopZaxis = FALSE;

	while (TRUE)
	{
		WaitSec(1);

		if ((m_bStopZaxis == TRUE) || (m_bRunLongTestStopFlag == TRUE))
		{
			strTemp.Format(_T("Z Axis Stop occur"));
			SaveLogFile("ZonePlateExChangeModuleReport", _T((LPSTR)(LPCTSTR)(strTemp)));
			return FALSE;
		}

		if (m_bEndstopReached[Z_AXIS] == TRUE)
		{
			strTemp.Format(_T("[Z_Axis] Reached endstop position."));
			AfxMessageBox(strTemp);
			SaveLogFile("ZonePlateExChangeModuleReport", _T((LPSTR)(LPCTSTR)(strTemp)));
			break;
		}

		if (m_bActivelyMoving[Z_AXIS] == FALSE)
			break;
	}


	nRet = SetVelocity(Z_AXIS, 50);
	strTemp.Format(_T("[Z Axis] Set Velocity. ( 50 um/s )"));
	SaveLogFile("ZonePlateExChangeModuleReport", _T((LPSTR)(LPCTSTR)(strTemp)));
	if (nRet != SA_CTL_ERROR_NONE)
	{
		strTemp.Format(_T("[Z Axis] Fail to set velocity. ( %d )"), nRet);
		AfxMessageBox(strTemp);
		SaveLogFile("ZonePlateExChangeModuleReport", _T((LPSTR)(LPCTSTR)(strTemp)));
		return FALSE;
	}

	nRet = Move(Z_AXIS, SetZaxisTargetPos);
	strTemp.Format(_T("Moving Until Slow Speed Zone. ( high Seed Zone range : %lf )"), SetZaxisTargetPos);
	SaveLogFile("ZonePlateExChangeModuleReport", _T((LPSTR)(LPCTSTR)(strTemp)));
	if (nRet != SA_CTL_ERROR_NONE)
	{
		strTemp.Format(_T("[Z Axis] Fail to move. ( %d )"), nRet);
		AfxMessageBox(strTemp);
		SaveLogFile("ZonePlateExChangeModuleReport", _T((LPSTR)(LPCTSTR)(strTemp)));
		return FALSE;
	}

	m_bStopZaxis = FALSE;

	while (TRUE)
	{
		WaitSec(1);

		if ((m_bStopZaxis == TRUE) || (m_bRunLongTestStopFlag == TRUE))
		{
			strTemp.Format(_T("Z Axis Stop occur"));
			SaveLogFile("ZonePlateExChangeModuleReport", _T((LPSTR)(LPCTSTR)(strTemp)));
			return FALSE;
		}

		if (m_bEndstopReached[Z_AXIS] == TRUE)
		{
			strTemp.Format(_T("[Z_Axis] Reached endstop position."));
			AfxMessageBox(strTemp);
			SaveLogFile("ZonePlateExChangeModuleReport", _T((LPSTR)(LPCTSTR)(strTemp)));
			break;
		}

		if (m_bActivelyMoving[Z_AXIS] == FALSE)
			break;
	}

	return TRUE;
}
BOOL CZPExchangeDlg::GAxisGripZoneMove()
{
	int nRet;
	CString strTemp;

	if (m_bConnected != TRUE)
	{
		strTemp = _T("Device not connected.");
		::AfxMessageBox(strTemp, MB_ICONERROR);
		SaveLogFile("ZonePlateExChangeModuleReport", _T((LPSTR)(LPCTSTR)(strTemp)));
		return FALSE;
	}

	if (m_bActivelyMoving[Z_AXIS] == TRUE || m_bActivelyMoving[R_AXIS] == TRUE || m_bActivelyMoving[G_AXIS] == TRUE)
	{
		strTemp = _T("스테이지가 동작중이므로 동작할 수 없습니다.");
		::AfxMessageBox(strTemp, MB_ICONERROR);
		SaveLogFile("ZonePlateExChangeModuleReport", _T((LPSTR)(LPCTSTR)(strTemp)));
		return FALSE;;
	}

	UpdateData(TRUE);

	nRet = SetMoveMode(G_AXIS, SA_CTL_MOVE_MODE_CL_RELATIVE);
	if (nRet != SA_CTL_ERROR_NONE)
	{
		strTemp.Format(_T("[ReGripper] Fail to set relative move mode. ( %d )"), nRet);
		SaveLogFile("ZonePlateExChangeModuleReport", _T((LPSTR)(LPCTSTR)(strTemp)));
		AfxMessageBox(strTemp);
		return FALSE;
	}

	////////////////////////////////////////////////////////
	// 3mm 하강 후 1um 씩 30nm/s 로 Grip 진행.
	///////////////////////////////////////////////////////

	nRet = SetVelocity(G_AXIS, 50);
	strTemp.Format(_T("[ReGripper] Set Velocity. ( 0.05 um/s )"));
	SaveLogFile("ZonePlateExChangeModuleReport", _T((LPSTR)(LPCTSTR)(strTemp)));
	if (nRet != SA_CTL_ERROR_NONE)
	{
		strTemp.Format(_T("[ReGripper] Fail to set velocity. ( %d )"), nRet);
		SaveLogFile("ZonePlateExChangeModuleReport", _T((LPSTR)(LPCTSTR)(strTemp)));
		AfxMessageBox(strTemp);
		return FALSE;
	}

	nRet = SetAcceleration(G_AXIS, 100);
	if (nRet != SA_CTL_ERROR_NONE)
	{
		strTemp.Format(_T("[ReGripper] Fail to set acceleration. ( %d )"), nRet);
		SaveLogFile("ZonePlateExChangeModuleReport", _T((LPSTR)(LPCTSTR)(strTemp)));
		AfxMessageBox(strTemp);
		return FALSE;
	}

	m_bStopGrip = FALSE;

	strTemp.Format(_T("[Gripper] Grip move to -1um "));
	SaveLogFile("ZonePlateExChangeModuleReport", _T((LPSTR)(LPCTSTR)(strTemp)));

	/* End Stop 될 때 까지 진행 */

	while (TRUE)
	{
		WaitSec(1);

		nRet = Move(G_AXIS, -1);
		if (nRet != SA_CTL_ERROR_NONE)
		{
			strTemp.Format(_T("[ReGripper] Fail to move. ( %d )"), nRet);
			SaveLogFile("ZonePlateExChangeModuleReport", _T((LPSTR)(LPCTSTR)(strTemp)));
			AfxMessageBox(strTemp);
			return FALSE;
		}

		if (m_bEndstopReached[G_AXIS] == TRUE)
		{
			m_dEndStopPos = m_dGpos;
			m_dEndStopPos -= 0.5;
			strTemp.Format(_T("[ReGripper] Reached endstop position. ( %lf) "), m_dEndStopPos);
			SaveLogFile("ZonePlateExChangeModuleReport", _T((LPSTR)(LPCTSTR)(strTemp)));

			if (GAxisActiveHoldingPositionMove(m_dEndStopPos) == FALSE)
			{
				strTemp.Format(_T("[GAxisActiveHoldingPositionMove] Gripper Hoding Moving Fail "));
				SaveLogFile("ZonePlateExChangeModuleReport", _T((LPSTR)(LPCTSTR)(strTemp)));
				return FALSE;
			}
			return TRUE;
		}

		if ((m_bStopGrip == TRUE) || (m_bRunLongTestStopFlag == TRUE))
		{
			strTemp.Format(_T("G Axis Stop occur"));
			SaveLogFile("ZonePlateExChangeModuleReport", _T((LPSTR)(LPCTSTR)(strTemp)));
			return FALSE;
		}

		if (m_dGpos < GRIPHOLDLIMINTVALUE)
		{
			strTemp.Format(_T("소재 떨어짐 경고 알람"));
			SaveLogFile("ZonePlateExChangeModuleReport", _T((LPSTR)(LPCTSTR)(strTemp)));
			return FALSE;
		}
		
	}

	WaitSec(1);
	
	if (m_bClosedLoopActive[G_AXIS] == FALSE)
	{
		strTemp.Format(_T("[Gripper Holding Moving] Cloase Loop Active No Apply."));
		AfxMessageBox(strTemp);
		return FALSE;
	}
}

BOOL CZPExchangeDlg::GAxisGripZoneMove_NoEndStop()
{
	int nRet;
	CString strTemp;

	if (m_bConnected != TRUE)
	{
		strTemp = _T("Device not connected.");
		::AfxMessageBox(strTemp, MB_ICONERROR);
		SaveLogFile("ZonePlateExChangeModuleReport", _T((LPSTR)(LPCTSTR)(strTemp)));
		return FALSE;
	}

	if (m_bActivelyMoving[Z_AXIS] == TRUE || m_bActivelyMoving[R_AXIS] == TRUE || m_bActivelyMoving[G_AXIS] == TRUE)
	{
		strTemp = _T("스테이지가 동작중이므로 동작할 수 없습니다.");
		::AfxMessageBox(strTemp, MB_ICONERROR);
		SaveLogFile("ZonePlateExChangeModuleReport", _T((LPSTR)(LPCTSTR)(strTemp)));
		return FALSE;;
	}

	UpdateData(TRUE);

	nRet = SetMoveMode(G_AXIS, SA_CTL_MOVE_MODE_CL_ABSOLUTE);
	if (nRet != SA_CTL_ERROR_NONE)
	{
		strTemp.Format(_T("[ReGripper] Fail to set Absolute move mode. ( %d )"), nRet);
		SaveLogFile("ZonePlateExChangeModuleReport", _T((LPSTR)(LPCTSTR)(strTemp)));
		AfxMessageBox(strTemp);
		return FALSE;
	}

	////////////////////////////////////////////////////////
	// 3mm 하강 후 1um 씩 30nm/s 로 Grip 진행.
	///////////////////////////////////////////////////////

	nRet = SetVelocity(G_AXIS, 50);
	strTemp.Format(_T("[ReGripper] Set Velocity. ( 0.05 um/s )"));
	SaveLogFile("ZonePlateExChangeModuleReport", _T((LPSTR)(LPCTSTR)(strTemp)));
	if (nRet != SA_CTL_ERROR_NONE)
	{
		strTemp.Format(_T("[ReGripper] Fail to set velocity. ( %d )"), nRet);
		SaveLogFile("ZonePlateExChangeModuleReport", _T((LPSTR)(LPCTSTR)(strTemp)));
		AfxMessageBox(strTemp);
		return FALSE;
	}

	nRet = SetAcceleration(G_AXIS, 100);
	if (nRet != SA_CTL_ERROR_NONE)
	{
		strTemp.Format(_T("[ReGripper] Fail to set acceleration. ( %d )"), nRet);
		SaveLogFile("ZonePlateExChangeModuleReport", _T((LPSTR)(LPCTSTR)(strTemp)));
		AfxMessageBox(strTemp);
		return FALSE;
	}

	m_bStopGrip = FALSE;

	strTemp.Format(_T("[Gripper] Grip move to -1um "));
	SaveLogFile("ZonePlateExChangeModuleReport", _T((LPSTR)(LPCTSTR)(strTemp)));

	/* Define Pos 까지 진행 */

	nRet = Move(G_AXIS, GRIPPER_GRIP_NOENDSTOP_POS);
	if (nRet != SA_CTL_ERROR_NONE)
	{
		strTemp.Format(_T("[ReGripper] Fail to move. ( %d )"), nRet);
		SaveLogFile("ZonePlateExChangeModuleReport", _T((LPSTR)(LPCTSTR)(strTemp)));
		AfxMessageBox(strTemp);
		return FALSE;
	}

	while (TRUE)
	{
		WaitSec(1);

		if (m_bEndstopReached[G_AXIS] == TRUE)
		{
			m_dEndStopPos = m_dGpos;
			m_dEndStopPos += 1;
			strTemp.Format(_T("[ReGripper] Reached endstop position. ( %lf) "), m_dEndStopPos);
			SaveLogFile("ZonePlateExChangeModuleReport", _T((LPSTR)(LPCTSTR)(strTemp)));

			if (GAxisActiveHoldingPositionMove(m_dEndStopPos) == FALSE)
			{
				strTemp.Format(_T("[GAxisActiveHoldingPositionMove] Gripper Hoding Moving Fail "));
				SaveLogFile("ZonePlateExChangeModuleReport", _T((LPSTR)(LPCTSTR)(strTemp)));
				return FALSE;
			}
			return TRUE;
		}

		if ((m_bStopGrip == TRUE) || (m_bRunLongTestStopFlag == TRUE))
		{
			strTemp.Format(_T("G Axis Stop occur"));
			SaveLogFile("ZonePlateExChangeModuleReport", _T((LPSTR)(LPCTSTR)(strTemp)));
			return FALSE;
		}

		if (m_dGpos < GRIPHOLDLIMINTVALUE)
		{
			strTemp.Format(_T("소재 떨어짐 경고 알람"));
			SaveLogFile("ZonePlateExChangeModuleReport", _T((LPSTR)(LPCTSTR)(strTemp)));
			return FALSE;
		}

	}

	WaitSec(1);

	if (m_bClosedLoopActive[G_AXIS] == FALSE)
	{
		strTemp.Format(_T("[Gripper Holding Moving] Cloase Loop Active No Apply."));
		AfxMessageBox(strTemp);
		return FALSE;
	}
}


BOOL CZPExchangeDlg::GAxisGripMove()
{
	int nRet;
	CString strTemp;
	m_dEndStopPos = 0.0;

	if (m_bConnected != TRUE)
	{
		strTemp = _T("Device not connected.");
		::AfxMessageBox(strTemp, MB_ICONERROR);
		SaveLogFile("ZonePlateExChangeModuleReport", _T((LPSTR)(LPCTSTR)(strTemp)));
		return FALSE;
	}

	if (m_bActivelyMoving[Z_AXIS] == TRUE || m_bActivelyMoving[R_AXIS] == TRUE || m_bActivelyMoving[G_AXIS] == TRUE)
	{
		strTemp = _T("스테이지가 동작중이므로 동작할 수 없습니다.");
		::AfxMessageBox(strTemp, MB_ICONERROR);
		SaveLogFile("ZonePlateExChangeModuleReport", _T((LPSTR)(LPCTSTR)(strTemp)));
		return FALSE;;
	}

	UpdateData(TRUE);

	nRet = SetMoveMode(G_AXIS, SA_CTL_MOVE_MODE_CL_ABSOLUTE);
	if (nRet != SA_CTL_ERROR_NONE)
	{
		strTemp.Format(_T("[Gripper] Fail to set absolute move mode. ( %d )"), nRet);
		SaveLogFile("ZonePlateExChangeModuleReport", _T((LPSTR)(LPCTSTR)(strTemp)));
		AfxMessageBox(strTemp);
		return FALSE;
	}

	////////////////////////////////////////
	// Step 1.
	//  50um/s 로 EndStopReached 까지 진행
	////////////////////////////////////////
	nRet = SetVelocity(G_AXIS, 50);
	strTemp.Format(_T("[Gripper] Set Velocity. ( 50 um/s )"));
	SaveLogFile("ZonePlateExChangeModuleReport", _T((LPSTR)(LPCTSTR)(strTemp)));
	if (nRet != SA_CTL_ERROR_NONE)
	{
		strTemp.Format(_T("[Gripper] Fail to set velocity. ( %d )"), nRet);
		SaveLogFile("ZonePlateExChangeModuleReport", _T((LPSTR)(LPCTSTR)(strTemp)));
		AfxMessageBox(strTemp);
		return FALSE;
	}

	nRet = SetAcceleration(G_AXIS, 100);
	if (nRet != SA_CTL_ERROR_NONE)
	{
		strTemp.Format(_T("[Gripper] Fail to set acceleration. ( %d )"), nRet);
		SaveLogFile("ZonePlateExChangeModuleReport", _T((LPSTR)(LPCTSTR)(strTemp)));
		AfxMessageBox(strTemp);
		return FALSE;
	}

	strTemp.Format(_T("[Gripper] Moving Until Grip Position. ( %lf )"), m_dGripPos);
	SaveLogFile("ZonePlateExChangeModuleReport", _T((LPSTR)(LPCTSTR)(strTemp)));
	nRet = Move(G_AXIS, m_dGripPos);
	if (nRet != SA_CTL_ERROR_NONE)
	{
		strTemp.Format(_T("[Gripper] Fail to move. ( %d )"), nRet);
		SaveLogFile("ZonePlateExChangeModuleReport", _T((LPSTR)(LPCTSTR)(strTemp)));
		AfxMessageBox(strTemp);
		return FALSE;
	}

	m_bStopGrip = FALSE;

	while (TRUE)
	{
		WaitSec(1);
	
		if (m_bEndstopReached[G_AXIS] == TRUE)
		{
			m_dEndStopPos = m_dGpos;
			strTemp.Format(_T("[Gripper] Reached endstop position. ( velocity = 5um/s ) "));
			SaveLogFile("ZonePlateExChangeModuleReport", _T((LPSTR)(LPCTSTR)(strTemp)));
			strTemp.Format("%lf", m_dEndStopPos);
			SaveLogFile("ZonePlateExChangeModuleReport", _T((LPSTR)(LPCTSTR)(strTemp)));
			break;
		}

		if ((m_bStopGrip == TRUE) || (m_bRunLongTestStopFlag == TRUE))
		{
			strTemp.Format(_T("G Axis Stop occur"));
			SaveLogFile("ZonePlateExChangeModuleReport", _T((LPSTR)(LPCTSTR)(strTemp)));
			return FALSE;
		}

		if (m_bActivelyMoving[G_AXIS] == FALSE)
		{
			m_dEndStopPos = m_dGpos;
			strTemp.Format(_T("[Gripper] ActivelyMoving Stop position. ::  ( %lf )"), m_dEndStopPos);
			SaveLogFile("ZonePlateExChangeModuleReport", _T((LPSTR)(LPCTSTR)(strTemp)));
			break;
		}
	}

	nRet = SetMoveMode(G_AXIS, SA_CTL_MOVE_MODE_CL_RELATIVE);
	if (nRet != SA_CTL_ERROR_NONE)
	{
		strTemp.Format(_T("[Gripper] Fail to set relative move mode. ( %d )"), nRet);
		SaveLogFile("ZonePlateExChangeModuleReport", _T((LPSTR)(LPCTSTR)(strTemp)));
		AfxMessageBox(strTemp);
		return FALSE;
	}

	////////////////////////////////////////
	// Step 2.
	//  30um/s 로 -1um씩 움직여 EndStopReached 까지 진행
	////////////////////////////////////////
	nRet = SetVelocity(G_AXIS, 30);
	strTemp.Format(_T("[Gripper] Set to Velocity. ( 30 um/s )"));
	SaveLogFile("ZonePlateExChangeModuleReport", _T((LPSTR)(LPCTSTR)(strTemp)));
	if (nRet != SA_CTL_ERROR_NONE)
	{
		strTemp.Format(_T("[Gripper] Fail to set velocity. ( %d )"), nRet);
		SaveLogFile("ZonePlateExChangeModuleReport", _T((LPSTR)(LPCTSTR)(strTemp)));
		AfxMessageBox(strTemp);
		return FALSE;
	}

	nRet = SetAcceleration(G_AXIS, 100);
	if (nRet != SA_CTL_ERROR_NONE)
	{
		strTemp.Format(_T("[Gripper] Fail to set acceleration. ( %d )"), nRet);
		AfxMessageBox(strTemp);
		return FALSE;
	}

	strTemp.Format(_T("[Gripper] Grip move to -1um "));
	SaveLogFile("ZonePlateExChangeModuleReport", _T((LPSTR)(LPCTSTR)(strTemp)));
	
	//nRet = Move(G_AXIS, 1);
	//if (nRet != SA_CTL_ERROR_NONE)
	//{
	//	strTemp.Format(_T("[Gripper] Fail to move. ( %d )"), nRet);
	//	SaveLogFile("ZonePlateExChangeModuleReport", _T((LPSTR)(LPCTSTR)(strTemp)));
	//	AfxMessageBox(strTemp);
	//	return FALSE;
	//}

	WaitSec(1);

	while (TRUE)
	{
		WaitSec(1);
		nRet = Move(G_AXIS, -1);
		if (nRet != SA_CTL_ERROR_NONE)
		{
			strTemp.Format(_T("[Gripper] Fail to move. ( %d )"), nRet);
			SaveLogFile("ZonePlateExChangeModuleReport", _T((LPSTR)(LPCTSTR)(strTemp)));
			AfxMessageBox(strTemp);
			return FALSE;
		}
		
		if (m_bEndstopReached[G_AXIS] == TRUE)
		{
			m_dEndStopPos = m_dGpos;
			m_dEndStopPos +=1;
			strTemp.Format(_T("[Gripper] Reached endstop position. ( velocity = 10nm/s ) "));
			SaveLogFile("ZonePlateExChangeModuleReport", _T((LPSTR)(LPCTSTR)(strTemp)));
			strTemp.Format("%lf", m_dEndStopPos);
			SaveLogFile("ZonePlateExChangeModuleReport", _T((LPSTR)(LPCTSTR)(strTemp)));
			

			////////////////////////////////////////
			// Step 3.
			//  10nm/s 로 -1um씩 움직여 EndStopReached 까지 진행 후 그 Postion 에서 Active holding 진행.
			////////////////////////////////////////
			if (GAxisActiveHoldingPositionMove(m_dEndStopPos) == FALSE)
			{
				strTemp.Format(_T("[GAxisActiveHoldingPositionMove] Gripper Hoding Moving Fail "));
				SaveLogFile("ZonePlateExChangeModuleReport", _T((LPSTR)(LPCTSTR)(strTemp)));
				return FALSE;
			}
			return TRUE;
		}

		if ((m_bStopGrip == TRUE) || (m_bRunLongTestStopFlag == TRUE))
		{
			strTemp.Format(_T("G Axis Stop occur"));
			SaveLogFile("ZonePlateExChangeModuleReport", _T((LPSTR)(LPCTSTR)(strTemp)));
			return FALSE;
		}

		if (m_dGpos < GRIPHOLDLIMINTVALUE)
		{
				strTemp.Format(_T("소재 떨어짐 경고 알람"));
				SaveLogFile("ZonePlateExChangeModuleReport", _T((LPSTR)(LPCTSTR)(strTemp)));
				return FALSE;
		}
	}

	WaitSec(1);
	
	if (m_bClosedLoopActive[G_AXIS] == FALSE)
	{
		strTemp.Format(_T("[Gripper Holding Moving] Cloase Loop Active No Apply."));
		AfxMessageBox(strTemp);
		return FALSE;
	}

}

BOOL CZPExchangeDlg::GAxisActiveHoldingPositionMove(double EndStopPosition)
{
	int nRet;
	CString strTemp;
	
	nRet = SetMoveMode(G_AXIS, SA_CTL_MOVE_MODE_CL_ABSOLUTE);
	if (nRet != SA_CTL_ERROR_NONE)
	{
		strTemp.Format(_T("[Gripper Holding Moving] Fail to set absolute move mode. ( %d )"), nRet);
		SaveLogFile("ZonePlateExChangeModuleReport", _T((LPSTR)(LPCTSTR)(strTemp)));
		AfxMessageBox(strTemp);
		return FALSE;
	}

	nRet = SetVelocity(G_AXIS, 1);
	if (nRet != SA_CTL_ERROR_NONE)
	{
		strTemp.Format(_T("[Gripper Holding Moving] Fail to set velocity. ( %d )"), nRet);
		SaveLogFile("ZonePlateExChangeModuleReport", _T((LPSTR)(LPCTSTR)(strTemp)));
		AfxMessageBox(strTemp);
		return FALSE;
	}

	strTemp.Format(_T("[Gripper Holding Moving] Moving until Holding Position . ( %f )"), m_dEndStopPos);
	SaveLogFile("ZonePlateExChangeModuleReport", _T((LPSTR)(LPCTSTR)(strTemp)));

	nRet = Move(G_AXIS, (EndStopPosition));
	if (nRet != SA_CTL_ERROR_NONE)
	{
		strTemp.Format(_T("[Gripper Holding Moving] Fail to move. ( %d )"), nRet);
		SaveLogFile("ZonePlateExChangeModuleReport", _T((LPSTR)(LPCTSTR)(strTemp)));
		AfxMessageBox(strTemp);
		return FALSE;
	}

	while (TRUE)
	{
		WaitSec(1);
		/////////////////////////////////////////////////////////
		// EndStopReached Position 으로 이동시 EndStopReached 가 발생 한다면, 
		// 1um 만 이동
		/////////////////////////////////////////////////////////
		if (m_bEndstopReached[G_AXIS] == TRUE)
		{
			strTemp.Format(_T("[Gripper Holding Moving] Reached endstop position"));
			SaveLogFile("ZonePlateExChangeModuleReport", _T((LPSTR)(LPCTSTR)(strTemp)));

			strTemp.Format(_T("[Gripper Holding Moving] 한번 더 1 um 실행"));
			SaveLogFile("ZonePlateExChangeModuleReport", _T((LPSTR)(LPCTSTR)(strTemp)));
		
			nRet = SetMoveMode(G_AXIS, SA_CTL_MOVE_MODE_CL_RELATIVE);
			if (nRet != SA_CTL_ERROR_NONE)
			{
				strTemp.Format(_T("[Gripper Holding Moving] Fail to set absolute move mode. ( %d )"), nRet);
				SaveLogFile("ZonePlateExChangeModuleReport", _T((LPSTR)(LPCTSTR)(strTemp)));
				AfxMessageBox(strTemp);
				return FALSE;
			}

			nRet = Move(G_AXIS, 1);
			if (nRet != SA_CTL_ERROR_NONE)
			{
				strTemp.Format(_T("[Gripper Holding Moving] Fail to move. ( %d )"), nRet);
				SaveLogFile("ZonePlateExChangeModuleReport", _T((LPSTR)(LPCTSTR)(strTemp)));
				AfxMessageBox(strTemp);
				return FALSE;
			}

			while (TRUE)
			{
				WaitSec(1);
				/////////////////////////////////////////////////////////////
				// 1um 만 이동 중에 다시 EndstopReached 가 또 뜬다면,
				// 2um 만 그자리에서 다시 이동.
				/////////////////////////////////////////////////////////////
				if (m_bEndstopReached[G_AXIS] == TRUE)
				{
					strTemp.Format(_T("[Gripper Holding Moving] Reached endstop position ( 1 um One more time )"));
					SaveLogFile("ZonePlateExChangeModuleReport", _T((LPSTR)(LPCTSTR)(strTemp)));
					nRet = Move(G_AXIS, 2);
					if (nRet != SA_CTL_ERROR_NONE)
					{
						strTemp.Format(_T("[Gripper Holding Moving] Fail to move. ( %d )"), nRet);
						SaveLogFile("ZonePlateExChangeModuleReport", _T((LPSTR)(LPCTSTR)(strTemp)));
						AfxMessageBox(strTemp);
						return FALSE;
					}

					while (TRUE)
					{
						WaitSec(1);
						if (m_bEndstopReached[G_AXIS] == TRUE)
						{
							strTemp.Format(_T("[Gripper Holding Moving] Reached endstop position ( -0.1 um One more time )"));
							SaveLogFile("ZonePlateExChangeModuleReport", _T((LPSTR)(LPCTSTR)(strTemp)));
							AfxMessageBox(strTemp);
							return FALSE;
						}

						if (m_dGpos < GRIPHOLDLIMINTVALUE)
						{
							strTemp.Format(_T("소재 떨어짐 경고 알람"));
							SaveLogFile("ZonePlateExChangeModuleReport", _T((LPSTR)(LPCTSTR)(strTemp)));
							return FALSE;
						}

						if ((m_bActivelyMoving[G_AXIS] == FALSE) || (m_bStopGrip == TRUE) || (m_bRunLongTestStopFlag == TRUE))
							break;
					}

				return TRUE;
				}

				if (m_dGpos < GRIPHOLDLIMINTVALUE)
				{
					strTemp.Format(_T("소재 떨어짐 경고 알람"));
					SaveLogFile("ZonePlateExChangeModuleReport", _T((LPSTR)(LPCTSTR)(strTemp)));
					return FALSE;
				}

				if ((m_bActivelyMoving[G_AXIS] == FALSE) || (m_bStopGrip == TRUE) || (m_bRunLongTestStopFlag == TRUE))
					break;
			}
			return TRUE;
		}

		if (m_dGpos < GRIPHOLDLIMINTVALUE)
		{
			strTemp.Format(_T("소재 떨어짐 경고 알람"));
			SaveLogFile("ZonePlateExChangeModuleReport", _T((LPSTR)(LPCTSTR)(strTemp)));
			return FALSE;
		}

		if ((m_bStopGrip == TRUE) || (m_bRunLongTestStopFlag == TRUE))
		{
			strTemp.Format(_T("G Axis Stop occur"));
			SaveLogFile("ZonePlateExChangeModuleReport", _T((LPSTR)(LPCTSTR)(strTemp)));
			return FALSE;
		}

		if (m_bActivelyMoving[G_AXIS] == FALSE)
		{
			m_dEndStopPos = m_dGpos;
			strTemp.Format(_T("[Gripper] ActivelyMoving Stop position. ::  ( %lf )"), m_dEndStopPos);
			SaveLogFile("ZonePlateExChangeModuleReport", _T((LPSTR)(LPCTSTR)(strTemp)));
			break;
		}
	}

	if (m_bClosedLoopActive[G_AXIS] == FALSE)
	{
		strTemp.Format(_T("[Gripper Holding Moving] Cloase Loop Active No Apply."));
		AfxMessageBox(strTemp);
		return FALSE;
	}

	return TRUE;
}

BOOL CZPExchangeDlg::GAxisReleaseMove()
{
	if (m_bConnected != TRUE)
	{
		AfxMessageBox(_T("Device not connected."));
		return FALSE;
	}

	if (m_bActivelyMoving[Z_AXIS] == TRUE || m_bActivelyMoving[R_AXIS] == TRUE || m_bActivelyMoving[G_AXIS] == TRUE)
	{
		AfxMessageBox(_T("스테이지가 동작중이므로 동작할 수 없습니다."), MB_ICONWARNING);
		return FALSE;
	}

	UpdateData(TRUE);

	int nRet;
	CString strTemp;

	nRet = SetMoveMode(G_AXIS, SA_CTL_MOVE_MODE_CL_ABSOLUTE);
	if (nRet != SA_CTL_ERROR_NONE)
	{
		strTemp.Format(_T("[Gripper] Fail to set absolute move mode. ( %d )"), nRet);
		AfxMessageBox(strTemp);
		return FALSE;
	}

	nRet = SetVelocity(G_AXIS, 50);
	if (nRet != SA_CTL_ERROR_NONE)
	{
		strTemp.Format(_T("[Gripper] Fail to set velocity. ( %d )"), nRet);
		AfxMessageBox(strTemp);
		return FALSE;
	}

	nRet = SetAcceleration(G_AXIS, 100);
	if (nRet != SA_CTL_ERROR_NONE)
	{
		strTemp.Format(_T("[Gripper] Fail to set acceleration. ( %d )"), nRet);
		AfxMessageBox(strTemp);
		return FALSE;
	}

	nRet = Move(G_AXIS, m_dReleasePos);
	if (nRet != SA_CTL_ERROR_NONE)
	{
		strTemp.Format(_T("[Gripper] Fail to move. ( %d )"), nRet);
		AfxMessageBox(strTemp);
		return FALSE;
	}

	m_bStopGrip = FALSE;

	while (TRUE)
	{
		WaitSec(1);

		if (m_bEndstopReached[G_AXIS] == TRUE)
		{
			strTemp.Format(_T("[Gripper] Reached endstop position."));
			AfxMessageBox(strTemp);
			
			break;
		}

		if (m_bActivelyMoving[G_AXIS] == FALSE || m_bStopGrip == TRUE)
			break;
	}

	return TRUE;
}

void CZPExchangeDlg::OnBnClickedCasGrip()
{
	ModuleTargetPick();
}


BOOL CZPExchangeDlg::ModuleTargetPick()
{
	CString strTemp;
	CString z_move_pos_str;
	CString stage_cas_str;
	CString g_move_pos_str;

	strTemp.Empty();

	strTemp = _T(" ModuleTargetPick() Click!");
	::AfxMessageBox((strTemp));
	SaveLogFile("ZonePlateExChangeModuleReport", _T((LPSTR)(LPCTSTR)(strTemp)));


	UpdateData(TRUE);
	double ZSetPos = 0.0;

	stage_cas_str.Format("%d", m_nStagePosNum);

	if ((m_nStagePosNum >= 0) && (m_nStagePosNum < 8))
	{
		ZSetPos = m_stCassetPos[m_nStagePosNum].zg_pos;
		z_move_pos_str.Format("%lf", ZSetPos);
	}
	else
	{
		strTemp.Format(_T("Casset Num 선택이 올바르지 않습니다. 확인 필요 ( %d )", m_nStagePosNum));
		SaveLogFile("ZonePlateExChangeModuleReport", _T((LPSTR)(LPCTSTR)(strTemp)));
		AfxMessageBox(strTemp, MB_ICONERROR);
		return FALSE;
	}

	// Step 1. Gripper Release Check. 아니면 Release 동작 알람 후 Return.
	g_move_pos_str.Format("%lf", m_dGpos);
	if (m_dGpos < m_dReleasePos)
	{
		strTemp = _T("Gripper 가 Release 되어있지 않습니다. 확인요망");
		::AfxMessageBox(strTemp + _T(" , G Axis Position : ") + g_move_pos_str, MB_ICONERROR);
		SaveLogFile("ZonePlateExChangeModuleReport", _T((LPSTR)(LPCTSTR)(strTemp)));
		return FALSE;

		//if (!GAxisReleaseMove())
		//{
		//	return FALSE;
		//}
	}

	strTemp = _T("Gripper Release 확인 완료 ");
	::AfxMessageBox(strTemp + _T(" , G Axis Position : ") + g_move_pos_str);
	SaveLogFile("ZonePlateExChangeModuleReport", _T((LPSTR)(LPCTSTR)(strTemp)));
	
	// Step 2. Z축 Target 위치 까지 이동.
	strTemp = _T("Z Axis Target 위치 까지 이동 ");
	::AfxMessageBox(strTemp);
	SaveLogFile("ZonePlateExChangeModuleReport", _T((LPSTR)(LPCTSTR)(strTemp)));
	if (!ZAxisTargetMove(ZSetPos))
	{
		strTemp = _T("Z Axis Target 위치 까지 이동 실패 ");
		::AfxMessageBox(strTemp, MB_ICONERROR);
		SaveLogFile("ZonePlateExChangeModuleReport", _T((LPSTR)(LPCTSTR)(strTemp)));
		return FALSE;
	}

	// Step 3. Grip 진행
	strTemp = _T("G Axis Grip 진행 ");
	::AfxMessageBox(strTemp);
	SaveLogFile("ZonePlateExChangeModuleReport", _T((LPSTR)(LPCTSTR)(strTemp)));
	if (!GAxisGripMove())
	{
		return FALSE;
	}

	// Step 4. Z축 스테이지 가능 위치로 이동.
	if (!ZAxisReleaseMove())
	{
		return FALSE;
	}

	AfxMessageBox(_T("Taget Grip Mode 가 완료 되었습니다."));
	return TRUE;
}

BOOL CZPExchangeDlg::ModuleTargetRelease()
{
	CString strTemp;
	double ZSetPos = 0.0;

	UpdateData(TRUE);
	

	strTemp = _T(" ModuleTargetRelease() Click!");
	::AfxMessageBox((strTemp));
	SaveLogFile("ZonePlateExChangeModuleReport", _T((LPSTR)(LPCTSTR)(strTemp)));


	if ((m_nStagePosNum >= 0) && (m_nStagePosNum < 8))
	{
		ZSetPos = m_stCassetPos[m_nStagePosNum].zg_pos;
	}
	else
	{
		strTemp.Format(_T("Casset Num 선택이 올바르지 않습니다. 확인 필요 ( %d )" , m_nStagePosNum));
		SaveLogFile("ZonePlateExChangeModuleReport", _T((LPSTR)(LPCTSTR)(strTemp)));
		AfxMessageBox(strTemp, MB_ICONERROR);
		return FALSE;
	}

	if (m_dGpos > m_dGripPos)
	{
		AfxMessageBox(_T("Griper 가 Open 되어 있습니다. 확인이 필요 합니다."), MB_ICONERROR);
		return FALSE;
	}

	// Step 2. Z축 스테이지 Target 위치로 이동.
	if (!ZAxisTargetMove(ZSetPos))
	{
		return FALSE;
	}

	if (!GAxisReleaseMove())
	{
		return FALSE;
	}

	if (!ZAxisReleaseMove())
	{
		return FALSE;
	}

	AfxMessageBox(_T("Taget Release Mode 가 완료 되었습니다."));
	return TRUE;
}

void CZPExchangeDlg::OnBnClickedCasRel()
{
	ModuleTargetRelease();
}


void CZPExchangeDlg::OnBnClickedCasStop()
{
	int nRet;
	CString strTemp;

	strTemp = _T(" OnBnClickedCasStop() Click!");
	::AfxMessageBox((strTemp));
	SaveLogFile("ZonePlateExChangeModuleReport", _T((LPSTR)(LPCTSTR)(strTemp)));

	if (m_bConnected != TRUE)
	{
		strTemp = _T(" Device not connected!");
		::AfxMessageBox((strTemp), MB_ICONERROR);
		SaveLogFile("ZonePlateExChangeModuleReport", _T((LPSTR)(LPCTSTR)(strTemp)));
		return;
	}

	m_bStopZaxis = TRUE;
	nRet = Stop(Z_AXIS);
	if (nRet != SA_CTL_ERROR_NONE)
	{
		strTemp.Format(_T("[Z-axis] Fail to stop motion. ( %d )"), nRet);
		AfxMessageBox(strTemp);
		SaveLogFile("ZonePlateExChangeModuleReport", _T((LPSTR)(LPCTSTR)(strTemp)));
	}

	m_bStopGrip = TRUE;
	nRet = Stop(G_AXIS);
	if (nRet != SA_CTL_ERROR_NONE)
	{
		strTemp.Format(_T("[Gripper] Fail to stop. ( %d )"), nRet);
		AfxMessageBox(strTemp);
		SaveLogFile("ZonePlateExChangeModuleReport", _T((LPSTR)(LPCTSTR)(strTemp)));
	}

	strTemp.Format(_T("Stop Complete"));
	SaveLogFile("ZonePlateExChangeModuleReport", _T((LPSTR)(LPCTSTR)(strTemp)));

	m_bRunLongTestStopFlag = TRUE;

	strTemp = _T(" >>>>> Stop 버튼 누름");
	m_teststate.SetWindowTextA(strTemp);

	if (m_bActivelyMoving[Z_AXIS] == TRUE)
	{
		if (m_bConnected != TRUE)
		{
			AfxMessageBox(_T("Device not connected."));
			return;
		}

		m_bStopZaxis = TRUE;

		int nRet;
		CString strTemp;

		nRet = Stop(Z_AXIS);
		if (nRet != SA_CTL_ERROR_NONE)
		{
			strTemp.Format(_T("[Z-axis] Fail to stop motion. ( %d )"), nRet);
			AfxMessageBox(strTemp);
		}
	}

	if (m_bActivelyMoving[R_AXIS] == TRUE)
	{
		if (m_bConnected != TRUE)
		{
			AfxMessageBox(_T("Device not connected."));
			return;
		}

		m_bStopRaxis = TRUE;

		int nRet;
		CString strTemp;

		nRet = Stop(R_AXIS);
		if (nRet != SA_CTL_ERROR_NONE)
		{
			strTemp.Format(_T("[R-axis] Fail to stop motion. ( %d )"), nRet);
			AfxMessageBox(strTemp);
		}
	}

	if (m_bActivelyMoving[G_AXIS] == TRUE)
	{
		if (m_bConnected != TRUE)
		{
			AfxMessageBox(_T("Device not connected."));
			return;
		}

		m_bStopGrip = TRUE;

		nRet = Stop(G_AXIS);
		if (nRet != SA_CTL_ERROR_NONE)
		{
			strTemp.Format(_T("[Gripper] Fail to stop. ( %d )"), nRet);
			AfxMessageBox(strTemp);
		}
	}
}


void CZPExchangeDlg::OnBnClickedCasZposSet()
{
	double current_z_pos;
	CString str;
	CString z_move_pos_str;
	CString current_cas_pos_str;
	current_z_pos = m_dZpos;

	z_move_pos_str.Format("%lf", current_z_pos);
	current_cas_pos_str.Format("%d", m_nStagePosNum);

	str = _T(" OnBnClickedCasZposSet() Click!");
	::AfxMessageBox((str), MB_ICONERROR);
	SaveLogFile("ZonePlateExChangeModuleReport", _T((LPSTR)(LPCTSTR)(str + ",  Sel Casset Num : " + current_cas_pos_str)));

	UpdateData(TRUE);
	if ((m_nStagePosNum >= 0) && (m_nStagePosNum < 7))
	{
		m_stCassetPos[m_nStagePosNum].zg_pos = m_dZpos;
		m_stCassetPos[m_nStagePosNum].zr_pos = m_dZpos - m_ZGZROffSet;

		str = _T(" 선택된 카세트 위치에 Z 좌표 저장!");
		::AfxMessageBox((str));
		SaveLogFile("ZonePlateExChangeModuleReport", _T((LPSTR)(LPCTSTR)(str + ",  Sel Casset Num : " + current_cas_pos_str + " , ZPos : " + z_move_pos_str)));
	}
	else
	{
		str = _T(" 카세트 선택이 올바르지 않습니다. 확인필요!");
		::AfxMessageBox((str), MB_ICONERROR);
		SaveLogFile("ZonePlateExChangeModuleReport", _T((LPSTR)(LPCTSTR)(str + ",  Sel Casset Num : " + current_cas_pos_str)));
	}


	OnReadCasettePos();
	SaveTeachingPos();
}


BOOL CZPExchangeDlg::CasLongruntest()
{
	BOOL LongRunResultReturn = TRUE;
	CString str;
	CString ZPosValue;
	CString ZLimitValue;
	double x_move_pos = 0.0;
	double y_move_pos = 0.0;
	double ZSetPos = 0.0;
	double ZrSetPos = 0.0;

	CString x_move_pos_str;
	CString y_move_pos_str;
	CString z_move_pos_str;
	CString g_move_pos_str;
	CString strTemp;
	CString stage_cas_str;
	
	double x_pos = 0.0;
	double y_pos = 0.0;
	double z_pos = 0.0;
	double r_pos = 0.0;
	double g_pos = 0.0;
	double cyc = 0.0;

	int cycle = 0;

	m_bRunLongTestStopFlag = FALSE;

	SaveLogFile("ZonePlateExChangeModuleReport", _T((LPSTR)(LPCTSTR)(" OnBnClickedCasMove")));

	if (m_bActivelyMoving[Z_AXIS] == TRUE || m_bActivelyMoving[R_AXIS] == TRUE || m_bActivelyMoving[G_AXIS] == TRUE)
	{
		str = _T("스테이지가 동작중이므로 동작할 수 없습니다.");
		AfxMessageBox((str), MB_ICONWARNING);
		SaveLogFile("ZonePlateExChangeModuleReport", _T((LPSTR)(LPCTSTR)(str)));
		return FALSE;
	}

	///////////////////////////////////////
	// Stage 움직일 수 있는 Z 축 위치 확인
	///////////////////////////////////////

	str = _T(" Z축 높이 확인 (Stage 움직임 가능 높이 확인)!");
	ZPosValue.Format("%5.5f", m_dZpos);
	ZLimitValue.Format("%5.5f", StageMovingZLimit);
	SaveLogFile("ZonePlateExChangeModuleReport", _T((LPSTR)(LPCTSTR)(str + " Z축 높이 : " + ZPosValue + " , Z축 Limit 높이 : " + ZLimitValue)));

	if (m_dZpos > StageMovingZLimit)
	{
		str = _T(" Z축 높이 확인 필요. Stage 동작 불가!");
		ZPosValue.Format("%5.5f", m_dZpos);
		ZLimitValue.Format("%5.5f", StageMovingZLimit);
	
		::AfxMessageBox((str), MB_ICONERROR);
		SaveLogFile("ZonePlateExChangeModuleReport", _T((LPSTR)(LPCTSTR)(str + " Z축 높이 : " + ZPosValue + " , Z축 Limit 높이 : " + ZLimitValue)));
		return FALSE;
	
	}

	int StagCasNum = 0;


	GRIPPER_GRIP_NOENDSTOP_POS = m_GripperPos_NoEndStop;
	/////////////////////////////////////////
	// Long Run 시작
	/////////////////////////////////////////
	while (!m_bRunLongTestStopFlag)
	{
		/* Long Run 횟수 변수*/
		cycle++;
		strTemp.Format("%d", cycle);
		m_testcnt.SetWindowTextA(strTemp);

		//////////////////////////////////////////////////////////////////
		/* Stage 이동 */
		//////////////////////////////////////////////////////////////////
		strTemp.Format(_T(" >>>>> %d Cycle Start"), cycle);
		SaveLogFile("ZonePlateExChangeModuleReport", _T((LPSTR)(LPCTSTR)(strTemp)));
		SaveLogFile("GripperReport", _T((LPSTR)(LPCTSTR)(strTemp)));
		strTemp.Format("%d", StagCasNum);
		m_currentcassette.SetWindowTextA(strTemp);
		if (StagCasNum == 0)
		{
			x_move_pos = m_stCassetPos[StagCasNum].x_pos;
			y_move_pos = m_stCassetPos[StagCasNum].y_pos;
			ZSetPos = m_stCassetPos[StagCasNum].zg_pos;
			ZrSetPos = m_stCassetPos[StagCasNum].zr_pos;
			StagCasNum = 1;
		}
		else if (StagCasNum == 1)
		{
			x_move_pos = m_stCassetPos[StagCasNum].x_pos;
			y_move_pos = m_stCassetPos[StagCasNum].y_pos;
			ZSetPos = m_stCassetPos[StagCasNum].zg_pos;
			ZrSetPos = m_stCassetPos[StagCasNum].zr_pos;
			StagCasNum = 0;
		}


		strTemp.Format(_T(">>>>> 샘플 %d 카세트 취득 Start ! "), StagCasNum);
		SaveLogFile("ZonePlateExChangeModuleReport", _T((LPSTR)(LPCTSTR)(strTemp)));
		strTemp.Format(_T(">>>>> Stage 카세트 %d 위치로 이동"), StagCasNum);
		SaveLogFile("ZonePlateExChangeModuleReport", _T((LPSTR)(LPCTSTR)(strTemp)));
		
		x_move_pos_str.Format("%3.7f", x_move_pos);
		y_move_pos_str.Format("%3.7f", y_move_pos);
		stage_cas_str.Format("%d", StagCasNum);

		/* 해당 카세트 위치로 Stage 이동 */
		if (g_pNavigationStage->MoveAbsolutePosition(x_move_pos, y_move_pos) != XY_NAVISTAGE_OK)
		{
			str = _T(" Navigation Stage 동작 불가!");
			::AfxMessageBox((str), MB_ICONERROR);
			SaveLogFile("ZonePlateExChangeModuleReport", _T((LPSTR)(LPCTSTR)(str)));
			return FALSE;
		}
		else
		{
			str = _T(">>>>> Navigation Stage Moving");
			SaveLogFile("ZonePlateExChangeModuleReport", _T((LPSTR)(LPCTSTR)(str + " 카세트 번호 : " + stage_cas_str + " , X Position : " + x_move_pos_str + " , Y Position :" + y_move_pos_str)));
		}
		
		str.Format(_T(" G Axis Release "));
		SaveLogFile("ZonePlateExChangeModuleReport", _T((LPSTR)(LPCTSTR)(str)));

		if (!GAxisReleaseMove())
		{
			str.Format(_T(" G Axis Release Fail!"));
			SaveLogFile("ZonePlateExChangeModuleReport", _T((LPSTR)(LPCTSTR)(str)));
			return FALSE;
		}
		
		str.Format(_T(" G Axis Release 완료 "));
		SaveLogFile("ZonePlateExChangeModuleReport", _T((LPSTR)(LPCTSTR)(str)));
		/////////////////////////////////////////////////////////
		/* Pick Part */
		/////////////////////////////////////////////////////////


		strTemp = _T(">>>>> Module Target Pick!");
		SaveLogFile("ZonePlateExChangeModuleReport", _T((LPSTR)(LPCTSTR)(strTemp)));
		m_teststate.SetWindowTextA(strTemp);

		UpdateData(TRUE);

		//////////////////////////////////////////////////////////
		// Z축 높이 확인
		//////////////////////////////////////////////////////////
		z_move_pos_str.Format("%lf", ZSetPos);
		strTemp = _T(">>>>> ZAxis Position :: ") + z_move_pos_str;
		SaveLogFile("ZonePlateExChangeModuleReport", _T((LPSTR)(LPCTSTR)(strTemp)));

		///////////////////////////////////////////////////////////////////////////
		// Step 1. Gripper Release Check. 아니면 Release 동작 알람 후 Return.
		// 현재 Gripper 설정된 Release 의 상태 인지 확인 
		// Grripper Release 값은 UI Setting 가능 및 확인 
		///////////////////////////////////////////////////////////////////////////

		g_move_pos_str.Format("%lf", m_dGpos);

		strTemp = _T(">>>>> Gripper Release 되어 있는지 확인");
		SaveLogFile("ZonePlateExChangeModuleReport", _T((LPSTR)(LPCTSTR)(strTemp)));
		strTemp = _T(">>>>> Gripper Position :: ") + g_move_pos_str;
		SaveLogFile("ZonePlateExChangeModuleReport", _T((LPSTR)(LPCTSTR)(strTemp)));

		if (m_dGpos < m_dReleasePos - 5.0)
		{
			strTemp = _T(">>>>> Gripper 가 Release 되어있지 않습니다. 확인요망");
			::AfxMessageBox(strTemp + _T(" , G Axis Position : ") + g_move_pos_str, MB_ICONERROR);
			SaveLogFile("ZonePlateExChangeModuleReport", _T((LPSTR)(LPCTSTR)(strTemp)));
			return FALSE;
		}

		strTemp = _T(">>>>> Gripper Release 확인 완료 ");
		SaveLogFile("ZonePlateExChangeModuleReport", _T((LPSTR)(LPCTSTR)(strTemp)));

		////////////////////////////////////////////////
		// Step 2. Z축 Target 위치 까지 이동.
		////////////////////////////////////////////////
		strTemp = _T(">>>>> Z Axis Target 위치 까지 이동 ");
		SaveLogFile("ZonePlateExChangeModuleReport", _T((LPSTR)(LPCTSTR)(strTemp)));
		strTemp = _T(">>>>> Z Axis Target Position ::") + z_move_pos_str;
		SaveLogFile("ZonePlateExChangeModuleReport", _T((LPSTR)(LPCTSTR)(strTemp)));

		strTemp = _T(">>>>> Module Target Pick ! ( Z축 Taget 위치 까지 이동 중 )");
		m_teststate.SetWindowTextA(strTemp);


		if (!ZAxisTargetMove(ZSetPos))
		{
			strTemp = _T(">>>>> Z Axis Target 위치 까지 이동 실패 ");
			::AfxMessageBox(strTemp, MB_ICONERROR);
			SaveLogFile("ZonePlateExChangeModuleReport", _T((LPSTR)(LPCTSTR)(strTemp)));
			strTemp = _T(">>>>> Module Target Pick ! (Z축 Taget 위치 까지 이동 실패 )");
			m_teststate.SetWindowTextA(strTemp);
			return FALSE;
		}
		strTemp = _T(">>>>> Z Axis Target 위치 까지 이동 완료 ");
		SaveLogFile("ZonePlateExChangeModuleReport", _T((LPSTR)(LPCTSTR)(strTemp)));

		//////////////////////////////////////////////
		// Step 3. Grip 진행 (5um/s -> 10nm/s) 
		///////////////////////////////////////////////

		strTemp = _T(">>>>> G Axis Grip 진행 ");
		SaveLogFile("ZonePlateExChangeModuleReport", _T((LPSTR)(LPCTSTR)(strTemp)));

		strTemp = _T(">>>>> Module Target Pick ! ( G축 Taget 위치에서 Grip 진행 중 )");
		m_teststate.SetWindowTextA(strTemp);

		if (!GAxisGripMove())
		{
			strTemp = _T(">>>>> Module Target Pick ! (G축 Taget 위치에서 Grip 진행 실패 )");
			m_teststate.SetWindowTextA(strTemp);
			return FALSE;
		}
		strTemp = _T(">>>>> G Axis Grip 완료 ");
		SaveLogFile("ZonePlateExChangeModuleReport", _T((LPSTR)(LPCTSTR)(strTemp)));

		m_dEndStopPos = m_dGpos;
		strTemp.Format(_T(" >>>>>>> [%d 번카세트 Target 위치 ] Gipper Position. ::  ( %lf )"), StagCasNum, m_dEndStopPos);
		SaveLogFile("ZonePlateExChangeModuleReport", _T((LPSTR)(LPCTSTR)(strTemp)));
		SaveLogFile("GripperReport", _T((LPSTR)(LPCTSTR)(strTemp)));


		x_pos = g_pNavigationStage->GetPosmm(STAGE_X_AXIS);
		y_pos = g_pNavigationStage->GetPosmm(STAGE_Y_AXIS);
		z_pos = m_dZpos;
		r_pos = m_dRpos;
		g_pos = m_dGpos;
		cyc = cycle;

		strTemp.Format(_T(" Xpos : %lf , Ypos : %lf, Zpos : %lf , Rpos : %lf, Gpos : %lf , Cassete : %d 번 , Cycle : %d 번 (Target 위치)"), x_pos, y_pos, z_pos, r_pos, g_pos,StagCasNum, cyc);
		SaveLogFile("StageReport", _T((LPSTR)(LPCTSTR)(strTemp)));

		//////////////////////////////////////////////////////////////////
		// Steip 4. Z축 스테이지 천천히 3mm 하강 ( 3mm 하강 )
		//////////////////////////////////////////////////////////////////

		strTemp = _T(">>>>> Z Grip Zone Move Mode ! ");
		SaveLogFile("ZonePlateExChangeModuleReport", _T((LPSTR)(LPCTSTR)(strTemp)));

		strTemp = _T(">>>>> Module Target Pick ! ( Z축 소재 Grip 후 3mm 하강 중 )");
		m_teststate.SetWindowTextA(strTemp);

		if (!ZAxisReGripPosZoneMove(ZSetPos))
		{
			strTemp = _T(">>>>> Module Target Pick ! ( Z축 소재 Grip 후 3mm 하강 실패 )");
			m_teststate.SetWindowTextA(strTemp);
			return FALSE;
		}
		strTemp = _T(">>>>> Z Grip Zone Move Mode 완료! ");
		SaveLogFile("ZonePlateExChangeModuleReport", _T((LPSTR)(LPCTSTR)(strTemp)));

		//////////////////////////////////////////////
		// Step 3. 3mm 하강 후 Grip 재 진행 ( 10 nm/s)
		///////////////////////////////////////////////

		strTemp = _T(">>>>> G Axis Grip 진행 (3mm 하강 후 ) ");
		SaveLogFile("ZonePlateExChangeModuleReport", _T((LPSTR)(LPCTSTR)(strTemp)));

		strTemp = _T(">>>>> Module Target Pick ! ( G축 3mm 하강 후 Grip 진행 중 )");
		m_teststate.SetWindowTextA(strTemp);

		if (!GAxisGripZoneMove_NoEndStop())
		{
			strTemp = _T(">>>>> Module Target Pick ! ( Z축 3mm 하강 후 G축 다시 Grip 실패 )");
			m_teststate.SetWindowTextA(strTemp);
			return FALSE;
		}
		strTemp.Format(_T(">>>>> G Axis Grip 완료 (3mm 하강 후 ) "));
		SaveLogFile("ZonePlateExChangeModuleReport", _T((LPSTR)(LPCTSTR)(strTemp)));

		m_dEndStopPos = m_dGpos;
		strTemp.Format(_T(" >>>>>>> [%d 번카세트 Target 위치 (3mm 하강 후) ] Gipper Position. ::  ( %lf )"), StagCasNum, m_dEndStopPos);
		SaveLogFile("ZonePlateExChangeModuleReport", _T((LPSTR)(LPCTSTR)(strTemp)));
		SaveLogFile("GripperReport", _T((LPSTR)(LPCTSTR)(strTemp)));


		x_pos = g_pNavigationStage->GetPosmm(STAGE_X_AXIS);
		y_pos = g_pNavigationStage->GetPosmm(STAGE_Y_AXIS);
		z_pos = m_dZpos;
		r_pos = m_dRpos;
		g_pos = m_dGpos;
		cyc = cycle;

		strTemp.Format(_T(" Xpos : %lf , Ypos : %lf, Zpos : %lf , Rpos : %lf, Gpos : %lf , Cassete : %d 번 , Cycle : %d 번 (3mm 하강 후 )"), x_pos, y_pos, z_pos, r_pos, g_pos, StagCasNum, cyc);
		SaveLogFile("StageReport", _T((LPSTR)(LPCTSTR)(strTemp)));


		//////////////////////////////////////////////////////////////////
		// Steip 5. Z축 스테이지 Release 위치로 이동 
		//////////////////////////////////////////////////////////////////

		strTemp.Format(_T(">>>>> Z Axis Stage Release Move Mode ! "));
		SaveLogFile("ZonePlateExChangeModuleReport", _T((LPSTR)(LPCTSTR)(strTemp)));

		strTemp.Format(_T(">>>>> Module Target Pick ! ( Z축 소재 Grip 후 Release 위치로 하강 중 )"));
		m_teststate.SetWindowTextA(strTemp);

		if (!ZAxisReleaseMove())
		{
			strTemp = _T(">>>>> Module Target Pick ! ( G축 Grip 후 Z축 Release 위치 이동 실패 )");
			m_teststate.SetWindowTextA(strTemp);

			return FALSE;
		}
		strTemp.Format(_T(">>>>> Z Axis Stage Release Move Mode 완료! "));
		SaveLogFile("ZonePlateExChangeModuleReport", _T((LPSTR)(LPCTSTR)(strTemp)));

		/////////////////////////////////////////////////////////////////////
		// Step 6. Grip 재 진행 (50nm/s -> 10nm/s)
		////////////////////////////////////////////////////////////////////
		//strTemp.Format(_T(">>>>> Module Target Pick ! ( Z축 (Release 위치) 후 G축 다시 Grip 중 )"));
		//m_teststate.SetWindowTextA(strTemp);
		//
		//strTemp.Format(_T(">>>>> G Axis Grip 재 진행 (Release 위치) "));
		//SaveLogFile("ZonePlateExChangeModuleReport", _T((LPSTR)(LPCTSTR)(strTemp)));
		//if (!GAxisGripZoneMove())
		//{
		//
		//	strTemp = _T(">>>>> Module Target Pick ! ( Z축 (Release 위치) G축 다시 Grip 실패 )");
		//	m_teststate.SetWindowTextA(strTemp);
		//	return FALSE;
		//}
		//strTemp.Format(_T(">>>>> G Axis Grip 재 진행 완료 (Release 위치)"));
		//SaveLogFile("ZonePlateExChangeModuleReport", _T((LPSTR)(LPCTSTR)(strTemp)));
		//
		//m_dEndStopPos = m_dGpos;
		//strTemp.Format(_T(" >>>>>>> [ %d 번카세트 하강 후 (Release 위치)] Gipper Position. ::  ( %lf )"), StagCasNum, m_dEndStopPos);
		//SaveLogFile("ZonePlateExChangeModuleReport", _T((LPSTR)(LPCTSTR)(strTemp)));
		//SaveLogFile("GripperReport", _T((LPSTR)(LPCTSTR)(strTemp)));
		//
		//x_pos = g_pNavigationStage->GetPosmm(STAGE_ACS_X_AXIS);
		//y_pos = g_pNavigationStage->GetPosmm(STAGE_ACS_Y_AXIS);
		//z_pos = m_dZpos;
		//r_pos = m_dRpos;
		//g_pos = m_dGpos;
		//cyc = cycle;
		//
		//strTemp.Format(_T(" Xpos : %lf , Ypos : %lf, Zpos : %lf , Rpos : %lf, Gpos : %lf , Cassete : %d 번, Cycle : %d 번 (Release 위치)"), x_pos, y_pos, z_pos, r_pos, g_pos, StagCasNum, cyc);
		//SaveLogFile("StageReport", _T((LPSTR)(LPCTSTR)(strTemp)));

		strTemp.Format(_T(">>>>> 샘플 %d 카세트 취득 완료 ! "), StagCasNum);
		SaveLogFile("ZonePlateExChangeModuleReport", _T((LPSTR)(LPCTSTR)(strTemp)));

		strTemp.Format(_T(">>>>> Module Target Pick ! ( G축 Grip 후 Z축 Release 위치로 이동 완료 )"));
		m_teststate.SetWindowTextA(strTemp);

		

		//////////////////////////////////////////////////////////////////
		/* Stage 이동 */
		//////////////////////////////////////////////////////////////////
		
		//StagCasNum = 1;

		x_move_pos = m_stCassetPos[StagCasNum].x_pos;
		y_move_pos = m_stCassetPos[StagCasNum].y_pos;
		ZSetPos = m_stCassetPos[StagCasNum].zg_pos;
		ZrSetPos = m_stCassetPos[StagCasNum].zg_pos;

		strTemp.Format("%d", StagCasNum);
		m_currentcassette.SetWindowTextA(strTemp);

		strTemp.Format(_T(">>>>> 샘플 %d 카세트로 Start ! "), StagCasNum);
		SaveLogFile("ZonePlateExChangeModuleReport", _T((LPSTR)(LPCTSTR)(strTemp)));

		strTemp.Format(_T(">>>>> Stage 카세트 %d 위치로 이동", StagCasNum));
		SaveLogFile("ZonePlateExChangeModuleReport", _T((LPSTR)(LPCTSTR)(strTemp)));
		
		x_move_pos_str.Format("%3.7f", x_move_pos);
		y_move_pos_str.Format("%3.7f", y_move_pos);
		stage_cas_str.Format("%d", StagCasNum);


		if (g_pNavigationStage->MoveAbsolutePosition(x_move_pos, y_move_pos) != XY_NAVISTAGE_OK)
		{
			str = _T(" >>>>> Navigation Stage 동작 불가!");
			::AfxMessageBox((str), MB_ICONERROR);
			SaveLogFile("ZonePlateExChangeModuleReport", _T((LPSTR)(LPCTSTR)(str)));
			return FALSE;
		}
		else
		{
			str = _T(" >>>>> Navigation Stage Moving");
			SaveLogFile("ZonePlateExChangeModuleReport", _T((LPSTR)(LPCTSTR)(str + " 카세트 번호 : " + stage_cas_str + " , X Position : " + x_move_pos_str + " , Y Position :" + y_move_pos_str)));
		}
		
		
		/////////////////////////////////////////////////////////
		/* Release Part */
		/////////////////////////////////////////////////////////
		CString strTemp;
		//ZSetPos = 0.0;
		
		UpdateData(TRUE);
		
		strTemp.Format(_T(" >>>>> Module Target Release!"));
		SaveLogFile("ZonePlateExChangeModuleReport", _T((LPSTR)(LPCTSTR)(strTemp)));
		m_teststate.SetWindowTextA(strTemp);
		
		//ZSetPos = m_stCassetPos[StagCasNum].z_pos;
		
		//if (Gpos > m_dGripPos + 30)
		//{
		//	AfxMessageBox(_T("Griper 가 Open 되어 있습니다. 확인이 필요 합니다."), MB_ICONERROR);
		//	return;
		//}
		
		//////////////////////////////////////////////////////////////
		// Step 2. Z축 스테이지 Target 위치로 이동.
		//////////////////////////////////////////////////////////////
		strTemp.Format(_T(">>>>> Z축 Target 위치 까지 이동 !"));
		SaveLogFile("ZonePlateExChangeModuleReport", _T((LPSTR)(LPCTSTR)(strTemp)));
		
		strTemp.Format(_T(" >>>>> Module Target Release ! (Z축 Target 위치 이동 중 )"));
		m_teststate.SetWindowTextA(strTemp);

		if (!ZAxisTargetMove(ZrSetPos))
		{
			strTemp.Format(_T(" >>>>> Module Target Release ! (Z축 Target 위치 이동 실패 )"));
			m_teststate.SetWindowTextA(strTemp);
			return FALSE;
		}
		strTemp.Format(_T(">>>>> Z축 Target 위치 까지 이동 완료 !"));
		SaveLogFile("ZonePlateExChangeModuleReport", _T((LPSTR)(LPCTSTR)(strTemp)));
		
		strTemp.Format(_T(">>>>> G축 Release Mode !"));
		SaveLogFile("ZonePlateExChangeModuleReport", _T((LPSTR)(LPCTSTR)(strTemp)));

		strTemp.Format(_T(" >>>>> Module Target Release ! (G축 Release 진행 중 )"));
		m_teststate.SetWindowTextA(strTemp);

		//////////////////////////////////////////////////////////////
		// Step 3. G축 Release 진행 (Open).
		//////////////////////////////////////////////////////////////
		if (!GAxisReleaseMove())
		{
			strTemp.Format(_T(" >>>>> Module Target Release ! (G축 Release 실패 )"));
			m_teststate.SetWindowTextA(strTemp);
			return FALSE;
		}
		strTemp.Format( _T(">>>>> G축 Release Mode 완료 !"));
		SaveLogFile("ZonePlateExChangeModuleReport", _T((LPSTR)(LPCTSTR)(strTemp)));
		
		strTemp.Format(_T(">>>>> Z축 Release Mode !"));
		SaveLogFile("ZonePlateExChangeModuleReport", _T((LPSTR)(LPCTSTR)(strTemp)));

		strTemp.Format(_T(" >>>>> Module Target Release ! (Z축 Release 진행 중 )"));
		m_teststate.SetWindowTextA(strTemp);

		//////////////////////////////////////////////////////////////
		// Step 4. Z축 Release 진행.
		//////////////////////////////////////////////////////////////
		if (!ZAxisReleaseMove())
		{
			strTemp = _T(" >>>>> Module Target Release ! (Z축 Release 진행 실패 )");
			m_teststate.SetWindowTextA(strTemp);
			return FALSE;
		}
		strTemp.Format(_T(">>>>> Z축 Release Mode 완료 !"));
		SaveLogFile("ZonePlateExChangeModuleReport", _T((LPSTR)(LPCTSTR)(strTemp)));
		
		strTemp.Format(_T(">>>>> 샘플 %d 카세트로 이동 완료 ! "), StagCasNum);
		SaveLogFile("ZonePlateExChangeModuleReport", _T((LPSTR)(LPCTSTR)(strTemp)));
		
		strTemp.Format(_T(">>>>> %d Cycle 완료 !"), cycle);
		SaveLogFile("ZonePlateExChangeModuleReport", _T((LPSTR)(LPCTSTR)(strTemp)));
		

		x_pos = g_pNavigationStage->GetPosmm(STAGE_X_AXIS);
		y_pos = g_pNavigationStage->GetPosmm(STAGE_Y_AXIS);
		z_pos = m_dZpos;
		r_pos = m_dRpos;
		g_pos = m_dGpos;
		cyc = cycle;

		strTemp.Format(_T(" Xpos : %lf , Ypos : %lf, Zpos : %lf , Rpos : %lf, Gpos : %lf , Cassete : %d 번 , Cycle : %d 번"), x_pos, y_pos, z_pos, r_pos, g_pos, StagCasNum,cyc);
		SaveLogFile("StageReport", _T((LPSTR)(LPCTSTR)(strTemp)));


		/* Stage Break Time				*/
		/* 1 cycle 마다 진행 ( 3 분 )   */

		if (!GAxisOrginMove())
		{
			strTemp = _T(" >>>>> G Axis Origin Fail ! (G축 origin 진행 실패 )");
			m_teststate.SetWindowTextA(strTemp);
			return FALSE;
		}
		strTemp.Format(_T(">>>>> G축 Origin 완료 !"));
		SaveLogFile("ZonePlateExChangeModuleReport", _T((LPSTR)(LPCTSTR)(strTemp)));

		((CStatic*)GetDlgItem(IDC_ICON_ZPEX_GRIPPER_BREAK))->SetIcon(m_LedIcon[1]);
		strTemp.Format(_T(">>>>> 3분 Break time Start !"));
		SaveLogFile("ZonePlateExChangeModuleReport", _T((LPSTR)(LPCTSTR)(strTemp)));
		WaitSec(180);

		strTemp.Format(_T(">>>>> 3분 Break time End !"));
		SaveLogFile("ZonePlateExChangeModuleReport", _T((LPSTR)(LPCTSTR)(strTemp)));
		((CStatic*)GetDlgItem(IDC_ICON_ZPEX_GRIPPER_BREAK))->SetIcon(m_LedIcon[0]);
	}
			
			
			
}


BOOL CZPExchangeDlg::OnlyOneCasLongruntest()
{
	BOOL OnlyOneLongRunResultReturn = TRUE;
	CString str;
	CString ZPosValue;
	CString ZLimitValue;
	double x_move_pos = 0.0;
	double y_move_pos = 0.0;
	double ZSetPos = 0.0;
	double ZRSetPos = 0.0;

	CString x_move_pos_str;
	CString y_move_pos_str;
	CString z_move_pos_str;
	CString zr_move_pos_str;
	CString g_move_pos_str;
	CString strTemp;
	CString stage_cas_str;

	double x_pos = 0.0;
	double y_pos = 0.0;
	double z_pos = 0.0;
	double zr_pos = 0.0;
	double r_pos = 0.0;
	double g_pos = 0.0;
	double cyc = 0.0;

	int cycle = 0;

	m_bRunLongTestStopFlag = FALSE;

	SaveLogFile("ZonePlateExChangeModuleReport", _T((LPSTR)(LPCTSTR)(" OnBnClickedCasMove")));

	if (m_bActivelyMoving[Z_AXIS] == TRUE || m_bActivelyMoving[R_AXIS] == TRUE || m_bActivelyMoving[G_AXIS] == TRUE)
	{
		str = _T("스테이지가 동작중이므로 동작할 수 없습니다.");
		AfxMessageBox((str), MB_ICONWARNING);
		SaveLogFile("ZonePlateExChangeModuleReport", _T((LPSTR)(LPCTSTR)(str)));
		return FALSE;
	}

	///////////////////////////////////////
	// Stage 움직일 수 있는 Z 축 위치 확인
	///////////////////////////////////////

	str = _T(" Z축 높이 확인 (Stage 움직임 가능 높이 확인)!");
	ZPosValue.Format("%5.5f", m_dZpos);
	ZLimitValue.Format("%5.5f", StageMovingZLimit);
	SaveLogFile("ZonePlateExChangeModuleReport", _T((LPSTR)(LPCTSTR)(str + " Z축 높이 : " + ZPosValue + " , Z축 Limit 높이 : " + ZLimitValue)));

	if (m_dZpos > StageMovingZLimit)
	{
		str = _T(" Z축 높이 확인 필요. Stage 동작 불가!");
		ZPosValue.Format("%5.5f", m_dZpos);
		ZLimitValue.Format("%5.5f", StageMovingZLimit);

		::AfxMessageBox((str), MB_ICONERROR);
		SaveLogFile("ZonePlateExChangeModuleReport", _T((LPSTR)(LPCTSTR)(str + " Z축 높이 : " + ZPosValue + " , Z축 Limit 높이 : " + ZLimitValue)));
		return FALSE;

	}

	int StagCasNum = 0;

	/////////////////////////////////////////
	// Long Run 시작
	/////////////////////////////////////////
	while (!m_bRunLongTestStopFlag)
	{
		/* Long Run 횟수 변수*/
		cycle++;
		strTemp.Format("%d", cycle);
		m_testcnt.SetWindowTextA(strTemp);

		//////////////////////////////////////////////////////////////////
		/* Stage 이동 */
		//////////////////////////////////////////////////////////////////

		strTemp.Format(_T(">>>>> 샘플 %d 카세트 취득 Start ! "), StagCasNum);
		SaveLogFile("ZonePlateExChangeModuleReport", _T((LPSTR)(LPCTSTR)(strTemp)));
		strTemp.Format(_T(">>>>> Stage 카세트 %d 위치로 이동"), StagCasNum);
		SaveLogFile("ZonePlateExChangeModuleReport", _T((LPSTR)(LPCTSTR)(strTemp)));

		x_move_pos_str.Format("%3.7f", x_move_pos);
		y_move_pos_str.Format("%3.7f", y_move_pos);
		stage_cas_str.Format("%d", StagCasNum);

		/////////////////////////////////////////////////////////
		/* Pick Part */
		/////////////////////////////////////////////////////////

		strTemp = _T(">>>>> Module Target Pick!");
		SaveLogFile("ZonePlateExChangeModuleReport", _T((LPSTR)(LPCTSTR)(strTemp)));
		m_teststate.SetWindowTextA(strTemp);

		UpdateData(TRUE);

		//////////////////////////////////////////////////////////
		// Z축 높이 확인
		//////////////////////////////////////////////////////////
		ZSetPos = m_stCassetPos[StagCasNum].zg_pos;
		z_move_pos_str.Format("%lf", ZSetPos);
		strTemp = _T(">>>>> ZAxis Position :: ") + z_move_pos_str;
		SaveLogFile("ZonePlateExChangeModuleReport", _T((LPSTR)(LPCTSTR)(strTemp)));

		///////////////////////////////////////////////////////////////////////////
		// Step 1. Gripper Release Check. 아니면 Release 동작 알람 후 Return.
		// 현재 Gripper 설정된 Release 의 상태 인지 확인 
		// Grripper Release 값은 UI Setting 가능 및 확인 
		///////////////////////////////////////////////////////////////////////////

		g_move_pos_str.Format("%lf", m_dGpos);

		strTemp = _T(">>>>> Gripper Release 되어 있는지 확인");
		SaveLogFile("ZonePlateExChangeModuleReport", _T((LPSTR)(LPCTSTR)(strTemp)));
		strTemp = _T(">>>>> Gripper Position :: ") + g_move_pos_str;
		SaveLogFile("ZonePlateExChangeModuleReport", _T((LPSTR)(LPCTSTR)(strTemp)));

		if (m_dGpos < m_dReleasePos - 5.0)
		{
			strTemp = _T(">>>>> Gripper 가 Release 되어있지 않습니다. 확인요망");
			::AfxMessageBox(strTemp + _T(" , G Axis Position : ") + g_move_pos_str, MB_ICONERROR);
			SaveLogFile("ZonePlateExChangeModuleReport", _T((LPSTR)(LPCTSTR)(strTemp)));
			return FALSE;
		}

		strTemp = _T(">>>>> Gripper Release 확인 완료 ");
		SaveLogFile("ZonePlateExChangeModuleReport", _T((LPSTR)(LPCTSTR)(strTemp)));

		////////////////////////////////////////////////
		// Step 2. Z축 Target 위치 까지 이동.
		////////////////////////////////////////////////
		strTemp = _T(">>>>> Z Axis Target 위치 까지 이동 ");
		SaveLogFile("ZonePlateExChangeModuleReport", _T((LPSTR)(LPCTSTR)(strTemp)));
		strTemp = _T(">>>>> Z Axis Target Position ::") + z_move_pos_str;
		SaveLogFile("ZonePlateExChangeModuleReport", _T((LPSTR)(LPCTSTR)(strTemp)));

		strTemp = _T(">>>>> Module Target Pick ! ( Z축 Taget 위치 까지 이동 중 )");
		m_teststate.SetWindowTextA(strTemp);


		if (!ZAxisTargetMove(ZSetPos))
		{
			strTemp = _T(">>>>> Z Axis Target 위치 까지 이동 실패 ");
			::AfxMessageBox(strTemp, MB_ICONERROR);
			SaveLogFile("ZonePlateExChangeModuleReport", _T((LPSTR)(LPCTSTR)(strTemp)));
			strTemp = _T(">>>>> Module Target Pick ! (Z축 Taget 위치 까지 이동 실패 )");
			m_teststate.SetWindowTextA(strTemp);
			return FALSE;
		}
		strTemp = _T(">>>>> Z Axis Target 위치 까지 이동 완료 ");
		SaveLogFile("ZonePlateExChangeModuleReport", _T((LPSTR)(LPCTSTR)(strTemp)));

		//////////////////////////////////////////////
		// Step 3. Grip 진행 (5um/s -> 10nm/s) 
		///////////////////////////////////////////////

		strTemp = _T(">>>>> G Axis Grip 진행 ");
		SaveLogFile("ZonePlateExChangeModuleReport", _T((LPSTR)(LPCTSTR)(strTemp)));

		strTemp = _T(">>>>> Module Target Pick ! ( G축 Taget 위치에서 Grip 진행 중 )");
		m_teststate.SetWindowTextA(strTemp);

		if (!GAxisGripMove())
		{
			strTemp = _T(">>>>> Module Target Pick ! (G축 Taget 위치에서 Grip 진행 실패 )");
			m_teststate.SetWindowTextA(strTemp);
			return FALSE;
		}
		strTemp = _T(">>>>> G Axis Grip 완료 ");
		SaveLogFile("ZonePlateExChangeModuleReport", _T((LPSTR)(LPCTSTR)(strTemp)));

		m_dEndStopPos = m_dGpos;
		strTemp.Format(_T(" >>>>>>> [%d 번카세트 Target 위치 ] Gipper Position. ::  ( %lf )"), StagCasNum, m_dEndStopPos);
		SaveLogFile("ZonePlateExChangeModuleReport", _T((LPSTR)(LPCTSTR)(strTemp)));
		SaveLogFile("GripperReport", _T((LPSTR)(LPCTSTR)(strTemp)));


		x_pos = g_pNavigationStage->GetPosmm(STAGE_X_AXIS);
		y_pos = g_pNavigationStage->GetPosmm(STAGE_Y_AXIS);
		z_pos = m_dZpos;
		r_pos = m_dRpos;
		g_pos = m_dGpos;
		cyc = cycle;

		strTemp.Format(_T(" Xpos : %lf , Ypos : %lf, Zpos : %lf , Rpos : %lf, Gpos : %lf , Cassete : %d 번 , Cycle : %d 번 (Target 위치)"), x_pos, y_pos, z_pos, r_pos, g_pos, StagCasNum, cyc);
		SaveLogFile("StageReport", _T((LPSTR)(LPCTSTR)(strTemp)));

		//////////////////////////////////////////////////////////////////
		// Steip 4. Z축 스테이지 천천히 3mm 하강 ( 3mm 하강 )
		//////////////////////////////////////////////////////////////////

		strTemp = _T(">>>>> Z Grip Zone Move Mode ! ");
		SaveLogFile("ZonePlateExChangeModuleReport", _T((LPSTR)(LPCTSTR)(strTemp)));

		strTemp = _T(">>>>> Module Target Pick ! ( Z축 소재 Grip 후 3mm 하강 중 )");
		m_teststate.SetWindowTextA(strTemp);

		if (!ZAxisReGripPosZoneMove(ZSetPos))
		{
			strTemp = _T(">>>>> Module Target Pick ! ( Z축 소재 Grip 후 3mm 하강 실패 )");
			m_teststate.SetWindowTextA(strTemp);
			return FALSE;
		}
		strTemp = _T(">>>>> Z Grip Zone Move Mode 완료! ");
		SaveLogFile("ZonePlateExChangeModuleReport", _T((LPSTR)(LPCTSTR)(strTemp)));

		//////////////////////////////////////////////
		// Step 3. 3mm 하강 후 Grip 재 진행 ( 10 nm/s)
		///////////////////////////////////////////////

		strTemp = _T(">>>>> G Axis Grip 진행 (3mm 하강 후 ) ");
		SaveLogFile("ZonePlateExChangeModuleReport", _T((LPSTR)(LPCTSTR)(strTemp)));

		strTemp = _T(">>>>> Module Target Pick ! ( G축 3mm 하강 후 Grip 진행 중 )");
		m_teststate.SetWindowTextA(strTemp);

		if (!GAxisGripZoneMove())
		{
			strTemp = _T(">>>>> Module Target Pick ! ( Z축 3mm 하강 후 G축 다시 Grip 실패 )");
			m_teststate.SetWindowTextA(strTemp);
			return FALSE;
		}
		strTemp = _T(">>>>> G Axis Grip 완료 (3mm 하강 후 ) ");
		SaveLogFile("ZonePlateExChangeModuleReport", _T((LPSTR)(LPCTSTR)(strTemp)));

		m_dEndStopPos = m_dGpos;
		strTemp.Format(_T(" >>>>>>> [%d 번카세트 Target 위치 (3mm 하강 후) ] Gipper Position. ::  ( %lf )"), StagCasNum, m_dEndStopPos);
		SaveLogFile("ZonePlateExChangeModuleReport", _T((LPSTR)(LPCTSTR)(strTemp)));
		SaveLogFile("GripperReport", _T((LPSTR)(LPCTSTR)(strTemp)));


		x_pos = g_pNavigationStage->GetPosmm(STAGE_X_AXIS);
		y_pos = g_pNavigationStage->GetPosmm(STAGE_Y_AXIS);
		z_pos = m_dZpos;
		r_pos = m_dRpos;
		g_pos = m_dGpos;
		cyc = cycle;

		strTemp.Format(_T(" Xpos : %lf , Ypos : %lf, Zpos : %lf , Rpos : %lf, Gpos : %lf , Cassete : %d 번 , Cycle : %d 번 (3mm 하강 후 )"), x_pos, y_pos, z_pos, r_pos, g_pos, StagCasNum, cyc);
		SaveLogFile("StageReport", _T((LPSTR)(LPCTSTR)(strTemp)));


		//////////////////////////////////////////////////////////////////
		// Steip 5. Z축 스테이지 Release 위치로 이동 
		//////////////////////////////////////////////////////////////////

		strTemp = _T(">>>>> Z Axis Stage Release Move Mode ! ");
		SaveLogFile("ZonePlateExChangeModuleReport", _T((LPSTR)(LPCTSTR)(strTemp)));

		strTemp = _T(">>>>> Module Target Pick ! ( Z축 소재 Grip 후 Release 위치로 하강 중 )");
		m_teststate.SetWindowTextA(strTemp);

		if (!ZAxisReleaseMove())
		{
			strTemp = _T(">>>>> Module Target Pick ! ( G축 Grip 후 Z축 Release 위치 이동 실패 )");
			m_teststate.SetWindowTextA(strTemp);

			return FALSE;
		}
		strTemp = _T(">>>>> Z Axis Stage Release Move Mode 완료! ");
		SaveLogFile("ZonePlateExChangeModuleReport", _T((LPSTR)(LPCTSTR)(strTemp)));

		/////////////////////////////////////////////////////////////////////
		// Step 6. Grip 재 진행 (50nm/s -> 10nm/s)
		////////////////////////////////////////////////////////////////////
		//strTemp = _T(">>>>> Module Target Pick ! ( Z축 (Release 위치) 후 G축 다시 Grip 중 )");
		//m_teststate.SetWindowTextA(strTemp);
		//
		//strTemp = _T(">>>>> G Axis Grip 재 진행 (Release 위치) ");
		//SaveLogFile("ZonePlateExChangeModuleReport", _T((LPSTR)(LPCTSTR)(strTemp)));
		//if (!GAxisGripZoneMove())
		//{
		//
		//	strTemp = _T(">>>>> Module Target Pick ! ( Z축 (Release 위치) G축 다시 Grip 실패 )");
		//	m_teststate.SetWindowTextA(strTemp);
		//	return FALSE;
		//}
		//strTemp = _T(">>>>> G Axis Grip 재 진행 완료 (Release 위치)");
		//SaveLogFile("ZonePlateExChangeModuleReport", _T((LPSTR)(LPCTSTR)(strTemp)));

		m_dEndStopPos = m_dGpos;
		strTemp.Format(_T(" >>>>>>> [ %d 번카세트 하강 후 (Release 위치)] Gipper Position. ::  ( %lf )"), StagCasNum, m_dEndStopPos);
		SaveLogFile("ZonePlateExChangeModuleReport", _T((LPSTR)(LPCTSTR)(strTemp)));
		SaveLogFile("GripperReport", _T((LPSTR)(LPCTSTR)(strTemp)));

		x_pos = g_pNavigationStage->GetPosmm(STAGE_ACS_X_AXIS);
		y_pos = g_pNavigationStage->GetPosmm(STAGE_ACS_Y_AXIS);
		z_pos = m_dZpos;
		r_pos = m_dRpos;
		g_pos = m_dGpos;
		cyc = cycle;

		strTemp.Format(_T(" Xpos : %lf , Ypos : %lf, Zpos : %lf , Rpos : %lf, Gpos : %lf , Cassete : %d 번, Cycle : %d 번 (Release 위치)"), x_pos, y_pos, z_pos, r_pos, g_pos, StagCasNum, cyc);
		SaveLogFile("StageReport", _T((LPSTR)(LPCTSTR)(strTemp)));

		strTemp = _T(">>>>> 샘플 %d 카세트 취득 완료 ! ", StagCasNum);
		SaveLogFile("ZonePlateExChangeModuleReport", _T((LPSTR)(LPCTSTR)(strTemp)));

		strTemp = _T(">>>>> Module Target Pick ! ( G축 Grip 후 Z축 Release 위치로 이동 완료 )");
		m_teststate.SetWindowTextA(strTemp);



		//////////////////////////////////////////////////////////////////
		/* Stage 이동 */
		//////////////////////////////////////////////////////////////////
		
		ZRSetPos = m_stCassetPos[StagCasNum].zr_pos;

		strTemp.Format("%d", StagCasNum);
		m_currentcassette.SetWindowTextA(strTemp);

		strTemp = _T(">>>>> 샘플 %d 카세트로 Start ! ", StagCasNum);
		SaveLogFile("ZonePlateExChangeModuleReport", _T((LPSTR)(LPCTSTR)(strTemp)));

		strTemp = _T(">>>>> Stage 카세트 %d 위치로 이동", StagCasNum);
		SaveLogFile("ZonePlateExChangeModuleReport", _T((LPSTR)(LPCTSTR)(strTemp)));

		x_move_pos_str.Format("%3.7f", x_move_pos);
		y_move_pos_str.Format("%3.7f", y_move_pos);
		stage_cas_str.Format("%d", StagCasNum);

		/////////////////////////////////////////////////////////
		/* Release Part */
		/////////////////////////////////////////////////////////
		CString strTemp;
		//ZSetPos = 0.0;

		UpdateData(TRUE);

		strTemp = _T(" >>>>> Module Target Release!");
		SaveLogFile("ZonePlateExChangeModuleReport", _T((LPSTR)(LPCTSTR)(strTemp)));
		m_teststate.SetWindowTextA(strTemp);

		//ZSetPos = m_stCassetPos[StagCasNum].z_pos;

		//if (Gpos > m_dGripPos + 30)
		//{
		//	AfxMessageBox(_T("Griper 가 Open 되어 있습니다. 확인이 필요 합니다."), MB_ICONERROR);
		//	return;
		//}

		//////////////////////////////////////////////////////////////
		// Step 2. Z축 스테이지 Target 위치로 이동.
		//////////////////////////////////////////////////////////////
		strTemp = _T(">>>>> Z축 Release Target 위치 까지 이동 !");
		SaveLogFile("ZonePlateExChangeModuleReport", _T((LPSTR)(LPCTSTR)(strTemp)));

		strTemp = _T(" >>>>> Module Target Release ! (Z축 Release Target 위치 이동 중 )");
		m_teststate.SetWindowTextA(strTemp);

		if (!ZAxisTargetMove(ZRSetPos))
		{
			strTemp = _T(" >>>>> Module Target Release ! (Z축 Target 위치 이동 실패 )");
			m_teststate.SetWindowTextA(strTemp);
			return FALSE;
		}
		strTemp = _T(">>>>> Z축 Release Target 위치 까지 이동 완료 !");
		SaveLogFile("ZonePlateExChangeModuleReport", _T((LPSTR)(LPCTSTR)(strTemp)));

		strTemp = _T(">>>>> G축 Release Mode !");
		SaveLogFile("ZonePlateExChangeModuleReport", _T((LPSTR)(LPCTSTR)(strTemp)));

		strTemp = _T(" >>>>> Module Target Release ! (G축 Release 진행 중 )");
		m_teststate.SetWindowTextA(strTemp);

		//////////////////////////////////////////////////////////////
		// Step 3. G축 Release 진행 (Open).
		//////////////////////////////////////////////////////////////
		if (!GAxisReleaseMove())
		{
			strTemp = _T(" >>>>> Module Target Release ! (G축 Release 실패 )");
			m_teststate.SetWindowTextA(strTemp);
			return FALSE;
		}
		strTemp = _T(">>>>> G축 Release Mode 완료 !");
		SaveLogFile("ZonePlateExChangeModuleReport", _T((LPSTR)(LPCTSTR)(strTemp)));

		strTemp = _T(">>>>> Z축 Release Mode !");
		SaveLogFile("ZonePlateExChangeModuleReport", _T((LPSTR)(LPCTSTR)(strTemp)));

		strTemp = _T(" >>>>> Module Target Release ! (Z축 Release 진행 중 )");
		m_teststate.SetWindowTextA(strTemp);

		//////////////////////////////////////////////////////////////
		// Step 4. Z축 Release 진행.
		//////////////////////////////////////////////////////////////
		if (!ZAxisReleaseMove())
		{
			strTemp = _T(" >>>>> Module Target Release ! (Z축 Release 진행 실패 )");
			m_teststate.SetWindowTextA(strTemp);
			return FALSE;
		}
		strTemp = _T(">>>>> Z축 Release Mode 완료 !");
		SaveLogFile("ZonePlateExChangeModuleReport", _T((LPSTR)(LPCTSTR)(strTemp)));

		strTemp = _T(">>>>> 샘플 %d 카세트로 이동 완료 ! ", StagCasNum);
		SaveLogFile("ZonePlateExChangeModuleReport", _T((LPSTR)(LPCTSTR)(strTemp)));

		strTemp = _T(">>>>> %d Cycle 완료 !", cycle);
		SaveLogFile("ZonePlateExChangeModuleReport", _T((LPSTR)(LPCTSTR)(strTemp)));


		x_pos = g_pNavigationStage->GetPosmm(STAGE_X_AXIS);
		y_pos = g_pNavigationStage->GetPosmm(STAGE_Y_AXIS);
		z_pos = m_dZpos;
		zr_pos = m_dZpos;
		r_pos = m_dRpos;
		g_pos = m_dGpos;
		cyc = cycle;

		strTemp.Format(_T(" Xpos : %lf , Ypos : %lf, Zpos : %lf , Rpos : %lf, Gpos : %lf , Cassete : %d 번 , Cycle : %d 번"), x_pos, y_pos, zr_pos, r_pos, g_pos, StagCasNum, cyc);
		SaveLogFile("StageReport", _T((LPSTR)(LPCTSTR)(strTemp)));

	}
}



void CZPExchangeDlg::OnBnClickedCasLongrun()
{
	((CStatic*)GetDlgItem(IDC_ICON_ZPEX_LONGRUN))->SetIcon(m_LedIcon[1]);
	if (!CasLongruntest())
	{
		((CStatic*)GetDlgItem(IDC_ICON_ZPEX_LONGRUN))->SetIcon(m_LedIcon[0]);
	}
}


void CZPExchangeDlg::OnBnClickedCasLongrunTestStop()
{
	m_bRunLongTestStopFlag = TRUE;
	CString strTemp;

	strTemp = _T(" >>>>> Stop 버튼 누름");
	m_teststate.SetWindowTextA(strTemp);

	if (m_bActivelyMoving[Z_AXIS] == TRUE)
	{
		if (m_bConnected != TRUE)
		{
			AfxMessageBox(_T("Device not connected."));
			return;
		}

		m_bStopZaxis = TRUE;

		int nRet;
		CString strTemp;

		nRet = Stop(Z_AXIS);
		if (nRet != SA_CTL_ERROR_NONE)
		{
			strTemp.Format(_T("[Z-axis] Fail to stop motion. ( %d )"), nRet);
			AfxMessageBox(strTemp);
		}
	}

	if (m_bActivelyMoving[R_AXIS] == TRUE)
	{
		if (m_bConnected != TRUE)
		{
			AfxMessageBox(_T("Device not connected."));
			return;
		}

		m_bStopRaxis = TRUE;

		int nRet;
		CString strTemp;

		nRet = Stop(R_AXIS);
		if (nRet != SA_CTL_ERROR_NONE)
		{
			strTemp.Format(_T("[R-axis] Fail to stop motion. ( %d )"), nRet);
			AfxMessageBox(strTemp);
		}
	}

	if (m_bActivelyMoving[G_AXIS] == TRUE)
	{
		if (m_bConnected != TRUE)
		{
			AfxMessageBox(_T("Device not connected."));
			return;
		}

		m_bStopGrip = TRUE;

		int nRet;
		CString strTemp;

		nRet = Stop(G_AXIS);
		if (nRet != SA_CTL_ERROR_NONE)
		{
			strTemp.Format(_T("[Gripper] Fail to stop. ( %d )"), nRet);
			AfxMessageBox(strTemp);
		}
	}
}


void CZPExchangeDlg::OnBnClickedButtonMove2()
{
	int nRet;
	CString strTemp;

	if (m_bConnected != TRUE)
	{
		AfxMessageBox(_T("Device not connected."));
		return;
	}


	if (CreateOutputBuffer() != SA_CTL_ERROR_NONE)
	{
		AfxMessageBox("CreateOutputBuffer Error");
		return;
	}

	UpdateData(TRUE);

	/*
	
		Z, G Velocity Setting
	
	*/
	
	nRet = SetVelocity(Z_AXIS, m_dVelocityG);
	if (nRet != SA_CTL_ERROR_NONE)
	{
		strTemp.Format(_T("[Z-axis] Fail to set velocity. ( %d )"), nRet);
		AfxMessageBox(strTemp);
		return;
	}

	nRet = SetVelocity(G_AXIS, m_dVelocityG);
	if (nRet != SA_CTL_ERROR_NONE)
	{
		strTemp.Format(_T("[Gripper] Fail to set velocity. ( %d )"), nRet);
		AfxMessageBox(strTemp);
		return;
	}

	/*

		Z, G Acceleration Setting

	*/

	nRet = SetAcceleration(Z_AXIS, m_dAccelerationG);
	if (nRet != SA_CTL_ERROR_NONE)
	{
		strTemp.Format(_T("[Z-axis] Fail to set acceleration. ( %d )"), nRet);
		AfxMessageBox(strTemp);
		return;
	}


	nRet = SetAcceleration(G_AXIS, m_dAccelerationG);
	if (nRet != SA_CTL_ERROR_NONE)
	{
		strTemp.Format(_T("[Gripper] Fail to set acceleration. ( %d )"), nRet);
		AfxMessageBox(strTemp);
		return;
	}
	/*

		Z, G Relative Mode Setting

	*/

	nRet = SetMoveMode(Z_AXIS, SA_CTL_MOVE_MODE_CL_RELATIVE);
	if (nRet != SA_CTL_ERROR_NONE)
	{
		strTemp.Format(_T("[Z-axis] Fail to set relative move mode. ( %d )"), nRet);
		AfxMessageBox(strTemp);
		return;
	}

	nRet = SetMoveMode(G_AXIS, SA_CTL_MOVE_MODE_CL_RELATIVE);
	if (nRet != SA_CTL_ERROR_NONE)
	{
		strTemp.Format(_T("[Gripper] Fail to set relative move mode. ( %d )"), nRet);
		AfxMessageBox(strTemp);
		return;
	}


	nRet = Move(Z_AXIS, m_dMoveDistG);
	if (nRet != SA_CTL_ERROR_NONE)
	{
		strTemp.Format(_T("[Z-axis] Fail to move. ( %d )"), nRet);
		AfxMessageBox(strTemp);
		return;
	}


	nRet = Move(G_AXIS, m_dMoveDistG);
	if (nRet != SA_CTL_ERROR_NONE)
	{
		strTemp.Format(_T("[Gripper] Fail to move. ( %d )"), nRet);
		AfxMessageBox(strTemp);
		return;
	}

	m_bStopGrip = FALSE;
	m_bStopZaxis = FALSE;

	while (TRUE)
	{
		WaitSec(1);

		if (m_bEndstopReached[Z_AXIS] == TRUE || m_bEndstopReached[G_AXIS] == TRUE)
		{
			strTemp.Format(_T("[Z-axis] Reached endstop position."));
			AfxMessageBox(strTemp);
			break;
		}

		if (m_bActivelyMoving[Z_AXIS] == FALSE && m_bActivelyMoving[G_AXIS] == FALSE)
		{
			break;
		}
		if (m_bStopGrip == TRUE || m_bStopZaxis == TRUE)
		{
			break;
		}
			
	}

	//UpdateData(TRUE);

	if (g_pZPExchange->FlushOutputBuffer() != SA_CTL_ERROR_NONE)
	{
		AfxMessageBox("FlushOutputBuffer Error");
		return;
	}
}


void CZPExchangeDlg::OnBnClickedButtonZpmCancelBuffer()
{
	int nRet;
	nRet = CancelOutputBuffer();
	if (nRet != SA_CTL_ERROR_NONE)
	{
		CString sTemp;
		sTemp.Format(_T("Failed to cancel output buffer. (Error Code : %d)"), nRet);
		AfxMessageBox(sTemp);
	}
}




void CZPExchangeDlg::OnBnClickedCasSoloLongrun()
{
	OnlyOneCasLongruntest();
}


void CZPExchangeDlg::OnBnClickedButtonTargetPosR()
{
	UpdateData(TRUE);
	double ZSetPos = m_dZaxisReleaseTargetPos;
	ZAxisTargetMove(ZSetPos);
}


void CZPExchangeDlg::OnBnClickedButtonTargetPosSetR()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
}


void CZPExchangeDlg::OnBnClickedCasHome()
{
	if (m_bConnected != TRUE)
	{
		AfxMessageBox(_T("Device not connected."));
		return;
	}

	if (m_bActivelyMoving[Z_AXIS] == TRUE || m_bActivelyMoving[R_AXIS] == TRUE || m_bActivelyMoving[G_AXIS] == TRUE)
	{
		AfxMessageBox(_T("스테이지가 동작중이므로 동작할 수 없습니다."), MB_ICONWARNING);
		return;
	}

	UpdateData(TRUE);

	int nRet;
	CString strTemp;

	nRet = SetMoveMode(Z_AXIS, SA_CTL_MOVE_MODE_CL_ABSOLUTE);
	if (nRet != SA_CTL_ERROR_NONE)
	{
		strTemp.Format(_T("[Z-axis Home] Fail to set absolute move mode. ( %d )"), nRet);
		AfxMessageBox(strTemp);
		return;
	}

	nRet = SetVelocity(Z_AXIS, 100);
	if (nRet != SA_CTL_ERROR_NONE)
	{
		strTemp.Format(_T("[Z-axis Home] Fail to set velocity. ( %d )"), nRet);
		AfxMessageBox(strTemp);
		return;
	}

	nRet = SetAcceleration(Z_AXIS, m_dAccelerationZ);
	if (nRet != SA_CTL_ERROR_NONE)
	{
		strTemp.Format(_T("[Z-axis Home] Fail to set acceleration. ( %d )"), nRet);
		AfxMessageBox(strTemp);
		return;
	}

	nRet = Move(Z_AXIS, m_dMoveDistZ);
	if (nRet != SA_CTL_ERROR_NONE)
	{
		strTemp.Format(_T("[Z-axis Home] Fail to move. ( %d )"), nRet);
		AfxMessageBox(strTemp);
		return;
	}

	while (TRUE)
	{
		WaitSec(1);

		if (m_bEndstopReached[Z_AXIS] == TRUE)
		{
			strTemp.Format(_T("[Z-axis Home] Reached endstop position."));
			AfxMessageBox(strTemp);
			break;
		}

		if (m_bActivelyMoving[Z_AXIS] == FALSE || m_bStopZaxis == TRUE)
			break;
	}


	nRet = SetMoveMode(G_AXIS, SA_CTL_MOVE_MODE_CL_ABSOLUTE);
	if (nRet != SA_CTL_ERROR_NONE)
	{
		strTemp.Format(_T("[Gripper Home] Fail to set absolute move mode. ( %d )"), nRet);
		AfxMessageBox(strTemp);
		return;
	}

	nRet = SetVelocity(G_AXIS, 100);
	if (nRet != SA_CTL_ERROR_NONE)
	{
		strTemp.Format(_T("[Gripper Home] Fail to set velocity. ( %d )"), nRet);
		AfxMessageBox(strTemp);
		return;
	}

	nRet = SetAcceleration(G_AXIS, m_dAccelerationG);
	if (nRet != SA_CTL_ERROR_NONE)
	{
		strTemp.Format(_T("[Gripper Home] Fail to set acceleration. ( %d )"), nRet);
		AfxMessageBox(strTemp);
		return;
	}

	nRet = Move(G_AXIS, 0);
	if (nRet != SA_CTL_ERROR_NONE)
	{
		strTemp.Format(_T("[Gripper Home] Fail to move. ( %d )"), nRet);
		AfxMessageBox(strTemp);
		return;
	}

	m_bStopGrip = FALSE;

	while (TRUE)
	{
		WaitSec(1);

		if (m_bEndstopReached[G_AXIS] == TRUE)
		{
			strTemp.Format(_T("[Gripper Home] Reached endstop position."));
			AfxMessageBox(strTemp);
			break;
		}

		if (m_bActivelyMoving[G_AXIS] == FALSE || m_bStopGrip == TRUE)
			break;
	}


}


BOOL CZPExchangeDlg::GAxisOrginMove()
{
	if (m_bConnected != TRUE)
	{
		AfxMessageBox(_T("Device not connected."));
		return FALSE;
	}

	if (m_bActivelyMoving[Z_AXIS] == TRUE || m_bActivelyMoving[R_AXIS] == TRUE || m_bActivelyMoving[G_AXIS] == TRUE)
	{
		AfxMessageBox(_T("스테이지가 동작중이므로 동작할 수 없습니다."), MB_ICONWARNING);
		return FALSE;
	}

	UpdateData(TRUE);

	int nRet;
	CString strTemp;

	nRet = SetMoveMode(G_AXIS, SA_CTL_MOVE_MODE_CL_ABSOLUTE);
	if (nRet != SA_CTL_ERROR_NONE)
	{
		strTemp.Format(_T("[Gripper] Fail to set absolute move mode. ( %d )"), nRet);
		AfxMessageBox(strTemp);
		return FALSE;
	}

	nRet = SetVelocity(G_AXIS, 100);
	if (nRet != SA_CTL_ERROR_NONE)
	{
		strTemp.Format(_T("[Gripper] Fail to set velocity. ( %d )"), nRet);
		AfxMessageBox(strTemp);
		return FALSE;
	}

	nRet = SetAcceleration(G_AXIS, 100);
	if (nRet != SA_CTL_ERROR_NONE)
	{
		strTemp.Format(_T("[Gripper] Fail to set acceleration. ( %d )"), nRet);
		AfxMessageBox(strTemp);
		return FALSE;
	}

	nRet = Move(G_AXIS, 0);
	if (nRet != SA_CTL_ERROR_NONE)
	{
		strTemp.Format(_T("[Gripper] Fail to move. ( %d )"), nRet);
		AfxMessageBox(strTemp);
		return FALSE;
	}

	m_bStopGrip = FALSE;

	while (TRUE)
	{
		WaitSec(1);

		if (m_bEndstopReached[G_AXIS] == TRUE)
		{
			strTemp.Format(_T("[Gripper] Reached endstop position."));
			AfxMessageBox(strTemp);

			break;
		}

		if (m_bActivelyMoving[G_AXIS] == FALSE || m_bStopGrip == TRUE)
			break;
	}

	return TRUE;
}