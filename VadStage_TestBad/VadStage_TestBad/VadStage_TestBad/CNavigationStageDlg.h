﻿#pragma once


// CNavigationStageDlg 대화 상자

class CNavigationStageDlg : public CDialogEx, public CACSMC4UCtrl
{
	DECLARE_DYNAMIC(CNavigationStageDlg)

public:
	CNavigationStageDlg(CWnd* pParent = nullptr);   // 표준 생성자입니다.
	virtual ~CNavigationStageDlg();

// 대화 상자 데이터입니다.
//#ifdef AFX_DESIGN_TIME
	enum { IDD = IDD_NAVIGATION_STAGE_DIALOG };
//#endif

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 지원입니다.

	DECLARE_MESSAGE_MAP()
public:
	HICON	m_LedIcon[3];

	afx_msg void OnDestroy();
	afx_msg void OnTimer(UINT_PTR nIDEvent);
	virtual BOOL OnInitDialog();
	CString m_strMovingDistance;
	afx_msg void OnBnClickedButtonMovepos();

	BOOL EStopFlag = FALSE;

	double m_dMovingDistance;

	int MoveAbsolutePosition(double dXmm, double dYmm);
	int MoveRelativePosition(int nAxis, double dPosmm);

	void Check_X_axis_amp_state();
	void Check_Y_axis_amp_state();
	void Check_home_state();
	void StageCheck();


	int home_flag_x = 0;
	int home_flag_y = 0;


	CString m_strGetXPos;
	CString m_strGetYPos;
	
	afx_msg void OnBnClickedButtonYplus();
	afx_msg void OnEnChangeEditIncstep();
	afx_msg void OnBnClickedButtonYminus();
	afx_msg void OnBnClickedButtonXplus();
	afx_msg void OnBnClickedButtonXminus();
	afx_msg void OnDeltaposIncstepspin(NMHDR *pNMHDR, LRESULT *pResult);
	afx_msg void OnBnClickedButtonStop();
	afx_msg void OnBnClickedButtonZeroreturn();
	afx_msg void OnBnClickedButtonXhome();
	afx_msg void OnBnClickedButtonYhome();
	afx_msg void OnBnClickedButtonXenable();
	afx_msg void OnBnClickedButtonYenable();
	afx_msg void OnBnClickedButtonFaultclear();
	CEdit m_XPosCtrl;
	CEdit m_YPosCtrl;
	afx_msg void OnBnClickedButtonXdisable();
	afx_msg void OnBnClickedButtonYdisable();
	afx_msg void OnBnClickedButton0Move();
	afx_msg void OnBnClickedButtonConnect();
	afx_msg void OnBnClickedButtonDisconnect();
	CEdit m_Connect;
	afx_msg void OnBnClickedButtonBufferStart();
	afx_msg void OnBnClickedButtonBufferStop();
	bool stage_buffer_state;
	CEdit m_getxposload;
	CEdit m_getyposload;
	afx_msg void OnBnClickedButtonBufferStart2();
	afx_msg void OnBnClickedButtonBufferStop2();
	CEdit m_getxposload_max;
	CEdit m_getxposload_min;
	CEdit m_getyposload_max;
	CEdit m_getyposload_min;
	CEdit m_edit_x_inpos;
	CEdit m_edit_y_inpos;
};
