﻿
// VadStage_TestBadDlg.cpp: 구현 파일
//

#include "pch.h"
#include "framework.h"
#include "Include.h"
#include "Extern.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif


CVadStageTestBadDlg::CVadStageTestBadDlg(CWnd* pParent /*=nullptr*/)
	: CDialogEx(IDD_VADSTAGE_TESTBAD_DIALOG, pParent)
{
	m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);
}

void CVadStageTestBadDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
}

BEGIN_MESSAGE_MAP(CVadStageTestBadDlg, CDialogEx)
	ON_WM_PAINT()
	ON_WM_QUERYDRAGICON()
	ON_WM_DESTROY()
	ON_BN_CLICKED(IDC_BUTTON_EXIT, &CVadStageTestBadDlg::OnBnClickedButtonExit)
END_MESSAGE_MAP()


// CVadStageTestBadDlg 메시지 처리기

BOOL CVadStageTestBadDlg::OnInitDialog()
{
	CDialogEx::OnInitDialog();

	// 이 대화 상자의 아이콘을 설정합니다.  응용 프로그램의 주 창이 대화 상자가 아닐 경우에는
	//  프레임워크가 이 작업을 자동으로 수행합니다.
	SetIcon(m_hIcon, TRUE);			// 큰 아이콘을 설정합니다.
	SetIcon(m_hIcon, FALSE);		// 작은 아이콘을 설정합니다.

	// TODO: 여기에 추가 초기화 작업을 추가합니다.
	//MIL ALLOC 초기화
	MappAlloc(M_DEFAULT, &g_milApplication);
	MsysAlloc(M_SYSTEM_HOST, M_DEF_SYSTEM_NUM, M_COMPLETE, &g_milSystemHost);
	MsysAlloc(M_SYSTEM_DEFAULT, M_DEF_SYSTEM_NUM, M_COMPLETE, &g_milSystemGigEVision);

	//ShowWindow(SW_MAXIMIZE);
	CreateModules();
	
	// Create Modules
	//CreateModules();

	return TRUE;  // 포커스를 컨트롤에 설정하지 않으면 TRUE를 반환합니다.
}

// 대화 상자에 최소화 단추를 추가할 경우 아이콘을 그리려면
//  아래 코드가 필요합니다.  문서/뷰 모델을 사용하는 MFC 응용 프로그램의 경우에는
//  프레임워크에서 이 작업을 자동으로 수행합니다.

void CVadStageTestBadDlg::OnPaint()
{
	if (IsIconic())
	{
		CPaintDC dc(this); // 그리기를 위한 디바이스 컨텍스트입니다.

		SendMessage(WM_ICONERASEBKGND, reinterpret_cast<WPARAM>(dc.GetSafeHdc()), 0);

		// 클라이언트 사각형에서 아이콘을 가운데에 맞춥니다.
		int cxIcon = GetSystemMetrics(SM_CXICON);
		int cyIcon = GetSystemMetrics(SM_CYICON);
		CRect rect;
		GetClientRect(&rect);
		int x = (rect.Width() - cxIcon + 1) / 2;
		int y = (rect.Height() - cyIcon + 1) / 2;

		// 아이콘을 그립니다.
		dc.DrawIcon(x, y, m_hIcon);
	}
	else
	{
		CDialogEx::OnPaint();
	}
}

// 사용자가 최소화된 창을 끄는 동안에 커서가 표시되도록 시스템에서
//  이 함수를 호출합니다.
HCURSOR CVadStageTestBadDlg::OnQueryDragIcon()
{
	return static_cast<HCURSOR>(m_hIcon);
}



void CVadStageTestBadDlg::OnDestroy()
{
	CDialogEx::OnDestroy();

	KillTimer(NAVISTAGE_UPDATE_TIMER);
	KillTimer(TEST_UPDATE_TIMER);
	KillTimer(LOGGING_START);
	KillTimer(STAGE_UPDATE_TIMER);
	KillTimer(STAGE_REPORT_TIMER);
	KillTimer(ZPEXCHANGE_UPDATE_TIMER);
	KillTimer(REVOLVER_UPDATE_TIMER);
	KillTimer(AFM_UPDATE_TIMER);

	WaitSec(1);

	DELETEDIALOG(g_pNavigationStage);
	DELETEDIALOG(g_pTab);
	DELETEDIALOG(g_pStageRun);
	//DELETEDIALOG(g_pTemp);
	DELETEDIALOG(g_pStageReport);
	DELETEDIALOG(g_pZPExchange);
	DELETEDIALOG(g_pCamZPExchange);
	DELETEDIALOG(g_pDetecterApertureChange);
	DELETEDIALOG(g_pLRevolver);
	DELETEDIALOG(g_pAF);

	if (g_milSystemGigEVision != M_NULL)
	{
		MsysFree(g_milSystemGigEVision);
		g_milSystemGigEVision = M_NULL;
	}

	if (g_milSystemHost != M_NULL)
	{
		MsysFree(g_milSystemHost);
		g_milSystemHost = M_NULL;
	}

	if (g_milApplication != M_NULL)
	{
		MappFree(g_milApplication);
		g_milApplication = M_NULL;
	}
}


int CVadStageTestBadDlg::CreateModules()
{
	CRect rROI;
	rROI.SetRectEmpty();
	int nRet = -1;

	CREATEDIALOG(g_pNavigationStage, CNavigationStageDlg, this, CPoint(rROI.left + 30, rROI.top + 20), SW_SHOW);
	if (g_pNavigationStage != NULL)
	{
		//nRet = g_pNavigationStage->ConnectACSController(ETHERNET, g_pConfig->m_chIP[ETHERNET_NAVI_STAGE], g_pConfig->m_nPORT[ETHERNET_NAVI_STAGE]);
		//
		// VAD STAGE ETHERNET CONFIGRATION
		// VAD STAGE IP ADDRESS :: 10.0.0.100 (Default)
		// VAD STAGE CONTROLLER PORT : 701 (Default)
		//
		nRet = g_pNavigationStage->ConnectACSController(ETHERNET, "10.0.0.100", 701 );

		if (nRet == FALSE)
		{
		//	g_pCommStat->NavigationStageCommStatus(FALSE);
		//	SetEquipmentStatus(_T("NAVI STAGE"));
		//	g_pLoadingScreen.SetTextMessage(_T("Navigation Stage Connection Fail!"));
			g_pNavigationStage->m_Connect.SetWindowTextA(_T("Stage COnnection Fail"));
			AfxMessageBox(_T("Stage Connection Fail"));
		//	WaitSec(3);
		}
		else if (nRet == TRUE)
		{
			g_pNavigationStage->m_Connect.SetWindowTextA(_T("Stage Connection"));
		//	AfxMessageBox(_T("Stage Connection"));
		//	g_pCommStat->NavigationStageCommStatus(TRUE);
		//	g_pNavigationStage->SetLaserSwitchingFunction(FALSE);
		//	g_pLoadingScreen.SetTextMessage(_T("Navigation Stage Connection Success!"));
		}
	}


	CREATEDIALOG(g_pTab, CTabDlg, this, CPoint(rROI.left + 400, rROI.top + 20), SW_SHOW);
	if (g_pTab != NULL)
	{
	
		if (nRet == FALSE)
		{
	
		}
		else if (nRet == TRUE)
		{
	
		}
	}

	CREATEDIALOG(g_pStageRun, CStateTestDlg, this, CPoint(rROI.left + 430, rROI.top + 100), SW_SHOW);
	if (g_pStageRun != NULL)
	{
	
		if (nRet == FALSE)
		{
	
		}
		else if (nRet == TRUE)
		{
	
		}
	}

	CREATEDIALOG(g_pAF, CAFModuleDlg, this, CPoint(rROI.left + 430, rROI.top + 130), SW_HIDE);
	if (g_pStageRun != NULL)
	{

		if (nRet == FALSE)
		{

		}
		else if (nRet == TRUE)
		{

		}
	}

	CREATEDIALOG(g_pLRevolver, CLinearRevolverDlg, this, CPoint(rROI.left + 1100, rROI.top + 130), SW_HIDE);
	if (g_pStageRun != NULL)
	{

		if (nRet == FALSE)
		{

		}
		else if (nRet == TRUE)
		{

		}
	}


	//CREATEDIALOG(g_pTemp, CTempDialog, this, CPoint(rROI.left + 400, rROI.top + 550), SW_SHOW);
	//if (g_pTemp != NULL)
	//{
	//	char *str = _T("192.168.0.114");
	//	nRet = g_pTemp->OpenTcpIpSocket(str,502, FALSE);
	//	if (nRet != 0)
	//	{
	//		AfxMessageBox(_T("Temp Moudle Connect Fail"));
	//	}
	//	else 
	//	{
	//
	//	}
	//}

	CREATEDIALOG(g_pStageReport, CStageReportDlg, this, CPoint(rROI.left + 430, rROI.top + 650), SW_SHOW);
	if (g_pStageReport != NULL)
	{
	
	}

	CREATEDIALOG(g_pZPExchange, CZPExchangeDlg, this, CPoint(rROI.left + 430, rROI.top + 100), SW_HIDE);
	if (g_pZPExchange != NULL)
	{

	}

	CREATEDIALOG(g_pCamZPExchange, CCamZPExchangeDlg, this, CPoint(rROI.left + 1600, rROI.top), SW_SHOW);
	if(g_pCamZPExchange != NULL)
	{
		nRet = g_pCamZPExchange->OpenDevice();
	}

	CREATEDIALOG(g_pDetecterApertureChange, CDetecterApertureChangeDlg, this, CPoint(rROI.left + 430, rROI.top +100), SW_HIDE);
	if (g_pDetecterApertureChange != NULL)
	{
		//nRet = g_pCamZPExchange->OpenDevice();
	}

	return 0;
}

void CVadStageTestBadDlg::OnBnClickedButtonExit()
{
	g_pStageRun->StageLongRunStopFlag = true;
	CDialogEx::OnOK();


}
