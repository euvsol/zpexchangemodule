﻿// CAutoMessageDlg.cpp: 구현 파일
//

#include "pch.h"
#include "VadStage_TestBad.h"
#include "CAutoMessageDlg.h"
#include "afxdialogex.h"


// CAutoMessageDlg 대화 상자

IMPLEMENT_DYNAMIC(CAutoMessageDlg, CDialogEx)

CAutoMessageDlg::CAutoMessageDlg(CWnd* pParent /*=nullptr*/)
	: CDialogEx(IDD_AUTOMESSAGE_DIALOG, pParent)
	, m_strMessageString(_T(""))
{
	m_strMessage = _T("");
	m_strMessageString = _T("");
	m_WaitTime = 1;
}

CAutoMessageDlg::~CAutoMessageDlg()
{
}

void CAutoMessageDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
	DDX_Text(pDX, IDC_MESSAGE_TXT, m_strMessageString);
}


BEGIN_MESSAGE_MAP(CAutoMessageDlg, CDialogEx)
	ON_WM_TIMER()
END_MESSAGE_MAP()


// CAutoMessageDlg 메시지 처리기

BOOL CAutoMessageDlg::OnInitDialog()
{
	CDialogEx::OnInitDialog();

	UpdateData(false);
	SetTimer(1, m_WaitTime * 1000, NULL);

	return TRUE;  // return TRUE unless you set the focus to a control
				  // 예외: OCX 속성 페이지는 FALSE를 반환해야 합니다.
}

void CAutoMessageDlg::OnTimer(UINT_PTR nIDEvent)
{
	// TODO: 여기에 메시지 처리기 코드를 추가 및/또는 기본값을 호출합니다.

	KillTimer(1);
	OnOK();

	CDialogEx::OnTimer(nIDEvent);
}


INT_PTR CAutoMessageDlg::DoModal(CString str, double WaitTime)
{
	// TODO: 여기에 특수화된 코드를 추가 및/또는 기본 클래스를 호출합니다.
	m_strMessageString = str;
	m_WaitTime = WaitTime;
	return CDialogEx::DoModal();
}


void CAutoMessageDlg::OnOK()
{
	CDialogEx::OnOK();
}



