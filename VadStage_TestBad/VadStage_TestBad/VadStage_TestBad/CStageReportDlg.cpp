﻿// CStageReportDlg.cpp: 구현 파일
//


#include "pch.h"
#include "Include.h"
#include "Extern.h"
#include <iomanip>


// CStageReportDlg 대화 상자

IMPLEMENT_DYNAMIC(CStageReportDlg, CDialogEx)

CStageReportDlg::CStageReportDlg(CWnd* pParent /*=nullptr*/)
	: CDialogEx(IDD_STAGE_REPORT_DIALOG, pParent)
{
	get_xLoad_data  = nullptr;
	get_yLoad_data  = nullptr;
	get_xInPos_data = nullptr;
	get_yInPos_data = nullptr;
}

CStageReportDlg::~CStageReportDlg()
{
}

void CStageReportDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_EDIT_AXIS_0_LOAD, m_Axis0_load);
	DDX_Control(pDX, IDC_EDIT_AXIS_1_LOAD, m_Axis1_load);
	DDX_Control(pDX, IDC_EDIT_AXIS_0_INPOS, m_Axis0_Inpos);
	DDX_Control(pDX, IDC_EDIT_AXIS_1_INPOS, m_Axis1_Inpos);
	DDX_Control(pDX, IDC_EDIT_AXIS_0_INPOS_AVG, m_Axis0_Inpos_ave);
	DDX_Control(pDX, IDC_EDIT_AXIS_1_INPOS_AVG, m_Axis1_Inpos_ave);
	DDX_Control(pDX, IDC_EDIT_AXIS_0_INPOS_STD, m_Axis0_Inpos_std);
	DDX_Control(pDX, IDC_EDIT_AXIS_1_INPOS_STD, m_Axis1_Inpos_std);
	DDX_Control(pDX, IDC_EDIT_AXIS_0_LOAD_AVE, m_Axis0_load_ave);
	DDX_Control(pDX, IDC_EDIT_AXIS_1_LOAD_AVE, m_Axis1_load_ave);
	DDX_Control(pDX, IDC_EDIT_AXIS_0_LOAD_STD, m_Axis0_load_std);
	DDX_Control(pDX, IDC_EDIT_AXIS_1_LOAD_STD, m_Axis1_load_std);
	DDX_Control(pDX, IDC_GET_DATA_COUNT, m_getdatacnt);
	DDX_Control(pDX, IDC_EDIT_AXIS_0_LOAD_MAX, m_axix_0_load_max);
	DDX_Control(pDX, IDC_EDIT_AXIS_1_LOAD_MAX, m_axix_1_load_max);
	DDX_Control(pDX, IDC_EDIT_AXIS_0_INPOS_MAX, m_axix_0_inpos_max);
	DDX_Control(pDX, IDC_EDIT_AXIS_1_INPOS_MAX, m_axix_1_inpos_max);
}


BEGIN_MESSAGE_MAP(CStageReportDlg, CDialogEx)
	ON_WM_DESTROY()
	ON_WM_TIMER()
	ON_BN_CLICKED(IDC_BTN, &CStageReportDlg::OnBnClickedBtn)
	ON_BN_CLICKED(IDC_BTN_AUTO, &CStageReportDlg::OnBnClickedBtnAuto)
	ON_BN_CLICKED(IDC_BTN2, &CStageReportDlg::OnBnClickedBtn2)
	ON_BN_CLICKED(IDC_BTN3, &CStageReportDlg::OnBnClickedBtn3)
END_MESSAGE_MAP()


// CStageReportDlg 메시지 처리기

BOOL CStageReportDlg::OnInitDialog()
{
	CDialogEx::OnInitDialog();

	if (g_pNavigationStage->m_bConnect)
	{
		//SetTimer(STAGE_UPDATE_TIMER, 200, NULL);
		//SetTimer(STAGE_REPORT_TIMER, 500, NULL);
	}

	m_getdatacnt.SetWindowTextA(_T("100"));

	DataMeasurement_flag = false;
	DataReset_flag = false;
	return TRUE;  // return TRUE unless you set the focus to a control
				  // 예외: OCX 속성 페이지는 FALSE를 반환해야 합니다.
}


void CStageReportDlg::OnDestroy()
{
	CDialogEx::OnDestroy();
	KillTimer(STAGE_UPDATE_TIMER);
	KillTimer(STAGE_REPORT_TIMER);
	
	if(get_xLoad_data != nullptr)
		delete[] get_xLoad_data;
	if (get_yLoad_data != nullptr)
		delete[] get_yLoad_data;
	if (get_xInPos_data != nullptr)
		delete[] get_xInPos_data;
	if (get_yInPos_data != nullptr)
		delete[] get_yInPos_data;

}


void CStageReportDlg::OnTimer(UINT_PTR nIDEvent)
{
	KillTimer(nIDEvent);
	switch (nIDEvent)
	{
	case STAGE_UPDATE_TIMER:
		if (g_pNavigationStage->m_bConnect == TRUE)
		{
			StageReport();
		}
		SetTimer(nIDEvent, 100, NULL);
		break;
	case STAGE_REPORT_TIMER:
		if (g_pNavigationStage->m_bConnect == TRUE)
		{
			StageReportSTD();
		}
		SetTimer(nIDEvent, 200, NULL);
		break;
	default:
		break;
	}

	CDialogEx::OnTimer(nIDEvent);
}

void CStageReportDlg::StageReport()
{
	CString tmp;
	tmp.Empty();


	//get_xLoad_data_pre = abs(g_pNavigationStage->GetGlobalRealVariable("DOUT_Y2"));
	//get_yLoad_data_pre = abs(g_pNavigationStage->GetGlobalRealVariable("DOUT_Y1"));
	//get_xInPos_data_pre = abs(g_pNavigationStage->GetGlobalRealVariable("PE1"));
	//get_yInPos_data_pre = abs(g_pNavigationStage->GetGlobalRealVariable("PE0"));
	
	tmp.Empty();
	tmp.Format("%3.7f", get_xLoad_data_pre);
	if (m_Axis0_load.m_hWnd != NULL)
		m_Axis0_load.SetWindowText(tmp);
	tmp.Empty();
	tmp.Format("%3.7f", get_yLoad_data_pre);
	if (m_Axis1_load.m_hWnd != NULL)
		m_Axis1_load.SetWindowText(tmp);
	tmp.Empty();
	tmp.Format("%3.7e", get_xInPos_data_pre);
	if (m_Axis0_Inpos.m_hWnd != NULL)
		m_Axis0_Inpos.SetWindowText(tmp);
	tmp.Empty();
	tmp.Format("%3.7e", get_yInPos_data_pre);
	if (m_Axis1_Inpos.m_hWnd != NULL)
		m_Axis1_Inpos.SetWindowText(tmp);
	
	//Inpos 표준편차

}

void CStageReportDlg::StageReportSTD()
{
	int num = 0;
	CString tmp;
	tmp.Empty();
	CString XAXIS, YAXIS, XAXISINPOS_AVE, YAXISINPOS_AVE, XAXISINPOS_STD, YAXISINPOS_STD, XAXISLOAD_AVE, YAXISLOAD_AVE, XAXISLOAD_STD, YAXISLOAD_STD;
	CString XAXISINPOS_MAX, YAXISINPOS_MAX, XAXISLOAD_MAX, YAXISLOAD_MAX;
	double pos_mm[2];
	
	double xload_max, xinpos_max;
	double yload_max, yinpos_max;


	XAXIS.Empty();
	YAXIS.Empty();
	XAXISINPOS_AVE.Empty();
	YAXISINPOS_AVE.Empty();
	XAXISINPOS_STD.Empty();
	YAXISINPOS_STD.Empty();
	XAXISLOAD_AVE.Empty();
	YAXISLOAD_AVE.Empty();
	XAXISLOAD_STD.Empty();
	YAXISLOAD_STD.Empty();

	pos_mm[0] = 0.0;
	pos_mm[1] = 0.0;

	xload_max = 0.0;
	xinpos_max = 0.0;
	yload_max = 0.0;
	yinpos_max = 0.0;


	XInposSum = 0.0;
	YInposSum = 0.0;
	XInposAvg = 0.0;
	YInposAvg = 0.0;

	XLoadSum = 0.0;
	YLoadSum = 0.0;
	XLoadAvg = 0.0;
	YLoadAvg = 0.0;

	XInposVariance = 0.0;
	YInposVariance = 0.0;
	XInposSTD = 0.0;
	YInposSTD = 0.0;

	XLoadVariance = 0.0;
	YLoadVariance = 0.0;
	XLoadSTD = 0.0;
	YLoadSTD = 0.0;

	X_Inpos_value = 0.0;
	Y_Inpos_value = 0.0;
	X_Load_value = 0.0;
	Y_Load_value = 0.0;



	for (int i = 0; i < STAGE_AXIS_NUMBER; i++)
	{
		pos_mm[i] = g_pNavigationStage->GetPosmm(i);
	}

	XAXIS.Format("%3.7f", pos_mm[STAGE_X_AXIS]);
	YAXIS.Format("%3.7f", pos_mm[STAGE_Y_AXIS]);

	//do
	//{
	//
	//	Sleep(10);
	//
	//	//get_xLoad_data_[num] = 0.0;
	//	//get_yLoad_data_[num] = 0.0;
	//	//get_xInPos_data_[num] = 0.0;
	//	//get_yInPos_data_[num] = 0.0;
	//
	//	get_xLoad_data;
	//	get_yLoad_data;
	//	get_xInPos_data;
	//	get_yInPos_data;
	//
	//	num += 1;
	//
	//} while (num < GetDataCnt);
//	} while (num < STAGE_CHECK_DATA_SIZE);
//	num = 0;


	do
	{

		//Sleep(10);

		get_xLoad_data[num] = abs(g_pNavigationStage->GetGlobalRealVariable("DOUT_Y2"));
		get_yLoad_data[num] = abs(g_pNavigationStage->GetGlobalRealVariable("DOUT_Y1"));
		get_xInPos_data[num] = g_pNavigationStage->GetGlobalRealVariable("PE1");
		get_yInPos_data[num] = g_pNavigationStage->GetGlobalRealVariable("PE0");

		///////////////////////////////////////////////////////////////////////////////////////
		//ORG
		///////////////////////////////////////////////////////////////////////////////////////

		//get_xLoad_data_[num] = abs(g_pNavigationStage->GetGlobalRealVariable("DOUT_Y1"));
		//get_yLoad_data_[num] = abs(g_pNavigationStage->GetGlobalRealVariable("DOUT_Y2"));
		////get_xInPos_data_[num] = abs(g_pNavigationStage->GetGlobalRealVariable("PE0"));
		////get_yInPos_data_[num] = abs(g_pNavigationStage->GetGlobalRealVariable("PE1"));
		//
		//get_xInPos_data_[num] = g_pNavigationStage->GetGlobalRealVariable("PE0");
		//get_yInPos_data_[num] = g_pNavigationStage->GetGlobalRealVariable("PE1");

		num += 1;

	} while (num < GetDataCnt);
	//} while (num < STAGE_CHECK_DATA_SIZE);
	num = 0;



	//for (int i = 0; i < STAGE_CHECK_DATA_SIZE; i++)
	for (int i = 0; i < GetDataCnt; i++)
	{
		XLoadSum += get_xLoad_data[i];
		YLoadSum += get_yLoad_data[i];
		XInposSum += get_xInPos_data[i];
		YInposSum += get_yInPos_data[i];


		///////////////////////////////////////////////////////////////////////////////////////
		//ORG
		///////////////////////////////////////////////////////////////////////////////////////
		//XLoadSum += get_xLoad_data_[i];
		//YLoadSum += get_yLoad_data_[i];
		//XInposSum += get_xInPos_data_[i];
		//YInposSum += get_yInPos_data_[i];
	}

	XLoadAvg = (XLoadSum / GetDataCnt);
	YLoadAvg = (YLoadSum / GetDataCnt);
	XInposAvg = (XInposSum / GetDataCnt);
	YInposAvg = (YInposSum / GetDataCnt);
	
	XAXISLOAD_AVE.Format("%3.7f", XLoadAvg);
	YAXISLOAD_AVE.Format("%3.7f", YLoadAvg);
	XAXISINPOS_AVE.Format("%3.7e", XInposAvg);
	YAXISINPOS_AVE.Format("%3.7e", YInposAvg);
	
	XInposSum = 0.0;
	YInposSum = 0.0;
	XLoadSum = 0.0;
	YLoadSum = 0.0;

	tmp.Empty();
	tmp.Format("%3.7e", XInposAvg);
	if (m_Axis0_Inpos_ave.m_hWnd != NULL)
		m_Axis0_Inpos_ave.SetWindowText(tmp);
	tmp.Empty();
	tmp.Format("%3.7e", YInposAvg);
	if (m_Axis1_Inpos_ave.m_hWnd != NULL)
		m_Axis1_Inpos_ave.SetWindowText(tmp);
	tmp.Empty();
	tmp.Format("%3.7f", XLoadAvg);
	if (m_Axis0_load_ave.m_hWnd != NULL)
		m_Axis0_load_ave.SetWindowText(tmp);
	tmp.Empty();
	tmp.Format("%3.7f", YLoadAvg);
	if (m_Axis1_load_ave.m_hWnd != NULL)
		m_Axis1_load_ave.SetWindowText(tmp);

	//for (int i = 0; i < STAGE_CHECK_DATA_SIZE; i++)
	for (int i = 0; i < GetDataCnt; i++)
	{


		X_Inpos_value += pow((get_xInPos_data[i] - XInposAvg), 2);
		Y_Inpos_value += pow((get_yInPos_data[i] - YInposAvg), 2);
		X_Load_value += pow((get_xLoad_data[i] - XLoadAvg), 2);
		Y_Load_value += pow((get_yLoad_data[i] - YLoadAvg), 2);

		//X_Inpos_value += pow((get_xInPos_data_[i] - XInposAvg), 2);
		//Y_Inpos_value += pow((get_yInPos_data_[i] - YInposAvg), 2);
		//X_Load_value += pow((get_xLoad_data_[i] - XLoadAvg), 2);
		//Y_Load_value += pow((get_yLoad_data_[i] - YLoadAvg), 2);

		if (xload_max < get_xLoad_data[i])
		{
			xload_max = get_xLoad_data[i];
		}

		if (yload_max < get_yLoad_data[i])
		{
			yload_max = get_yLoad_data[i];
		}

		if (xinpos_max < get_xInPos_data[i])
		{
			xinpos_max = get_xInPos_data[i];
		}

		if (yinpos_max < get_yInPos_data[i])
		{
			yinpos_max = get_yInPos_data[i];
		}

	}

	XInposVariance = (X_Inpos_value / GetDataCnt);
	YInposVariance = (Y_Inpos_value / GetDataCnt);
	XLoadVariance = (X_Load_value / GetDataCnt);
	YLoadVariance = (Y_Load_value / GetDataCnt);


	XInposSTD = sqrt(XInposVariance);
	YInposSTD = sqrt(YInposVariance);
	XLoadSTD = sqrt(XLoadVariance);
	YLoadSTD = sqrt(YLoadVariance);

	X_Inpos_value = 0.0;
	Y_Inpos_value = 0.0;
	X_Load_value = 0.0;
	Y_Load_value = 0.0;


	XAXISLOAD_STD.Empty();
	XAXISLOAD_STD.Format("%3.7e", XLoadSTD/100);
	if (m_Axis0_load_std.m_hWnd != NULL)
		m_Axis0_load_std.SetWindowText(XAXISLOAD_STD);
	YAXISLOAD_STD.Empty();
	YAXISLOAD_STD.Format("%3.7e", YLoadSTD/100);
	if (m_Axis1_load_std.m_hWnd != NULL)
		m_Axis1_load_std.SetWindowText(YAXISLOAD_STD);

	XAXISINPOS_STD.Empty();
	//tmp.Format("%3.7e", XInposSTD);
	XAXISINPOS_STD.Format("%3.7e", XInposSTD);
	if (m_Axis0_Inpos_std.m_hWnd != NULL)
		m_Axis0_Inpos_std.SetWindowText(XAXISINPOS_STD);
	YAXISINPOS_STD.Empty();
	//tmp.Format("%3.7e", YInposSTD);
	YAXISINPOS_STD.Format("%3.7e", YInposSTD);
	if (m_Axis1_Inpos_std.m_hWnd != NULL)
		m_Axis1_Inpos_std.SetWindowText(YAXISINPOS_STD);

	YAXISINPOS_MAX.Empty();
	YAXISINPOS_MAX.Format("%3.7e", yinpos_max);
	if (m_axix_0_inpos_max.m_hWnd != NULL)
		m_axix_0_inpos_max.SetWindowText(YAXISINPOS_MAX);

	XAXISINPOS_MAX.Empty();
	XAXISINPOS_MAX.Format("%3.7e", xinpos_max);
	if (m_axix_1_inpos_max.m_hWnd != NULL)
		m_axix_1_inpos_max.SetWindowText(XAXISINPOS_MAX);

	XAXISLOAD_MAX.Empty();
	XAXISLOAD_MAX.Format("%3.7f", xload_max);
	if (m_axix_1_load_max.m_hWnd != NULL)
		m_axix_1_load_max.SetWindowText(XAXISLOAD_MAX);

	YAXISLOAD_MAX.Empty();
	YAXISLOAD_MAX.Format("%3.7f", yload_max);
	if (m_axix_0_load_max.m_hWnd != NULL)
		m_axix_0_load_max.SetWindowText(YAXISLOAD_MAX);

	if (DataMeasurement_flag)
	{
		GetDlgItem(IDC_BTN)->EnableWindow(true);
		SaveLogFile("STAGE_REPORT", _T((LPSTR)(LPCTSTR)("Xposition : " + XAXIS + " ,  Yposition :" + YAXIS + " ,   XLoad(AVE) : " + XAXISLOAD_AVE + " ,   YLoad(AVE) : " + YAXISLOAD_AVE + " ,   XLoad(STD) : " + XAXISLOAD_STD + " ,   XLoad(MAX) : " + XAXISLOAD_MAX +" ,   YLoad(MAX) : "+ YAXISLOAD_MAX + " ,   YLoad(STD) : " + YAXISLOAD_STD + " ,   XInPosition(AVE) : " + XAXISINPOS_AVE + ",  YInPosition(AVE) : " + YAXISINPOS_AVE + " ,   XInPosition(STD) : " + XAXISINPOS_STD + ",  YInPosition(STD) : " + YAXISINPOS_STD)));
	}

	//DataMeasurement_flag = false;

}

void CStageReportDlg::StageReportReset()
{
	int num = 0;
	CString tmp;
	tmp.Empty();

	XInposSum = 0.0;
	YInposSum = 0.0;
	XInposAvg = 0.0;
	YInposAvg = 0.0;

	XLoadSum = 0.0;
	YLoadSum = 0.0;
	XLoadAvg = 0.0;
	YLoadAvg = 0.0;

	XInposVariance = 0.0;
	YInposVariance = 0.0;
	XInposSTD = 0.0;
	YInposSTD = 0.0;

	XLoadVariance = 0.0;
	YLoadVariance = 0.0;
	XLoadSTD = 0.0;
	YLoadSTD = 0.0;

	X_Inpos_value = 0.0;
	Y_Inpos_value = 0.0;
	X_Load_value = 0.0;
	Y_Load_value = 0.0;

	//do
	//{
	//
	//	Sleep(10);
	//
	//	//get_xLoad_data_[num] = 0.0;
	//	//get_yLoad_data_[num] = 0.0;
	//	//get_xInPos_data_[num] = 0.0;
	//	//get_yInPos_data_[num] = 0.0;
	//
	//
	//	num += 1;
	//
	//} while (num < STAGE_CHECK_DATA_SIZE);
	//num = 0;



	memset(get_xLoad_data, 0, sizeof(double)*GetDataCnt);
	memset(get_yLoad_data, 0, sizeof(double)*GetDataCnt);
	memset(get_xInPos_data, 0, sizeof(double)*GetDataCnt);
	memset(get_yInPos_data, 0, sizeof(double)*GetDataCnt);

	tmp.Empty();
	tmp.Format("%3.7e", 0.0);

	if (m_Axis0_Inpos_ave.m_hWnd != NULL)
		m_Axis0_Inpos_ave.SetWindowText(tmp);
	if (m_Axis1_Inpos_ave.m_hWnd != NULL)
		m_Axis1_Inpos_ave.SetWindowText(tmp);
	if (m_Axis0_Inpos_std.m_hWnd != NULL)
		m_Axis0_Inpos_std.SetWindowText(tmp);
	if (m_Axis1_Inpos_std.m_hWnd != NULL)
		m_Axis1_Inpos_std.SetWindowText(tmp);
	if (m_Axis0_load_ave.m_hWnd != NULL)
		m_Axis0_load_ave.SetWindowText(tmp);
	if (m_Axis1_load_ave.m_hWnd != NULL)
		m_Axis1_load_ave.SetWindowText(tmp);
	if (m_Axis0_load_std.m_hWnd != NULL)
		m_Axis0_load_std.SetWindowText(tmp);
	if (m_Axis1_load_std.m_hWnd != NULL)
		m_Axis1_load_std.SetWindowText(tmp);
	if (m_axix_0_inpos_max.m_hWnd != NULL)
		m_axix_0_inpos_max.SetWindowText(tmp);
	if (m_axix_1_inpos_max.m_hWnd != NULL)
		m_axix_1_inpos_max.SetWindowText(tmp);
	if (m_axix_1_load_max.m_hWnd != NULL)
		m_axix_1_load_max.SetWindowText(tmp);
	if (m_axix_0_load_max.m_hWnd != NULL)
		m_axix_0_load_max.SetWindowText(tmp);
	
	if(DataReset_flag) 	GetDlgItem(IDC_BTN2)->EnableWindow(true);
	DataReset_flag = false;


	delete[] get_xLoad_data;
	delete[] get_yLoad_data;
	delete[] get_xInPos_data;
	delete[] get_yInPos_data;
}

void CStageReportDlg::OnBnClickedBtn()
{
	CString str;
	str.Empty();
	int cnt = 0;

	KillTimer(STAGE_REPORT_TIMER);
	KillTimer(STAGE_UPDATE_TIMER);

	DataMeasurement_flag = true;
	GetDlgItem(IDC_BTN)->EnableWindow(false);
	
	m_getdatacnt.GetWindowTextA(str);
	cnt = atoi(str);
	GetDataCnt = cnt;

	get_xLoad_data = new double[GetDataCnt];
	memset(get_xLoad_data, 0, sizeof(double)*GetDataCnt);
	get_yLoad_data = new double[GetDataCnt];
	memset(get_yLoad_data, 0, sizeof(double)*GetDataCnt);
	get_xInPos_data = new double[GetDataCnt];
	memset(get_xInPos_data, 0, sizeof(double)*GetDataCnt);
	get_yInPos_data = new double[GetDataCnt];
	memset(get_yInPos_data, 0, sizeof(double)*GetDataCnt);


	SetTimer(STAGE_UPDATE_TIMER, 500, NULL);
	SetTimer(STAGE_REPORT_TIMER, 500, NULL);
}

void CStageReportDlg::OnBnClickedBtnAuto()
{
	//KillTimer(STAGE_REPORT_TIMER);
	//SetTimer(STAGE_REPORT_TIMER, 500, NULL);
	//DataMeasurement_flag = false;
}

void CStageReportDlg::OnBnClickedBtn2()
{
	KillTimer(STAGE_UPDATE_TIMER);
	KillTimer(STAGE_REPORT_TIMER);
	DataReset_flag = true;
	GetDlgItem(IDC_BTN2)->EnableWindow(false);
	StageReportReset();
}




void CStageReportDlg::OnBnClickedBtn3()
{

	DataReset_flag = false;
	KillTimer(STAGE_REPORT_TIMER);
	KillTimer(STAGE_UPDATE_TIMER);


}
