﻿#pragma once


// CTabDlg 대화 상자

class CTabDlg : public CDialogEx
{
	DECLARE_DYNAMIC(CTabDlg)

public:
	CTabDlg(CWnd* pParent = nullptr);   // 표준 생성자입니다.
	virtual ~CTabDlg();

// 대화 상자 데이터입니다.
//#ifdef AFX_DESIGN_TIME
	enum { IDD = IDD_TAB_DIALOG };
//#endif

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 지원입니다.

	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnBnClickedBtnTabStage();

	int SetDisplay(int Mode);
	afx_msg void OnBnClickedBtnTabZonplate();
	afx_msg void OnBnClickedBtnTabOpticstageChange();
	afx_msg void OnBnClickedBtnTabCamera();
};
