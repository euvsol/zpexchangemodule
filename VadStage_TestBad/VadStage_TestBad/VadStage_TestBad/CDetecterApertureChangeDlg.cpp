﻿// CDetecterApertureChangeDlg.cpp: 구현 파일
//

#include "pch.h"
#include "Include.h"
#include "Extern.h"

// CDetecterApertureChangeDlg 대화 상자

IMPLEMENT_DYNAMIC(CDetecterApertureChangeDlg, CDialogEx)

CDetecterApertureChangeDlg::CDetecterApertureChangeDlg(CWnd* pParent /*=nullptr*/)
	: CDialogEx(IDD_DETECTER_APERTURE_CHANGE_DIALOG, pParent)
	, m_dMoveDistDetecter(0)
	, m_nMoveTypeDetecter(0)
	, m_dVelocityDetecter (1000)
	, m_dAccelerationDetecter (1000)
	, m_strPosTypeDetecter(_T(""))
	, m_strPosTypeAperture(_T(""))
	, m_nMoveTypeAperture(0)
	, m_dVelocityAperture(1000)
	, m_dAccelerationAperture(1000)
	, m_dMoveDistAperture(0)
	, m_detecter_start_pos(0.0)
	, m_detecter_end_pos(0.0)
	, m_detecter_dwell_pos(0.0)
	, m_aperture_start_pos(0)
	, m_aperture_end_pos(0)
	, m_aperture_dwell(0)
	, m_detector_step(0)
	, m_detector_test_vel(0)
	, m_aperture_test_vel(0)
{

	m_bStopDetecterAxis = FALSE;
	m_bStopApertureAxis = FALSE;
	m_bDecteterTestFlag = FALSE;
}

CDetecterApertureChangeDlg::~CDetecterApertureChangeDlg()
{
}

void CDetecterApertureChangeDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
	DDX_Text(pDX, IDC_EDIT_DETECTER_VELOCITY, m_dVelocityDetecter);
	DDX_Text(pDX, IDC_EDIT_DETECTER_ACCELERATION, m_dAccelerationDetecter);
	DDX_Text(pDX, IDC_EDIT_DETECTER_MOVE_DISTANCE, m_dMoveDistDetecter);
	DDX_Radio(pDX, IDC_RADIO_DETECTER_RELATIVE_MODE, m_nMoveTypeDetecter);
	DDX_Control(pDX, IDC_STATIC_DETECTER_CURRENT_POS, m_dCurrentPosDetecter);
	DDX_Control(pDX, IDC_STATIC_APERTURE_CURRENT_POS, m_dCurrentPosAperture);
	DDX_Text(pDX, IDC_STATIC_DETECTER_POSITIONER_TYPE, m_strPosTypeDetecter);
	DDX_Text(pDX, IDC_STATIC_APERTURE_POSITIONER_TYPE, m_strPosTypeAperture);
	DDX_Radio(pDX, IDC_RADIO_APERTURE_RELATIVE_MODE, m_nMoveTypeAperture);
	DDX_Text(pDX, IDC_EDIT_APERTURE_VELOCITY, m_dVelocityAperture);
	DDX_Text(pDX, IDC_EDIT_APERTURE_ACCELERATION, m_dAccelerationAperture);
	DDX_Text(pDX, IDC_EDIT_APERTURE_MOVE_DISTANCE, m_dMoveDistAperture);
	DDX_Control(pDX, IDC_ICON_DETECTER_AMPLIFIER_ENABLED, m_detector_amp_enable);
	DDX_Control(pDX, IDC_ICON_DETECTER_CLOSED_LOOP_ACTIVE, m_detector_closeloop);
	DDX_Control(pDX, IDC_ICON_DETECTER_ACTIVERY_MOVING, m_detector_activemove);
	DDX_Control(pDX, IDC_ICON_DETECTER_SENSOR_PRESENT, m_detector_sensor);
	DDX_Control(pDX, IDC_ICON_DETECTER_CALIBRATE, m_detector_calibrated);
	DDX_Control(pDX, IDC_ICON_DETECTER_REFERENCED, m_detector_referenced);
	DDX_Control(pDX, IDC_ICON_DETECTER_ENDSTOP_REACHED, m_detector_endstop);
	DDX_Control(pDX, IDC_ICON_DETECTER_RANGE_LIMIT_REACHED, m_detector_rangelimit);
	DDX_Control(pDX, IDC_ICON_DETECTER_FOLLOWING_ERROR_LIMIT_REACHED, m_detector_followingerror);
	DDX_Control(pDX, IDC_ICON_APERTURE_AMPLIFIER_ENABLED, m_aperture_ampenable);
	DDX_Control(pDX, IDC_ICON_APERTURE_CLOSED_LOOP_ACTIVE, m_aperture_closeloop);
	DDX_Control(pDX, IDC_ICON_APERTURE_ACTIVERY_MOVING, m_aperture_active);
	DDX_Control(pDX, IDC_ICON_APERTURE_SENSOR_PRESENT, m_aperture_sensor);
	DDX_Control(pDX, IDC_ICON_APERTURE_CALIBRATE, m_aperture_calibrated);
	DDX_Control(pDX, IDC_ICON_APERTURE_REFERENCED, m_aperture_referenced);
	DDX_Control(pDX, IDC_ICON_APERTURE_ENDSTOP_REACHED, m_aperture_endstop);
	DDX_Control(pDX, IDC_ICON_APERTURE_RANGE_LIMIT_REACHED, m_aperture_rangelimit);
	DDX_Control(pDX, IDC_ICON_APERTURE_FOLLOWING_ERROR_LIMIT_REACHED, m_aperture_followingerror);
	DDX_Control(pDX, IDC_ICON_DETECTER_APERTURE_CONNECT, m_detetor_aperture_icon_con);
	DDX_Text(pDX, IDC_EDIT_DETECTER_TARGET_POS, m_detecter_start_pos);
	DDX_Text(pDX, IDC_EDIT_DETECTER_REL_POS, m_detecter_end_pos);
	DDX_Text(pDX, IDC_EDIT_DETECTER_REL_POS2, m_detecter_dwell_pos);
	DDX_Text(pDX, IDC_EDIT_APERTURE_TARGET_POS, m_aperture_start_pos);
	DDX_Text(pDX, IDC_EDIT_APERTURE_REL_POS, m_aperture_end_pos);
	DDX_Text(pDX, IDC_EDIT_APERTURE_REL_POS2, m_aperture_dwell);
	DDX_Text(pDX, IDC_EDIT_DETECTER_TEST_STEP, m_detector_step);
	DDX_Control(pDX, IDC_STATIC_DETECTER_CURRENT_POS_TARGET, m_detect_target_text);
	DDX_Control(pDX, IDC_STATIC_DETECTER_CURRENT_POS_REAL, m_detect_current_text);
	DDX_Text(pDX, IDC_EDIT_DETECTER_TEST_VEL, m_detector_test_vel);
	DDX_Text(pDX, IDC_EDIT_APERTURE_TEST_VEL, m_aperture_test_vel);
	DDX_Control(pDX, IDC_EDIT_DETECTER_CNT, m_detector_test_count);
	DDX_Control(pDX, IDC_EDIT_APERTURE_CNT, m_aperture_test_count);
}


BEGIN_MESSAGE_MAP(CDetecterApertureChangeDlg, CDialogEx)
	ON_WM_DESTROY()
	ON_WM_TIMER()
	ON_BN_CLICKED(IDC_BUTTON_DETECTER_MOVE, &CDetecterApertureChangeDlg::OnBnClickedButtonDetecterMove)
	ON_BN_CLICKED(IDC_BUTTON_DETECTER_STOP, &CDetecterApertureChangeDlg::OnBnClickedButtonDetecterStop)
	ON_BN_CLICKED(IDC_BUTTON_DETECTER_REFERENCE, &CDetecterApertureChangeDlg::OnBnClickedButtonDetecterReference)
	ON_BN_CLICKED(IDC_BUTTON_DETECTER_CALIBRATE, &CDetecterApertureChangeDlg::OnBnClickedButtonDetecterCalibrate)
	ON_BN_CLICKED(IDC_BUTTON_DETECTER_APERTURE_CONNECT, &CDetecterApertureChangeDlg::OnBnClickedButtonDetecterApertureConnect)
	ON_BN_CLICKED(IDC_BUTTON_DETECTER_APERTURE_DISCONNECT, &CDetecterApertureChangeDlg::OnBnClickedButtonDetecterApertureDisconnect)
	ON_BN_CLICKED(IDC_BUTTON_APERTURE_MOVE, &CDetecterApertureChangeDlg::OnBnClickedButtonApertureMove)
	ON_BN_CLICKED(IDC_BUTTON_APERTURE_STOP, &CDetecterApertureChangeDlg::OnBnClickedButtonApertureStop)
	ON_BN_CLICKED(IDC_BUTTON_APERTURE_REFERENCE, &CDetecterApertureChangeDlg::OnBnClickedButtonApertureReference)
	ON_BN_CLICKED(IDC_BUTTON_APERTURE_CALIBRATE, &CDetecterApertureChangeDlg::OnBnClickedButtonApertureCalibrate)
	ON_BN_CLICKED(IDC_BUTTON_DETECTER_START_POS, &CDetecterApertureChangeDlg::OnBnClickedButtonDetecterStartPos)
	ON_BN_CLICKED(IDC_BUTTON_APERTURE_START, &CDetecterApertureChangeDlg::OnBnClickedButtonApertureStart)
	ON_BN_CLICKED(IDC_BUTTON_DETECTER_TEST_START, &CDetecterApertureChangeDlg::OnBnClickedButtonDetecterTestStart)
	ON_BN_CLICKED(IDC_BUTTON_DETECTER_TEST_STOP, &CDetecterApertureChangeDlg::OnBnClickedButtonDetecterTestStop)
	ON_BN_CLICKED(IDC_BUTTON_DETECTER_TEST_START2, &CDetecterApertureChangeDlg::OnBnClickedButtonDetecterTestStart2)
	ON_BN_CLICKED(IDC_BUTTON_DETECTER_TEST_START3, &CDetecterApertureChangeDlg::OnBnClickedButtonDetecterTestStart3)
	ON_BN_CLICKED(IDC_BUTTON_DETECTER_TEST_START4, &CDetecterApertureChangeDlg::OnBnClickedButtonDetecterTestStart4)
END_MESSAGE_MAP()


// CDetecterApertureChangeDlg 메시지 처리기


void CDetecterApertureChangeDlg::OnDestroy()
{
	CDialogEx::OnDestroy();

	// TODO: 여기에 메시지 처리기 코드를 추가합니다.
}

BOOL CDetecterApertureChangeDlg::OnInitDialog()
{
	CDialogEx::OnInitDialog();

	SetLogPath(LOG_PATH);

	m_LedIcon[0] = (HICON)::LoadImage(::AfxGetInstanceHandle(), MAKEINTRESOURCE(IDI_LEDOFF), IMAGE_ICON, 32, 32, LR_DEFAULTCOLOR);
	m_LedIcon[1] = (HICON)::LoadImage(::AfxGetInstanceHandle(), MAKEINTRESOURCE(IDI_LEDONGREEN), IMAGE_ICON, 32, 32, LR_DEFAULTCOLOR);
	m_LedIcon[2] = (HICON)::LoadImage(::AfxGetInstanceHandle(), MAKEINTRESOURCE(IDI_LEDONRED), IMAGE_ICON, 32, 32, LR_DEFAULTCOLOR);

	
	((CStatic*)GetDlgItem(IDC_ICON_DETECTER_APERTURE_CONNECT))->SetIcon(m_LedIcon[0]);
	((CStatic*)GetDlgItem(IDC_ICON_DETECTER_AMPLIFIER_ENABLED))->SetIcon(m_LedIcon[0]);
	((CStatic*)GetDlgItem(IDC_ICON_DETECTER_CLOSED_LOOP_ACTIVE))->SetIcon(m_LedIcon[0]);
	((CStatic*)GetDlgItem(IDC_ICON_DETECTER_ACTIVERY_MOVING))->SetIcon(m_LedIcon[0]);
	((CStatic*)GetDlgItem(IDC_ICON_DETECTER_SENSOR_PRESENT))->SetIcon(m_LedIcon[0]);
	((CStatic*)GetDlgItem(IDC_ICON_DETECTER_CALIBRATE))->SetIcon(m_LedIcon[0]);
	((CStatic*)GetDlgItem(IDC_ICON_DETECTER_REFERENCED))->SetIcon(m_LedIcon[0]);
	((CStatic*)GetDlgItem(IDC_ICON_DETECTER_ENDSTOP_REACHED))->SetIcon(m_LedIcon[0]);
	((CStatic*)GetDlgItem(IDC_ICON_DETECTER_RANGE_LIMIT_REACHED))->SetIcon(m_LedIcon[0]);
	((CStatic*)GetDlgItem(IDC_ICON_DETECTER_FOLLOWING_ERROR_LIMIT_REACHED))->SetIcon(m_LedIcon[0]);
	((CStatic*)GetDlgItem(IDC_ICON_APERTURE_AMPLIFIER_ENABLED))->SetIcon(m_LedIcon[0]);
	((CStatic*)GetDlgItem(IDC_ICON_APERTURE_CLOSED_LOOP_ACTIVE))->SetIcon(m_LedIcon[0]);
	((CStatic*)GetDlgItem(IDC_ICON_APERTURE_ACTIVERY_MOVING))->SetIcon(m_LedIcon[0]);
	((CStatic*)GetDlgItem(IDC_ICON_APERTURE_SENSOR_PRESENT))->SetIcon(m_LedIcon[0]);
	((CStatic*)GetDlgItem(IDC_ICON_APERTURE_CALIBRATE))->SetIcon(m_LedIcon[0]);
	((CStatic*)GetDlgItem(IDC_ICON_APERTURE_REFERENCED))->SetIcon(m_LedIcon[0]);
	((CStatic*)GetDlgItem(IDC_ICON_APERTURE_ENDSTOP_REACHED))->SetIcon(m_LedIcon[0]);
	((CStatic*)GetDlgItem(IDC_ICON_APERTURE_RANGE_LIMIT_REACHED))->SetIcon(m_LedIcon[0]);
	((CStatic*)GetDlgItem(IDC_ICON_APERTURE_FOLLOWING_ERROR_LIMIT_REACHED))->SetIcon(m_LedIcon[0]);


	d_pos_test_target = 0.0;

	return TRUE;  // return TRUE unless you set the focus to a control
				  // 예외: OCX 속성 페이지는 FALSE를 반환해야 합니다.
}


void CDetecterApertureChangeDlg::OnTimer(UINT_PTR nIDEvent)
{
	// TODO: 여기에 메시지 처리기 코드를 추가 및/또는 기본값을 호출합니다.

	CDialogEx::OnTimer(nIDEvent);
}




void CDetecterApertureChangeDlg::OnBnClickedButtonDetecterMove()
{
	//if (m_bConnected != TRUE)
	if (g_pZPExchange->m_bStageConnected != TRUE)
	{
		AfxMessageBox(_T("Device not connected."));
		return;
	}

	if (g_pZPExchange->m_bActivelyMoving[DETECTER_AXIS] == TRUE)
	{
		AfxMessageBox(_T("스테이지가 동작중이므로 동작할 수 없습니다."), MB_ICONWARNING);
		return;
	}

	UpdateData(TRUE);

	int nRet;
	CString strTemp;

	if (m_nMoveTypeDetecter == 0)
	{
		nRet = g_pZPExchange->SetMoveMode(DETECTER_AXIS, SA_CTL_MOVE_MODE_CL_RELATIVE);
		if (nRet != SA_CTL_ERROR_NONE)
		{
			strTemp.Format(_T("[Detecter-axis] Fail to set relative move mode. ( %d )"), nRet);
			AfxMessageBox(strTemp);
			return;
		}
	}
	else if (m_nMoveTypeDetecter == 1)
	{
		nRet = g_pZPExchange->SetMoveMode(DETECTER_AXIS, SA_CTL_MOVE_MODE_CL_ABSOLUTE);
		if (nRet != SA_CTL_ERROR_NONE)
		{
			strTemp.Format(_T("[Detecter-axis] Fail to set absolute move mode. ( %d )"), nRet);
			AfxMessageBox(strTemp);
			return;
		}
	}

	nRet = g_pZPExchange->SetVelocity(DETECTER_AXIS, m_dVelocityDetecter);
	if (nRet != SA_CTL_ERROR_NONE)
	{
		strTemp.Format(_T("[Detecter-axis] Fail to set velocity. ( %d )"), nRet);
		AfxMessageBox(strTemp);
		return;
	}

	nRet = g_pZPExchange->SetAcceleration(DETECTER_AXIS, m_dAccelerationDetecter);
	if (nRet != SA_CTL_ERROR_NONE)
	{
		strTemp.Format(_T("[Detecter-axis] Fail to set acceleration. ( %d )"), nRet);
		AfxMessageBox(strTemp);
		return;
	}

	nRet = g_pZPExchange->Move(DETECTER_AXIS, m_dMoveDistDetecter);
	if (nRet != SA_CTL_ERROR_NONE)
	{
		strTemp.Format(_T("[Detecter-axis] Fail to move. ( %d )"), nRet);
		AfxMessageBox(strTemp);
		return;
	}

	while (TRUE)
	{
		WaitSec(1);

		if (g_pZPExchange->m_bEndstopReached[DETECTER_AXIS] == TRUE)
		{
			strTemp.Format(_T("[Detecter-axis] Reached endstop position."));
			AfxMessageBox(strTemp);
			break;
		}

		if (g_pZPExchange->m_bActivelyMoving[DETECTER_AXIS] == FALSE || m_bStopDetecterAxis == TRUE)
			break;
	}
}


void CDetecterApertureChangeDlg::OnBnClickedButtonDetecterStop()
{
	//if (m_bConnected != TRUE)
	if (g_pZPExchange->m_bStageConnected != TRUE)
	{
		AfxMessageBox(_T("Device not connected."));
		return;
	}

	m_bStopDetecterAxis = TRUE;

	int nRet;
	CString strTemp;

	nRet = g_pZPExchange->Stop(DETECTER_AXIS);
	if (nRet != SA_CTL_ERROR_NONE)
	{
		strTemp.Format(_T("[Detecter-axis] Fail to stop motion. ( %d )"), nRet);
		AfxMessageBox(strTemp);
	}
}


void CDetecterApertureChangeDlg::OnBnClickedButtonDetecterReference()
{

	if (g_pZPExchange->m_bStageConnected != TRUE)
	{
		AfxMessageBox(_T("Device not connected."));
		return;
	}

	if (g_pZPExchange->m_bActivelyMoving[DETECTER_AXIS] == TRUE)
	{
		AfxMessageBox(_T("스테이지가 동작중이므로 동작할 수 없습니다."), MB_ICONWARNING);
		return;
	}

	int nRet;
	CString strTemp;

	nRet = g_pZPExchange->Reference(DETECTER_AXIS);
	if (nRet != 0)
	{
		strTemp.Format(_T("[Detecter-axis] Fail to reference. ( %d )"), nRet);
		AfxMessageBox(strTemp);
		return;
	}

	m_bStopDetecterAxis = FALSE;

	while (TRUE)
	{
		WaitSec(1);

		if (m_bStopDetecterAxis == TRUE)
			break;

		if (g_pZPExchange->m_bActivelyMoving[DETECTER_AXIS] == FALSE)
		{
			strTemp.Format(_T("[Detecter-axis] Success to reference."));
			AfxMessageBox(strTemp);
			break;
		}
	}
}


void CDetecterApertureChangeDlg::OnBnClickedButtonDetecterCalibrate()
{
	if (g_pZPExchange->m_bConnected != TRUE)
	{
		AfxMessageBox(_T("Device not connected."));
		return;
	}

	if (g_pZPExchange->m_bActivelyMoving[DETECTER_AXIS] == TRUE)
	{
		AfxMessageBox(_T("스테이지가 동작중이므로 동작할 수 없습니다."), MB_ICONWARNING);
		return;
	}

	int nRet;
	CString strTemp;

	nRet = g_pZPExchange->Calibrate(DETECTER_AXIS);
	if (nRet != 0)
	{
		strTemp.Format(_T("[Detecter-axis] Fail to calibrate. ( %d )"), nRet);
		AfxMessageBox(strTemp);
		return;
	}

	m_bStopDetecterAxis = FALSE;

	while (TRUE)
	{
		WaitSec(1);

		if (m_bStopDetecterAxis == TRUE)
			break;

		if (g_pZPExchange->m_bActivelyMoving[DETECTER_AXIS] == FALSE)
		{
			strTemp.Format(_T("[Detecter-axis] Success to calibrate."));
			AfxMessageBox(strTemp);
			break;
		}
	}
}


void CDetecterApertureChangeDlg::OnBnClickedButtonDetecterApertureConnect()
{
	int nRet;
	CString strTemp;

	strTemp = _T(" OnBnClickedButtonDetecterApertureConnect() Click!");
	::AfxMessageBox((strTemp));
	SaveLogFile("Detecter&ApertureChangeModuleReport", _T((LPSTR)(LPCTSTR)(strTemp)));

	if (g_pZPExchange->m_bStageConnected != TRUE)
	{
		g_pZPExchange->OnBnClickedButtonConnect();
	}

	//if (!m_bConnected)
	//{
	//	nRet = OpenDevice();
	//	if (nRet == 0)
	//	{
	//		nRet = GetPosType(Z_AXIS);
	//		if (nRet == 0) g_pZPExchange->m_strPosTypeZ = m_strPosType;
	//
	//		nRet = GetPosType(R_AXIS);
	//		if (nRet == 0) g_pZPExchange->m_strPosTypeR = m_strPosType;
	//
	//		nRet = GetPosType(G_AXIS);
	//		if (nRet == 0) g_pZPExchange->m_strPosTypeG = m_strPosType;
	//
	//		nRet = GetPosType(DETECTER_AXIS);
	//		if (nRet == 0) m_strPosTypeDetecter = m_strPosType;
	//
	//		nRet = GetPosType(APERTURE_AXIS);
	//		if (nRet == 0) m_strPosTypeAperture = m_strPosType;
	//
	//
	//		UpdateData(FALSE);
	//	}
	//	else
	//	{
	//		strTemp.Format("Fail to connect. ( %d )", nRet);
	//		AfxMessageBox(strTemp);
	//		SaveLogFile("Detecter&ApertureChangeModuleReport", _T((LPSTR)(LPCTSTR)(strTemp)));
	//	}
	//}
}


void CDetecterApertureChangeDlg::OnBnClickedButtonDetecterApertureDisconnect()
{
	CString strTemp;

	strTemp = _T(" OnBnClickedButtonDetecterApertureDisconnect() Click!");
	::AfxMessageBox((strTemp));
	SaveLogFile("Detecter&ApertureChangeModuleReport", _T((LPSTR)(LPCTSTR)(strTemp)));
	if (m_bConnected)
	{
		CloseDevice();
	}
}


void CDetecterApertureChangeDlg::OnBnClickedButtonApertureMove()
{
	//if (m_bConnected != TRUE)
	if (g_pZPExchange->m_bStageConnected != TRUE)
	{
		AfxMessageBox(_T("Device not connected."));
		return;
	}

	if (g_pZPExchange->m_bActivelyMoving[APERTURE_AXIS] == TRUE)
	{
		AfxMessageBox(_T("스테이지가 동작중이므로 동작할 수 없습니다."), MB_ICONWARNING);
		return;
	}

	UpdateData(TRUE);

	int nRet;
	CString strTemp;

	if (m_nMoveTypeAperture == 0)
	{
		nRet = g_pZPExchange->SetMoveMode(APERTURE_AXIS, SA_CTL_MOVE_MODE_CL_RELATIVE);
		if (nRet != SA_CTL_ERROR_NONE)
		{
			strTemp.Format(_T("[Aperture-axis] Fail to set relative move mode. ( %d )"), nRet);
			AfxMessageBox(strTemp);
			return;
		}
	}
	else if (m_nMoveTypeAperture == 1)
	{
		nRet = g_pZPExchange->SetMoveMode(APERTURE_AXIS, SA_CTL_MOVE_MODE_CL_ABSOLUTE);
		if (nRet != SA_CTL_ERROR_NONE)
		{
			strTemp.Format(_T("[Aperture-axis] Fail to set absolute move mode. ( %d )"), nRet);
			AfxMessageBox(strTemp);
			return;
		}
	}

	nRet = g_pZPExchange->SetVelocity(APERTURE_AXIS, m_dVelocityAperture);
	if (nRet != SA_CTL_ERROR_NONE)
	{
		strTemp.Format(_T("[Aperture-axis] Fail to set velocity. ( %d )"), nRet);
		AfxMessageBox(strTemp);
		return;
	}

	nRet = g_pZPExchange->SetAcceleration(APERTURE_AXIS, m_dAccelerationAperture);
	if (nRet != SA_CTL_ERROR_NONE)
	{
		strTemp.Format(_T("[Aperture-axis] Fail to set acceleration. ( %d )"), nRet);
		AfxMessageBox(strTemp);
		return;
	}

	nRet = g_pZPExchange->Move(APERTURE_AXIS, m_dMoveDistAperture);
	if (nRet != SA_CTL_ERROR_NONE)
	{
		strTemp.Format(_T("[Aperture-axis] Fail to move. ( %d )"), nRet);
		AfxMessageBox(strTemp);
		return;
	}

	while (TRUE)
	{
		WaitSec(1);

		if (g_pZPExchange->m_bEndstopReached[APERTURE_AXIS] == TRUE)
		{
			strTemp.Format(_T("[Aperture-axis] Reached endstop position."));
			AfxMessageBox(strTemp);
			break;
		}

		if (g_pZPExchange->m_bActivelyMoving[APERTURE_AXIS] == FALSE || m_bStopDetecterAxis == TRUE)
			break;
	}
}


void CDetecterApertureChangeDlg::OnBnClickedButtonApertureStop()
{
	//if (m_bConnected != TRUE)
	if (g_pZPExchange->m_bStageConnected != TRUE)
	{
		AfxMessageBox(_T("Device not connected."));
		return;
	}

	m_bStopApertureAxis = TRUE;

	int nRet;
	CString strTemp;

	nRet = g_pZPExchange->Stop(APERTURE_AXIS);
	if (nRet != SA_CTL_ERROR_NONE)
	{
		strTemp.Format(_T("[Aperture-axis] Fail to stop motion. ( %d )"), nRet);
		AfxMessageBox(strTemp);
	}
}


void CDetecterApertureChangeDlg::OnBnClickedButtonApertureReference()
{
	if (g_pZPExchange->m_bConnected != TRUE)
	{
		AfxMessageBox(_T("Device not connected."));
		return;
	}

	if (g_pZPExchange->m_bActivelyMoving[APERTURE_AXIS] == TRUE)
	{
		AfxMessageBox(_T("스테이지가 동작중이므로 동작할 수 없습니다."), MB_ICONWARNING);
		return;
	}

	int nRet;
	CString strTemp;

	nRet = g_pZPExchange->Reference(APERTURE_AXIS);
	if (nRet != 0)
	{
		strTemp.Format(_T("[Aperture-axis] Fail to reference. ( %d )"), nRet);
		AfxMessageBox(strTemp);
		return;
	}

	m_bStopApertureAxis = FALSE;

	while (TRUE)
	{
		WaitSec(1);

		if (m_bStopApertureAxis == TRUE)
			break;

		if (g_pZPExchange->m_bActivelyMoving[APERTURE_AXIS] == FALSE)
		{
			strTemp.Format(_T("[Aperture-axis] Success to reference."));
			AfxMessageBox(strTemp);
			break;
		}
	}
}


void CDetecterApertureChangeDlg::OnBnClickedButtonApertureCalibrate()
{
	if (g_pZPExchange->m_bConnected != TRUE)
	{
		AfxMessageBox(_T("Device not connected."));
		return;
	}

	if (g_pZPExchange->m_bActivelyMoving[APERTURE_AXIS] == TRUE)
	{
		AfxMessageBox(_T("스테이지가 동작중이므로 동작할 수 없습니다."), MB_ICONWARNING);
		return;
	}

	int nRet;
	CString strTemp;

	nRet = g_pZPExchange->Calibrate(APERTURE_AXIS);
	if (nRet != 0)
	{
		strTemp.Format(_T("[Aperture-axis] Fail to calibrate. ( %d )"), nRet);
		AfxMessageBox(strTemp);
		return;
	}

	m_bStopApertureAxis = FALSE;

	while (TRUE)
	{
		WaitSec(1);

		if (m_bStopApertureAxis == TRUE)
			break;

		if (g_pZPExchange->m_bActivelyMoving[APERTURE_AXIS] == FALSE)
		{
			strTemp.Format(_T("[Aperture-axis] Success to calibrate."));
			AfxMessageBox(strTemp);
			break;
		}
	}
}


void CDetecterApertureChangeDlg::DetectorBackForthStart()
{
	int longruncnt = 0;

	m_bStopDetecterAxis = FALSE;
	m_detector_test_count.SetWindowText(_T("0"));
	
	//if (m_bConnected != TRUE)
	if (g_pZPExchange->m_bStageConnected != TRUE)
	{
		AfxMessageBox(_T("Device not connected."));
		return;
	}

	if (g_pZPExchange->m_bActivelyMoving[DETECTER_AXIS] == TRUE)
	{
		AfxMessageBox(_T("스테이지가 동작중이므로 동작할 수 없습니다."), MB_ICONWARNING);
		return;
	}

	SaveLogFile("Detector_LongRunReport", _T((LPSTR)(LPCTSTR)("Detector Long Test Start")));
	UpdateData(TRUE);

	int nRet = SA_CTL_ERROR_NONE;
	CString strTemp;

	g_pZPExchange->SetMoveMode(DETECTER_AXIS, SA_CTL_MOVE_MODE_CL_ABSOLUTE);
	if (nRet != SA_CTL_ERROR_NONE)
	{
		strTemp.Format(_T("[Detecter-axis] Fail to set absolute move mode. ( %d )"), nRet);
		SaveLogFile("Detector_LongRunReport", _T((LPSTR)(LPCTSTR)(strTemp)));
		AfxMessageBox(strTemp);
		return;
	}

	nRet = g_pZPExchange->SetVelocity(DETECTER_AXIS, m_dVelocityDetecter);
	if (nRet != SA_CTL_ERROR_NONE)
	{
		strTemp.Format(_T("[Detecter-axis] Fail to set velocity. ( %d )"), nRet);
		SaveLogFile("Detector_LongRunReport", _T((LPSTR)(LPCTSTR)(strTemp)));
		AfxMessageBox(strTemp);
		return;
	}

	nRet = g_pZPExchange->SetAcceleration(DETECTER_AXIS, m_dAccelerationDetecter);
	if (nRet != SA_CTL_ERROR_NONE)
	{
		strTemp.Format(_T("[Detecter-axis] Fail to set acceleration. ( %d )"), nRet);
		SaveLogFile("Detector_LongRunReport", _T((LPSTR)(LPCTSTR)(strTemp)));
		AfxMessageBox(strTemp);
		return;
	}


	while (!m_bStopDetecterAxis)
	{
		if (g_pZPExchange->m_bSynFlag)
		{
			nRet = g_pZPExchange->TMove(DETECTER_AXIS, m_detecter_end_pos);
		}
		else
		{
			nRet = g_pZPExchange->Move(DETECTER_AXIS, m_detecter_end_pos);
		}

		if (nRet != SA_CTL_ERROR_NONE)
		{
			strTemp.Format(_T("[Detecter-axis] Fail to move. ( %d )"), nRet);
			SaveLogFile("Detector_LongRunReport", _T((LPSTR)(LPCTSTR)(strTemp)));
			AfxMessageBox(strTemp);
			return;
		}

		while (TRUE)
		{
			WaitSec(1);

			if (g_pZPExchange->m_bEndstopReached[DETECTER_AXIS] == TRUE)
			{
				strTemp.Format(_T("[Detecter-axis] Reached endstop position."));
				SaveLogFile("Detector_LongRunReport", _T((LPSTR)(LPCTSTR)(strTemp)));
				AfxMessageBox(strTemp);
				break;
			}

			if (g_pZPExchange->m_bActivelyMoving[DETECTER_AXIS] == FALSE)
			{
				strTemp.Format(_T("[Detecter-axis] Detecter position ( %f )"), g_pZPExchange->m_dDpos);
				SaveLogFile("Detector_LongRunReport", _T((LPSTR)(LPCTSTR)(strTemp)));
				break;
			}

			if (m_bStopDetecterAxis == TRUE)
				return;
		}


		WaitSec(m_detecter_dwell_pos);

		if (m_bStopDetecterAxis == TRUE)
		{
			SaveLogFile("Detector_LongRunReport", _T((LPSTR)(LPCTSTR)("Stop Button Click")));
			return;
		}

		if (g_pZPExchange->m_bSynFlag)
		{
			nRet = g_pZPExchange->TMove(DETECTER_AXIS, m_detecter_start_pos);
		}
		else
		{
			nRet = g_pZPExchange->Move(DETECTER_AXIS, m_detecter_start_pos);
		}

		if (nRet != SA_CTL_ERROR_NONE)
		{
			strTemp.Format(_T("[Detecter-axis] Fail to move. ( %d )"), nRet);
			SaveLogFile("Detector_LongRunReport", _T((LPSTR)(LPCTSTR)(strTemp)));
			AfxMessageBox(strTemp);
			return;
		}

		while (TRUE)
		{
			WaitSec(1);

			if (g_pZPExchange->m_bEndstopReached[DETECTER_AXIS] == TRUE)
			{
				strTemp.Format(_T("[Detecter-axis] Reached endstop position."));
				SaveLogFile("Detector_LongRunReport", _T((LPSTR)(LPCTSTR)(strTemp)));
				AfxMessageBox(strTemp);
				break;
			}

			if (g_pZPExchange->m_bActivelyMoving[DETECTER_AXIS] == FALSE)
			{
				strTemp.Format(_T("[Detecter-axis] Detecter position ( %f )"), g_pZPExchange->m_dDpos);
				SaveLogFile("Detector_LongRunReport", _T((LPSTR)(LPCTSTR)(strTemp)));
				break;
			}

			if (m_bStopDetecterAxis == TRUE)
			{
				SaveLogFile("Detector_LongRunReport", _T((LPSTR)(LPCTSTR)("Stop Button Click")));
				return;
			}
		}

		WaitSec(m_detecter_dwell_pos);
		longruncnt++;
		
		strTemp.Format(_T("[Detecter-axis] Detecter Long Run Count ( %d )"), longruncnt);
		SaveLogFile("Detector_LongRunReport", _T((LPSTR)(LPCTSTR)(strTemp)));

		strTemp.Format(_T("%d"), longruncnt);
		m_detector_test_count.SetWindowText(strTemp);

		if (m_bStopDetecterAxis == TRUE)
		{
			SaveLogFile("Detector_LongRunReport", _T((LPSTR)(LPCTSTR)("Stop Button Click")));
			return;
		}
	}
}

void CDetecterApertureChangeDlg::ApertureBackForthStart()
{
	m_bStopApertureAxis = FALSE;
	m_aperture_test_count.SetWindowText(_T("0"));
	int longruncnt = 0;

	//if (m_bConnected != TRUE)
	if (g_pZPExchange->m_bStageConnected != TRUE)
	{
		AfxMessageBox(_T("Device not connected."));
		return;
	}

	if (g_pZPExchange->m_bActivelyMoving[APERTURE_AXIS] == TRUE)
	{
		AfxMessageBox(_T("스테이지가 동작중이므로 동작할 수 없습니다."), MB_ICONWARNING);
		return;
	}

	SaveLogFile("Aperture_LongRunReport", _T((LPSTR)(LPCTSTR)("Aperture Long Test Start")));
	UpdateData(TRUE);

	int nRet = SA_CTL_ERROR_NONE;
	CString strTemp;

	g_pZPExchange->SetMoveMode(APERTURE_AXIS, SA_CTL_MOVE_MODE_CL_ABSOLUTE);
	//nRet = SetMoveMode(DETECTER_AXIS, SA_CTL_MOVE_MODE_CL_ABSOLUTE);
	if (nRet != SA_CTL_ERROR_NONE)
	{
		strTemp.Format(_T("[Aperture-axis] Fail to set absolute move mode. ( %d )"), nRet);
		SaveLogFile("Aperture_LongRunReport", _T((LPSTR)(LPCTSTR)(strTemp)));
		AfxMessageBox(strTemp);
		return;
	}

	nRet = g_pZPExchange->SetVelocity(APERTURE_AXIS, m_dVelocityAperture);
	if (nRet != SA_CTL_ERROR_NONE)
	{
		strTemp.Format(_T("[Aperture-axis] Fail to set velocity. ( %d )"), nRet);
		SaveLogFile("Aperture_LongRunReport", _T((LPSTR)(LPCTSTR)(strTemp)));
		AfxMessageBox(strTemp);
		return;
	}

	nRet = g_pZPExchange->SetAcceleration(APERTURE_AXIS, m_dAccelerationAperture);
	if (nRet != SA_CTL_ERROR_NONE)
	{
		strTemp.Format(_T("[Aperture-axis] Fail to set acceleration. ( %d )"), nRet);
		SaveLogFile("Aperture_LongRunReport", _T((LPSTR)(LPCTSTR)(strTemp)));
		AfxMessageBox(strTemp);
		return;
	}


	while (!m_bStopApertureAxis)
	{
		if (g_pZPExchange->m_bSynFlag)
		{
			nRet = g_pZPExchange->TMove(APERTURE_AXIS, m_aperture_end_pos);
		}
		else
		{
			nRet = g_pZPExchange->Move(APERTURE_AXIS, m_aperture_end_pos);
		}

		if (nRet != SA_CTL_ERROR_NONE)
		{
			strTemp.Format(_T("[Aperture-axis] Fail to move. ( %d )"), nRet);
			SaveLogFile("Aperture_LongRunReport", _T((LPSTR)(LPCTSTR)(strTemp)));
			AfxMessageBox(strTemp);
			return;
		}

		while (TRUE)
		{
			WaitSec(1);

			if (g_pZPExchange->m_bEndstopReached[APERTURE_AXIS] == TRUE)
			{
				strTemp.Format(_T("[Aperture-axis] Reached endstop position."));
				SaveLogFile("Aperture_LongRunReport", _T((LPSTR)(LPCTSTR)(strTemp)));
				AfxMessageBox(strTemp);
				break;
			}

			if (g_pZPExchange->m_bActivelyMoving[APERTURE_AXIS] == FALSE)
			{
				strTemp.Format(_T("[Aperture-axis] Aperture position ( %f )"), g_pZPExchange->m_dApos);
				SaveLogFile("Aperture_LongRunReport", _T((LPSTR)(LPCTSTR)(strTemp)));
				break;
			}

			if (m_bStopApertureAxis == TRUE)
			{
				SaveLogFile("Aperture_LongRunReport", _T((LPSTR)(LPCTSTR)("Stop Button Click")));
				return;
			}
		}


		WaitSec(m_aperture_dwell);

		if (m_bStopApertureAxis == TRUE)
		{
			SaveLogFile("Aperture_LongRunReport", _T((LPSTR)(LPCTSTR)("Stop Button Click")));
			return;
		}
		if (g_pZPExchange->m_bSynFlag)
		{
			nRet = g_pZPExchange->TMove(APERTURE_AXIS, m_aperture_start_pos);
		}
		else
		{
			nRet = g_pZPExchange->Move(APERTURE_AXIS, m_aperture_start_pos);
		}
		if (nRet != SA_CTL_ERROR_NONE)
		{
			strTemp.Format(_T("[Aperture-axis] Fail to move. ( %d )"), nRet);
			SaveLogFile("Aperture_LongRunReport", _T((LPSTR)(LPCTSTR)(strTemp)));
			AfxMessageBox(strTemp);
			return;
		}

		while (TRUE)
		{
			WaitSec(1);

			if (g_pZPExchange->m_bEndstopReached[APERTURE_AXIS] == TRUE)
			{
				strTemp.Format(_T("[Aperture-axis] Reached endstop position."));
				SaveLogFile("Aperture_LongRunReport", _T((LPSTR)(LPCTSTR)(strTemp)));
				AfxMessageBox(strTemp);
				break;
			}

			if (g_pZPExchange->m_bActivelyMoving[APERTURE_AXIS] == FALSE)
			{
				strTemp.Format(_T("[Aperture-axis] Aperture position ( %f )"), g_pZPExchange->m_dApos);
				SaveLogFile("Aperture_LongRunReport", _T((LPSTR)(LPCTSTR)(strTemp)));
				break;
			}

			if (m_bStopApertureAxis == TRUE)
			{
				SaveLogFile("Aperture_LongRunReport", _T((LPSTR)(LPCTSTR)("Stop Button Click")));
				return;
			}
		}

		WaitSec(m_aperture_dwell);
		longruncnt++;

		strTemp.Format(_T("[Aperture-axis] Aperture Long Run Count ( %d )"), longruncnt);
		SaveLogFile("Aperture_LongRunReport", _T((LPSTR)(LPCTSTR)(strTemp)));

		strTemp.Format(_T("%d"), longruncnt);
		m_aperture_test_count.SetWindowText(strTemp);

		if (m_bStopApertureAxis == TRUE)
		{
			SaveLogFile("Aperture_LongRunReport", _T((LPSTR)(LPCTSTR)("Stop Button Click")));
			return;
		}
	}
}

void CDetecterApertureChangeDlg::OnBnClickedButtonDetecterStartPos()
{
	DetectorBackForthStart();
}


void CDetecterApertureChangeDlg::OnBnClickedButtonApertureStart()
{
	ApertureBackForthStart();
}



void CDetecterApertureChangeDlg::OnBnClickedButtonDetecterTestStart()
{
	m_bDecteterTestFlag = TRUE;
	m_bStopDetecterAxis = FALSE;

	CString strTemp;
	

	if (g_pZPExchange->m_bStageConnected != TRUE)
	{
		AfxMessageBox(_T("Device not connected."));
		return;
	}

	if ((g_pZPExchange->m_bActivelyMoving[DETECTER_AXIS] == TRUE) || (g_pZPExchange->m_bActivelyMoving[APERTURE_AXIS] == TRUE))
	{
		AfxMessageBox(_T("스테이지가 동작중이므로 동작할 수 없습니다."), MB_ICONWARNING);
		return;
	}

	UpdateData(TRUE);

	int nRet = SA_CTL_ERROR_NONE;

	g_pZPExchange->SetMoveMode(DETECTER_AXIS, SA_CTL_MOVE_MODE_CL_RELATIVE);
	if (nRet != SA_CTL_ERROR_NONE)
	{
		strTemp.Format(_T("[Detecter-axis] Fail to set absolute move mode. ( %d )"), nRet);
		AfxMessageBox(strTemp);
		return;
	}

	nRet = g_pZPExchange->SetVelocity(DETECTER_AXIS, m_detector_test_vel);
	if (nRet != SA_CTL_ERROR_NONE)
	{
		strTemp.Format(_T("[Detecter-axis] Fail to set velocity. ( %d )"), nRet);
		AfxMessageBox(strTemp);
		return;
	}

	nRet = g_pZPExchange->SetAcceleration(DETECTER_AXIS, m_dAccelerationDetecter);
	if (nRet != SA_CTL_ERROR_NONE)
	{
		strTemp.Format(_T("[Detecter-axis] Fail to set acceleration. ( %d )"), nRet);
		AfxMessageBox(strTemp);
		return;
	}


	while (!m_bStopDetecterAxis)
	{
		if (g_pZPExchange->m_bSynFlag)
		{
			nRet = g_pZPExchange->TMove(DETECTER_AXIS, m_detector_step);
		}
		else
		{
			nRet = g_pZPExchange->Move(DETECTER_AXIS, m_detector_step);
		}

		d_pos_test_target = g_pZPExchange->m_dDpos + m_detector_step;
		strTemp.Format("%f", d_pos_test_target);
		m_detect_target_text.SetWindowText(strTemp);


		if (nRet != SA_CTL_ERROR_NONE)
		{
			strTemp.Format(_T("[Detecter-axis] Fail to move. ( %d )"), nRet);
			AfxMessageBox(strTemp);
			return;
		}



		while (TRUE)
		{
			WaitSec(1);

			if (g_pZPExchange->m_bEndstopReached[DETECTER_AXIS] == TRUE)
			{
				strTemp.Format(_T("[Detecter-axis] Reached endstop position."));
				AfxMessageBox(strTemp);
				break;
			}

			if (g_pZPExchange->m_bActivelyMoving[DETECTER_AXIS] == FALSE)
				break;

			if (m_bStopDetecterAxis == TRUE)
				return;
		}

		
		WaitSec(5);
	}
}


void CDetecterApertureChangeDlg::OnBnClickedButtonDetecterTestStop()
{
	m_bStopDetecterAxis = TRUE;
	m_bDecteterTestFlag = FALSE;
}


void CDetecterApertureChangeDlg::OnBnClickedButtonDetecterTestStart2()
{
	g_pZPExchange->m_bSynFlag = FALSE;
	if (g_pZPExchange->CreateOutputBuffer() != SA_CTL_ERROR_NONE)
	{
		AfxMessageBox("CreateOutputBuffer Error");
		return;
	}

	g_pZPExchange->m_bSynFlag = TRUE;
}


void CDetecterApertureChangeDlg::OnBnClickedButtonDetecterTestStart3()
{
	if (g_pZPExchange->FlushOutputBuffer() != SA_CTL_ERROR_NONE)
	{
		AfxMessageBox("FlushOutputBuffer Error");
	}

}




void CDetecterApertureChangeDlg::OnBnClickedButtonDetecterTestStart4()
{
	if (g_pZPExchange->CancelOutputBuffer() != SA_CTL_ERROR_NONE)
	{
		AfxMessageBox("CancelBuffer Error");
	}

	g_pZPExchange->m_bSynFlag = FALSE;
}
