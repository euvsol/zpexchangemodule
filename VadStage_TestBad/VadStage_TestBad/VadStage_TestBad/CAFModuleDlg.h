﻿#pragma once


// CAFModuleDlg 대화 상자

class CAFModuleDlg : public CDialogEx, public CWDIAFCtrl
{
	DECLARE_DYNAMIC(CAFModuleDlg)

public:
	CAFModuleDlg(CWnd* pParent = nullptr);   // 표준 생성자입니다.
	virtual ~CAFModuleDlg();

// 대화 상자 데이터입니다.
//#ifdef AFX_DESIGN_TIME
	enum { IDD = IDD_AUTOFOCUS_DIALOG };
//#endif

protected:
	CWinThread*		m_pCommandThread;
	HICON			m_LedIcon[3];

	CEdit			m_ZPosCtrl;
	CEdit			m_LedCurrEdit;
	CEdit			m_LedPwmEdit;
	CSliderCtrl		m_LedCurrSlider;
	CSliderCtrl		m_LedPwmSlider;

	BOOL			m_bDeviceConnect;
	BOOL			m_bHomePosition;
	BOOL			m_bInRangeStatus;
	BOOL			m_bInFocusStatus;
	BOOL			m_bBusyStatus;
	BOOL			m_bSensorLimitCW;
	BOOL			m_bSensorLimitCCW;
	BOOL			m_bAfOnStatus;

	int				m_nCurrentObjNum;
	int				m_nAbsolutePosZ;
	int				m_nAbsolutePosumZ;
	int				m_nCmd;

	static UINT CommandThread(LPVOID pClass);

	void GetDeviceStatus();
	void InitializeControls();
public :

	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 지원입니다.

	int OpenDevice();
	int MoveTopPosition();
	int GetErrorCode(int nCode);
	int m_nChangeobjnumber = 0;

	BOOL Is_AFM_AfOnStatus() { return m_bAfOnStatus; }
	BOOL Is_AFM_BusyStatus() { return m_bBusyStatus; }
	BOOL Is_AFM_Connected() { return m_bDeviceConnect; }

	BOOL Command(int nCmd);

	DECLARE_MESSAGE_MAP()
	afx_msg void OnDestroy();
	afx_msg void OnTimer(UINT_PTR nIDEvent);
	afx_msg void OnBnClickedAfmLaseronButton();
	afx_msg void OnBnClickedAfmLaseroffButton();
	afx_msg void OnBnClickedAfmAfonButton();
	afx_msg void OnBnClickedAfmAfoffButton();
	afx_msg void OnBnClickedAfmHomeButton();
	afx_msg void OnBnClickedAfmMake0Button();
	afx_msg void OnBnClickedAfmSaveButton();
	afx_msg void OnBnClickedAfmZupButton();
	afx_msg void OnBnClickedAfmZdownButton();
	afx_msg void OnBnClickedAfmMotionStopButton();
	afx_msg void OnNMCustomdrawAfmLedCurrentSlider(NMHDR *pNMHDR, LRESULT *pResult);
	afx_msg void OnNMCustomdrawAfmLedPwmSlider(NMHDR *pNMHDR, LRESULT *pResult);
	virtual BOOL OnInitDialog();
	afx_msg void OnBnClickedAfmConnectButton();
	afx_msg void OnBnClickedAfmDisconnectButton2();
	CEdit m_ZumPosCtrl;
};
