﻿#pragma once


// CZPExchangeDlg 대화 상자

class CZPExchangeDlg : public CDialogEx, public CSmarActCtrl, public CECommon
{
	DECLARE_DYNAMIC(CZPExchangeDlg)

public:
	CZPExchangeDlg(CWnd* pParent = nullptr);   // 표준 생성자입니다.
	virtual ~CZPExchangeDlg();

// 대화 상자 데이터입니다.
//#ifdef AFX_DESIGN_TIME
	enum { IDD = IDD_ZPEXCHANGE_DIALOG };
//#endif

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 지원입니다.

	DECLARE_MESSAGE_MAP()

public:
	HICON	m_LedIcon[3];

	void GetDeviceStatus();

	virtual BOOL OnInitDialog();
	afx_msg void OnDestroy();
	afx_msg void OnTimer(UINT_PTR nIDEvent);
	afx_msg void OnChangeMoveType(UINT value);
	afx_msg void OnChangeStagePos(UINT value);
	afx_msg void OnBnClickedButtonConnect();
	afx_msg void OnBnClickedButtonDisconnect();

	afx_msg void OnBnClickedButtonCalibrateZ();
	afx_msg void OnBnClickedButtonReferenceZ();
	afx_msg void OnBnClickedButtonMoveZ();
	afx_msg void OnBnClickedButtonStopZ();
	
	double m_dMoveDistZ;
	double m_dVelocityZ;
	double m_dAccelerationZ;
	double m_dVelocityR;
	double m_dAccelerationR;
	double m_dMoveDistR;
	afx_msg void OnBnClickedButtonCalibrateR();
	afx_msg void OnBnClickedButtonReferenceR();
	afx_msg void OnBnClickedButtonMoveR();
	afx_msg void OnBnClickedButtonStopR();
	double m_dVelocityG;
	double m_dAccelerationG;
	double m_dMoveDistG;
	afx_msg void OnBnClickedButtonCalibrateG();
	afx_msg void OnBnClickedButtonReferenceG();
	afx_msg void OnBnClickedButtonMoveG();
	afx_msg void OnBnClickedButtonStopG();
	CString m_strPosTypeZ;
	CString m_strPosTypeR;
	CString m_strPosTypeG;
	int m_nMoveTypeZ;
	int m_nMoveTypeR;
	int m_nMoveTypeG;
	afx_msg void OnBnClickedButtonZpexSetScale();
	CStatic m_CurrPositionZ;
	CStatic m_CurrPositionR;
	CStatic m_CurrPositionG;
	afx_msg void OnBnClickedButtonZpexRelease();
	afx_msg void OnBnClickedButtonZpexGrip();
	double m_dReleasePos;
	double m_dGripPos;


	double ZPositiveLimit = 6600;
	double StageMovingZLimit = 0;
	
	double GGripPos =-20;
	double GReleasePos = -300;

	typedef struct CassetPos
	{
		double x_pos;
		double y_pos;
		double zg_pos;
		double zr_pos;
	}stCassetPosData;
	stCassetPosData m_stCassetPos[7];
	
	double m_dZpos=0.0;
	double m_dRpos=0.0;
	double m_dGpos=0.0;
	double m_dDpos=0.0;
	double m_dApos=0.0;
	
	void OnReadCasettePos();
	void LoadTeachingPos();
	void SaveTeachingPos();

	afx_msg void OnBnClickedCasMove();
	
	CStatic m_xpos_cas_1;
	CStatic m_xpos_cas_2;
	CStatic m_xpos_cas_3;
	CStatic m_xpos_cas_4;
	CStatic m_xpos_cas_5;
	CStatic m_xpos_cas_6;
	CStatic m_xpos_cas_7;
	CStatic m_ypos_cas_1;
	CStatic m_ypos_cas_2;
	CStatic m_ypos_cas_3;
	CStatic m_ypos_cas_4;
	CStatic m_ypos_cas_5;
	CStatic m_ypos_cas_6;
	CStatic m_ypos_cas_7;

	BOOL m_bStopZaxis;
	BOOL m_bStopRaxis;
	BOOL m_bStopGrip;
	afx_msg void OnBnClickedCasPosSet();
	afx_msg void OnBnClickedButtonTargetPos();
	afx_msg void OnBnClickedButtonTargetPosSet();
	afx_msg void OnBnClickedButtonRelPosSet();
	afx_msg void OnBnClickedButtonZpexGripSet();
	afx_msg void OnBnClickedButtonZpexReleaseSet();
	afx_msg void OnBnClickedButtonRelPos();

	double m_dZaxisTargetPos;
	double m_dZaxisReleasePos;
	double m_dEndStopPos ;

	BOOL ZAxisReGripPosZoneMove(double ztargetpos);
	BOOL ZAxisReleaseMove();
	BOOL ZAxisTargetMove(double ZAxisPos);
	BOOL GAxisGripMove();
	BOOL GAxisGripZoneMove();
	BOOL GAxisReleaseMove();
	BOOL GAxisActiveHoldingPositionMove(double EndStopPosition);
	BOOL GAxisOrginMove();
	BOOL GAxisGripZoneMove_NoEndStop();


	afx_msg void OnBnClickedCasGrip();

	BOOL ModuleTargetPick();
	BOOL ModuleTargetRelease();
	afx_msg void OnBnClickedCasRel();
	afx_msg void OnBnClickedCasStop();
	int m_nStagePosNum;
	afx_msg void OnBnClickedCasZposSet();
	CStatic m_zpos_cas_1;
	CStatic m_zpos_cas_2;
	CStatic m_zpos_cas_3;
	CStatic m_zpos_cas_4;
	CStatic m_zpos_cas_5;
	CStatic m_zpos_cas_6;
	CStatic m_zpos_cas_7;
	CStatic m_zrpos_cas_1;
	CStatic m_zrpos_cas_2;
	CStatic m_zrpos_cas_3;
	CStatic m_zrpos_cas_4;
	CStatic m_zrpos_cas_5;
	CStatic m_zrpos_cas_6;
	CStatic m_zrpos_cas_7;
	afx_msg void OnBnClickedCasLongrun();
	BOOL CasLongruntest();
	BOOL OnlyOneCasLongruntest();


	BOOL m_bRunLongTestStopFlag;
	afx_msg void OnBnClickedCasLongrunTestStop();
	CStatic m_testcnt;
	CStatic m_currentcassette;
	CStatic m_teststate;
	BOOL StageMovableZAxisCheck();

	BOOL m_bStageConnected; 
	BOOL m_bSynFlag = FALSE;
	afx_msg void OnBnClickedButtonMove2();
	afx_msg void OnBnClickedButtonZpmCancelBuffer();
	afx_msg void OnBnClickedCasSoloLongrun();


	afx_msg void OnBnClickedButtonTargetPosR();
	double m_dZaxisReleaseTargetPos;
	afx_msg void OnBnClickedButtonTargetPosSetR();
	double m_ZGZROffSet;
	afx_msg void OnBnClickedCasHome();
	double m_GripperPos_NoEndStop;


	double GRIPPER_GRIP_NOENDSTOP_POS;	
	double GRIPHOLDLIMINTVALUE;			
	double m_Gripper_Holding_Limit;
};
