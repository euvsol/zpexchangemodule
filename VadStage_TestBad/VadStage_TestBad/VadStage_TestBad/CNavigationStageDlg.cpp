﻿// CNavigationStageDlg.cpp: 구현 파일
//

#include "pch.h"
#include "Include.h"
#include "Extern.h"

extern	CNavigationStageDlg *	g_pNavigationStage;

// CNavigationStageDlg 대화 상자

IMPLEMENT_DYNAMIC(CNavigationStageDlg, CDialogEx)

CNavigationStageDlg::CNavigationStageDlg(CWnd* pParent /*=nullptr*/)
	: CDialogEx(IDD_NAVIGATION_STAGE_DIALOG, pParent)
	, m_strMovingDistance(_T(""))
	, m_strGetXPos(_T(""))
	, m_strGetYPos(_T(""))
{

}

CNavigationStageDlg::~CNavigationStageDlg()
{
	if(!stage_buffer_state)
		g_pNavigationStage->StopBuffer(17);
}

void CNavigationStageDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
	DDX_Text(pDX, IDC_EDIT_INCSTEP, m_strMovingDistance);
	DDX_Text(pDX, IDC_EDIT_GETXPOS, m_strGetXPos);
	DDX_Text(pDX, IDC_EDIT_GETYPOS, m_strGetYPos);
	DDX_Control(pDX, IDC_EDIT_XFPOS, m_XPosCtrl);
	DDX_Control(pDX, IDC_EDIT_YFPOS, m_YPosCtrl);
	DDX_Control(pDX, IDC_EDIT_CONNECT_STATE, m_Connect);
	DDX_Control(pDX, IDC_EDIT_GETXPOSLOAD, m_getxposload);
	DDX_Control(pDX, IDC_EDIT_GETYPOSLOAD, m_getyposload);
	DDX_Control(pDX, IDC_EDIT_GETXPOSLOAD_MAX, m_getxposload_max);
	DDX_Control(pDX, IDC_EDIT_GETXPOSLOAD_MIN, m_getxposload_min);
	DDX_Control(pDX, IDC_EDIT_GETYPOSLOAD_MAX, m_getyposload_max);
	DDX_Control(pDX, IDC_EDIT_GETYPOSLOAD_MIN, m_getyposload_min);
	DDX_Control(pDX, IDC_EDIT_GETXPOSINPOS, m_edit_x_inpos);
	DDX_Control(pDX, IDC_EDIT_GETYPOSINPOS, m_edit_y_inpos);
}


BEGIN_MESSAGE_MAP(CNavigationStageDlg, CDialogEx)
	ON_WM_DESTROY()
	ON_WM_TIMER()
	ON_BN_CLICKED(IDC_BUTTON_MOVEPOS, &CNavigationStageDlg::OnBnClickedButtonMovepos)
	ON_BN_CLICKED(IDC_BUTTON_YPLUS, &CNavigationStageDlg::OnBnClickedButtonYplus)
	ON_EN_CHANGE(IDC_EDIT_INCSTEP, &CNavigationStageDlg::OnEnChangeEditIncstep)
	ON_BN_CLICKED(IDC_BUTTON_YMINUS, &CNavigationStageDlg::OnBnClickedButtonYminus)
	ON_BN_CLICKED(IDC_BUTTON_XPLUS, &CNavigationStageDlg::OnBnClickedButtonXplus)
	ON_BN_CLICKED(IDC_BUTTON_XMINUS, &CNavigationStageDlg::OnBnClickedButtonXminus)
	ON_NOTIFY(UDN_DELTAPOS, IDC_INCSTEPSPIN, &CNavigationStageDlg::OnDeltaposIncstepspin)
	ON_BN_CLICKED(IDC_BUTTON_STOP, &CNavigationStageDlg::OnBnClickedButtonStop)
	ON_BN_CLICKED(IDC_BUTTON_ZERORETURN, &CNavigationStageDlg::OnBnClickedButtonZeroreturn)
	ON_BN_CLICKED(IDC_BUTTON_XHOME, &CNavigationStageDlg::OnBnClickedButtonXhome)
	ON_BN_CLICKED(IDC_BUTTON_YHOME, &CNavigationStageDlg::OnBnClickedButtonYhome)
	ON_BN_CLICKED(IDC_BUTTON_XENABLE, &CNavigationStageDlg::OnBnClickedButtonXenable)
	ON_BN_CLICKED(IDC_BUTTON_YENABLE, &CNavigationStageDlg::OnBnClickedButtonYenable)
	ON_BN_CLICKED(IDC_BUTTON_FAULTCLEAR, &CNavigationStageDlg::OnBnClickedButtonFaultclear)
	ON_BN_CLICKED(IDC_BUTTON_XDISABLE, &CNavigationStageDlg::OnBnClickedButtonXdisable)
	ON_BN_CLICKED(IDC_BUTTON_YDISABLE, &CNavigationStageDlg::OnBnClickedButtonYdisable)
	ON_BN_CLICKED(IDC_BUTTON_0_MOVE, &CNavigationStageDlg::OnBnClickedButton0Move)
	ON_BN_CLICKED(IDC_BUTTON_CONNECT, &CNavigationStageDlg::OnBnClickedButtonConnect)
	ON_BN_CLICKED(IDC_BUTTON_DISCONNECT, &CNavigationStageDlg::OnBnClickedButtonDisconnect)
	ON_BN_CLICKED(IDC_BUTTON_BUFFER_START, &CNavigationStageDlg::OnBnClickedButtonBufferStart)
	ON_BN_CLICKED(IDC_BUTTON_BUFFER_STOP, &CNavigationStageDlg::OnBnClickedButtonBufferStop)
	ON_BN_CLICKED(IDC_BUTTON_BUFFER_START2, &CNavigationStageDlg::OnBnClickedButtonBufferStart2)
	ON_BN_CLICKED(IDC_BUTTON_BUFFER_STOP2, &CNavigationStageDlg::OnBnClickedButtonBufferStop2)
END_MESSAGE_MAP()


// CNavigationStageDlg 메시지 처리기




BOOL CNavigationStageDlg::OnInitDialog()
{
	CDialogEx::OnInitDialog();

	m_dMovingDistance = 0.0;
	
	m_LedIcon[0] = (HICON)::LoadImage(::AfxGetInstanceHandle(), MAKEINTRESOURCE(IDI_LEDOFF), IMAGE_ICON, 32, 32, LR_DEFAULTCOLOR);
	m_LedIcon[1] = (HICON)::LoadImage(::AfxGetInstanceHandle(), MAKEINTRESOURCE(IDI_LEDONGREEN), IMAGE_ICON, 32, 32, LR_DEFAULTCOLOR);
	m_LedIcon[2] = (HICON)::LoadImage(::AfxGetInstanceHandle(), MAKEINTRESOURCE(IDI_LEDONRED), IMAGE_ICON, 32, 32, LR_DEFAULTCOLOR);


	((CStatic*)GetDlgItem(IDC_ICON_XAMP))->SetIcon(m_LedIcon[0]);
	((CStatic*)GetDlgItem(IDC_ICON_YAMP))->SetIcon(m_LedIcon[0]);
	((CStatic*)GetDlgItem(IDC_ICON_CON))->SetIcon(m_LedIcon[0]);

	stage_buffer_state = true;
	if(g_pNavigationStage->RunBuffer(17) != XY_NAVISTAGE_OK)
		stage_buffer_state = false;

	OnBnClickedButtonConnect();
	Check_X_axis_amp_state();
	Check_Y_axis_amp_state();

	SetTimer(NAVISTAGE_UPDATE_TIMER, 100, NULL);

	return TRUE;  // return TRUE unless you set the focus to a control
				  // 예외: OCX 속성 페이지는 FALSE를 반환해야 합니다.
}


void CNavigationStageDlg::OnDestroy()
{
	CDialogEx::OnDestroy();


	// TODO: 여기에 메시지 처리기 코드를 추가합니다.
}


void CNavigationStageDlg::OnTimer(UINT_PTR nIDEvent)
{
	KillTimer(nIDEvent);
	switch (nIDEvent)
	{
	case NAVISTAGE_UPDATE_TIMER:
		if (m_bConnect == TRUE)
			StageCheck();
		SetTimer(nIDEvent, 100, NULL);
		break;
	default:
		break;
	}

	CDialogEx::OnTimer(nIDEvent);
}



void CNavigationStageDlg::StageCheck()
{
	if (this == NULL) return;

	int State, Fault, Value;
	CString str;
	double pos_mm[2];
	double x_load, y_load, x_load_max, x_load_min, y_load_max, y_load_min, x_inpos, y_inpos;
	CString tmp;

	//////////////////////////////////////////////////// STAGE POSITION READ ////////////////////////////////////////////////
	
	//for (int i = 0; i < STAGE_AXIS_NUMBER; i++) 
	//{
	//	pos_mm[i] = GetPosmm(i);
	//}

	pos_mm[0] = GetPosmm(STAGE_ACS_X_AXIS);
	pos_mm[1] = GetPosmm(STAGE_ACS_Y_AXIS);

	tmp.Format("%3.7f", pos_mm[STAGE_X_AXIS]);
	if (m_XPosCtrl.m_hWnd != NULL)
		m_XPosCtrl.SetWindowText(tmp);

	tmp.Format("%3.7f", pos_mm[STAGE_Y_AXIS]);
	if (m_YPosCtrl.m_hWnd != NULL)
		m_YPosCtrl.SetWindowText(tmp);
	
	if (m_bConnect)
	{

		//x_load = g_pNavigationStage->GetGlobalRealVariable("XDOUT_0");
		//y_load = g_pNavigationStage->GetGlobalRealVariable("YDOUT_2");
		//x_load_max = g_pNavigationStage->GetGlobalRealVariable("XMAX_D0");
		//x_load_min = g_pNavigationStage->GetGlobalRealVariable("XMIN_D0");
		//y_load_max = g_pNavigationStage->GetGlobalRealVariable("YMAX_D2");
		//y_load_min = g_pNavigationStage->GetGlobalRealVariable("YMIN_D2");
		home_flag_x = g_pNavigationStage->GetGlobalRealVariable("HOME_FLAG_0");
		home_flag_y = g_pNavigationStage->GetGlobalRealVariable("HOME_FLAG_2");
		//x_inpos = g_pNavigationStage->GetGlobalRealVariable("HOMEFLAG_0");
		//y_inpos = g_pNavigationStage->GetGlobalRealVariable("HOMEFLAG_2");

		//tmp.Format("%3.7f", x_load);
		//if (m_getxposload.m_hWnd != NULL)
		//	m_getxposload.SetWindowText(tmp);
		//
		//tmp.Format("%3.7f", x_load_max);
		//if (m_getxposload_max.m_hWnd != NULL)
		//	m_getxposload_max.SetWindowText(tmp);
		//
		//tmp.Format("%3.7f", x_load_min);
		//if (m_getxposload_min.m_hWnd != NULL)
		//	m_getxposload_min.SetWindowText(tmp);
		//
		//tmp.Format("%3.7f", y_load);
		//if (m_getyposload.m_hWnd != NULL)
		//	m_getyposload.SetWindowText(tmp);
		//
		//tmp.Format("%3.7f", y_load_max);
		//if (m_getyposload_max.m_hWnd != NULL)
		//	m_getyposload_max.SetWindowText(tmp);
		//
		//tmp.Format("%3.7f", y_load_min);
		//if (m_getyposload_min.m_hWnd != NULL)
		//	m_getyposload_min.SetWindowText(tmp);
	}
	else
	{
		if (m_getxposload.m_hWnd != NULL)
			m_getxposload.SetWindowText(_T("Buffer실행필요"));
	
		if (m_getyposload.m_hWnd != NULL)
			m_getyposload.SetWindowText(_T("Buffer실행필요"));
	}
	/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


	//////////////////////////////////////////////////// STAGE AMP CHECK ////////////////////////////////////////////////
	Check_X_axis_amp_state();
	Check_Y_axis_amp_state();
	///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	
	//////////////////////////////////////////////////// STAGE Homing CHECK ////////////////////////////////////////////////
	Check_home_state();
	///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


	//////////////////////////// MOTOR STATUS READ /////////////////////////////////////////

	//if (GetMotorStatus(STAGE_X_AXIS, &State) != XY_NAVISTAGE_OK)
	//	return;

	//if (State & ACSC_MST_ENABLE)
	//{
	//	m_chkXEnableCtrl.SetCheck(TRUE);
	//	m_XEnableCtrl.SetWindowText("X Disable");
	//}
	//else
	//{
	//	m_chkXEnableCtrl.SetCheck(FALSE);
	//	m_XEnableCtrl.SetWindowText("X Enable");
	//}
	//
	//if (State & ACSC_MST_INPOS)
	//	m_chkXInpositionCtrl.SetCheck(TRUE);
	//else
	//	m_chkXInpositionCtrl.SetCheck(FALSE);
	//
	//if (State & ACSC_MST_MOVE)
	//	m_chkXMovingCtrl.SetCheck(TRUE);
	//else
	//	m_chkXMovingCtrl.SetCheck(FALSE);
	//
	//if (GetMotorStatus(STAGE_Y_AXIS, &State) != XY_NAVISTAGE_OK)
	//	return;
	//
	//if (State & ACSC_MST_ENABLE)
	//{
	//	m_chkYEnableCtrl.SetCheck(TRUE);
	//	m_YEnableCtrl.SetWindowText("Y Disable");
	//}
	//else
	//{
	//	m_chkYEnableCtrl.SetCheck(FALSE);
	//	m_YEnableCtrl.SetWindowText("Y Enable");
	//}
	//
	//if (State & ACSC_MST_INPOS)
	//	m_chkYInpositionCtrl.SetCheck(TRUE);
	//else
	//	m_chkYInpositionCtrl.SetCheck(FALSE);
	//
	//if (State & ACSC_MST_MOVE)
	//	m_chkYMovingCtrl.SetCheck(TRUE);
	//else
	//	m_chkYMovingCtrl.SetCheck(FALSE);
	/////////////////////////////////////////////////////////////////////////////////////////
	//
	////////////////////////////// SAFERY FAULT READ ////////////////////////////////////////
	//if (GetFaultStatus(STAGE_X_AXIS, &Fault) != XY_NAVISTAGE_OK)
	//	return;
	//
	//if (Fault & ACSC_SAFETY_RL)
	//{
	//	m_chkXPlusLimitCtrl.SetCheck(TRUE);
	//}
	//else
	//{
	//	m_chkXPlusLimitCtrl.SetCheck(FALSE);
	//}
	//
	//if (Fault & ACSC_SAFETY_LL)
	//	m_chkXMinusLimitCtrl.SetCheck(TRUE);
	//else
	//	m_chkXMinusLimitCtrl.SetCheck(FALSE);
	//
	//if (Fault & ACSC_SAFETY_SRL)
	//	m_chkXSoftPlusLimitCtrl.SetCheck(TRUE);
	//else
	//	m_chkXSoftPlusLimitCtrl.SetCheck(FALSE);
	//
	//if (Fault & ACSC_SAFETY_SLL)
	//	m_chkXSoftMinusLimitCtrl.SetCheck(TRUE);
	//else
	//	m_chkXSoftMinusLimitCtrl.SetCheck(FALSE);
	//
	//if (Fault & ACSC_SAFETY_ES)
	//{
	//	m_chkXMotorFaultCtrl.SetCheck(TRUE);
	//}
	//else
	//	m_chkXMotorFaultCtrl.SetCheck(FALSE);
	//
	//if (Fault & ACSC_SAFETY_HOT)
	//	m_chkXMotorFaultCtrl.SetCheck(TRUE);
	//else
	//	m_chkXMotorFaultCtrl.SetCheck(FALSE);
	//if (Fault & ACSC_SAFETY_ENCNC)
	//	m_chkXMotorFaultCtrl.SetCheck(TRUE);
	//else
	//	m_chkXMotorFaultCtrl.SetCheck(FALSE);
	//if (Fault & ACSC_SAFETY_DRIVE)
	//	m_chkXMotorFaultCtrl.SetCheck(TRUE);
	//else
	//	m_chkXMotorFaultCtrl.SetCheck(FALSE);
	//if (Fault & ACSC_SAFETY_ENC)
	//	m_chkXMotorFaultCtrl.SetCheck(TRUE);
	//else
	//	m_chkXMotorFaultCtrl.SetCheck(FALSE);
	//if (Fault & ACSC_SAFETY_PE)
	//	m_chkXMotorFaultCtrl.SetCheck(TRUE);
	//else
	//	m_chkXMotorFaultCtrl.SetCheck(FALSE);
	//if (Fault & ACSC_SAFETY_CPE)
	//	m_chkXMotorFaultCtrl.SetCheck(TRUE);
	//else
	//	m_chkXMotorFaultCtrl.SetCheck(FALSE);
	//if (Fault & ACSC_SAFETY_VL)
	//	m_chkXMotorFaultCtrl.SetCheck(TRUE);
	//else
	//	m_chkXMotorFaultCtrl.SetCheck(FALSE);
	//if (Fault & ACSC_SAFETY_AL)
	//	m_chkXMotorFaultCtrl.SetCheck(TRUE);
	//else
	//	m_chkXMotorFaultCtrl.SetCheck(FALSE);
	//if (Fault & ACSC_SAFETY_CL)
	//	m_chkXMotorFaultCtrl.SetCheck(TRUE);
	//else
	//	m_chkXMotorFaultCtrl.SetCheck(FALSE);
	//if (Fault & ACSC_SAFETY_SP)
	//	m_chkXMotorFaultCtrl.SetCheck(TRUE);
	//else
	//	m_chkXMotorFaultCtrl.SetCheck(FALSE);
	//
	//if (GetFaultStatus(STAGE_Y_AXIS, &Fault) != XY_NAVISTAGE_OK)
	//	return;
	//
	//if (Fault & ACSC_SAFETY_RL)
	//{
	//	m_chkYPlusLimitCtrl.SetCheck(TRUE);
	//}
	//else
	//{
	//	m_chkYPlusLimitCtrl.SetCheck(FALSE);
	//}
	//
	//if (Fault & ACSC_SAFETY_LL)
	//	m_chkYMinusLimitCtrl.SetCheck(TRUE);
	//else
	//	m_chkYMinusLimitCtrl.SetCheck(FALSE);
	//
	//if (Fault & ACSC_SAFETY_SRL)
	//	m_chkYSoftPlusLimitCtrl.SetCheck(TRUE);
	//else
	//	m_chkYSoftPlusLimitCtrl.SetCheck(FALSE);
	//
	//if (Fault & ACSC_SAFETY_SLL)
	//	m_chkYSoftMinusLimitCtrl.SetCheck(TRUE);
	//else
	//	m_chkYSoftMinusLimitCtrl.SetCheck(FALSE);
	//
	//if (Fault & ACSC_SAFETY_ES)
	//{
	//	m_chkYMotorFaultCtrl.SetCheck(TRUE);
	//}
	//else
	//	m_chkYMotorFaultCtrl.SetCheck(FALSE);
	//
	//if (Fault & ACSC_SAFETY_HOT)
	//	m_chkYMotorFaultCtrl.SetCheck(TRUE);
	//else
	//	m_chkYMotorFaultCtrl.SetCheck(FALSE);
	//if (Fault & ACSC_SAFETY_ENCNC)
	//	m_chkYMotorFaultCtrl.SetCheck(TRUE);
	//else
	//	m_chkYMotorFaultCtrl.SetCheck(FALSE);
	//if (Fault & ACSC_SAFETY_DRIVE)
	//	m_chkYMotorFaultCtrl.SetCheck(TRUE);
	//else
	//	m_chkYMotorFaultCtrl.SetCheck(FALSE);
	//if (Fault & ACSC_SAFETY_ENC)
	//	m_chkYMotorFaultCtrl.SetCheck(TRUE);
	//else
	//	m_chkYMotorFaultCtrl.SetCheck(FALSE);
	//if (Fault & ACSC_SAFETY_PE)
	//	m_chkYMotorFaultCtrl.SetCheck(TRUE);
	//else
	//	m_chkYMotorFaultCtrl.SetCheck(FALSE);
	//if (Fault & ACSC_SAFETY_CPE)
	//	m_chkYMotorFaultCtrl.SetCheck(TRUE);
	//else
	//	m_chkYMotorFaultCtrl.SetCheck(FALSE);
	//if (Fault & ACSC_SAFETY_VL)
	//	m_chkYMotorFaultCtrl.SetCheck(TRUE);
	//else
	//	m_chkYMotorFaultCtrl.SetCheck(FALSE);
	//if (Fault & ACSC_SAFETY_AL)
	//	m_chkYMotorFaultCtrl.SetCheck(TRUE);
	//else
	//	m_chkYMotorFaultCtrl.SetCheck(FALSE);
	//if (Fault & ACSC_SAFETY_CL)
	//	m_chkYMotorFaultCtrl.SetCheck(TRUE);
	//else
	//	m_chkYMotorFaultCtrl.SetCheck(FALSE);
	//if (Fault & ACSC_SAFETY_SP)
	//	m_chkYMotorFaultCtrl.SetCheck(TRUE);
	//else
	//	m_chkYMotorFaultCtrl.SetCheck(FALSE);

}

void CNavigationStageDlg::OnBnClickedButtonMovepos()
{
	EStopFlag = FALSE;

	UpdateData(true);

	if (MoveAbsolutePosition((atof(m_strGetXPos)), (atof(m_strGetYPos))) != XY_NAVISTAGE_OK)
	//if (MoveAbsolutePosition(atof(m_strGetYPos), atof(m_strGetXPos)) != XY_NAVISTAGE_OK)
	{
		if (EStopFlag)
		{
			::AfxMessageBox(" Stage Stop!");
		}
		else
		{
			::AfxMessageBox(" Stage 동작 불가!");
		}
	}
}


int CNavigationStageDlg::MoveAbsolutePosition(double dXmm, double dYmm)
{

	WaitSec(0.5);
	if (MoveXYUntilInposition(dXmm, dYmm) != XY_NAVISTAGE_OK)
		return !XY_NAVISTAGE_OK;

	return XY_NAVISTAGE_OK;
}

int CNavigationStageDlg::MoveRelativePosition(int nAxis, double dPosmm)
{
	WaitSec(0.1);
	if (MoveRelative(nAxis, dPosmm) != XY_NAVISTAGE_OK)
		return !XY_NAVISTAGE_OK;
	WaitSec(1);
	return XY_NAVISTAGE_OK;
}


void CNavigationStageDlg::OnBnClickedButtonYplus()
{
	if (MoveRelativePosition(STAGE_ACS_Y_AXIS, m_dMovingDistance) != XY_NAVISTAGE_OK)
	{
		::AfxMessageBox(" Stage 동작 불가!");
	}
}


void CNavigationStageDlg::OnEnChangeEditIncstep()
{
	// TODO:  RICHEDIT 컨트롤인 경우, 이 컨트롤은
	// __super::OnInitDialog() 함수를 재지정 
	//하고 마스크에 OR 연산하여 설정된 ENM_CHANGE 플래그를 지정하여 CRichEditCtrl().SetEventMask()를 호출하지 않으면
	// 이 알림 메시지를 보내지 않습니다.

	// TODO:  여기에 컨트롤 알림 처리기 코드를 추가합니다.

	UpdateData(true);
	m_dMovingDistance = atof(m_strMovingDistance);
}


void CNavigationStageDlg::OnBnClickedButtonYminus()
{
	if (MoveRelativePosition(STAGE_ACS_Y_AXIS, -m_dMovingDistance) != XY_NAVISTAGE_OK)
	{
		::AfxMessageBox(" Stage 동작 불가!");
	}
}


void CNavigationStageDlg::OnBnClickedButtonXplus()
{
	if (MoveRelativePosition(STAGE_ACS_X_AXIS, m_dMovingDistance) != XY_NAVISTAGE_OK)
	{
		::AfxMessageBox(" Stage 동작 불가!");
	}
}


void CNavigationStageDlg::OnBnClickedButtonXminus()
{
	if (MoveRelativePosition(STAGE_ACS_X_AXIS, -m_dMovingDistance) != XY_NAVISTAGE_OK)
	{
		::AfxMessageBox(" Stage 동작 불가!");
	}
}


void CNavigationStageDlg::OnDeltaposIncstepspin(NMHDR *pNMHDR, LRESULT *pResult)
{
	LPNMUPDOWN pNMUpDown = reinterpret_cast<LPNMUPDOWN>(pNMHDR);
	if (pNMUpDown->iDelta < 0)
	{
		if (m_dMovingDistance > 0.0001f && m_dMovingDistance < 0.1f) m_dMovingDistance *= 10.f;
		else if (m_dMovingDistance >= 0.1f && m_dMovingDistance < 0.5f) m_dMovingDistance += 0.1f;
		else if (m_dMovingDistance >= 0.5f && m_dMovingDistance < 1.f) m_dMovingDistance = 1.0f;
		else if (m_dMovingDistance < 10.0f)	m_dMovingDistance *= 10.0f;
		else if (m_dMovingDistance >= 10.0f && m_dMovingDistance < SOFT_LIMIT_PLUS_X)	m_dMovingDistance += 10.0f;
	}
	else
	{
		if (m_dMovingDistance > 0.0001f && m_dMovingDistance <= 0.1f)
			m_dMovingDistance /= 10.f;
		else if (m_dMovingDistance > 0.1f && m_dMovingDistance <= 0.5f)
			m_dMovingDistance -= 0.1f;
		else if (m_dMovingDistance >= 0.5f && m_dMovingDistance <= 1.f)
			m_dMovingDistance = .5f;
		else if (m_dMovingDistance <= 10.0f)
			m_dMovingDistance = 1.0f;
		else if (m_dMovingDistance > 10.0f && m_dMovingDistance <= SOFT_LIMIT_PLUS_X)
			m_dMovingDistance -= 10.0f;
	}
	m_strMovingDistance.Format("%0.4f", m_dMovingDistance);

	UpdateData(false);
	*pResult = 0;
}


void CNavigationStageDlg::OnBnClickedButtonStop()
{
	//Stop(STAGE_ALL_AXIS) ;
	EStop(STAGE_ALL_AXIS) ;
	EStopFlag = TRUE;
}


void CNavigationStageDlg::OnBnClickedButtonZeroreturn()
{
	Home(STAGE_ALL_AXIS);
}


void CNavigationStageDlg::OnBnClickedButtonXhome()
{
	Home(STAGE_ACS_X_AXIS);
}


void CNavigationStageDlg::OnBnClickedButtonYhome()
{
	Home(STAGE_ACS_Y_AXIS);
}


void CNavigationStageDlg::OnBnClickedButtonXenable()
{
	SetAmpEnable(STAGE_ACS_X_AXIS, true);
}


void CNavigationStageDlg::OnBnClickedButtonYenable()
{
	SetAmpEnable(STAGE_ACS_Y_AXIS, true);
}


void CNavigationStageDlg::OnBnClickedButtonFaultclear()
{
	ClearAmpFault(STAGE_ALL_AXIS);
}


void CNavigationStageDlg::OnBnClickedButtonXdisable()
{
	SetAmpEnable(STAGE_ACS_X_AXIS, false);
}


void CNavigationStageDlg::OnBnClickedButtonYdisable()
{
	SetAmpEnable(STAGE_ACS_Y_AXIS, false);
}



void CNavigationStageDlg::Check_X_axis_amp_state()
{
	bool amp_stage = false;
	amp_stage = GetAmpEnable(STAGE_ACS_X_AXIS);

	if (amp_stage)
	{
		((CStatic*)GetDlgItem(IDC_ICON_XAMP))->SetIcon(m_LedIcon[1]);
		((CStatic*)GetDlgItem(IDC_EDIT_XAMP))->SetWindowTextA(_T("X Axis Amp Enable"));

	}
	else
	{
		((CStatic*)GetDlgItem(IDC_ICON_XAMP))->SetIcon(m_LedIcon[2]);
		((CStatic*)GetDlgItem(IDC_EDIT_XAMP))->SetWindowTextA(_T("X Axis Amp Disable"));

	}
}

void CNavigationStageDlg::Check_home_state()
{
	if (home_flag_x)
	{
		((CStatic*)GetDlgItem(IDC_ICON_XHOME))->SetIcon(m_LedIcon[1]);
	}
	else
	{
		((CStatic*)GetDlgItem(IDC_ICON_XHOME))->SetIcon(m_LedIcon[2]);
	}

	if (home_flag_y)
	{
		((CStatic*)GetDlgItem(IDC_ICON_YHOME))->SetIcon(m_LedIcon[1]);
	}
	else
	{
		((CStatic*)GetDlgItem(IDC_ICON_YHOME))->SetIcon(m_LedIcon[2]);
	}

}

void CNavigationStageDlg::Check_Y_axis_amp_state()
{
	bool amp_stage = false;
	amp_stage = GetAmpEnable(STAGE_ACS_Y_AXIS);

	if (amp_stage)
	{
		((CStatic*)GetDlgItem(IDC_ICON_YAMP))->SetIcon(m_LedIcon[1]);
		((CStatic*)GetDlgItem(IDC_EDIT_YAMP))->SetWindowTextA(_T("Y Axis Amp Enable"));
	}
	else
	{
		((CStatic*)GetDlgItem(IDC_ICON_YAMP))->SetIcon(m_LedIcon[2]);
		((CStatic*)GetDlgItem(IDC_EDIT_YAMP))->SetWindowTextA(_T("Y Axis Amp Disable"));
	}
}

void CNavigationStageDlg::OnBnClickedButton0Move()
{
	CString XPOS = _T("0.0");
	CString YPOS = _T("0.0");

	UpdateData(true);

	if (MoveAbsolutePosition(atof(XPOS), atof(YPOS)) != XY_NAVISTAGE_OK)
	{
		::AfxMessageBox(" Stage 동작 불가!");
	}
}


void CNavigationStageDlg::OnBnClickedButtonConnect()
{
	int nRet = -1;

	if (m_bConnect != TRUE)
	{
		nRet = ConnectACSController(ETHERNET, "10.0.0.100", 701);
		if (!nRet)
		{
			m_Connect.SetWindowTextA(_T("Stage Connection Fail"));
			((CStatic*)GetDlgItem(IDC_ICON_CON))->SetIcon(m_LedIcon[2]);
		}
		else
		{
			m_Connect.SetWindowTextA(_T("Stage Connection"));
			((CStatic*)GetDlgItem(IDC_ICON_CON))->SetIcon(m_LedIcon[1]);
		}
	}
}


void CNavigationStageDlg::OnBnClickedButtonDisconnect()
{
	if (m_bConnect == TRUE)
	{
		DisconnectComm();
		m_Connect.SetWindowTextA(_T("Stage Disconnect"));
		((CStatic*)GetDlgItem(IDC_ICON_CON))->SetIcon(m_LedIcon[2]);
		m_bConnect = FALSE;
	}
}


void CNavigationStageDlg::OnBnClickedButtonBufferStart()
{
	RunBuffer(17);
	stage_buffer_state = true;
}


void CNavigationStageDlg::OnBnClickedButtonBufferStop()
{
	StopBuffer(17);
	stage_buffer_state = false;
}


void CNavigationStageDlg::OnBnClickedButtonBufferStart2()
{
	RunBuffer(29);
}


void CNavigationStageDlg::OnBnClickedButtonBufferStop2()
{
	RunBuffer(30);
}
