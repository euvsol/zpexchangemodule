﻿// CStateTestDlg.cpp: 구현 파일
//

#include "pch.h"
#include "Include.h"
#include "Extern.h"
#include <iomanip>

// CStateTestDlg 대화 상자

IMPLEMENT_DYNAMIC(CStateTestDlg, CDialogEx)

CStateTestDlg::CStateTestDlg(CWnd* pParent /*=nullptr*/)
	: CDialogEx(IDD_STAGE_TEST_DIALOG, pParent)
{

}

CStateTestDlg::~CStateTestDlg()
{
	StageLongRunStopFlag = true;
}

void CStateTestDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_EDIT_Y_POSITION, m_y_pos_edit);
	DDX_Control(pDX, IDC_EDIT_X_POSITION, m_x_pos_edit);
	DDX_Control(pDX, IDC_EDIT_LOG, m_edit_log);
	DDX_Control(pDX, IDC_EDIT_X_LOAD, m_edit_x_load);
	DDX_Control(pDX, IDC_EDIT_Y_LOAD, m_edit_y_load);
	DDX_Control(pDX, IDC_EDIT_LOG2, m_edit_log2);
	DDX_Control(pDX, IDC_EDIT_X_INPOS, m_x_edit_inpos);
	DDX_Control(pDX, IDC_EDIT_Y_INPOS, m_y_edit_inpos);
	DDX_Control(pDX, IDC_EDIT_X_POSITION_CUR, m_current_xpos);
	DDX_Control(pDX, IDC_EDIT_Y_POSITION_CUR, m_current_ypos);
	DDX_Control(pDX, IDC_EDIT_X_LOAD_CUR, m_current_xload);
	DDX_Control(pDX, IDC_EDIT_Y_LOAD_CUR, m_current_yload);
	DDX_Control(pDX, IDC_EDIT_X_INPOS_CUR, m_current_xinpos);
	DDX_Control(pDX, IDC_EDIT_Y_INPOS_CUR, m_current_yinpos);
}


BEGIN_MESSAGE_MAP(CStateTestDlg, CDialogEx)
	ON_WM_TIMER()
	ON_WM_DESTROY()
	ON_BN_CLICKED(IDC_MEASUREMENT_START2, &CStateTestDlg::OnBnClickedMeasurementStart2)
	ON_BN_CLICKED(IDC_MEASUREMENT_START4, &CStateTestDlg::OnBnClickedMeasurementStart4)
	ON_BN_CLICKED(IDC_REALTIMESTAGE_REPORT_CHECKBOX, &CStateTestDlg::OnBnClickedRealtimestageReportCheckbox)
END_MESSAGE_MAP()


// CStateTestDlg 메시지 처리기

BOOL CStateTestDlg::OnInitDialog()
{
	CDialogEx::OnInitDialog();

	m_LedIcon[0] = (HICON)::LoadImage(::AfxGetInstanceHandle(), MAKEINTRESOURCE(IDI_LEDOFF), IMAGE_ICON, 32, 32, LR_DEFAULTCOLOR);
	m_LedIcon[1] = (HICON)::LoadImage(::AfxGetInstanceHandle(), MAKEINTRESOURCE(IDI_LEDONGREEN), IMAGE_ICON, 32, 32, LR_DEFAULTCOLOR);
	m_LedIcon[2] = (HICON)::LoadImage(::AfxGetInstanceHandle(), MAKEINTRESOURCE(IDI_LEDONRED), IMAGE_ICON, 32, 32, LR_DEFAULTCOLOR);


	((CStatic*)GetDlgItem(IDC_ICON_START_STOP))->SetIcon(m_LedIcon[0]);
	((CStatic*)GetDlgItem(IDC_ICON_REPORT_CAL))->SetIcon(m_LedIcon[0]);
	((CStatic*)GetDlgItem(IDC_ICON_STAGE_LOADING))->SetIcon(m_LedIcon[0]);

	m_edit_log.SetWindowTextA(_T("Stage 대기 중"));

	scan_start_default_x_pos = SCAN_XPOS_DEFAULT;
	scan_start_default_y_pos = SCAN_YPOS_DEFAULT;
	Stage_measurement_stop = false;


	StageLongRunStopFlag = false;

	SetDlgItemTextA(IDC_EDIT_X_CNT, "0");
	SetDlgItemTextA(IDC_EDIT_Y_CNT, "0");


	return TRUE;  // return TRUE unless you set the focus to a control
				  // 예외: OCX 속성 페이지는 FALSE를 반환해야 합니다.
}

void CStateTestDlg::OnDestroy()
{
	CDialogEx::OnDestroy();

	KillTimer(TEST_UPDATE_TIMER);
	Stage_measurement_stop = true;
	StageLongRunStopFlag = true;

}




void CStateTestDlg::OnTimer(UINT_PTR nIDEvent)
{
	KillTimer(nIDEvent);
	switch (nIDEvent)
	{
	case TEST_UPDATE_TIMER:
		if (StageLongRunStopFlag == false)
		{
			Stage_Pos_Check();
			SetTimer(nIDEvent, 100, NULL);
		}
	default:
		break;
	}

	CDialogEx::OnTimer(nIDEvent);
}

void CStateTestDlg::Stage_Pos_Check()
{
	double pos_mm[2];
	double x_load = 0.0;
	double y_load = 0.0;
	double x_Inpos = 0.0;
	double y_Inpos = 0.0;

	CString tmpx, tmpy;
	CString loadx, loady;
	CString inposx, inposy;


	pos_mm[0] = g_pNavigationStage->GetPosmm(STAGE_X_AXIS);
	pos_mm[1] = g_pNavigationStage->GetPosmm(STAGE_Y_AXIS);
	tmpx.Format("%3.7f", pos_mm[STAGE_X_AXIS]);
	tmpy.Format("%3.7f", pos_mm[STAGE_Y_AXIS]);

	//g_pNavigationStage->RunBuffer(25);
	
	x_load = g_pNavigationStage->GetGlobalRealVariable("DOUT_Y1");
	y_load = g_pNavigationStage->GetGlobalRealVariable("DOUT_Y2");
	loadx.Format("%3.7f", x_load);
	loady.Format("%3.7f", y_load);

	x_Inpos = g_pNavigationStage->GetGlobalRealVariable("PE1");
	y_Inpos = g_pNavigationStage->GetGlobalRealVariable("PE0");
	inposx.Format("%3.8E", x_Inpos);
	inposy.Format("%3.8E", y_Inpos);


	if (Stage_measurement_stop == false)
	{
		if (m_current_xpos.m_hWnd != NULL)
			m_current_xpos.SetWindowText(tmpx);
		
		if (m_current_ypos.m_hWnd != NULL)
			m_current_ypos.SetWindowText(tmpy);

		if (m_current_xinpos.m_hWnd != NULL)
			m_current_xinpos.SetWindowText(inposx);

		if (m_current_yinpos.m_hWnd != NULL)
			m_current_yinpos.SetWindowText(inposy);

		if (g_pNavigationStage->stage_buffer_state)
		{
			if (m_current_xload.m_hWnd != NULL)
				m_current_xload.SetWindowText(loadx);
		
			if (m_current_yload.m_hWnd != NULL)
				m_current_yload.SetWindowText(loady);
		}
		else
		{
			if (m_current_xload.m_hWnd != NULL)
				m_current_xload.SetWindowText("buffer Fail");
			
			if (m_current_yload.m_hWnd != NULL)
				m_current_yload.SetWindowText("buffer Fail");
		}

		
	}
	else
	{
		m_y_pos_edit.SetWindowText(_T("측정중이 아닙니다"));
		m_x_pos_edit.SetWindowText(_T("측정중이 아닙니다"));
	}
}




void CStateTestDlg::OnBnClickedMeasurementStart2()
{
	StageLongRunStopFlag = true;
	m_edit_log.SetWindowTextA(_T("Stage  STOP 버튼 누름 "));
	((CStatic*)GetDlgItem(IDC_ICON_START_STOP))->SetIcon(m_LedIcon[2]);
}

void CStateTestDlg::ReportData(double x, double y, double* x_load, double* y_load, double* x_Inpos, double* y_Inpos)
{
	int num = 0;

	double pos_mm[2];

	double *get_xLoad_data;
	double *get_yLoad_data;
	double *get_xInPos_data;
	double *get_yInPos_data;

	double xloadsumData = 0.0;
	double yloadsumData = 0.0;
	
	double xInpossumData = 0.0;
	double yInpossumData = 0.0;
	
	double xloadavgData = 0.0;
	double yloadavgData = 0.0;

	double xInposavgData = 0.0;
	double yInposavgData = 0.0;

	double xload_max, xinpos_max;
	double yload_max, yinpos_max;

	double X_Inpos_value, Y_Inpos_value;
	double XInposVariance, YInposVariance;
	double XInposSTD, YInposSTD;
	
	XInposVariance = 0.0;
	YInposVariance = 0.0;
	X_Inpos_value = 0.0;
	Y_Inpos_value = 0.0;
	XInposSTD = 0.0;
	YInposSTD = 0.0;

	pos_mm[0] = y;
	pos_mm[1] = x;

	xload_max = 0.0;
	xinpos_max = 0.0;
	yload_max = 0.0;
	yinpos_max = 0.0;

	get_xLoad_data = new double[cnt];
	get_yLoad_data = new double[cnt];
	get_xInPos_data = new double[cnt];
	get_yInPos_data = new double[cnt];
	
	CString Xpos;
	CString Ypos;
	CString Num_str;
	CString get_xLoad_data_str;
	CString get_yLoad_data_str;
	CString get_xInPos_data_str;
	CString get_yInPos_data_str;

	memset(get_xLoad_data, 0.0, sizeof(get_xLoad_data));
	memset(get_xLoad_data, 0.0, sizeof(get_yLoad_data));
	memset(get_xInPos_data, 0.0, sizeof(get_xLoad_data));
	memset(get_yInPos_data, 0.0, sizeof(get_yLoad_data));

	Xpos.Format("%lf", pos_mm[1]);
	Ypos.Format("%lf", pos_mm[0]);

	//pos_mm[0] = g_pNavigationStage->GetPosmm[STAGE_Y_AXIS];
	//pos_mm[1] = g_pNavigationStage->GetPosmm[STAGE_X_AXIS];

	do
	{
		get_xLoad_data[num] = abs(g_pNavigationStage->GetGlobalRealVariable("DOUT_Y2"));
		get_yLoad_data[num] = abs(g_pNavigationStage->GetGlobalRealVariable("DOUT_Y1"));
		get_xInPos_data[num] = g_pNavigationStage->GetGlobalRealVariable("PE1");
		get_yInPos_data[num] = g_pNavigationStage->GetGlobalRealVariable("PE0");
		
		get_xLoad_data_str.Format("%3.8f", get_xLoad_data[num]);
		get_yLoad_data_str.Format("%3.8f", get_yLoad_data[num]);
		get_xInPos_data_str.Format("%3.8E", get_xInPos_data[num]);
		get_yInPos_data_str.Format("%3.8E", get_yInPos_data[num]);
		num += 1;
		Num_str.Format("%d", num);
		
		
		SaveLogFile("Stage Inposition & Load Report SubData", _T((LPSTR)(LPCTSTR)("Num : " + Num_str + "    X POS (mm) : " + Xpos + "    Y POS (mm) : " + Ypos + "      XLoad (%) : " + get_xLoad_data_str + "     YLoad (%) : " + get_yLoad_data_str
			+ "      XInposition (mm) : " + get_xInPos_data_str + "     YInposition (mm) : " + get_yInPos_data_str)));
		WaitSec(1);
	} while (num < cnt);
	num = 0;


	for (int i = 0; i < cnt; i++)
	{
		xloadsumData += get_xLoad_data[i];
		yloadsumData += get_yLoad_data[i];
		xInpossumData += get_xInPos_data[i];
		yInpossumData += get_yInPos_data[i];
	}

	xloadavgData = (xloadsumData / cnt);
	yloadavgData = (yloadsumData / cnt);
	xInposavgData = (xInpossumData / cnt);
	xInposavgData = (yInpossumData / cnt);

	for (int i = 0; i < cnt; i++)
	{
		X_Inpos_value += pow((get_xInPos_data[i] - xInposavgData), 2);
		Y_Inpos_value += pow((get_yInPos_data[i] - xInposavgData), 2);
	}

	XInposVariance = (X_Inpos_value / cnt);
	YInposVariance = (Y_Inpos_value / cnt);

	XInposSTD = sqrt(XInposVariance);
	YInposSTD = sqrt(YInposVariance);

	*x_load = xloadavgData;
	*y_load = yloadavgData;
	*x_Inpos = XInposSTD;
	*y_Inpos = YInposSTD;
	
	delete[] get_xLoad_data ;
	delete[] get_yLoad_data ;
	delete[] get_xInPos_data ;
	delete[] get_yInPos_data ;
}

void CStateTestDlg::OnBnClickedMeasurementStart4()
{
	if (StageLongRun() != TRUE)
	{
		((CStatic*)GetDlgItem(IDC_ICON_START_STOP))->SetIcon(m_LedIcon[2]);
		KillTimer(TEST_UPDATE_TIMER);
		IniEditControl();
	}
}

void CStateTestDlg::IniEditControl()
{

	CString str;
	str.Format("-");

	GetDlgItem(IDC_EDIT_POSITION_POINT)->SetWindowTextA(str);
	GetDlgItem(IDC_EDIT_Y_START_POS)->SetWindowTextA(str);
	GetDlgItem(IDC_EDIT_X_START_POS)->SetWindowTextA(str);
	
	m_x_pos_edit.SetWindowTextA(str);
	m_y_pos_edit.SetWindowTextA(str);
	m_edit_x_load.SetWindowTextA(str);
	m_edit_y_load.SetWindowTextA(str);
	m_x_edit_inpos.SetWindowTextA(str);
	m_y_edit_inpos.SetWindowTextA(str);

}

BOOL CStateTestDlg::StageLongRun()
{

	BOOL Return_Flag = TRUE;
	((CStatic*)GetDlgItem(IDC_ICON_START_STOP))->SetIcon(m_LedIcon[1]);
	CString x_str;
	CString y_str;
	CString position_str;

	CString std_x_load;
	CString std_y_load;
	CString std_x_Inpos;
	CString std_y_Inpos;

	std_x_load.Empty();
	std_y_load.Empty();
	std_x_Inpos.Empty();
	std_y_Inpos.Empty();

	position_str.Empty();
	x_str.Empty();
	y_str.Empty();

	m_edit_log.SetWindowTextA(_T("Stage 측정을 시작 합니다"));
	StageLongRunStopFlag = false;

	int cont = 0;
	int testCont = 0;

	double target_x_pos; // 측정 범위 가능 검사 변수
	double target_y_pos;

	double move_x_pos;	// 실제 이동 위치 변수
	double move_y_pos;


	double x_pos = 0.0;
	double y_pos = 0.0;
	double x_load = 0.0;
	double y_load = 0.0;

	double x_InposSTD = 0.0;
	double y_InposSTD = 0.0;

	double x_loadavg = 0.0;
	double y_loadavg = 0.0;

	int position = 0;
	int x = 0, y = 0;

	int nRet = RUN;

	double current_x_pos = 0.0;
	double current_y_pos = 0.0;
	CString str_current_x_pos;
	CString str_current_y_pos;
	double home_current_x_pos = 0.0;
	double home_current_y_pos = 0.0;

	double get_x_load = 0.0;
	double get_y_load = 0.0;

	CString strTemp;

	if (g_pNavigationStage == NULL)
	{
		Return_Flag = FALSE;
		return Return_Flag;
	}



	//STAGE 움직임 가능 여부 확인. 
	CAutoMessageDlg MsgBoxAuto(this);

	// 측정 중 일 경우 FALSE, 측정 끝나거나 중지되면 TRUE, 
	//m_Stage_Measrue_Start = FALSE;
	//if (m_Stage_Measrue_Start == FALSE)	SetTimer(MASK_FLATNESS_MEASUREMENT_TIMER, 100, NULL);
	////////////////////////////////////
	// MASK SIZE :: 152.4 x 152.4 mm
	////////////////////////////////////

	m_edit_log.SetWindowTextA(_T("Stage 0,0 이동 중"));
	g_pNavigationStage->MoveXYUntilInposition(0.0, 0.0);

	CString start_x_pos_str;
	CString start_y_pos_str;
	double orgin_x_pos = g_pNavigationStage->GetPosmm(STAGE_X_AXIS);
	double orgin_y_pos = g_pNavigationStage->GetPosmm(STAGE_Y_AXIS);
	start_x_pos_str.Format("%3.7f", orgin_x_pos);
	start_y_pos_str.Format("%3.7f", orgin_y_pos);
	SetDlgItemTextA(IDC_EDIT_X_START_POS, start_x_pos_str);
	SetDlgItemTextA(IDC_EDIT_Y_START_POS, start_y_pos_str);


	SetTimer(TEST_UPDATE_TIMER, 100, NULL);
	
	if (StageLongRunStopFlag == true)
	{
		::AfxMessageBox(" Stage Long Run Stop ! ");
		m_edit_log.SetWindowTextA(_T("Stage  STOP 버튼 누름 "));
		position = 0;
		KillTimer(TEST_UPDATE_TIMER);
		m_y_pos_edit.SetWindowText(_T("-"));
		m_x_pos_edit.SetWindowText(_T("-"));
		position = 0;
		Return_Flag = FALSE;
		return Return_Flag;
	}
	WaitSec(2);

	y = 0;
	x = 0;

	position = 0;
	position_str.Format("%d", position);

	GetDlgItem(IDC_EDIT_POSITION_POINT)->SetWindowTextA(position_str);

	int mon = CTime::GetCurrentTime().GetMonth();
	int day = CTime::GetCurrentTime().GetDay();
	int hour = CTime::GetCurrentTime().GetHour();
	int min = CTime::GetCurrentTime().GetMinute();

	CString datafile;
	CString path;

	path = LOG_PATH;

	if (!IsFolderExist(path))
	{
		CreateDirectory(path, NULL);
	}

	CString strDate, strTime;
	GetSystemDateTime(&strDate, &strTime);
	path += "\\";
	path += strDate;
	if (!IsFolderExist(path))
	{
		CreateDirectory(path, NULL);
	}

	datafile.Format(_T("%s\\Stage_Long_Run_Measurement_data_%d_%d_%d_%d.txt"), path, mon, day, hour, min);


	std::ofstream stagelongrun_measurement_datafile(datafile);

	stagelongrun_measurement_datafile << "Date" << "\t" << "Time" << "Position" << "\t" << "x_pos" << "\t" << "y_pos" << "\t" << "x_load" << "\t" << "y_load" << "\t" << "x_Inpos" << "\t" << "y_Inpos" << "\n";
	
	if (StageLongRunStopFlag == true)
	{
		::AfxMessageBox("  Stage Long Run Stop ! ");
		m_edit_log.SetWindowTextA(_T("Stage  STOP 버튼 누름 "));
		KillTimer(TEST_UPDATE_TIMER);
		m_y_pos_edit.SetWindowText(_T("-"));
		m_x_pos_edit.SetWindowText(_T("-"));
		position = 0;
		stagelongrun_measurement_datafile << std::endl;
		if (stagelongrun_measurement_datafile.is_open() == true)
		{
			stagelongrun_measurement_datafile.close();
		}
		Return_Flag = FALSE;
		return Return_Flag;
	}

	m_edit_log.SetWindowTextA(_T("Stage 측정 중 !!"));

	do
	{
		while ((y < STAGE_SCALE)) // Y축 이동
		{
			if (y == STAGE_SCALE)	y_pos = orgin_y_pos;
			else y_pos = g_pNavigationStage->GetPosmm(STAGE_Y_AXIS);

			if (StageLongRunStopFlag == true)
			{
				KillTimer(TEST_UPDATE_TIMER);
				m_y_pos_edit.SetWindowText(_T("-"));
				m_x_pos_edit.SetWindowText(_T("-"));
				::AfxMessageBox("  Stage Long Run Stop ! ");
				m_edit_log.SetWindowTextA(_T("Stage  STOP 버튼 누름 "));
				position = 0;
				nRet = -1;
				stagelongrun_measurement_datafile << std::endl;
				if (stagelongrun_measurement_datafile.is_open() == true)
				{
					stagelongrun_measurement_datafile.close();
				}
				Return_Flag = FALSE;
				return Return_Flag;
			}

			while ((x < STAGE_SCALE)) // X 축 이동
			{
				if (x == 0)	x_pos = orgin_x_pos;
				else x_pos = g_pNavigationStage->GetPosmm(STAGE_X_AXIS);

				if (StageLongRunStopFlag == true)
				{
					KillTimer(TEST_UPDATE_TIMER);
					m_y_pos_edit.SetWindowText(_T("-"));
					m_x_pos_edit.SetWindowText(_T("-"));
					::AfxMessageBox("  Stage Long Run Stop ! ");
					m_edit_log.SetWindowTextA(_T("Stage  STOP 버튼 누름 "));
					position = 0;
					nRet = -1;
					stagelongrun_measurement_datafile << std::endl;
					if (stagelongrun_measurement_datafile.is_open() == true)
					{
						stagelongrun_measurement_datafile.close();
					}
					Return_Flag = FALSE;
					return Return_Flag;
				}

				//z_pos_str.Format("%3.7f", z_pos);
				//x_pos_str.Format("%3.7f", x_pos);
				//y_pos_str.Format("%3.7f", y_pos);

				position_str.Format("%d", position);
				WaitSec(2);

				cont = position + 1;

				testCont++;
				strTemp.Format(_T("%d 번째 Stage 측정 중 입니다."), testCont);
				m_edit_log.SetWindowTextA(strTemp);

				int year = CTime::GetCurrentTime().GetYear();
				int mon = CTime::GetCurrentTime().GetMonth();
				int day = CTime::GetCurrentTime().GetDay();
				int hour = CTime::GetCurrentTime().GetHour();
				int min = CTime::GetCurrentTime().GetMinute();
				int sec = CTime::GetCurrentTime().GetSecond();

				//flatness_measurement_datafile << std::setprecision(3)<< mon<<"."<<day << "."<<hour << "."<<min << "."<<sec<< "\t" << position_str << "\t" << x_pos_str << "\t" << y_pos_str << "\t" << "\n";
				//flatness_measurement_4cap_value << std::setprecision(3) << position_str << "\t" << x_pos_str << "\t" << y_pos_str << "\t" << tx_pos_str << "\t" << ty_pos_str << "\t" << z_pos_str << "\t" << cap1_str << "\t" << cap2_str << "\t" << cap3_str << "\t" << cap4_str << "\n";
				//flatness_measurement_cap_value << std::setprecision(3) << cap1_str << "\n";

				
				current_x_pos = g_pNavigationStage->GetPosmm(STAGE_X_AXIS);
				current_y_pos = g_pNavigationStage->GetPosmm(STAGE_Y_AXIS);
				
				str_current_x_pos.Format("%3.7f", current_x_pos);
				str_current_y_pos.Format("%3.7f", current_y_pos);

				x_load = g_pNavigationStage->GetGlobalRealVariable("DOUT_Y1");
				y_load = g_pNavigationStage->GetGlobalRealVariable("DOUT_Y2");

				((CStatic*)GetDlgItem(IDC_ICON_REPORT_CAL))->SetIcon(m_LedIcon[1]);
				ReportData(current_x_pos, current_y_pos , &x_loadavg , &y_loadavg, &x_InposSTD, &y_InposSTD);
				((CStatic*)GetDlgItem(IDC_ICON_REPORT_CAL))->SetIcon(m_LedIcon[0]);
				
				std_x_load.Format("%3.4f", x_loadavg);
				std_y_load.Format("%3.4f", y_loadavg);

				std_x_Inpos.Format("%3.8e", x_InposSTD);
				std_y_Inpos.Format("%3.8e", y_InposSTD);

				//double ReportInpos(double x_inpos, double y_inpos);

				stagelongrun_measurement_datafile << std::setprecision(3) << year << "." << mon << "."<< day << "\t" << hour << ":" << min << ":" << sec << "\t" << position_str << "\t" << str_current_x_pos << "\t" << str_current_y_pos << "\t" << x_loadavg << "\t" << x_loadavg << "\t" << x_InposSTD << "\t" << y_InposSTD << "\n";

				//home_current_x_pos = g_pNavigationStage->GetPosmm(STAGE_X_AXIS);
				//home_current_y_pos = g_pNavigationStage->GetPosmm(STAGE_Y_AXIS);
				//stagelongrun_measurement_datafile << std::setprecision(3) << mon << "-" << day << "\t" << hour << ":" << min << ":" << sec << "\t" << position_str << "\t" << home_current_x_pos << "\t" << home_current_y_pos << "\t" << x_loadavg << "\t" << y_loadavg << "\t" << "\n";
				//g_pNavigationStage->MoveXYUntilInposition(-current_x_pos, -current_y_pos);
				//WaitSec(10);

				CString cont_str;
				cont_str.Format("%d", cont);
				GetDlgItem(IDC_EDIT_POSITION_POINT)->SetWindowTextA(cont_str);
				
				m_x_pos_edit.SetWindowTextA(str_current_x_pos);
				m_y_pos_edit.SetWindowTextA(str_current_y_pos);
				m_edit_x_load.SetWindowTextA(std_x_load);
				m_edit_y_load.SetWindowTextA(std_y_load);
				m_x_edit_inpos.SetWindowTextA(std_x_Inpos);
				m_y_edit_inpos.SetWindowTextA(std_y_Inpos);

				// 25 mm 씩 감소하여 이동.
				move_x_pos = x_pos - STAGE_X_RANGE;
				move_y_pos = y_pos;
				if (g_pNavigationStage->MoveXYUntilInposition(move_x_pos, move_y_pos) != XY_NAVISTAGE_OK)
				{
					AfxMessageBox("LongRunTest :: Stage 구동 불가");
					Return_Flag = FALSE;
					return Return_Flag;
				}
				current_x_pos = g_pNavigationStage->GetPosmm(STAGE_X_AXIS);
				current_y_pos = g_pNavigationStage->GetPosmm(STAGE_Y_AXIS);

				((CStatic*)GetDlgItem(IDC_ICON_STAGE_LOADING))->SetIcon(m_LedIcon[1]);
				WaitSec(10);
				((CStatic*)GetDlgItem(IDC_ICON_STAGE_LOADING))->SetIcon(m_LedIcon[0]);
				position++; // LOG 기록용 
				x++;
	
				x_str.Format("%d", x);
				y_str.Format("%d", y);

				SetDlgItemTextA(IDC_EDIT_X_CNT, x_str);
				SetDlgItemTextA(IDC_EDIT_Y_CNT, y_str);

				if (x == STAGE_SCALE) break;
			}

			if (nRet != RUN)
			{
				Return_Flag = FALSE;
				break; // x 방향 Error 발생.
			}

			move_x_pos = orgin_x_pos; // X 축 초기 값으로 이동.
			move_y_pos = y_pos - STAGE_Y_RANGE; // Y 축 15 mm 씩 이동.

			g_pNavigationStage->MoveXYUntilInposition(move_x_pos, move_y_pos);
			WaitSec(2);

			if (nRet != RUN) break; // y 방향 Error 발생.

			x = 0;
			y++;

			if (y == STAGE_SCALE) break;
		}
		
		if (StageLongRunStopFlag == true)
		{
			KillTimer(TEST_UPDATE_TIMER);
			m_y_pos_edit.SetWindowText(_T("-"));
			m_x_pos_edit.SetWindowText(_T("-"));
			::AfxMessageBox("  Stage Long Run Stop ! ");
			m_edit_log.SetWindowTextA(_T("Stage  STOP 버튼 누름 "));
			position = 0;
			nRet = -1;
			stagelongrun_measurement_datafile << std::endl;
			if (stagelongrun_measurement_datafile.is_open() == true)
			{
				stagelongrun_measurement_datafile.close();
			}
			Return_Flag = FALSE;
			return Return_Flag;
		}

		x = 0;
		y = 0;
		position = 0;

		g_pNavigationStage->MoveXYUntilInposition(0.0, 0.0);
		WaitSec(3);

		orgin_x_pos = g_pNavigationStage->GetPosmm(STAGE_X_AXIS);
		orgin_y_pos = g_pNavigationStage->GetPosmm(STAGE_Y_AXIS);
		start_x_pos_str.Format("%3.7f", orgin_x_pos);
		start_y_pos_str.Format("%3.7f", orgin_y_pos);
		SetDlgItemTextA(IDC_EDIT_X_START_POS, start_x_pos_str);
		SetDlgItemTextA(IDC_EDIT_Y_START_POS, start_y_pos_str);

	} while (!StageLongRunStopFlag);

	stagelongrun_measurement_datafile << std::endl;
	if (stagelongrun_measurement_datafile.is_open() == true)
	{
		stagelongrun_measurement_datafile.close();
	}

	//flatness_measurement_4cap_value << std::endl;
	//if (flatness_measurement_4cap_value.is_open() == true)
	//{
	//	flatness_measurement_4cap_value.close();
	//}
	//
	//flatness_measurement_cap_value << std::endl;
	//if (flatness_measurement_cap_value.is_open() == true)
	//{
	//	flatness_measurement_cap_value.close();
	//}
	//GetDlgItem(IDC_MASK_PRO)->SetWindowTextA(_T("측정 완료"));

	if (nRet != RUN)
	{
		Return_Flag = FALSE;
		return Return_Flag; // Error 발생 시
	}
	
	//GetDlgItem(IDC_CHECK_MASK_FLAT_SET)->EnableWindow(true);
	//GetDlgItem(IDC_CHECK_MASK_FLAT_DEFAULT)->EnableWindow(true);
	//GetDlgItem(IDC_CHECK_SOURCE)->EnableWindow(true);
	//GetDlgItem(IDC_CHECK_MASK_FLAT)->EnableWindow(true);
	//GetDlgItem(IDC_EDIT_TEST_MASK_FLAT_NUM)->SetWindowTextA(_T("0"));

	position = 0;
	m_edit_log.SetWindowTextA(_T("Stage 측정을 종료 합니다"));
	KillTimer(TEST_UPDATE_TIMER);

	return Return_Flag;

}

void CStateTestDlg::OnBnClickedRealtimestageReportCheckbox()
{
	CButton* btn = (CButton*)GetDlgItem(IDC_REALTIMESTAGE_REPORT_CHECKBOX);
	int nCheck = btn->GetCheck();
	if (nCheck)
	{
		btn->SetWindowTextA(_T("Monitor Stop"));
		KillTimer(TEST_UPDATE_TIMER);
	}
	else
	{
		btn->SetWindowTextA(_T("Monitoring"));
		SetTimer(TEST_UPDATE_TIMER, 100, NULL);
	}
}
