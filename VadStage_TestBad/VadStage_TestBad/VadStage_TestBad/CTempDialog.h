﻿#pragma once


// CTempDialog 대화 상자

class CTempDialog : public CDialogEx , public CEthernetCom
{
	DECLARE_DYNAMIC(CTempDialog)

public:
	CTempDialog(CWnd* pParent = nullptr);   // 표준 생성자입니다.
	virtual ~CTempDialog();

// 대화 상자 데이터입니다.
//#ifdef AFX_DESIGN_TIME
	enum { IDD = IDD_TEMP_DIALOG };
//#endif

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 지원입니다.

	DECLARE_MESSAGE_MAP()


public:
//	virtual	int				SendData(char *lParam, int nTimeOut = 0, int nRetryCnt = 3);
//	virtual	int				ReceiveData(char *lParam); ///< Recive
	//virtual int				OpenTcpIpSocket(char *Ip, int nPort, BOOL bIsServerSocket, int nReceiveMaxBufferSize);


	//virtual	int				SendData(char *lParam, int nTimeOut = 0, int nRetryCnt = 3);
	//virtual int				SendData(int nSize, BYTE *lParam, int nTimeOut = 0, int nRetryCnt = 3);

	//virtual	int				ReceiveData(char *lParam); ///< Recive

	//virtual	int				ReceiveData(char *lParam, DWORD dwRead); ///< Recive
	//virtual int				OpenPort(CString sPortName, DWORD dwBaud, BYTE wByte, BYTE wStop, BYTE wParity);

	virtual BOOL OnInitDialog();
	afx_msg void OnDestroy();
	afx_msg void OnTimer(UINT_PTR nIDEvent);


	BYTE						m_BYTE[15];
	bool						m_TMP_SendState;

};
