﻿#pragma once


// CStateTestDlg 대화 상자

class CStateTestDlg : public CDialogEx , public CECommon
{
	DECLARE_DYNAMIC(CStateTestDlg)

public:
	CStateTestDlg(CWnd* pParent = nullptr);   // 표준 생성자입니다.
	virtual ~CStateTestDlg();

// 대화 상자 데이터입니다.
//#ifdef AFX_DESIGN_TIME
	enum { IDD = IDD_STAGE_TEST_DIALOG };
//#endif

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 지원입니다.

	DECLARE_MESSAGE_MAP()
public:
	HICON m_LedIcon[3];

	afx_msg void OnTimer(UINT_PTR nIDEvent);
	afx_msg void OnDestroy();
	afx_msg void OnBnClickedMeasurementStart2();
	afx_msg void OnBnClickedMeasurementStart4();
	afx_msg void OnBnClickedRealtimestageReportCheckbox();

	virtual BOOL OnInitDialog();

	BOOL m_Stage_Measrue_Start;
	bool m_Logging_Flage;


	double scan_start_default_x_pos; // scan area pos Default 변수 값
	double scan_start_default_y_pos;

	double get_x_pos; // 사용자 입력 좌표값 변수 & 실제 측정에 사용 되는 좌표 변수.
	double get_y_pos;

	bool Stage_measurement_stop;

	CEdit m_y_pos_edit;
	CEdit m_x_pos_edit;
	CEdit m_edit_log;
	CEdit m_edit_x_load;
	CEdit m_edit_y_load;
	CEdit m_edit_log2;
	CEdit m_x_edit_inpos;
	CEdit m_y_edit_inpos;
	CEdit m_current_xpos;
	CEdit m_current_ypos;
	CEdit m_current_xload;
	CEdit m_current_yload;
	CEdit m_current_xinpos;
	CEdit m_current_yinpos;
	
	void Stage_Pos_Check();
	void IniEditControl();
	void ReportData(double x, double y, double* x_load, double* y_load, double* x_Inpos, double* y_Inpos);

	BOOL StageLongRun();
	bool StageLongRunStopFlag;

	int cnt = 10;

};
