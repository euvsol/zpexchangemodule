#pragma once

//#define MAIN_PATH								_T("C:\\EUVSolution")											// 설비 루트 디렉토리 경로
//#define CONFIG_PATH								_T("C:\\EUVSolution\\Config")									// Config. 디렉토리 경로
//#define CONFIG_FILE_FULLPATH					_T("C:\\EUVSolution\\Config\\ESOL_Config.ini")					// Config. 파일 경로
//#define RECOVERY_FILE_FULLPATH					_T("C:\\EUVSolution\\Config\\Recovery.ini")						// Recovery 파일 경로
//#define IOMAP_FILE_FULLPATH						_T("C:\\EUVSolution\\Config\\IOMap_SREM.ini")					// IO map 파일 경로
//#define LOG_PATH								_T("C:\\EUVSolution\\Log")										// Log 디렉토리 경로
//#define LOG_RUN_PATH							_T("C:\\EUVSolution\\Long_Run_Log")							// Log 디렉토리 경로
//#define RECIPE_PATH								_T("C:\\EUVSolution\\Recipe")									// Recipe 파일 경로
//#define RECIPE_MAP_IMAGE_PATH					_T("C:\\EUVSolution\\Recipe\\MapImage")							// Map Image 파일 경로
//#define ALARMCODEFILE							_T("C:\\EUVSolution\\Config\\ALARMCode.ini")					// AlarmCode 파일 경로
//#define IMAGEFILE_PATH							_T("D:\\EUVSolution\\ADAM")										// Image 파일 경로  --> D 드라이브로 수정_2020/04/10_KYD
//#define OM_IMAGEFILE_PATH						_T("C:\\EUVSolution\\OM")										// Om Image 파일 경로
//#define STAGEPOSITION_FILE_FULLPATH				_T("C:\\EUVSolution\\Config\\Stage_Position_DB.cdb")			// Stage Position 저장 파일 경로
//#define PISTAGE_TEST_POSITION_FILE_FULLPATH		_T("C:\\EUVSolution\\Config\\PI_Stage_Test_Position_DB.cdb")	// PI Stage Test Point Position 저장 파일 경로
//#define OM_CAMERA_CCF_FILE_FULLPATH				_T("C:\\EUVSolution\\Config\\Nano-C2020_OmDefaultConfig.dcf")	// Om Camera CCF 파일 경로
//#define XRAY_CAMERA_CONFIG_FILE_FULLPATH		_T("C:\\EUVSolution\\Config\\XRAY_Config.ini")
//#define PHASE_RESULT_PATH						_T("C:\\Phase Result")
#define PM_IMAGE_PATH							_T("D:\\EUVSolution\\Config\\ImgModel\\ZpExchangePM.bmp")
#define DUMMY_IMAGE_PATH						_T("D:\\EUVSolution\\Config\\ImgModel\\DummyImage.bmp")
#define CAMERA_DCF_FILE							_T("D:\\EUVSolution\\Config\\Nano-C2020_OmDefaultConfig.dcf")
#define LOG_PATH								_T("D:\\EUVSolution\\Log")
#define CONFIG_FILE_FULLPATH					_T("D:\\EUVSolution\\Config\\ESOL_Config.ini")

// 윈도우 생성/소멸
#define CREATECLASS(ptr, Class)										if(ptr == NULL) { ptr = new Class;}
#define CREATEWINDOW(ptr, Class, pWnd, rect, dwStyle, nShow)		if(ptr == NULL) { ptr = new Class; LPCTSTR className = AfxRegisterWndClass(CS_DBLCLKS,  AfxGetApp()->LoadStandardCursor(IDC_ARROW)); ptr->CreateEx(NULL, className, NULL, dwStyle, rect, this, 0); ptr->ShowWindow(nShow);}
#define CREATEDIALOG(ptr, Class, pWnd, rectoffset, nShow)			if(ptr == NULL) { ptr = new Class; ptr->Create(Class::IDD, pWnd);	CRect rt; ptr->GetWindowRect(&rt); rt.OffsetRect(rectoffset); ptr->MoveWindow(rt); ptr->ShowWindow(nShow);}
#define DELETECLASS(ptr)											if(ptr) { delete ptr; ptr = NULL;}
#define DELETEWINDOW(ptr)											if(ptr) {(ptr)->DestroyWindow(); delete (ptr); ptr = NULL;}
#define DELETEDIALOG(ptr)											if(ptr) {(ptr)->DestroyWindow(); delete (ptr); ptr = NULL;}


#define NAVISTAGE_UPDATE_TIMER	 1
#define TEST_UPDATE_TIMER		 2
#define LOGGING_START			 3
#define	STAGE_UPDATE_TIMER		 4
#define STAGE_REPORT_TIMER		 5
#define ZPEXCHANGE_UPDATE_TIMER	 6
#define ZPEXCHANGE_DISPLAY_TIMER 7
#define	REVOLVER_UPDATE_TIMER	 8
#define AFM_UPDATE_TIMER		 9

// LinearRevolver 동작
#define LR_HOME  0
#define LR_LEFT  1
#define LR_RIGHT 2

#define RUN						1
#define RUN_OFF					0

//3. 컬러 Definition
#define WHITE										RGB(255,255,255)
#define BLACK										RGB(0,0,0)
#define RED											RGB(255,0,0)
#define GREEN										RGB(0,255,0) 
#define BLUE										RGB(0,0,255)
#define YELLOW										RGB(255,255,0)
#define ORANGE										RGB(255,155,50)
#define LIGHT_GRAY									RGB(225,225,225)	
#define GRAY										RGB(200, 200, 200)
#define MIDDLE_GRAY									RGB(180,180,180)
#define DARK_GRAY									RGB(128,128,128)
#define FOREST_GREEN								RGB(0,192,0)
#define DARK_GREEN									RGB(0,128,0)
#define LOW_BLUE									RGB(200, 255, 255)
#define SKY_BLUE									RGB(140,255,255)
#define GREENBLUE									RGB(0, 255, 255)
#define DARKBLUE									RGB(30,30,60)
#define DARK_BLUE									RGB(0,0,128)
#define ROYAL_BLUE									RGB(0,0,190)
#define PURPLE										RGB(155,0,200)
#define BROWN										RGB(80,50,0)
#define HOT_PINK									RGB(255,50,150)
#define CYAN										RGB(0,255,255)
#define LAVENDER									RGB(200,175,255)
#define PEACH										RGB(255,225,175)
#define TURQUOISE									RGB(0,190,190)
#define TAN											RGB(255,200,100)
#define MAROON										RGB(128,0,0)
#define DUSK										RGB(255,140,110)

#define SCAN_XPOS_DEFAULT			170.0
#define SCAN_YPOS_DEFAULT			0.0
#define SCAN_SCALE					5
#define SCAN_RANGE					30.0

#define STAGE_XPOS_DEFAULT			360.0
#define STAGE_YPOS_DEFAULT			180.0
#define STAGE_SCALE					25
#define STAGE_X_RANGE				14
#define STAGE_Y_RANGE				7

#define STAGE_CHECK_DATA_SIZE		50

#define STAGE						0
#define ZONPLATE_EXCHANGE			1
#define DETECTER_APERTURE_CHANGE	2
#define CAMERA_CONFIG				3


#define Z_AXIS						0
#define R_AXIS						1
#define G_AXIS						2
#define DETECTER_AXIS				3
#define APERTURE_AXIS				4

#define AFTERMOVEENDSTOPSIGANLON	-1     // um 단위

//#define 
//AF MODULE
#define ATF_NO_ACCESS_ERROR						-17001
#define ATF_WRONG_TYPE_ERROR					-17002
#define ATF_OUT_OF_BOUND_ERROR					-17003
#define ATF_INVALID_ERROR						-17004
#define ATF_UNAVAILABLE_ERROR					-17005
#define ATF_NOT_SUPPORTED_ERROR					-17006
#define ATF_SNTAX_ERROR							-17007
#define ATF_NO_RESOURCES_ERROR					-17008
#define ATF_INTERNAL_ERROR						-17009
#define ATF_OPER_FAILED_ERROR					-17010
#define ATF_TIMEOUT_ERROR						-17011
#define ATF_CHKSUM_ERROR						-17012
#define ATF_UNKNOWN_ERROR						-17013
#define ATF_OPER_FAILED_WITH_NACK				-17014