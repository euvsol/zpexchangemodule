﻿#pragma once

#ifndef VC_EXTRALEAN
#define VC_EXTRALEAN            // 거의 사용되지 않는 내용은 Windows 헤더에서 제외합니다.
#endif

#include "targetver.h"

#define _ATL_CSTRING_EXPLICIT_CONSTRUCTORS      // 일부 CString 생성자는 명시적으로 선언됩니다.

// MFC의 공통 부분과 무시 가능한 경고 메시지에 대한 숨기기를 해제합니다.
#define _AFX_ALL_WARNINGS

#include <afxwin.h>         // MFC 핵심 및 표준 구성 요소입니다.
#include <afxext.h>         // MFC 확장입니다.





#ifndef _AFX_NO_OLE_SUPPORT
#include <afxdtctl.h>           // Internet Explorer 4 공용 컨트롤에 대한 MFC 지원입니다.
#endif
#ifndef _AFX_NO_AFXCMN_SUPPORT
#include <afxcmn.h>             // Windows 공용 컨트롤에 대한 MFC 지원입니다.
#endif // _AFX_NO_AFXCMN_SUPPORT

#include <afxcontrolbars.h>     // MFC의 리본 및 컨트롤 막대 지원

#include "..\..\DllECommon\CECommon.h"
#include "..\..\DllSerialCom\CSerialCom.h"
#include "..\..\DllEthernetCom\\CEthernetCom.h"
#include "..\..\DllACSMC4U\CACSMC4UCtrl.h"
#include "..\..\DllCrevisIO\CCrevisIOCtrl.h"
#include "..\DllSmarAct\CSmarActCtrl.h"
#include "..\..\DllWDIAF\CWDIAFCtrl.h"
#include "..\..\DllLinearRevolver\CLinearRevolverCtrl.h"
#include <afxcontrolbars.h>
#include <afxcontrolbars.h>
#include <afxcontrolbars.h>
#include <afxcontrolbars.h>
#include <afxcontrolbars.h>


#ifdef _DEBUG
#pragma comment(lib, "..\\x64\\Debug\\DllECommon.lib")
#pragma comment(lib, "..\\x64\\Debug\\DllSerialCom.lib")
#pragma comment(lib, "..\\x64\\Debug\\DllEthernetCom.lib")
#pragma comment(lib, "..\\x64\\Debug\\DllACSMC4U.lib")
#pragma comment(lib, "..\\x64\\Debug\\DllCrevisIO.lib")
#pragma comment(lib, "..\\x64\\Debug\\DllSmarAct.lib")
#pragma comment(lib, "..\\x64\\Debug\\DllWDIAF.lib")
#pragma comment(lib, "..\\x64\\Debug\\DllLinearRevolver.lib")
//#pragma comment(lib, "..\\x64\\Debug\\atf_lib_dll_x64.lib")
//#pragma comment(lib, "..\\x64\\Debug\\LLC2_Library.lib")
//#pragma comment(lib, "..\\..\\DllWDIAF\Lib\\atf_lib_dll_x64.lib")
//#pragma comment(lib, "..\\..\\DllLinearRevolver\Lib\\LLC2_Library.lib")
//#pragma comment(lib, "..\\x64\\Debug\\")
#else
#pragma comment(lib, "..\\x64\\Release\\DllECommon.lib")
#pragma comment(lib, "..\\x64\\Release\\DllSerialCom.lib")
#pragma comment(lib, "..\\x64\\Release\\DllEthernetCom.lib")
#pragma comment(lib, "..\\x64\\Release\\DllACSMC4U.lib")
#pragma comment(lib, "..\\x64\\Release\\DllCrevisIO.lib")
#pragma comment(lib, "..\\x64\\Release\\DllSmarAct.lib")
#endif

#pragma comment(lib,"version.lib")











