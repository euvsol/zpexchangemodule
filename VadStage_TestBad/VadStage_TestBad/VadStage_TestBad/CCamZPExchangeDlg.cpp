﻿// CCamZPExchangeDlg.cpp: 구현 파일
//

#include "pch.h"
#include "Include.h"
#include "Extern.h"


// CCamZPExchangeDlg 대화 상자

IMPLEMENT_DYNAMIC(CCamZPExchangeDlg, CDialogEx)

CCamZPExchangeDlg::CCamZPExchangeDlg(CWnd* pParent /*=nullptr*/)
	: CDialogEx(IDD_CAM_ZPEXCHANGE_DIALOG, pParent)
{
	m_pStatusThread	  = NULL;
	m_bThreadExitFlag = FALSE;
	m_bGrab			  = FALSE;
	m_bConnected	  = FALSE;

	MilDigitizer	  = M_NULL;
	m_MilGrayModel	  = M_NULL;
	m_MilImageModel   = M_NULL;
	m_MilPMModel	  = M_NULL;
	m_MilResultModel  = M_NULL;
}

CCamZPExchangeDlg::~CCamZPExchangeDlg()
{
}

void CCamZPExchangeDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
}


BEGIN_MESSAGE_MAP(CCamZPExchangeDlg, CDialogEx)
	ON_BN_CLICKED(IDC_BUTTON_ZPEX_GRAB, &CCamZPExchangeDlg::OnBnClickedButtonZpexGrab)
	ON_BN_CLICKED(IDC_BUTTON_ZPEX_STOP, &CCamZPExchangeDlg::OnBnClickedButtonZpexStop)
	ON_BN_CLICKED(IDC_BUTTON_ZPEX_FIND_MODEL, &CCamZPExchangeDlg::OnBnClickedButtonZpexFindModel)
	ON_BN_CLICKED(IDC_BUTTON_ZPEX_REG_PM, &CCamZPExchangeDlg::OnBnClickedButtonZpexRegPm)
	ON_WM_DESTROY()
	ON_BN_CLICKED(IDC_CHECK_ZPEX_SHOW_ROI, &CCamZPExchangeDlg::OnBnClickedCheckZpexShowRoi)
END_MESSAGE_MAP()


// CCamZPExchangeDlg 메시지 처리기

BOOL CCamZPExchangeDlg::OnInitDialog()
{
	CDialogEx::OnInitDialog();

	CRect rect;
	CWnd *pWnd = GetDlgItem(IDC_STATIC_CAM_ZPEXCHANGE);
	pWnd->GetClientRect(rect);
	m_CameraWnd.Create(WS_CHILD | WS_VISIBLE /*| WS_VSCROLL | WS_HSCROLL*/, rect, pWnd);
	m_CameraWnd.ShowWindow(SW_SHOW);
	m_CameraWnd.m_bCrossLine = TRUE;

	CRect rc = m_CameraWnd.m_RectTrackerOM.m_rect;
	MbufAlloc2d(g_milSystemGigEVision, rc.Width(), rc.Height(), 8L + M_UNSIGNED, M_IMAGE + M_PROC + M_DISP, &m_MilImageModel);
	MbufClear(m_MilImageModel, 0);

	//더미 이미지 불러오기
	if (TRUE)
	{
		MbufAllocColor(g_milSystemGigEVision, m_CameraWnd.m_nDigBand, m_CameraWnd.m_nDigSizeX, m_CameraWnd.m_nDigSizeY, 8 + M_UNSIGNED, M_IMAGE + M_PROC + M_DISP + M_DIB + M_GDI, &m_CameraWnd.m_MilOMDisplay);
		MbufClear(m_CameraWnd.m_MilOMDisplay, M_COLOR_GRAY);
		CString str = DUMMY_IMAGE_PATH;
		if (IsFileExist(str))
		{
			MbufLoad((char*)(LPCTSTR)str, m_CameraWnd.m_MilOMDisplay);
			m_CameraWnd.SetTimer(ZPEXCHANGE_DISPLAY_TIMER, 100, 0);
		}
	}

	//등록된 모델 불러오기
	CString str = PM_IMAGE_PATH;
	if (IsFileExist(str))
	{
		MbufLoad((char*)(LPCTSTR)str, m_MilImageModel);
		MpatAllocModel(g_milSystemGigEVision, m_MilImageModel, 0, 0, rc.Width(), rc.Height(), M_NORMALIZED, &m_MilPMModel);
	}

	MbufAlloc2d(g_milSystemGigEVision, 1544, 1544, 8L + M_UNSIGNED, M_IMAGE + M_PROC + M_DIB, &m_MilGrayModel);
	MbufClear(m_MilGrayModel, 0);
	MpatAllocResult(g_milSystemGigEVision, 1L, &m_MilResultModel);

	return TRUE;  // return TRUE unless you set the focus to a control
				  // 예외: OCX 속성 페이지는 FALSE를 반환해야 합니다.
}

UINT CCamZPExchangeDlg::ZpCamGrabThread(LPVOID pParam)
{
	CCamZPExchangeDlg* pDlg = (CCamZPExchangeDlg*)pParam;

	while (!pDlg->m_bThreadExitFlag)
	{
		if (pDlg->m_bGrab)
			MdigGrab(pDlg->MilDigitizer, pDlg->m_CameraWnd.m_MilImage);

		Sleep(50);
	}

	return 0;
}

int CCamZPExchangeDlg::OpenDevice()
{
	int nRet = 0;

	CString str = CAMERA_DCF_FILE;

	char szCCFFile[128] = { 0, };
	sprintf(szCCFFile, str);

	MdigAlloc(g_milSystemGigEVision, M_DEV0, (MIL_TEXT_PTR)szCCFFile, M_DEFAULT, &MilDigitizer);

	if (MilDigitizer == M_NULL) return -1;

	m_CameraWnd.m_nDigSizeX = MdigInquire(MilDigitizer, M_SIZE_X, M_NULL);
	m_CameraWnd.m_nDigSizeY = MdigInquire(MilDigitizer, M_SIZE_Y, M_NULL);
	m_CameraWnd.m_nDigBand = MdigInquire(MilDigitizer, M_SIZE_BAND, M_NULL);

	MbufAllocColor(g_milSystemGigEVision, m_CameraWnd.m_nDigBand, m_CameraWnd.m_nDigSizeX, m_CameraWnd.m_nDigSizeY, 8 + M_UNSIGNED, M_IMAGE + M_DISP + M_GRAB, &m_CameraWnd.m_MilImage);
	MbufClear(m_CameraWnd.m_MilImage, 0x0);
	
	if (m_CameraWnd.m_MilOMDisplay != M_NULL)
	{
		MbufFree(m_CameraWnd.m_MilOMDisplay);
		m_CameraWnd.m_MilOMDisplay = M_NULL;
	}
	MbufAllocColor(g_milSystemGigEVision, m_CameraWnd.m_nDigBand, m_CameraWnd.m_nDigSizeX, m_CameraWnd.m_nDigSizeY, 8 + M_UNSIGNED, M_IMAGE + M_PROC + M_DIB + M_GDI, &m_CameraWnd.m_MilOMDisplay);
	MbufClear(m_CameraWnd.m_MilOMDisplay, 0x0);

	m_CameraWnd.SetTimer(ZPEXCHANGE_DISPLAY_TIMER, 100, 0);
	m_pStatusThread = AfxBeginThread(ZpCamGrabThread, (LPVOID)this, THREAD_PRIORITY_NORMAL);
	m_bConnected = TRUE;

	return nRet;
}

CString CCamZPExchangeDlg::GetRootPath()
{
	char buffer[MAX_PATH] = { 0 };
	GetModuleFileName(NULL, buffer, MAX_PATH);
	CString sTemp;
	sTemp.Format("%s", buffer);
	int pos = sTemp.ReverseFind(_T('\\'));
	if (pos == -1)
		return "";

	return sTemp.Left(pos);
}

void CCamZPExchangeDlg::OnBnClickedButtonZpexGrab()
{
	m_bGrab = TRUE;
}

void CCamZPExchangeDlg::OnBnClickedButtonZpexStop()
{
	m_bGrab = FALSE;
}

void CCamZPExchangeDlg::OnDestroy()
{
	CDialogEx::OnDestroy();

	m_bThreadExitFlag = TRUE;

	if (m_pStatusThread != NULL)
	{
		HANDLE threadHandle = m_pStatusThread->m_hThread;
		DWORD dwResult;
		dwResult = ::WaitForSingleObject(threadHandle, /*INFINITE*/1000);
		if (dwResult == WAIT_TIMEOUT)
		{
			DWORD dwExitCode = STILL_ACTIVE;
			::GetExitCodeThread(threadHandle, &dwExitCode); // 요기서 무한루프 => dwCode가 계속 STILL_ACTIVE가 됩니다.
			if (dwExitCode == STILL_ACTIVE)	//259
			{
				TerminateThread(threadHandle, 0/*dwExitCode*/);
				CloseHandle(threadHandle);
			}
		}
		m_pStatusThread = NULL;
	}

	if (m_MilGrayModel != M_NULL)
	{
		MbufFree(m_MilGrayModel);
		m_MilGrayModel = M_NULL;
	}

	if (MilDigitizer != M_NULL)
		MdigHalt(MilDigitizer);

	if (MilDigitizer != M_NULL)
	{
		MdigFree(MilDigitizer);
		MilDigitizer = M_NULL;
	}

	if (m_MilImageModel != M_NULL)
	{
		MbufFree(m_MilImageModel);
		m_MilImageModel = M_NULL;
	}

	if (m_MilPMModel != M_NULL)
	{
		MpatFree(m_MilPMModel);
		m_MilPMModel = M_NULL;
	}

	if (m_MilResultModel != M_NULL)
	{
		MpatFree(m_MilResultModel);
		m_MilResultModel = M_NULL;
	}
}


void CCamZPExchangeDlg::OnBnClickedButtonZpexFindModel()
{
	int ret = 0;

	m_CameraWnd.m_bPMRectDisplay = FALSE;
	
	if (m_MilPMModel > 0)
	{
		MimConvert(g_pCamZPExchange->m_CameraWnd.m_MilOMDisplay, m_MilGrayModel, M_RGB_TO_L);
		//MpatSetAcceptance(m_MilPMModel[nViewType], nSuccessScore);
		MpatSetAcceptance(m_MilPMModel, 50L);
		MpatSetCertainty(m_MilPMModel, 80L);
		MpatSetAccuracy(m_MilPMModel, M_LOW);	//M_LOW: Low accuracy(typically ± 0.20 pixels),M_MEDIUM: Medium accuracy(typically ± 0.10 pixels),M_HIGH: High accuracy(typically ± 0.05 pixels)
		MpatSetSpeed(m_MilPMModel, M_LOW);

		MpatPreprocModel(m_MilGrayModel, m_MilPMModel, M_DEFAULT);
		MpatSetNumber(m_MilPMModel, 1L);

		MpatFindModel(m_MilGrayModel, m_MilPMModel, m_MilResultModel);
		double x = 0., y = 0., angle = 0., Score = 0.;
		int getnum = MpatGetNumber(m_MilResultModel, M_NULL);
		if (getnum > 0)
		{
			MpatGetResult(m_MilResultModel, M_POSITION_X, &x);
			MpatGetResult(m_MilResultModel, M_POSITION_Y, &y);
			//MpatGetResult(m_MilResultModel, M_ANGLE, &angle);
			MpatGetResult(m_MilResultModel, M_SCORE, &Score);
			m_CameraWnd.m_bPMSuccess = TRUE;
		}
		else
		{
			m_CameraWnd.m_bPMSuccess = FALSE;
		}

		m_CameraWnd.m_dResultPosX = x;
		m_CameraWnd.m_dResultPosY = y;
		m_CameraWnd.m_dResultAngle = angle;
		m_CameraWnd.m_dResultScore = Score;
		UpdateData(false);
	}
}


void CCamZPExchangeDlg::OnBnClickedButtonZpexRegPm()
{
	if (m_MilPMModel > 0)
		MpatFree(m_MilPMModel);
	
	CRect rc = m_CameraWnd.m_RectTrackerOM.m_rect;

	MimConvert(g_pCamZPExchange->m_CameraWnd.m_MilOMDisplay, m_MilGrayModel, M_RGB_TO_L);
	MpatAllocModel(g_milSystemGigEVision, m_MilGrayModel, rc.left, rc.top, rc.Width(), rc.Height(), M_NORMALIZED, &m_MilPMModel);
	MpatCopy(m_MilPMModel, m_MilImageModel, M_DEFAULT);

	CString str = PM_IMAGE_PATH;
	MbufExport((char*)(LPCSTR)str, M_BMP, m_MilImageModel);

	AfxMessageBox(_T("MODEL 등록 완료!"));
}

void CCamZPExchangeDlg::OnBnClickedCheckZpexShowRoi()
{
	BOOL bCheck = ((CButton*)GetDlgItem(IDC_CHECK_ZPEX_SHOW_ROI))->GetCheck();
	m_CameraWnd.m_bPMSuccess = FALSE;
	m_CameraWnd.m_bPMRectDisplay = bCheck;
}
