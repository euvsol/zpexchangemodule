#include "stdafx.h"
#include "CACSMC4UCtrl.h"


CACSMC4UCtrl::CACSMC4UCtrl()
{
	m_hComm = ACSC_INVALID;
	m_strIPaddress = "127.0.0.1"; //default address
	m_bConnect = FALSE;
	m_nErrorCode = 0;

	m_stXaxis.bFault = FALSE;
	m_stXaxis.bMoving = FALSE;
	m_stXaxis.bZeroReturn = FALSE;
	m_stXaxis.strStatus = "";
	m_stYaxis.bFault = FALSE;
	m_stYaxis.bMoving = FALSE;
	m_stYaxis.bZeroReturn = FALSE;
	m_stYaxis.strStatus = "";
	m_stStage.bFault = FALSE;
	m_stStage.bMoving = FALSE;
	m_stStage.bZeroReturn = FALSE;
	m_stStage.strStatus = "";
	m_bLaserFeedbackFlag = FALSE;
	m_bLaserSwichingModeFlag = FALSE;
}

CACSMC4UCtrl::~CACSMC4UCtrl()
{
	if (m_bConnect == TRUE)
	{
		DisconnectComm();
	}
}

BOOL CACSMC4UCtrl::ConnectACSController(int CommunicationType, char *Ip, int nPort)
{
	m_hComm = ACSC_INVALID;
	m_bConnect = FALSE;
	switch (CommunicationType)
	{
	case ETHERNET:
		//m_hComm = acsc_OpenCommEthernet((LPSTR)LPCTSTR(m_strIPaddress), ACSC_SOCKET_STREAM_PORT/*ACSC_SOCKET_DGRAM_PORT*/);
		m_hComm = acsc_OpenCommEthernet(Ip, ACSC_SOCKET_STREAM_PORT/*ACSC_SOCKET_DGRAM_PORT*/);
		if (m_hComm == ACSC_INVALID)
		{
			m_nErrorCode = acsc_GetLastError();
		}
		m_strIPaddress = Ip;
		break;
	case SERIAL:
		break;
	case SIMULATOR:
		//m_hComm = acsc_OpenCommDirect();
		m_hComm = acsc_OpenCommSimulator();
		if (m_hComm == ACSC_INVALID)
		{
			m_nErrorCode = acsc_GetLastError();
		}
		break;
	}
	if (m_hComm != ACSC_INVALID)
	{
		m_bConnect = TRUE;
		acsc_OpenHistoryBuffer(m_hComm, 100000);
		m_stStage.bZeroReturn = TRUE;			//추후 프로그램을 껏다 켜도 HOMMING 여부를 판단하는 방법을 찾자
	}
	return m_bConnect;
}

int CACSMC4UCtrl::DisconnectComm()
{
	acsc_CloseHistoryBuffer(m_hComm);
	acsc_ReleaseComm(m_hComm);
	acsc_CloseComm(m_hComm);
	m_hComm = ACSC_INVALID;
	m_bConnect = FALSE;
	return 0;
}

int CACSMC4UCtrl::SetLaserSwitchingFunction(BOOL bOnOff)
{
	if (!m_bConnect)
		return !XY_NAVISTAGE_OK;

	if (bOnOff)
	{
		if (acsc_WaitProgramEnd(m_hComm, 20, 100))
		{
			if (!acsc_RunBuffer(m_hComm, 20, NULL, NULL))
			{
				ErrorsHandler();
				return !XY_NAVISTAGE_OK;
			}
		}
		if (acsc_WaitProgramEnd(m_hComm, 21, 100))
		{
			if (!acsc_RunBuffer(m_hComm, 21, NULL, NULL))
			{
				ErrorsHandler();
				return !XY_NAVISTAGE_OK;
			}
		}
		if (acsc_WaitProgramEnd(m_hComm, 23, 100))
		{
			if (!acsc_RunBuffer(m_hComm, 23, NULL, NULL))
			{
				ErrorsHandler();
				return !XY_NAVISTAGE_OK;
			}
		}
	}
	else
	{
		if (!acsc_StopBuffer(m_hComm, 23, NULL))
		{
			ErrorsHandler();
			return !XY_NAVISTAGE_OK;
		}
		if (!acsc_StopBuffer(m_hComm, 24, NULL))
		{
			ErrorsHandler();
			return !XY_NAVISTAGE_OK;
		}
		if (!acsc_StopBuffer(m_hComm, 20, NULL))
		{
			ErrorsHandler();
			return !XY_NAVISTAGE_OK;
		}
		if (!acsc_StopBuffer(m_hComm, 21, NULL))
		{
			ErrorsHandler();
			return !XY_NAVISTAGE_OK;
		}
		if (acsc_WaitProgramEnd(m_hComm, 23, 100))
		{
			if (!acsc_RunBuffer(m_hComm, 23, NULL, NULL))
			{
				ErrorsHandler();
				return !XY_NAVISTAGE_OK;
			}
		}
	}

	return XY_NAVISTAGE_OK;
}

BOOL CACSMC4UCtrl::IsLaserSwichingMode()
{
	if (acsc_WaitProgramEnd(m_hComm, 10, 100))	//10,20,21,29,30번 버퍼 중 하나라도 실행되어있지 않으면 Laser-Scale Switching 불가.
	{
		m_bLaserSwichingModeFlag = FALSE;
		return TRUE;
	}
	if (acsc_WaitProgramEnd(m_hComm, 20, 100))	
	{
		m_bLaserSwichingModeFlag = FALSE;
		return TRUE;
	}
	if (acsc_WaitProgramEnd(m_hComm, 21, 100))	
	{
		m_bLaserSwichingModeFlag = FALSE;
		return TRUE;
	}
	if (acsc_WaitProgramEnd(m_hComm, 29, 100))	
	{
		m_bLaserSwichingModeFlag = FALSE;
		return TRUE;
	}
	if (acsc_WaitProgramEnd(m_hComm, 30, 100))	
	{
		m_bLaserSwichingModeFlag = FALSE;
		return TRUE;
	}
	m_bLaserSwichingModeFlag = TRUE;
	return TRUE;
}

int CACSMC4UCtrl::SetFeedbackType(int nFeedbackType)
{
	if (!m_bConnect)
		return !XY_NAVISTAGE_OK;

	if (nFeedbackType == FEEDBACK_LASER)
	{
		if (!acsc_StopBuffer(m_hComm, 23, NULL))
		{
			ErrorsHandler();
			return !XY_NAVISTAGE_OK;
		}
		if (!acsc_RunBuffer(m_hComm, 24, NULL, NULL))
		{
			ErrorsHandler();
			return !XY_NAVISTAGE_OK;
		}
	}
	else if (nFeedbackType == FEEDBACK_ENCODER)
	{
		if (!acsc_StopBuffer(m_hComm, 24, NULL))
		{
			ErrorsHandler();
			return !XY_NAVISTAGE_OK;
		}
		if (!acsc_RunBuffer(m_hComm, 23, NULL, NULL))
		{
			ErrorsHandler();
			return !XY_NAVISTAGE_OK;
		}
	}

	return XY_NAVISTAGE_OK;

}

BOOL CACSMC4UCtrl::IsLaserFeedback()
{
	if (!acsc_WaitProgramEnd(m_hComm, 24, 100))	//24번 버퍼가 실행중이라고 Laser Feedback 상태라는 보장은 없음. 추후 더 확실한 보완책 필요.
	{
		m_bLaserFeedbackFlag = TRUE;
		return TRUE;
	}
	m_bLaserFeedbackFlag = FALSE;
	return FALSE;
}


int CACSMC4UCtrl::Home(int axis)
{
	if (!m_bConnect)
		return !XY_NAVISTAGE_OK;

	switch (axis) {
	case STAGE_ALL_AXIS:
		m_stStage.bZeroReturn = FALSE;
		m_stXaxis.bZeroReturn = FALSE;
		m_stYaxis.bZeroReturn = FALSE;
		if (!acsc_StopBuffer(m_hComm, STAGE_X_AXIS, NULL))
		{
			ErrorsHandler();
			return !XY_NAVISTAGE_OK;
		}
		if (!acsc_StopBuffer(m_hComm, STAGE_Y_AXIS, NULL))
		{
			ErrorsHandler();
			return !XY_NAVISTAGE_OK;
		}
		break;
	case STAGE_X_AXIS:
		m_stStage.bZeroReturn = FALSE;
		m_stXaxis.bZeroReturn = FALSE;
		if (!acsc_StopBuffer(m_hComm, STAGE_X_AXIS, NULL))
		{
			ErrorsHandler();
			return !XY_NAVISTAGE_OK;
		}
		break;
	case STAGE_Y_AXIS:
		m_stStage.bZeroReturn = FALSE;
		m_stYaxis.bZeroReturn = FALSE;
		if (!acsc_StopBuffer(m_hComm, STAGE_Y_AXIS, NULL))
		{
			ErrorsHandler();
			return !XY_NAVISTAGE_OK;
		}
		break;
	case STAGE_ACS_Y_AXIS:
		m_stStage.bZeroReturn = FALSE;
		m_stYaxis.bZeroReturn = FALSE;
		if (!acsc_StopBuffer(m_hComm, STAGE_ACS_Y_AXIS, NULL))
		{
			ErrorsHandler();
			return !XY_NAVISTAGE_OK;
		}
		break;
	default:
		break;
	}

	switch (axis) {
	case STAGE_ALL_AXIS:
		if (!acsc_RunBuffer(m_hComm, STAGE_X_AXIS, NULL, NULL))
		{
			ErrorsHandler();
			return !XY_NAVISTAGE_OK;
		}
		if (!acsc_RunBuffer(m_hComm, STAGE_Y_AXIS, NULL, NULL))
		{
			ErrorsHandler();
			return !XY_NAVISTAGE_OK;
		}
		break;
	case STAGE_X_AXIS:
		if (!acsc_RunBuffer(m_hComm, STAGE_X_AXIS, NULL, NULL))
		{
			ErrorsHandler();
			return !XY_NAVISTAGE_OK;
		}
		break;
	case STAGE_Y_AXIS:
		if (!acsc_RunBuffer(m_hComm, STAGE_Y_AXIS, NULL, NULL))
		{
			ErrorsHandler();
			return !XY_NAVISTAGE_OK;
		}
		break;
	case STAGE_ACS_Y_AXIS:
		if (!acsc_RunBuffer(m_hComm, STAGE_ACS_Y_AXIS, NULL, NULL))
		{
			ErrorsHandler();
			return !XY_NAVISTAGE_OK;
		}
		break;
	default:
		break;
	}

	switch (axis) {
	case STAGE_ALL_AXIS:
		if (WaitTimeStageHomeAll(HOMMING_TIMEOUT_SEC, STAGE_X_AXIS))
		{
			return !XY_NAVISTAGE_OK;
		}
		m_stXaxis.bZeroReturn = TRUE;
		if (WaitTimeStageHomeAll(HOMMING_TIMEOUT_SEC, STAGE_Y_AXIS))
		{
			return !XY_NAVISTAGE_OK;
		}
		m_stYaxis.bZeroReturn = TRUE;
		break;
	case STAGE_X_AXIS:
		if (WaitTimeStageHomeAll(HOMMING_TIMEOUT_SEC, STAGE_X_AXIS))
		{
			return !XY_NAVISTAGE_OK;
		}
		m_stXaxis.bZeroReturn = TRUE;
		break;
	case STAGE_Y_AXIS:
		if (WaitTimeStageHomeAll(HOMMING_TIMEOUT_SEC, STAGE_Y_AXIS))
		{
			return !XY_NAVISTAGE_OK;
		}
		m_stYaxis.bZeroReturn = TRUE;
		break;
	default:
		break;
	}

	m_stStage.bZeroReturn = TRUE;

	return XY_NAVISTAGE_OK;
}

void CACSMC4UCtrl::ErrorsHandler()
{
	char chAcsErrorString[256];
	int nAcsErrorCode, nAcsErrorStringNo;
	CString str;

	nAcsErrorCode = acsc_GetLastError();
	if (acsc_GetErrorString(m_hComm, nAcsErrorCode, chAcsErrorString, 255, &nAcsErrorStringNo))
	{
		chAcsErrorString[nAcsErrorStringNo] = '\0';
		str.Format("Error:%d,%s", nAcsErrorCode, chAcsErrorString);
		AfxMessageBox(str);

		DisconnectComm();
		m_stStage.strStatus = str;
		m_stStage.bFault = TRUE;
	}
	//에러내용 저장하자

	if (nAcsErrorCode == 197)	//Communication Error 발생시 자동 연결 ? 필요한가 ?
	{
		m_bConnect = FALSE;
		ConnectACSController(ETHERNET, (LPSTR)(LPCTSTR)m_strIPaddress, ACSC_SOCKET_STREAM_PORT);
		if (m_bConnect == FALSE)
		{
			m_stStage.strStatus.Format("Stage와 통신이 끊겼습니다!");
			m_stStage.bFault = TRUE;
		}
	}
}

int CACSMC4UCtrl::GetAmpFault(int axis)
{
	if (!m_bConnect)
		return !XY_NAVISTAGE_OK;

	int Fault;
	if (!acsc_GetFault(m_hComm, axis, &Fault, NULL))
	{
		ErrorsHandler();
		return !XY_NAVISTAGE_OK;
	}
	if (Fault)
		return !XY_NAVISTAGE_OK;
	return XY_NAVISTAGE_OK;
}

int CACSMC4UCtrl::ClearAmpFault(int axis)
{
	if (!m_bConnect)
		return !XY_NAVISTAGE_OK;

	int Axes[] = { STAGE_X_AXIS,STAGE_Y_AXIS,-1 };

	switch (axis) {
	case STAGE_ALL_AXIS:
		if (!acsc_FaultClearM(m_hComm, Axes, NULL))
		{
			ErrorsHandler();
			return !XY_NAVISTAGE_OK;
		}
		break;
	default:
		if (!acsc_FaultClear(m_hComm, axis, NULL))
		{
			ErrorsHandler();
			return !XY_NAVISTAGE_OK;
		}
		break;
	}
	return XY_NAVISTAGE_OK;
}

int CACSMC4UCtrl::Stop(int axis)
{
	if (!m_bConnect)
		return !XY_NAVISTAGE_OK;

	int Axes[] = { STAGE_X_AXIS,STAGE_Y_AXIS };

	switch (axis) {
	case STAGE_ALL_AXIS:
		if (!acsc_HaltM(m_hComm, Axes, NULL))
		{
			ErrorsHandler();
			return !XY_NAVISTAGE_OK;
		}
		break;
	default:
		if (!acsc_Halt(m_hComm, axis, NULL))
		{
			ErrorsHandler();
			return !XY_NAVISTAGE_OK;
		}
		break;
	}
	return XY_NAVISTAGE_OK;
}

int CACSMC4UCtrl::EStop(int axis)
{
	if (!m_bConnect)
		return !XY_NAVISTAGE_OK;

	switch (axis) {
	case STAGE_ALL_AXIS:
		if (!acsc_KillAll(m_hComm, NULL))
		{
			ErrorsHandler();
			return !XY_NAVISTAGE_OK;
		}
		break;
	default:
		if (!acsc_Kill(m_hComm, axis, NULL))
		{
			ErrorsHandler();
			return !XY_NAVISTAGE_OK;
		}
		break;
	}
	return XY_NAVISTAGE_OK;
}

int CACSMC4UCtrl::SetPosmm(int axis, double pos_mm)
{
	if (!acsc_SetFPosition(m_hComm, axis, pos_mm, NULL))
	{
		ErrorsHandler();
		return !XY_NAVISTAGE_OK;
	}
	return XY_NAVISTAGE_OK;
}

double CACSMC4UCtrl::GetPosmm(int axis)
{
	if (!m_bConnect)
		return 0;

	double get_pos_mm = 0.0f;

	if (!acsc_GetFPosition(m_hComm, axis, &get_pos_mm, NULL))
	{
		ErrorsHandler();
		return !XY_NAVISTAGE_OK;
	}

	return get_pos_mm;
}

double CACSMC4UCtrl::GetGlobalRealVariable(char* varName)
{
	if (!m_bConnect)
		return 0;

	double get_value = 0.0f;

	if (!acsc_ReadReal(m_hComm, ACSC_NONE, varName, ACSC_NONE, ACSC_NONE, ACSC_NONE, ACSC_NONE, &get_value, NULL))
	{
		ErrorsHandler();
		return !XY_NAVISTAGE_OK;
	}
	return get_value;
}

int CACSMC4UCtrl::SetAmpEnable(int axis, BOOL value)
{
	if (!m_bConnect)
		return !XY_NAVISTAGE_OK;

	int Axes[] = { STAGE_X_AXIS,STAGE_Y_AXIS };

	switch (axis) {
	case STAGE_ALL_AXIS:
		if (value == TRUE)
		{
			if (!acsc_EnableM(m_hComm, Axes, NULL))
			{
				ErrorsHandler();
				return !XY_NAVISTAGE_OK;
			}
		}
		else
		{
			if (!acsc_DisableAll(m_hComm, NULL))
			{
				ErrorsHandler();
				return !XY_NAVISTAGE_OK;
			}
		}
		break;
	default:
		if (value == TRUE)
		{
			if (!acsc_Enable(m_hComm, axis, NULL))
			{
				ErrorsHandler();
				return !XY_NAVISTAGE_OK;
			}
		}
		else
		{
			if (!acsc_Disable(m_hComm, axis, NULL))
			{
				ErrorsHandler();
				return !XY_NAVISTAGE_OK;
			}
		}
		break;
	}
	return XY_NAVISTAGE_OK;
}

BOOL CACSMC4UCtrl::GetAmpEnable(int axis)
{
	if (!m_bConnect)
		return !XY_NAVISTAGE_OK;

	int state = 0;

	if (!acsc_GetMotorState(m_hComm, axis, &state, NULL))
	{
		ErrorsHandler();
		return !XY_NAVISTAGE_OK;
	}

	if (state & ACSC_MST_ENABLE)
		return TRUE;
	else
		return FALSE;
}

int CACSMC4UCtrl::GetMotorStatus(int axis, int* state)
{
	if (!m_bConnect)
		return !XY_NAVISTAGE_OK;

	if (!acsc_GetMotorState(m_hComm, axis, state, NULL))
	{
		ErrorsHandler();
		return !XY_NAVISTAGE_OK;
	}
	return XY_NAVISTAGE_OK;
}

int CACSMC4UCtrl::GetFaultStatus(int axis, int* fault)
{
	if (!m_bConnect)
		return !XY_NAVISTAGE_OK;

	if (!acsc_GetFault(m_hComm, axis, fault, NULL))
	{
		ErrorsHandler();
		return !XY_NAVISTAGE_OK;
	}
	return XY_NAVISTAGE_OK;
}

int CACSMC4UCtrl::GetDigitalInput(int axis, int* value)
{
	if (!m_bConnect)
		return !XY_NAVISTAGE_OK;

	switch (axis) {
	case STAGE_X_AXIS:
		if (!acsc_GetInput(m_hComm, 0, 0, value, NULL))
		{
			ErrorsHandler();
			return !XY_NAVISTAGE_OK;
		}
		//if (!acsc_GetInputPort(m_hComm, 0, value, NULL))
		//{
		//	ErrorsHandler();
		//	return !XY_NAVISTAGE_OK;
		//}
		break;
	case STAGE_Y_AXIS:
		if (!acsc_GetInput(m_hComm, 0, 1, value, NULL))
		{
			ErrorsHandler();
			return !XY_NAVISTAGE_OK;
		}
		break;
	}
	return XY_NAVISTAGE_OK;
}

int CACSMC4UCtrl::GetAnalogInput(int axis)
{
	if (!m_bConnect)
		return !XY_NAVISTAGE_OK;

	int Value;
	switch (axis) {
	case STAGE_X_AXIS:
		if (!acsc_GetAnalogInput(m_hComm, X_LOADINGPOS_SENSOR, &Value, NULL))
		{
			ErrorsHandler();
			return !XY_NAVISTAGE_OK;
		}
		break;
	case STAGE_Y_AXIS:
		if (!acsc_GetAnalogInput(m_hComm, Y_LOADINGPOS_SENSOR, &Value, NULL))
		{
			ErrorsHandler();
			return !XY_NAVISTAGE_OK;
		}
		break;
	}
	return Value;
}

int CACSMC4UCtrl::InPosition(int axis, int MaxTime, double pos_mm)
{
	clock_t t = clock();
	while (MaxTime - ((clock() - t) / CLOCKS_PER_SEC) > 0)
	{
		int State;
		if (!acsc_GetMotorState(m_hComm, axis, &State, NULL))
			ErrorsHandler();
		if (State & ACSC_MST_INPOS)
		{
			double tarpos = GetPosmm(axis);
			if (fabs(tarpos - pos_mm) > 0.5)	//
			{
				return !XY_NAVISTAGE_OK;
			}

			return XY_NAVISTAGE_OK;
		}
		ProcessMessages();
	}
	return !XY_NAVISTAGE_OK;
}

int CACSMC4UCtrl::WaitInPosition(int axis, double pos_mm)
{
	if (InPosition(axis, MOVING_TIMEOUT_SEC, pos_mm) != XY_NAVISTAGE_OK)
		return !XY_NAVISTAGE_OK;
	return XY_NAVISTAGE_OK;
}

int CACSMC4UCtrl::MoveXYUntilInposition(double Xposition, double Yposition)
{
	if (m_bConnect == FALSE || m_stStage.bZeroReturn == FALSE)
		return !XY_NAVISTAGE_OK;

	if (MoveAbsolute(STAGE_ACS_X_AXIS, Xposition) != XY_NAVISTAGE_OK) 
		return !XY_NAVISTAGE_OK;
	if (MoveAbsolute(STAGE_ACS_Y_AXIS, Yposition) != XY_NAVISTAGE_OK)
		return !XY_NAVISTAGE_OK;
	if (WaitInPosition(STAGE_ACS_X_AXIS, Xposition) != XY_NAVISTAGE_OK)
		return !XY_NAVISTAGE_OK;
	if (WaitInPosition(STAGE_ACS_Y_AXIS, Yposition) != XY_NAVISTAGE_OK)
		return !XY_NAVISTAGE_OK;

	return XY_NAVISTAGE_OK;
}

int CACSMC4UCtrl::MoveXYOnTheFly(double Xposition, double Yposition)
{
	if (m_bConnect == FALSE || m_stStage.bZeroReturn == FALSE)
		return !XY_NAVISTAGE_OK;

	if (MoveAbsolute(STAGE_X_AXIS, Xposition) != XY_NAVISTAGE_OK) return !XY_NAVISTAGE_OK;
	if (MoveAbsolute(STAGE_Y_AXIS, Yposition) != XY_NAVISTAGE_OK) return !XY_NAVISTAGE_OK;
	return XY_NAVISTAGE_OK;
}

int CACSMC4UCtrl::MoveRelative(int axis, double posmm)
{
	if (m_bConnect == FALSE || m_stStage.bZeroReturn == FALSE)
		return !XY_NAVISTAGE_OK;

	if (GetAmpFault(axis) != XY_NAVISTAGE_OK)
	{
		int ret = ClearAmpFault(axis);
		if (ret != XY_NAVISTAGE_OK)
			return !XY_NAVISTAGE_OK;
	}
	if (GetAmpEnable(axis) == FALSE)
	{
		CString str;
		int ret;
		ret = SetAmpEnable(axis, TRUE);
		if (ret != XY_NAVISTAGE_OK)
		{
			return !XY_NAVISTAGE_OK;
		}
	}

	double			get_pos_mm = 0.0f, rel_pos_mm = 0.0f;

	get_pos_mm = GetPosmm(axis);

	switch (axis) {
	case STAGE_ACS_X_AXIS:
		rel_pos_mm = posmm;
		if (((get_pos_mm + rel_pos_mm) > SOFT_LIMIT_PLUS_X))
			rel_pos_mm = SOFT_LIMIT_PLUS_X - get_pos_mm;
		else if ((get_pos_mm + rel_pos_mm) < SOFT_LIMIT_MINUS_X)
			rel_pos_mm = SOFT_LIMIT_MINUS_X - get_pos_mm;
		break;
	case STAGE_ACS_Y_AXIS:
		rel_pos_mm = posmm;
		if (((get_pos_mm + rel_pos_mm) > SOFT_LIMIT_PLUS_Y))
			rel_pos_mm = SOFT_LIMIT_PLUS_Y - get_pos_mm;
		else if ((get_pos_mm + rel_pos_mm) < SOFT_LIMIT_MINUS_Y)
			rel_pos_mm = SOFT_LIMIT_MINUS_Y - get_pos_mm;
		break;
	}

	if (!acsc_FaultClear(m_hComm, axis, NULL))
	{
		ErrorsHandler();
		return !XY_NAVISTAGE_OK;
	}

	if (GetAmpEnable(axis) == TRUE)
	{
		if (!acsc_ToPoint(m_hComm, ACSC_AMF_RELATIVE, axis, rel_pos_mm, NULL))
		{
			ErrorsHandler();
			return !XY_NAVISTAGE_OK;
		}
	}
	return XY_NAVISTAGE_OK;
}

int CACSMC4UCtrl::MoveAbsolute(int axis, double posmm)
{
	if (m_bConnect == FALSE || m_stStage.bZeroReturn == FALSE)
		return !XY_NAVISTAGE_OK;

	if (GetAmpEnable(axis) == FALSE)
	{
		CString str;
		int ret;
		ret = SetAmpEnable(axis, TRUE);
		if (ret != XY_NAVISTAGE_OK)
		{
			return !XY_NAVISTAGE_OK;
		}
	}
	if (GetAmpFault(axis) != XY_NAVISTAGE_OK)
	{
		int ret = ClearAmpFault(axis);
		if (ret != XY_NAVISTAGE_OK)
			return !XY_NAVISTAGE_OK;
	}

	double	target_position_mm;

	ClearAmpFault(axis);

	switch (axis) {
	case STAGE_ACS_X_AXIS:
		target_position_mm = posmm;
		if (abs(target_position_mm) < SOFT_LIMIT_MINUS_X)
		{
			target_position_mm = SOFT_LIMIT_MINUS_X;
			break;
		}
		else if (abs(target_position_mm) > abs(SOFT_LIMIT_PLUS_X))
		{
			target_position_mm = SOFT_LIMIT_PLUS_X;
		}
		break;
	case STAGE_ACS_Y_AXIS:
		target_position_mm = posmm;
		if (abs(target_position_mm) < abs(SOFT_LIMIT_MINUS_Y))
		{
			target_position_mm = SOFT_LIMIT_MINUS_Y;
			break;
		}
		else if (abs(target_position_mm) > abs(SOFT_LIMIT_PLUS_Y))
		{
			target_position_mm = SOFT_LIMIT_PLUS_Y;
		}
		break;
	}

	if (!acsc_FaultClear(m_hComm, axis, NULL))
	{
		ErrorsHandler();
		return !XY_NAVISTAGE_OK;
	}

	if (GetAmpEnable(axis) == TRUE)
	{
		if (!acsc_ToPoint(m_hComm, 0, axis, target_position_mm, NULL))
		{
			ErrorsHandler();
			return !XY_NAVISTAGE_OK;
		}
	}

	return XY_NAVISTAGE_OK;
}

int CACSMC4UCtrl::WaitTimeStageHomeAll(double sec, int axis)
{
	int timeflag = FALSE;
	clock_t t = clock();
	while (sec - (((clock() - t) / CLOCKS_PER_SEC)) > 0)
	{
		if (acsc_WaitProgramEnd(m_hComm, axis, 100))
		{
			timeflag = FALSE;
			break;
		}
		else
		{
			timeflag = TRUE;
		}

	}

	return timeflag;
}

int CACSMC4UCtrl::RunBuffer(int nBufferNo)
{
	if (!m_bConnect)
		return !XY_NAVISTAGE_OK;

	if (!acsc_StopBuffer(m_hComm, nBufferNo, NULL))
	{
		ErrorsHandler();
		return !XY_NAVISTAGE_OK;
	}
	if (!acsc_RunBuffer(m_hComm, nBufferNo, NULL, NULL))
	{
		ErrorsHandler();
		return !XY_NAVISTAGE_OK;
	}

	return XY_NAVISTAGE_OK;
}

int CACSMC4UCtrl::StopBuffer(int nBufferNo)
{
	if (!m_bConnect)
		return !XY_NAVISTAGE_OK;

	if (!acsc_StopBuffer(m_hComm, nBufferNo, NULL))
	{
		ErrorsHandler();
		return !XY_NAVISTAGE_OK;
	}

	return XY_NAVISTAGE_OK;
}