/**
 * ACS MC4U Stage Control Class
 *
 * Copyright 2019 by E-SOL, Inc.,
 *
 **/
#pragma once
#include "ACSC.h"

 //통신 방식 정의
#define ETHERNET				0
#define SERIAL					1
#define SIMULATOR				2

//Stage 축 정의
#define STAGE_AXIS_NUMBER		2
#define STAGE_ALL_AXIS			9
#define STAGE_ACS_X_AXIS		0
#define STAGE_ACS_Y_AXIS		2
#define STAGE_X_AXIS			0
#define STAGE_Y_AXIS			1

//Digital Input No
#define X_LOADINGPOS_SENSOR		0	//IN(0).0
#define Y_LOADINGPOS_SENSOR		1	//IN(0).1

//Stage 상태 정의
#define SERVO_ON				1
#define SERVO_OFF				0

#define XY_NAVISTAGE_OK				1
#define STAGE_ERROR				-1
#define STAGE_X_ERROR			-2
#define STAGE_Y_ERROR			-3
#define	STAGE_LIMIT_ERROR		-4

//Software Limit은 ACS Controller에서 설정하던지, 추후 설비 Configuaration으로 빼자.
#define SOFT_LIMIT_PLUS_X		360	//mm, Full stroke: 350mm
#define SOFT_LIMIT_MINUS_X		-0.9999	//mm
#define SOFT_LIMIT_PLUS_Y		360	//mm, Full stroke: 180mm
#define SOFT_LIMIT_MINUS_Y		-0.9999	//mm

#define HOMMING_TIMEOUT_SEC		180 //180초
#define MOVING_TIMEOUT_SEC		30  // 30초

//Feedback Type 정의
#define FEEDBACK_ENCODER		0
#define FEEDBACK_LASER			1

class AFX_EXT_CLASS CACSMC4UCtrl : public CECommon
{
public:
	CACSMC4UCtrl();
	~CACSMC4UCtrl();

	HANDLE m_hComm;				// communication handle
	CString m_strIPaddress;
	BOOL m_bConnect;		/** Stage Controller와의 통신 연결 여부 */
	int m_nErrorCode;		/** Stage Controller에서 보내온 Error Code */
	void ErrorsHandler();	/** Stage Controller에서 보내온 Error 처리 */

	/**
	 * 각 축의 현재 상태에 대한 구조체.
	 * @param	bZeroReturn	: Zero Return 여부.
	 * @param	bEnable		: Servo Enable 여부.
	 * @param	bMoving		: Moving 여부.
	 * @param	bFault		: Error 여부.
	 * @param	strStatus	: 현 상태를 Txt로 표현.
	 */
	struct AXIS_STATUS {
		BOOL bZeroReturn;
		BOOL bEnable;
		BOOL bMoving;
		BOOL bFault;
		CString strStatus;
	};
	AXIS_STATUS m_stXaxis, m_stYaxis, m_stStage;

	/**
	 * Stage Controller와 통신 연결하기.
	 * @param	CommunicationType	: 선택변수
									ETHERNET	:	Ethernet으로 연결할 때.
									SERIAL		:	Serial Port로 연결할 때.
									SIMULATOR	:	HW가 없을경우 Simulation해 볼경우 연결.
	 */
	BOOL ConnectACSController(int CommunicationType, char *Ip, int nPort);

	int DisconnectComm();	/** 통신 port를 닫는다.	 */

	/**
	 * Stage의 각 축의 정지.
	 * @param	axis	: 선택변수
						 STAGE_X_AXIS	:	X axis
						 STAGE_Y_AXIS	:	Y axis
						 STAGE_ALL_AXIS	:	All axis
	 */
	int Stop(int axis);

	/**
	 * Stage의 각 축의 비상 정지.
	 * @param	axis	: 선택변수
						 STAGE_X_AXIS	:	X axis
						 STAGE_Y_AXIS	:	Y axis
						 STAGE_ALL_AXIS	:	All axis
	 */
	int EStop(int axis);

	/**
	 * Stage의 각 축의 Servo On/Off 실행.
	 * @param	axis	: 선택변수
						 STAGE_X_AXIS	:	X axis
						 STAGE_Y_AXIS	:	Y axis
	 * @param	value	: 선택변수
						 true	:	Servo On
						 false	:	Servo Off
	 */
	int SetAmpEnable(int axis, BOOL value);

	/**
	 * Stage의 각 축의 Servo On 상태 여부확인.
	 * @param	axis	: 선택변수
						 STAGE_X_AXIS	:	X axis
						 STAGE_Y_AXIS	:	Y axis
	 */
	BOOL GetAmpEnable(int axis);

	/**
	 * Stage의 각 축의 모터 동작 상태 확인.
	 * @param	axis	: 선택변수
						 STAGE_X_AXIS	:	X axis
						 STAGE_Y_AXIS	:	Y axis
	 */
	BOOL GetMotorStatus(int axis, int* state);

	/**
	 * Stage의 각 축의 Motor Fault 상태 확인.
	 * @param	axis	: 선택변수
						 STAGE_X_AXIS	:	X axis
						 STAGE_Y_AXIS	:	Y axis
	 */
	BOOL GetFaultStatus(int axis, int* fault);

	/**
	 * Stage의 Digital Input 상태 확인.
	 * @param	axis	: 선택변수
						 STAGE_X_AXIS	:	X axis
						 STAGE_Y_AXIS	:	Y axis
	 */
	int GetDigitalInput(int axis, int* value);

	/**
	 * Stage의 Analog Input 상태 확인.
	 * @param	axis	: 선택변수
						 STAGE_X_AXIS	:	X axis
						 STAGE_Y_AXIS	:	Y axis
	 */
	int GetAnalogInput(int axis);

	/**
	 * Stage의 각 축의 Amp Fault 여부를 확인.
	 * @param	axis	: 선택변수
						 STAGE_X_AXIS	:	X axis
						 STAGE_Y_AXIS	:	Y axis
	 */
	int GetAmpFault(int axis);

	/**
	 * Stage의 각 축의 Amp Fault를 Clear 시킴.
	 * @param	axis	: 선택변수
						 STAGE_X_AXIS	:	X axis
						 STAGE_Y_AXIS	:	Y axis
	 */
	int ClearAmpFault(int axis);






	//Stage Handle

	/**
	 * Stage의 각 축의 Homming 실시.
	 * @param	axis	: 선택변수
						 STAGE_X_AXIS	:	X axis
						 STAGE_Y_AXIS	:	Y axis
						 STAGE_ALL_AXIS	:	All axis
	 */
	int Home(int axis);

	int WaitTimeStageHomeAll(double sec, int axis);

	/**
	 * Stage X,Y축의 현재 위치 좌표 읽기
	 * @param	axis	: 선택변수
						 STAGE_X_AXIS	:	X axis
						 STAGE_Y_AXIS	:	Y axis
	 */
	double GetPosmm(int axis);

	/**
	 * Stage X,Y축의 현재 위치 좌표 재설정
	 * @param	axis	: 선택변수
						 STAGE_X_AXIS	:	X axis
						 STAGE_Y_AXIS	:	Y axis
	 */
	int SetPosmm(int axis, double pos_mm);

	/**
	 * Stage Controller에 정의된 standard controller real variable, user global real variable을 읽어오는 함수
	 * @param	varName	: 읽어올 변수
	 */
	double GetGlobalRealVariable(char* varName);

	/**
	 * Stage의 현재 위치에서 상대적인 위치로 이동하기.
	 * @param	axis	: 선택변수
						 STAGE_X_AXIS	:	X axis
						 STAGE_Y_AXIS	:	Y axis
	 * @param	*position_mm : 상대 이동할 거리.
	 */
	int MoveRelative(int axis, double posmm);

	/**
	 * Stage의 절대적인 위치로 이동하기.
	 * @param	axis	: 선택변수
						 STAGE_X_AXIS	:	X axis
						 STAGE_Y_AXIS	:	Y axis
	 * @param	*position_mm : 절대적으로 이동할 Stage 위치.
	 */
	int MoveAbsolute(int axis, double posmm);

	/**
	 * Stage X 축과 Y축을 동시에 이동하기.(On-the-Fly 기능 사용)
	 * @param	Xposition	:	X 축의 이동 좌표.
	 * @param	*Yposition :	Y 축의 이동 좌표.
	 */
	int MoveXYOnTheFly(double Xposition, double Yposition);

	/**
	 * Stage X 축과 Y축을 동시에 이동하기.(On-the-Fly 기능 미사용)
	 * @param	Xposition	:	X 축의 이동 좌표.
	 * @param	*Yposition :	Y 축의 이동 좌표.
	 */
	int MoveXYUntilInposition(double Xposition, double Yposition);

	/**
	 * Stage의 이동 명령 후 Inposition이 될때까지 기다림.
	 * @param	axis	: 선택변수
						 STAGE_X_AXIS	:	X axis
						 STAGE_Y_AXIS	:	Y axis
	 * @param	pos_mm	: Inposition 위치.
	 */
	int WaitInPosition(int axis, double pos_mm);

	/**
	 * Stage의 이동 명령 후 Inposition이 될때까지 기다림.
	 * @param	axis	: 선택변수
						 STAGE_X_AXIS	:	X axis
						 STAGE_Y_AXIS	:	Y axis
	 * @param	pos_mm	: Inposition 될때까지 기다리는 시간.
	 * @param	pos_mm	: Inposition 위치.
	 */
	int InPosition(int axis, int MaxTime, double pos_mm);

	/**
	 * Stage의 각 축의 Laser Swiching 기능 On/Off.
	 * @param	bOnOff	: On/Off 여부
	 */
	int SetLaserSwitchingFunction(BOOL bOnOff);

	BOOL IsLaserSwichingMode();
	BOOL m_bLaserSwichingModeFlag;

	/**
	 * Stage의 각 축의 Laser - Encoder feedback 선택
	 * @param	bOnOff	: On/Off 여부
	 */
	int SetFeedbackType(int nFeedbackType);

	BOOL IsLaserFeedback();
	BOOL m_bLaserFeedbackFlag;

	int RunBuffer(int nBufferNo);
	int StopBuffer(int nBufferNo);
};

