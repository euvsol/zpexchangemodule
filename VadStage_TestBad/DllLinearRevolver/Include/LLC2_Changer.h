#pragma once

#include "Serial.h"
#include "LLC2_Config.h"
class CLLC2_Changer
{
public:
	CLLC2_Changer(void);
	~CLLC2_Changer(void);

	////////////////////////////////////////
	// COM Communication
	////////////////////////////////////////
	CSerial serComm;

	static const UINT RECV_DATA_MAX = 128;
	static const UINT TRANS_DATA_MAX = 128;
	char transDataBuffer[TRANS_DATA_MAX], recvDataBuffer[RECV_DATA_MAX];
	int readSize;

	////////////////////////////////////////
	// LLC2 제어
	////////////////////////////////////////
//	unsigned int	m_uiMotorStatus;
//	bool			m_bIsOnMotorMove;

	unsigned int m_nClock;           // 'K'
	unsigned int m_nNofSteps_Flight; // 'W'
	unsigned int m_nNofSteps;        // 'N'

	// Define EditBox Variables and Controls
	UINT m_uiSpeed;
	UINT m_uiDistance;
	//CEdit m_ctlSpeed;

	UINT m_uiTmpSpeed;

	double m_dblSpeed;
	double m_dblAC_DC;

	BOOL m_bIsRight;
	BOOL m_bIsOnProcess;
	BOOL m_bIsOnMotorMove;
	UINT m_uiMotorStatus;


	int		m_nSettingPort;
	DWORD	m_nBaudRate;

	long	OpenComport(long comnum);
	long	CloseComport();

	void	Home();
    void	Go_Right();
    void	Go_Left();
	long	Check_Status();
	long	Check_Direction();

	int		Get_MotorMoveStatus();

	// Define Library Functions
	void MoveRight(void);
	void MoveLeft(void);
	void SaveSpeed(unsigned int m_uiSpeed);
	void FactoryDefault(void);
	void MoveScheme(BOOL IsRight);
	UINT CMD_Tokenizer(char *recvDataBuffer, UINT size);
	void DATA_Tokenizer(char *transDataBuffer, UINT data, UINT size);
	UINT GetCPLD_Version(char recvDataBuffer[], char strCPLD[]);
	void ChangeDistance(UINT uiDistance);

	////////////////////////////////////////
	// 조명 제어
	////////////////////////////////////////
	long LED_On(int Channel);
	long LED_Off(int Channel);

	bool			setLEDPWM(int channel, short PWM);
	bool			setLEDCurr(int channel, unsigned short Curr);

	int				getLEDPWM(int channel);
	unsigned short	getLEDCurr(int channel);
	bool			getLEDIsOn(int channel);

	bool GetLedStatus(unsigned char LedStatus);
};

